# MobiSystems
Ready Solution here:
https://plovdiv-web.com/api/mobisystems/?act=activate&platform=windows&activationKey=WINA-CTVK

=================================================================================================
REQUIREMENTS
## Create Activation key API service

Please create an API with which a User can 'activate' some predefined activation keys for a specific platform.

Valid platforms are: `windows`, `ios`, `android`.

Valid keys for those platforms are:
```
'windows': [
    'ASDF-ASDF',
    'WINA-CTVK',
],
'ios': [
    'IOSA-CTVK',
],
'android': [
    'ANDR-ACTV',
],
```

Those keys should be stored in a **DB table** with information about the status of each key and whether it has already been "activated", and if yes - when.

The activation of a key should happen via an API service, accessible by HTTP, which communicates with the DB and which provides a way for the User to:

- Check if some specified key is valid
- Activate a key by submitting `platform` and `activationKey` parameters
- Return full information and status for all activation keys (no additional security required, make this API public)

The service should be created with PHP (usage of any framework is allowed) and MySQL (or MariaDB).

### Acceptance criteria:
- The creation and initial population of the database should be included as code (migrations).
- The API should return all responses as valid `JSON`.
- The user should be able to use the API to check if a key exists for a specific platform and if it's not used.
- A proper error is expected if the activation didn't happen - wrong platform/key combination, key was already used, etc.

Usage of external libraries is allowed.

### Code Submit
Please organize your code as if it were going into production and provide instruction on how to run the API service locally.