<?php
	// Функции използвани за конфигурацията
	function define_once($const, $value)
	{
		if(!defined($const))
		{
			define($const, $value, true);
		}
	}

	function include_all($folder)
	{
		foreach (glob("{$folder}/*.php") as $filename)
		{
			include $filename;
		}
	}

	function get_function_name($controller, $action)
	{
		return 'action_'.$controller.'_'.$action;
	}

	function link_to($options_array)
	{
		Global $is_empty_layout, $source;

		// Set Default value for $options_array['OPTION_SET_CLASS']
		if(!isset($options_array['OPTION_SET_CLASS']))
		{
			$OPTION_SET_CLASS = true;
		}
		else
		{
			$OPTION_SET_CLASS = $options_array['OPTION_SET_CLASS'];
			unset($options_array['OPTION_SET_CLASS']);
		}

		if(!isset($options_array[CONTROLLER]))
		{
			$options_array[CONTROLLER] = (false ? $source[CONTROLLER] : 'page');
		}

		if(!isset($options_array[ACTION]))
		{
			$options_array[ACTION] = (false ? $source[ACTION] : 'index');
		}

		$page = $options_array[CONTROLLER];
		$controller = $page;

		if(!file_exists(LOCAL_PATH.DIR_CONTROLLERS.$page.'.php'))
		{
			$link = '#';
			$link_class = 'broken_link';
		}
		else
		{

			//require_once(LOCAL_PATH.DIR_CONTROLLERS.$page.'.php');
			//if(!function_exists(get_function_name($options_array[CONTROLLER], $options_array[ACTION])))

			# Вместо require_once използваме file_get_contents, а вместо function_exists използваме stripos,
			# защото ако извикаме require_oncе, това ще промени броя на дефинираните функции,
			# които пък са ни нужни за да генерираме автоматично под-страниците за всеки модул

			$file_content = file_get_contents(LOCAL_PATH.DIR_CONTROLLERS.$page.'.php');
			if(stripos($file_content, 'function '.get_function_name($options_array[CONTROLLER], str_ireplace('-', '', $options_array[ACTION]))) === false)
			{
				$link = '#';
				$link_class = 'broken_link';
			}
			else
			{
				$action = $options_array[ACTION];

				unset($options_array[ACTION]);
				unset($options_array[CONTROLLER]);

				if(isset($options_array[ID]))
				{
					$id = $options_array[ID].'/';
					unset($options_array[ID]);
				}
				else
				{
					$id = '';
				}

				if(count($options_array))
				{
					foreach($options_array as $k => $v)
					{
						$options_array[$k] = $k.'='.$v;
					}

					$options_array = '?'.implode('&', $options_array);
				}
				else
				{
					$options_array = '';
				}

				$link = SITE_URL.$controller.'/'.$action.'/'.$id . $options_array;
				$link_class = 'normal_link';
			}
		}

		if($OPTION_SET_CLASS)
		{
			//Тук няма грешка! Първата кавичка затваря href, втората отваря class. Пример: echo '<a href="'.link_to(array(PAGE => $v)).'">Text</a>';
			$link_class = '" class="'.$link_class;
		}
		else
		{
			$link_class = '';
		}

		return $link.$link_class;
	}

	function header_link($options_array)
	{
		$options_array['OPTION_SET_CLASS'] = false;
		return link_to($options_array);
	}

	function page_not_found()
	{
		//wp_redirect( home_url( '404' ), 302 );
		add_info_message('404: Страницата не е намерена!');
		header('location:'.header_link(array(CONTROLLER => 'page', ACTION => '404')));
		exit;
	}

	function page_not_exists($object_name = null)
	{
		echo '<span style="font-weight:bold; text-decoration:underline; color:red;">409:</span> <i>'.(is_null($object_name) ? 'Обекта' : $object_name).' не съществува!</i>';
		wp_redirect( home_url( '404' ), 302 );
		exit();
	}

	function page_not_access($message = null, $go_to_page = false)
	{
		if(is_null($message))
		{
			$message = '401: Вие нямате достъп до тази страница!';
		}

		add_error_message($message);

		if($go_to_page)
		{
			header('location:'.header_link(array(CONTROLLER => 'page', ACTION => $go_to_page)));
			exit;
		}

		echo '<div id="design_footer_fix"></div>';
		echo '<img src="'.DIR_IMAGES.'gifs/restricted.gif" alt="Restricted" class="center" />';
		include_layout(true);
	}

	function load_page()
	{
		Global $source, $ALL_PAGES, $is_logged_user, $is_admin_user, $controller_functions, $user_id, $is_empty_layout, $admin_menu;


			if(isset($source[CONTROLLER]) && '' != $source[CONTROLLER])
			{
				$page = strtolower($source[CONTROLLER]);
			}
			else
			{
				$page = 'page'; // null
			}

			if(isset($source[ACTION]) && '' != $source[ACTION])
			{
				$action = strtolower($source[ACTION]);
			}
			else
			{
				$action = 'index';
			}

			// Преобразуваме в долен регистър, за да няма проблеми в зависимост от регистъра на буквите
			foreach($ALL_PAGES as $k => $v)
			{
				$ALL_PAGES[$k] = strtolower($v);
			}
			$action = str_ireplace('-', '', $action); // For SEO Friendly URLS, like Terms-And-Conditions

			if($is_admin_user)
			{
				$icon_list = array(
					'exam' => 'list-alt',
					'homeworks' => 'check',
					'course' => 'book',
					'certificates' => 'certificate', //'bookmark',
					'question' => 'question-sign',
					'user' => 'user',
					'logs' => 'eye-open',
					'mail' => 'envelope',
					'statistic' => 'stats',
					'club' => 'usd',
					'tests' => 'wrench',
					'page' => 'file'
				);

				// Показваме всички страници (контролери)
				foreach($ALL_PAGES as $k => $v)
				{
					$short_menu[] = '<a href="'.link_to(array(CONTROLLER => $v)).'">&nbsp;<span title="'.ucwords($v).'" class="'.(isset($icon_list[$v]) ? 'glyphicon glyphicon-'.$icon_list[$v] : '').'"></span>&nbsp;</a>';
				}

				$short_menu[] = '<a href="/glyphicons-icons/" target="_blank">&nbsp;<span class="glyphicon glyphicon-picture"></span>&nbsp;</a>';
			}

			// Управляваме зареждането на страниците
			if(is_null($page))
			{
				if($is_admin_user)
				{

					$icon_list = array(
						'exam' => 'list-alt',
						'homeworks' => 'check',
						'course' => 'book',
						'certificates' => 'certificate', //'bookmark',
						'question' => 'question-sign',
						'user' => 'user',
						'logs' => 'eye-open'
					);

					// Показваме всички страници (контролери)
					foreach($ALL_PAGES as $k => $v)
					{
						$ALL_PAGES[$k] = '<a href="'.link_to(array(CONTROLLER => $v)).'"><span class="'.(isset($icon_list[$v]) ? 'glyphicon glyphicon-'.$icon_list[$v] : '').'"></span> '.$v.'</a>';
					}

					$my_pages = array_merge(array('<a href="../profile-2/"><span class="glyphicon glyphicon-th"></span> My Profile</a>'), $ALL_PAGES, array('<a href="../glyphicons-icons/"><span class="glyphicon glyphicon-picture"></span> GlyphIcons</a>'));

					//$pinned_menu .= return_table($my_pages, array('Страници'.' <a href="/wp-admin/"><span class="glyphicon glyphicon-home"></span></a>'));
					//$pinned_menu .= return_table($short_menu);
					//echo $pinned_menu;

					database_open();
					checkCourses();
					database_close();

					//echo '<div class="container"><div class="row"><div class="col-lg-12">';
					//show_all_messages();
					//echo '</div></div></div>';
					clear_all_messages();
				}
				else
				{
					/*
					if(!empty($_POST))
					{
						if(isset($_POST['from']) && $_POST['from'] != 0)
						{
							if(isset($_POST['about']) && mb_strlen(trim($_POST['about']),'UTF-8') > 3)
							{
								if(isset($_POST['descr']) && mb_strlen(trim($_POST['descr']),'UTF-8') > 3)
								{
									$data_array = array(
										'from_user' => $_POST['from'],
										'to_user' => 1,
										'about' => encrypt(trim($_POST['about'])),
										'description' => encrypt(trim($_POST['descr'])),
										'author_id' => $user_id,
										'send_date' => strtotime('now') + TIME_CORRECTION,
										'read_date' => 0,
										'status' => 1
									);

									database_open();
									$result = insert_query('alerts', $data_array);
									database_close();

									if($result)
									{
										add_success_message('Оплакването е изпратено успешно!');
									}
									else
									{
										add_error_message('Неуспешно изпращане на оплакване! Опитайте отново!');
									}
								}
								else
								{
									add_error_message('Полето "Съобщение" трябва да съдържа повече от 3 символа! Опитайте отново!');
								}
							}
							else
							{
								add_error_message('Полето "относно" трябва да съдържа повече от 3 символа! Опитайте отново!');
							}
						}
						else
						{
							add_error_message('Трябва да изберете от кого искате да се оплачете! Опитайте отново!');
						}
					}
					header('location: '.SITE_URL.'/profile-2/'); exit;
					*/
				}
			}
			else
			{
				if(in_array($page, $ALL_PAGES))
				{
					if(!file_exists(LOCAL_PATH.DIR_CONTROLLERS.$page.'.php'))
					{
						echo page_not_found();
					}
					else
					{
//						$old_functions = get_defined_functions();
//						$old_functions = $old_functions['user'];
//
						require_once(LOCAL_PATH.DIR_CONTROLLERS.$page.'.php');
//
//						$new_functions = get_defined_functions();
//						$new_functions = $new_functions['user'];


//						$controller_functions = array_diff($new_functions, $old_functions);
//  						asort($controller_functions); // Подреждаме ги  авбучно, за да се намират по лесно! :)
//
//						foreach($controller_functions as &$controller_page)
//						{
//							$controller_page = str_ireplace('action_', '', $controller_page);
//
//							$controller_page = explode('_', $controller_page);
//							$controller_page = '&nbsp;<a href="'.header_link(array( CONTROLLER => $controller_page[0], ACTION => $controller_page[1])).'">'.implode('_', $controller_page).'</a>&nbsp;';
//            }
//
//						unset($old_functions);
//						unset($new_functions);

//						if($is_admin_user)
//						{
//							$compliment = count($short_menu) - (count($controller_functions) + 2);
//							if($compliment > 0)
//							{
//								for($i = 1; $i <= $compliment; $i++)
//								{
//									$controller_functions[] = '&nbsp;';
//								}
//							}

         if($is_admin_user)
		{
         $controlersFunc = [];

         $needle = "function action_";
            foreach (glob("pages/controllers/*.php") as $filename)
            {
              $stringData = file_get_contents($filename);
              preg_match_all('/function (\w+)/', $stringData, $match);

			  // natcasesort функцията ги подреждабез значение размера на буквите.
			  // sort функцията щеше да подреди първо тези с главните букви отгоре.
			  natcasesort($match[1]);
              $newMenuItems = implode(" ", $match[1]);
              $controlersFunc[$filename]=$newMenuItems ;
            }
            arsort($controlersFunc);
            $newMenu = "";
            foreach ($controlersFunc as $key => $value)
            {
                    $getFileName = basename($key, ".php");

                    $newMenu .='<details style="text-transform: capitalize ;text-align: left;border-radius: 30px; background-color: black;" class="details-animated btn btn-info btn-sm btn-block"><summary style="font: menu; width: 90%; display:inline-block">'.(isset($icon_list[$getFileName]) ? '<i class="glyphicon glyphicon-'.$icon_list[$getFileName].'"></i>' : '').'&nbsp;'. $getFileName .'</summary>';
                    $exp = explode(" ", $value);
                    foreach ($exp as $funcLink)
                    {
                      $funcLink  = str_ireplace('action_', '', $funcLink );
                      $newCutedLink = explode('_', $funcLink);
                     // $newMenu .='<details class="'.(isset($icon_list[$getFileName]) ? 'glyphicon glyphicon-'.$icon_list[$getFileName] : '').'" style="text-transform: capitalize ;text-align: left;" ><summary>' . $getFileName .'</summary>';

                     $newMenu .='<a class="innerText" href="'.header_link(array( CONTROLLER => $getFileName, ACTION => $newCutedLink[1])).'" ><p class="innerText">'. $funcLink.'</p></a>';
                    }
                    $newMenu .='</details>';
                    $admin_menu = $newMenu;

            }



							$short_menu[] = '&nbsp;<a href="#" id="btn_toggle"><span class="glyphicon glyphicon-resize-horizontal"></span></a>&nbsp;';

							if(!$is_empty_layout)
							{
								$pinned_menu .= '<div id="menu_container" class="alert" style="background-color:rgba(255,255,255,1); padding:0px; z-index:2000;">';
								$pinned_menu .= '<span style="border:2px solid lightblue; display:inline-block;">';

								$pinned_menu .= return_table($short_menu);
								$pinned_menu .= '</span>';

								$pinned_menu .= '<span id="functions_list" style="border:2px solid lightblue; display:inline-block; vertical-align:top;">';
								$pinned_menu .= return_table($controller_functions, array('Страници <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="margin:3px 5px 0px 0px;">×</button>'));
								$pinned_menu .= (SHOW_DEBUG_INFO ? '&nbsp;<label><input type="checkbox" id="chk_debug_info" checked="checked" /> DEBUG_INFO</label>' : '<input type="range" id="menu_opacity" min="0" max="100" value="100" style="border:2px solid lightblue;" />');
								$pinned_menu .= '</span>';
								$pinned_menu .= '</div>';
								$pinned_menu .= '<script>$(function(){ $("#menu_opacity").change(function(){ $("#menu_container").css("background-color", "rgba(255,255,255,"+($("#menu_opacity").val() / 100)+")"); }); });</script>';
								$pinned_menu .= '<script>$(function(){ $("#chk_debug_info").trigger("click"); $("#chk_debug_info").click(function(){ $(".debug_info").prop("hidden", !$(this).prop("checked") ); }); $("#btn_toggle").click(function(){ $("#functions_list").fadeToggle("slow"); }); $( "#btn_toggle" ).trigger( "click" ); $("#menu_opacity").change(function(){ $("#menu_container").css("opacity", $("#menu_opacity").val() / 100); $("#menu_container td:last").html($("#menu_opacity").val()); }); });</script>';
							}
						}

						//echo $pinned_menu;

						$function_name = get_function_name($page, $action);
						if(!function_exists($function_name))
						{
							echo page_not_found();
						}
						else
						{
							$function_name(); // Execute Action Function
							if(file_exists(LOCAL_PATH.DIR_VIEWS.$page.DIRECTORY_SEPARATOR.$action.'.php'))
							{
								include(LOCAL_PATH.DIR_VIEWS.$page.DIRECTORY_SEPARATOR.$action.'.php');
							}
						}
					}
				}
				else
				{
					echo page_not_found();
				}
			}
	}

	function set($var, $val)
	{
		if(substr($var,0,1) == '$')
		{
			$var = substr($var, 1);
		}

		$GLOBALS[$var] = $val;
	}

	function prepare_url($original_url, $append_site_url = true)
	{
		$url = $original_url;
		$original_url = mb_strtolower($original_url);

		if(mb_substr($original_url,0,7,'UTF-8') == 'http://' || mb_substr($original_url,0,8,'UTF-8') == 'https://')
		{
			// This is absolute URL. Return Copy of Original url which is stored on $url variable
			return $url;
		}

		if(mb_substr($url, 0, 1, 'UTF-8') == '/')
		{
			$url = mb_substr($url, 1, null, 'UTF-8');
		}

		if($append_site_url)
		{
			$url = SITE_URL.'/'.$url;
		}
		else
		{

		}

		return $url;
	}

	// В България лятно часово време имаме от последната неделя на Март до последната неделя на Октомври.
	// В зависимост от лятното часово време разликата между Българското време и Гринуич е 2 или съответно 3 часа.
	function is_summer_time()
	{
		$current_year = date('Y');
		$now = strtotime('now');

		$summer_start = strtotime('last Sunday', strtotime('04/01/'.$current_year));
		$summer_end = strtotime('last Sunday', strtotime('11/01/'.$current_year));

		return ($summer_start <= $now && $now <= $summer_end);
	}

	function include_view($sub_view = null)
	{
		Global $source;

		if(isset($sub_view))
		{
			// File has not extension, so add ".php" at the end
			if(stripos($sub_view, '.') === false)
			{
				$sub_view .= '.php';
			}

			// Remove first /, because DIR_VIEW have directory separator at the end. Example: /user/index, will be user/index
			if(substr($sub_view, 0, 1) == '/')
			{
				$sub_view = substr($sub_view, 1);
			}

			// This is full path like user/index, include it exactly how it is
			if(stripos($sub_view, '/') !== false)
			{
				$view_file = $sub_view;
			}
			else // Include view from current CONTROLLER folder
			{
				$page = $source[CONTROLLER];

				if(substr($sub_view, 0, 1) == '_') // _form will include user/import_form.php
				{
					$view_file = $page.DIRECTORY_SEPARATOR.$source[ACTION].$sub_view;
				}
				else // 'homeworks' will include user/homeworks.php
				{
					$view_file = $page.DIRECTORY_SEPARATOR.$sub_view;
				}
			}
		}
		else
		{
			$page = $source[CONTROLLER];
			$action = $source[ACTION];

			$view_file = $page.DIRECTORY_SEPARATOR.$action.'.php';
		}

		$view_file = LOCAL_PATH.DIR_VIEWS.$view_file;
		if(file_exists($view_file))
		{
			include($view_file);
		}
	}

	function include_layout($and_kill = false)
	{
		if(isset($_REQUEST['layout']))
		{
			$_REQUEST['layout'] = strtolower($_REQUEST['layout']);
			switch($_REQUEST['layout'])
			{
				case 'empty':
				//case 'clean':
				case 'blank';
					$layout = $_REQUEST['layout'];
					break;
				default : $layout = 'default'; break;
			}
		}
		else
		{
			$layout = 'default';
		}

		include('design/layouts/'.$layout.'.php');
		if($and_kill)
		{
			exit();
		}
	}
?>