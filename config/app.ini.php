;<?php
;die(); // For further security
;/*

[directories] ; Конфигурация за папки
DIR_CONTROLLERS = "pages/controllers/"
DIR_VIEWS = "pages/views/"
DIR_IMAGES = "http://localhost/WA/wp-content/themes/Artificial-Reason-WP/tests_system/images/"

[paths]
;SELF_FILE", $_SERVER["REDIRECT_URL"]); //$_SERVER["PHP_SELF"]; //$_SERVER["REDIRECT_URL"];
;SITE_URL", get_site_url() );
;SYSTEM_URL", SITE_URL."/course/");
;LOCAL_PATH", "wp-content/themes/Artificial-Reason-WP/tests_system/");
;WP_EDIT_PROFILE_URL", "http://localhost/wordpress/wp-admin/profile.php");

[rooting]
VIEW = "v"
MODULE = "m"
CONTROLLER = "c"
ACTION = "a"
PAGE = "page"
ID = "id"
ORDER_BY = "o"
ORDER_DIRECTION = "od"

[crypt]
CRYPT_PASS = "*(yord@n~enev)#2019^PlovdivW3b!"  ;"*(yordan~enev)#2016^W3bAc@demy!"
CRYPT_ALGO = MCRYPT_BLOWFISH
CRYPT_MODE = MCRYPT_MODE_CBC
;*/
;?>
<?php
	Global $is_admin_user, $user_id;

	// Конфигурация за връзка с База Данни
	define_once("DB_HOST", "localhost");
	define_once("DB_NAME", "");
	define_once("DB_USER", "root");
	define_once("DB_PASS", "");
	define_once("DISPLAY_DB_ERRORS", true);
	define_once("DIE_ON_DATABASE_ERROR", true);


	// Други дефиниции


	define_once("SHOW_DEBUG_INFO", true);
	define_once("MAX_TEST_TIME", 6); // Задаваме времето в минути глобално
	define_once("FILTER_KEY", "fk");
	define_once("FILTER_VALUE", "fv");
	define_once("TIME_CORRECTION", (is_summer_time() ? 3 : 2) * 60 * 60);
	$QUESTIONS_TYPES = array(1 => "CHK", 2 => "T/F", 3 => "TXT", 4 => "TBX", 5 => "UPL"); // CHK = Checkbox, Multy Choise TBX = TextBox, i.e. <textarea> UPL = File Upload
	$QUESTIONS_GROUPS = array(0 => "white", 1 => "lightblue", 2 => "lightgreen", 3 => "orange", 4 => "yellow", 5 => "red");
?>