<?php
	function get_header()
	{
		//echo '<html><head><meta charset="utf-8" /></head><body>';
	}

	function get_footer()
	{
		//echo '</body></html>';
	}

	function is_user_logged_in()
	{
		return isset($_SESSION[CURRENT_USER]);
	}

	function get_site_url()
	{
		return 'https://plovdiv-web.com/'; // './'
	}

	function get_current_user_id()
	{
		return (is_user_logged_in() ? $_SESSION[CURRENT_USER]['user_id'] : 0);
	}

	function wp_redirect($location = null, $code_status = null)
	{
		header("Location: ".$location, TRUE, $code_status);
		exit;
	}

	function home_url($address = null)
	{
		if(substr($address, 0, 1) == '/')
		{
			$address = substr($address, 1);
		}

		return get_site_url().$address; //'https://plovdiv-web.com/'.$address;
	}

	function site_url($str)
	{
		return get_site_url().$str;
	}

	function esc_html($str)
	{
		return strip_tags($str);
	}
?>