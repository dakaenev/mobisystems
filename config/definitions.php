<?php
	// Конфигурация за връзка с База Данни
	if('127.0.0.1' == $_SERVER['SERVER_ADDR'] || '::1' == $_SERVER['SERVER_ADDR'])
	{ // Localhost Definitions
		define_once('DB_HOST', 'localhost');
		define_once('DB_NAME', 'bg_academy');
		define_once('DB_USER', 'root');
		define_once('DB_PASS', '');
		define_once('DISPLAY_DB_ERRORS', true);
		define_once('DIE_ON_DATABASE_ERROR', true);
	}
	else
	{ // Server Definitions
		define_once('DB_HOST', 'localhost');
		define_once('DB_NAME', 'DB_NAME....'); //enevsoft_academy
		define_once('DB_USER', 'SetYourDBUserHere');
		define_once('DB_PASS', 'SetYourPasswordHere,');
		define_once('DISPLAY_DB_ERRORS', false);
		define_once('DIE_ON_DATABASE_ERROR', false);
	}

	define_once('IS_PRODUCTION', true);
	define_once('SELF_FILE', $_SERVER['PHP_SELF']);
	define_once('SITE_URL', get_site_url() );
	define_once('SYSTEM_URL', SITE_URL);

	// Конфигурация за папки
	define_once('DIR_CONTROLLERS', 'pages/controllers/');
	define_once('DIR_VIEWS', 'pages/views/');
	define_once('DIR_IMAGES', '/images/');
	define_once('DS', DIRECTORY_SEPARATOR);

	// Други дефиниции
	if(IS_PRODUCTION)
	{
		ini_set('display_errors', 0);
		ini_set('display_startup_errors', 0);
		error_reporting(0);
	}
	else
	{
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);
	}

	define_once('VIEW', 'v');
	define_once('MODULE', 'm');
	define_once('CONTROLLER','c');
	define_once('ACTION', 'a');
	define_once('PAGE', 'page');
	define_once('ID','id');

	define_once('SHOW_DEBUG_INFO', false);
	define_once('MAX_TEST_TIME', 6); // Задаваме времето в минути глобално

	define_once('ORDER_BY', 'o');
	define_once('ORDER_DIRECTION', 'd');
	define_once('FILTER_KEY', 'fk');
	define_once('FILTER_VALUE', 'fv');

	$QUESTIONS_TYPES = array(1 => 'CHK', 2 => 'T/F', 3 => 'TXT', 4 => 'TBX', 5 => 'UPL'); // CHK = Checkbox, Multy Choise TBX = TextBox, i.e. <textarea> UPL = File Upload
	$QUESTIONS_GROUPS = array(0 => 'white', 1 => 'lightblue', 2 => 'lightgreen', 3 => 'orange', 4 => 'yellow', 5 => 'red');

	define_once('LOCAL_PATH', '');
	define_once('WP_EDIT_PROFILE_URL', 'http://localhost/wordpress/wp-admin/profile.php');
	define_once('TIME_CORRECTION', (is_summer_time() ? 0 : 0) * 60 * 60);

	define('CRYPT_PASS', '*(yordan~enev)#2016^W3bAc@demy!', true); // 
	define('CRYPT_ALGO', MCRYPT_BLOWFISH, true); 
	define('CRYPT_MODE', MCRYPT_MODE_CBC, true);
?>