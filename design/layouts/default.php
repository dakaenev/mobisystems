<?php
	Global $is_admin_user, $is_empty_layout, $admin_menu, $HTML_CONTENT;

	if(!isset($HTML_CONTENT))
	{
		$HTML_CONTENT = ob_get_clean();
	}

// Settings
	database_open();
	$we_are_sad = false;
	$hide_source = false && !$is_admin_user;
	$default_theme = ($we_are_sad ? 'gray' : 'blue2');

	$THEME_COLORS = array(
		'blue','blue2','blue3','blue4','blue5','green',
		'green2','green3','green4','green5','red','red2',
		'red3','fuchsia','pink','yellow','yellow2','orange',
		'orange2','orange3','violet','violet2','violet3','aqua',
		'grays' // This not Exists for tests purposes :)
	);

// Prepare
	$color_options = '';
	foreach($THEME_COLORS as $color_name)
	{
		$color_options .= (file_exists('assets/css/style-'.$color_name.'.css') ? PHP_EOL.'<a href="javascript:void(0);" rel="style-'.$color_name.'.css" class="color-box color-'.$color_name.'" title="'.$color_name.'">'.$color_name.'</a>' : '');
	}

	$change_language_url = '?'.($_SERVER['QUERY_STRING'] != '' ? $_SERVER['QUERY_STRING'].'&' : '').'lang='.(LANG_CODE == 'bg' ? 'en' : 'bg');
	$change_language_icon = '<a href="'.$change_language_url.'" class="button button-pill btn_change_language"><img src="'.DIR_IMAGES.'ui/'.LANG_CODE.'.png" alt="'.LANG_CODE.'"><span>'.translate('LANGUAGE_NEXT').'</span></a>';
	$change_language_icon = ''; // Да изтрия този ред, когато английската версия бъде преведена.

	if(!is_user_logged_in())
	{
		$link_register = '<a href="'.header_link(array(CONTROLLER => 'user', ACTION => 'register')).'" class="btn_image"><img src="'.DIR_IMAGES.'ui/user_add.png" alt="'.translate('REGISTER', true).'" title="'.translate('REGISTER', true).'"><span class="hidden-sm"> '.translate('REGISTER').'</span></a>';
		$link_login = '<a href="'.header_link(array(CONTROLLER => 'user', ACTION => 'login')).'" class="btn_image"><img src="'.DIR_IMAGES.'ui/lock_key.png" alt="'.translate('LOGIN', true).'" title="'.translate('LOGIN', true).'"><span class="hidden-sm"> '.translate('LOGIN').'</span></a>';
	}
	else
	{
		$link_register = '';
		$link_login = '';
	}
	//pre_print($_SESSION, true);
	if(isset($_GET['lang']))
	{
		header('location:?'.remove_url_element('lang'));
		exit;
	}

	$social_links = array(
		'rss' => '',
		'facebook' => 'https://www.facebook.com/PlovdivWeb/',
		'twitter' => '',
		'pinterest' => '',
		'instagram' => '',
		'youtube' => '',
		'linkedin' => ''
	);

	$facebook_link = $social_links['facebook'];
	// 6LemeLIUAAAAAPixPCt4g6m5CRumTNSb1TQTkJSp
	// 6LemeLIUAAAAANxoT5qk0ZgqtNyLQ8wt_gIHyjTj

	//$_SESSION['SHOW_SITEMAP'] = false;
	//$_SESSION['SHOW_ADS'] = false;


	ob_start();
?><!DOCTYPE html>
<html lang="bg">

<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <title><?php echo translate('SITE_MOTO'); ?></title>

    <link rel="shortcut icon" href="/assets/img/favicon.png" />

    <meta name="description" content="">

    <!-- CSS -->
    <link href="/assets/css/preload.css" rel="stylesheet">
    <link href="/assets/css/custom_menu.css" rel="stylesheet">


    <!-- Compiled in vendors.js -->
	<!--
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/bootstrap-switch.min.css" rel="stylesheet">
    <link href="/assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="/assets/css/animate.min.css" rel="stylesheet">
    <link href="/assets/css/slidebars.min.css" rel="stylesheet">
    <link href="/assets/css/lightbox.css" rel="stylesheet">
    <link href="/assets/css/jquery.bxslider.css" rel="stylesheet" />
    <link href="/assets/css/buttons.css" rel="stylesheet">
    -->

    <link href="/assets/css/vendors.css" rel="stylesheet">
    <link href="/assets/css/syntaxhighlighter/shCore.css" rel="stylesheet" >

    <link href="/assets/css/style-<?php echo $default_theme; ?>.css" rel="stylesheet" title="default">
    <link href="/assets/css/width-boxed.css" rel="stylesheet" title="default">
	<link href="/assets/css/pwc-style.css" rel="stylesheet" title="default">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="/assets/js/html5shiv.min.js"></script>
        <script src="/assets/js/respond.min.js"></script>
    <![endif]-->

	<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-173021035-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-173021035-1');
	</script>

	<?php if( is_default_language() ){ echo '<style>.align_correct {text-align: justify; display: inline-block; }</style>'; } else { echo '<style>.align_correct {text-align: left;}</style>'; } ?>
</head>

<!-- Preloader -->
<!--
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>
-->
<script src="/assets/js/ads.js" type="text/javascript"></script>
<?php if($hide_source) { ?>
<body oncontextmenu="return false">
	<script>
	document.onkeydown = function(e) {
		if(e.keyCode == 123) {
		 return false;
		}
		if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
		 return false;
		}
		if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
		 return false;
		}
		if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
		 return false;
		}
		if(e.ctrlKey && e.keyCode == 'S'.charCodeAt(0)){
		 return false;
		}
		if(e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)){
		 return false;
		}
	 }
	</script>
<?php } else { ?>
<body>
<?php } ?>
<?php if( (!$we_are_sad && user_can('MODIFY_THEME')) || is_admin_user() ){ ?>
	<div id="agm-configurator">
		<div id="agm-configurator-content">
			<div class="panel-group" id="accordion-conf" role="tablist" aria-multiselectable="true">
				<?php if($is_admin_user) { ?>
				<!-- collapse panel -->
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingAdmin">
						<a role="button" data-toggle="collapse" data-parent="#accordion-conf" href="#collapseAdmin" aria-expanded="true" aria-controls="collapseAdmin">
							Admin
						</a>
					</div>
					<div id="collapseAdmin" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingAdmin">
						<div class="panel-body">
							<div id="admin-options" style="height:80vh; overflow-y: auto;">
								<?php echo $admin_menu; ?>
							</div>
						</div>
					</div>
				</div>
				<?php } ?>
				<!-- collapse panel -->
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingOne">
						<a role="button" data-toggle="collapse" data-parent="#accordion-conf" href="#collapseOne1" aria-expanded="true" aria-controls="collapseOne1" class="collapsed">
							Color Selector
						</a>
					</div>
					<div id="collapseOne1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
						<div class="panel-body">
							<div id="color-options">
								<?php echo $color_options; ?>
							</div>
						</div>
					</div>
				</div>

				<!-- collapse panel -->
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingTwo">
						<a role="button" data-toggle="collapse" data-parent="#accordion-conf" href="#collapseTwo2" aria-expanded="true" aria-controls="collapseTwo2" class="collapsed">
							Header Options
						</a>
					</div>
					<div id="collapseTwo2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
						<div class="panel-body">
							<h5>Header Style</h5>
							<form id="header-option">
								<div class="radio radio-dark">
									<input type="radio" name="headerRadio" id="header-full-radio" value="header-full" checked>
									<label for="header-full-radio">Light Header</label>
								</div>
								<div class="radio radio-dark">
									<input type="radio" name="headerRadio" id="header-full-dark-radio" value="header-full-dark">
									<label for="header-full-dark-radio">Dark Header</label>
								</div>
								<div class="radio radio-dark">
									<input type="radio" name="headerRadio" id="no-header-radio" value="no-header">
									<label for="no-header-radio">No Header (Navbar mode)</label>
								</div>
							</form>
							<h5>Navbar Color</h5>
							<form id="navbar-option">
								<div class="radio radio-dark">
									<input type="radio" name="navbarRadio" id="navbar-light-radio" value="navbar-light">
									<label for="navbar-light-radio">Light Color</label>
								</div>
								<div class="radio radio-dark">
									<input type="radio" name="navbarRadio" id="navbar-dark-radio" value="navbar-dark" checked>
									<label for="navbar-dark-radio">Dark Color</label>
								</div>
								<div class="radio radio-dark">
									<input type="radio" name="navbarRadio" id="navbar-inverse-radio" value="navbar-inverse">
									<label for="navbar-inverse-radio">Primary Color</label>
								</div>
							</form>
						</div>
					</div>
				</div>

				<!-- collapse panel -->
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingThree">
						<a role="button" data-toggle="collapse" data-parent="#accordion-conf" href="#collapseThree3" aria-expanded="true" aria-controls="collapseThree3" class="collapsed">
							Container Options
						</a>
					</div>
					<div id="collapseThree3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
						<div class="panel-body">
							<form id="container-option">
								<div class="radio radio-dark">
									<input type="radio" name="containerRadio" id="width-boxed-radio" value="width-boxed">
									<label for="width-boxed-radio">Boxed Container</label>
								</div>
								<div class="radio radio-dark">
									<input type="radio" name="containerRadio" id="width-full-radio" value="width-full" checked>
									<label for="width-full-radio">Full Width Container</label>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<button class="btn btn-danger btn-sm center" onclick="$('#agm-configurator').remove();" style="    right: 50px; top: 5px; position: absolute;">X</button>
		</div>

		<div id="agm-configurator-button">
			<button type="button" name="button" id="agm-configurator-btn"><i class="fa fa-gear"></i></button>
		</div>
	</div>
<?php } ?>
<div class="sb-site-container">
<div class="boxed">

<header id="header-full-top" class="hidden-xs header-full">
    <div class="container">
        <div class="header-full-title">
            <h1 class="animated fadeInRight"><a href="<?php echo header_link(array(CONTROLLER => 'page', ACTION => 'index')); ?>" class="plovdiv"><?php echo translate('SITE_MOTO'); ?></a></h1>
            <p class="animated fadeInRight"><?php echo translate('SITE_SUB_MOTO'); ?><!-- <img src="https://plovdiv-web.com/images/others/community.png" style="height: 62px;"> --></p>
        </div>
        <nav class="top-nav">
			<!--
            <ul class="top-nav-social hidden-sm">
                <li><a href="#" class="animated fadeIn animation-delay-6 rss"><i class="fa fa-rss"></i></a></li>
                <li><a href="#" class="animated fadeIn animation-delay-7 twitter"><i class="fa fa-twitter"></i></a></li>
                <li><a href="<?php echo $facebook_link; ?>" class="animated fadeIn animation-delay-8 facebook"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#" class="animated fadeIn animation-delay-9 google-plus"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#" class="animated fadeIn animation-delay-9 instagram"><i class="fa fa-instagram"></i></a></li>
                <li><a href="#" class="animated fadeIn animation-delay-8 vine"><i class="fa fa-vine"></i></a></li>
                <li><a href="#" class="animated fadeIn animation-delay-7 linkedin"><i class="fa fa-linkedin"></i></a></li>
                <li><a href="#" class="animated fadeIn animation-delay-6 flickr"><i class="fa fa-flickr"></i></a></li>
            </ul>

            <div class="dropdown animated fadeInDown animation-delay-11">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Login</a>
                <div class="dropdown-menu dropdown-menu-right dropdown-login-box animated flipCenter">
                    <form role="form">
                        <h4>Login Form</h4>

                        <div class="form-group">
                            <div class="input-group login-input">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" class="form-control" placeholder="Username">
                            </div>
                            <br>
                            <div class="input-group login-input">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <input type="password" class="form-control" placeholder="Password">
                            </div>
                            <div class="checkbox pull-left">
                                  <input type="checkbox" id="checkbox_remember1">
                                  <label for="checkbox_remember1">
                                     Remember me
                                  </label>
                            </div>
                            <button type="submit" class="btn btn-ar btn-primary pull-right">Login</button>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                </div>
            </div>
			-->
			<div class="dropdown animated fadeInDown animation-delay-13">
                <!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-search"></i></a> -->
				<?php echo $link_register.' '.$link_login; ?>
			</div>
			<!--
            <div class="dropdown animated fadeInDown animation-delay-13">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-search"></i></a>
                <div class="dropdown-menu dropdown-menu-right dropdown-search-box animated fadeInUp">
                    <form role="form">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button class="btn btn-ar btn-primary" type="button">Go!</button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>
			-->
        </nav>
    </div>
</header>
<nav class="navbar navbar-default navbar-header-full navbar-inverse yamm navbar-static-top" role="navigation" id="header">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <i class="fa fa-bars"></i>
            </button>
            <a id="ar-brand" class="navbar-brand hidden-lg hidden-md hidden-sm" href="<?php echo header_link(array(CONTROLLER => 'page', ACTION => 'index')); ?>"><?php echo translate('SITE_MOTO'); ?> <span></span></a>
        </div>

        <div class="pull-right">
			<?php echo $change_language_icon; ?>
            <a href="javascript:void(0);" class="sb-icon-navbar sb-toggle-right hidden-lg hidden-md hidden-sm hidden-print"><i class="fa fa-bars"></i></a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<?php navbar_menu('main'); ?>
        </div>
    </div>
</nav>
<?php JS_Cookie_Accept(); ?>
<?php
	if(have_messages())
	{
		echo '<div class="container"><div class="row">';
		show_all_messages();
		clear_all_messages();
		echo '</div></div>';
	}
?>
<?php echo $HTML_CONTENT; ?>
	<div id="design_footer_fix"></div>
<?php if($_SESSION['SHOW_ADS']){ ?>
	<aside id="footer-widgets" style="padding:0; margin:0;">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<?php show_ads(); ?>
				</div>
			</div>
		</div>
	</aside>
<?php } ?>
<?php if($_SESSION['SHOW_SITEMAP']){ ?>
	<aside id="footer-widgets">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<h3 class="footer-widget-title">Sitemap</h3>
					<ul class="list-unstyled three_cols">
						<li><a href="index.html">Home</a></li>
						<li><a href="blog.html">Blog</a></li>
						<li><a href="portfolio_sidebar.html">Portafolio</a></li>
						<li><a href="portfolio_topvar.html">Works</a></li>
						<li><a href="page_timeline_left.html">Timeline</a></li>
						<li><a href="page_pricing.html">Pricing</a></li>
						<li><a href="page_about2.html">About Us</a></li>
						<li><a href="page_team.html">Our Team</a></li>
						<li><a href="page_services.html">Services</a></li>
						<li><a href="page_support.html">FAQ</a></li>
						<li><a href="page_login_full.html">Login</a></li>
						<li><a href="page_contact.html">Contact</a></li>
					</ul>
				</div>
				<div class="col-md-4">
					<h3 class="footer-widget-title">Subscribe</h3>
					<p>Lorem ipsum Amet fugiat elit nisi anim mollit minim labore ut esse Duis ullamco ad dolor veniam velit.</p>
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Email Adress">
						<span class="input-group-btn">
							<button class="btn btn-ar btn-primary" type="button">Subscribe</button>
						</span>
					</div>
				</div>
			</div>
		</div>
	</aside>
<?php } ?>
<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6 social_links">
				<span class="hidden-xs">Подкрепете ни:</span>
				<?php foreach($social_links as $name => $href) { echo ('' == $href ? '' : '<a href="'.$href.'" class="social-icon-ar sm '.$name.'"><i class="fa fa-'.$name.'"></i></a>'); } ?>
				<!--
				<a href="#" class="social-icon-ar sm git"><i class="fa fa-git"></i></a>
				<a href="#" class="social-icon-ar sm git"><i class="fa fa-github"></i></a>
				<a href="#" class="social-icon-ar sm wordpress"><i class="fa fa-wordpress"></i></a>
				-->
			</div>
			<div class="col-xs-12 col-sm-6 copyrights">&copy; Plovdiv Web <span class="hidden-xs"><?php echo date('Y'); ?>. All rights reserved.</span></div>
			<!-- <div class="col-xs-6 copyrights">&copy; <a href="http://enevsoft.net/">EnevSoft</a> 2019. <span class="hidden-xs">All rights reserved.</span></div> -->
		</div>
	</div>
</footer>

</div> <!-- boxed -->
</div> <!-- sb-site -->
<?php if(true){ ?>
<div class="sb-slidebar sb-right sb-style-overlay">
    <div class="input-group">
        <input type="text" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
            <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
        </span>
    </div>
	<!--
    <h2 class="slidebar-header no-margin-bottom">Navigation</h2>
    <ul class="slidebar-menu">
        <li><a href="index.html">Home</a></li>
        <li><a href="portfolio_topbar.html">Portfolio</a></li>
        <li><a href="page_about3.html">About us</a></li>
        <li><a href="blog.html">Blog</a></li>
        <li><a href="page_contact.html">Contact</a></li>
    </ul>
	Social Media

    <h2 class="slidebar-header">Социални медии</h2>
    <div class="slidebar-social-icons">
        <a href="#" class="social-icon-ar rss"><i class="fa fa-rss"></i></a>
        <a href="<?php echo $facebook_link; ?>" class="social-icon-ar facebook"><i class="fa fa-facebook"></i></a>
        <a href="#" class="social-icon-ar twitter"><i class="fa fa-twitter"></i></a>
        <a href="#" class="social-icon-ar pinterest"><i class="fa fa-pinterest"></i></a>
        <a href="#" class="social-icon-ar instagram"><i class="fa fa-instagram"></i></a>
        <a href="#" class="social-icon-ar wordpress"><i class="fa fa-wordpress"></i></a>
        <a href="#" class="social-icon-ar linkedin"><i class="fa fa-linkedin"></i></a>
    </div>
	-->
	<h2 class="slidebar-header no-margin-bottom">Други</h2>
    <ul class="slidebar-menu">
		<li><?php echo $link_register; ?></li>
		<li><?php echo $link_login;	?></li>
		<li><?php echo $change_language_icon; ?></li>
    </ul>
</div>
<?php } ?>
<div id="back-top">
    <a href="#header"><i class="fa fa-chevron-up"></i></a>
</div>

<!-- Scripts -->
<!-- Compiled in vendors.js -->
<!--
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/jquery.cookie.js"></script>
<script src="/assets/js/imagesloaded.pkgd.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/bootstrap-switch.min.js"></script>
<script src="/assets/js/wow.min.js"></script>
<script src="/assets/js/slidebars.min.js"></script>
<script src="/assets/js/jquery.bxslider.min.js"></script>
<script src="/assets/js/holder.js"></script>
<script src="/assets/js/buttons.js"></script>
<script src="/assets/js/jquery.mixitup.min.js"></script>
<script src="/assets/js/circles.min.js"></script>
<script src="/assets/js/masonry.pkgd.min.js"></script>
<script src="/assets/js/jquery.matchHeight-min.js"></script>
-->

<script src="/assets/js/vendors.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
<?php
	if( (!$we_are_sad && user_can('MODIFY_THEME')) || is_admin_user() )
	{
		echo '<script src="/assets/js/styleswitcher.js"></script>';
	}
?>
<!-- Syntaxhighlighter -->
<script src="/assets/js/syntaxhighlighter/shCore.js"></script>
<script src="/assets/js/syntaxhighlighter/shBrushXml.js"></script>
<script src="/assets/js/syntaxhighlighter/shBrushJScript.js"></script>

<script src="/assets/js/DropdownHover.js"></script>
<script src="/assets/js/app.js"></script>
<script src="/assets/js/holder.js"></script>
<script src="/assets/js/home_info.js"></script>
<script src="/assets/js/active_page.js"></script>
<script src="/assets/js/login_facebook.js"></script>

<script>
	$(document).ready(function(){
		<?php if($_SESSION['FIX_FOOTER']){ ?>
		var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
		var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
		var fix_height = h-$('.boxed').height();

		if(fix_height > 0){
			$('#design_footer_fix').css('height', fix_height);
		}
		<?php } ?>

		$('body').on('click', 'input[name=payment_selected]', function(){
			$('.payment').addClass('hidden');
			$('#'+$(this).val()).removeClass('hidden');
		});

		$('body').on('click', '.important_btn', function(){
			$(this).removeClass('important_btn');
		});
	});

	function autoResize(o) {
		o.style.height=o.contentWindow.document.body.scrollHeight+'px';
	}
</script>
<?php
	if(!$is_empty_layout && $is_admin_user){
		echo '<i style="color:silver; float:left;">&nbsp;Заредено за '.  round(PAGE_PARSE_END_TIME - PAGE_PARSE_START_TIME, 3).' секунди в '.date('H:i:s', strtotime('now') + TIME_CORRECTION).'&nbsp;</i>';
		echo '<i class="debug hidden-xs hidden-sm hidden-md">Large Mode</i>
			  <i class="debug hidden-xs hidden-sm hidden-lg">Medium Mode</i>
			  <i class="debug hidden-xs hidden-md hidden-lg">Small Mode</i>
			  <i class="debug hidden-sm hidden-md hidden-lg">Extra Small Mode</i>';
	}
?>
</body>
</html><?php
/*
	if($hide_source)
	{
		$source = ob_get_contents();
		ob_end_clean();
		echo str_ireplace(PHP_EOL, '', $source);
	}
	else
	{
		ob_end_flush();
	}
*/
?>