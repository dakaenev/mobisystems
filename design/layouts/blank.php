<?php
	Global $is_admin_user, $HTML_CONTENT;

	if(!isset($HTML_CONTENT))
	{
		$HTML_CONTENT = ob_get_clean();
	}


// Settings
	database_open();
	$we_are_sad = false;
	$hide_source = false && !$is_admin_user;
	$default_theme = ($we_are_sad ? 'gray' : 'blue2');

	$THEME_COLORS = array(
		'blue','blue2','blue3','blue4','blue5','green',
		'green2','green3','green4','green5','red','red2',
		'red3','fuchsia','pink','yellow','yellow2','orange',
		'orange2','orange3','violet','violet2','violet3','aqua',
		'grays' // This not Exists for tests purposes :)
	);

// Prepare
	$color_options = '';
	foreach($THEME_COLORS as $color_name)
	{
		$color_options .= (file_exists('assets/css/style-'.$color_name.'.css') ? PHP_EOL.'<a href="javascript:void(0);" rel="style-'.$color_name.'.css" class="color-box color-'.$color_name.'" title="'.$color_name.'">'.$color_name.'</a>' : '');
	}

	$change_language_url = '?'.($_SERVER['QUERY_STRING'] != '' ? $_SERVER['QUERY_STRING'].'&' : '').'lang='.(LANG_CODE == 'bg' ? 'en' : 'bg');
	$change_language_icon = '<a href="'.$change_language_url.'" class="button button-pill btn_change_language"><img src="'.DIR_IMAGES.'ui/'.LANG_CODE.'.png" alt="'.LANG_CODE.'"><span>'.translate('LANGUAGE_NEXT').'</span></a>';
	$change_language_icon = ''; // Да изтрия този ред, когато английската версия бъде преведена.

	if(!is_user_logged_in())
	{
		$link_register = '<a href="'.header_link(array(CONTROLLER => 'user', ACTION => 'register')).'" class="btn_image"><img src="'.DIR_IMAGES.'ui/user_add.png" alt="'.translate('REGISTER', true).'" title="'.translate('REGISTER', true).'"><span class="hidden-sm"> '.translate('REGISTER').'</span></a>';
		$link_login = '<a href="'.header_link(array(CONTROLLER => 'user', ACTION => 'login')).'" class="btn_image"><img src="'.DIR_IMAGES.'ui/lock_key.png" alt="'.translate('LOGIN', true).'" title="'.translate('LOGIN', true).'"><span class="hidden-sm"> '.translate('LOGIN').'</span></a>';
	}
	else
	{
		$link_register = '';
		$link_login = '';
	}
	//pre_print($_SESSION, true);
	if(isset($_GET['lang']))
	{
		header('location:?'.remove_url_element('lang'));
		exit;
	}

	$social_links = array(
		'rss' => '',
		'facebook' => 'https://www.facebook.com/PlovdivWeb/',
		'twitter' => '',
		'pinterest' => '',
		'instagram' => '',
		'youtube' => '',
		'linkedin' => ''
	);

	$facebook_link = $social_links['facebook'];
	// 6LemeLIUAAAAAPixPCt4g6m5CRumTNSb1TQTkJSp
	// 6LemeLIUAAAAANxoT5qk0ZgqtNyLQ8wt_gIHyjTj

	//$_SESSION['SHOW_SITEMAP'] = false;
	//$_SESSION['SHOW_ADS'] = false;


	ob_start();
?><!DOCTYPE html>
<html lang="bg">

<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <title><?php echo translate('SITE_MOTO'); ?></title>

    <link rel="shortcut icon" href="/assets/img/favicon.png" />

    <meta name="description" content="">

    <!-- CSS -->
    <link href="/assets/css/preload.css" rel="stylesheet">
    <link href="/assets/css/custom_menu.css" rel="stylesheet">


    <!-- Compiled in vendors.js -->
	<!--
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/css/bootstrap-switch.min.css" rel="stylesheet">
    <link href="/assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="/assets/css/animate.min.css" rel="stylesheet">
    <link href="/assets/css/slidebars.min.css" rel="stylesheet">
    <link href="/assets/css/lightbox.css" rel="stylesheet">
    <link href="/assets/css/jquery.bxslider.css" rel="stylesheet" />
    <link href="/assets/css/buttons.css" rel="stylesheet">
    -->

    <link href="/assets/css/vendors.css" rel="stylesheet">
    <link href="/assets/css/syntaxhighlighter/shCore.css" rel="stylesheet" >

    <link href="/assets/css/style-<?php echo $default_theme; ?>.css" rel="stylesheet" title="default">
    <link href="/assets/css/width-full.css" rel="stylesheet" title="default">
	<link href="/assets/css/pwc-style.css" rel="stylesheet" title="default">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="/assets/js/html5shiv.min.js"></script>
        <script src="/assets/js/respond.min.js"></script>
    <![endif]-->

	<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-173021035-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-173021035-1');
	</script>

	<?php if( is_default_language() ){ echo '<style>.align_correct {text-align: justify; display: inline-block; }</style>'; } else { echo '<style>.align_correct {text-align: left;}</style>'; } ?>
</head>

<!-- Preloader -->
<!--
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>
-->
<script src="/assets/js/ads.js" type="text/javascript"></script>
<?php if($hide_source) { ?>
<body oncontextmenu="return false">
	<script>
	document.onkeydown = function(e) {
		if(e.keyCode == 123) {
		 return false;
		}
		if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
		 return false;
		}
		if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
		 return false;
		}
		if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
		 return false;
		}
		if(e.ctrlKey && e.keyCode == 'S'.charCodeAt(0)){
		 return false;
		}
		if(e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)){
		 return false;
		}
	 }
	</script>
<?php } else { ?>
<body>
<?php } ?>
<div class="sb-site-container">
<div class="boxed">

<?php
	if(have_messages())
	{
		echo '<div class="container"><div class="row">';
		show_all_messages();
		clear_all_messages();
		echo '</div></div>';
	}
?>
<?php echo $HTML_CONTENT; ?>


</div> <!-- boxed -->
</div> <!-- sb-site -->

<div id="back-top">
    <a href="#header"><i class="fa fa-chevron-up"></i></a>
</div>

<!-- Scripts -->
<!-- Compiled in vendors.js -->
<!--
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/jquery.cookie.js"></script>
<script src="/assets/js/imagesloaded.pkgd.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/bootstrap-switch.min.js"></script>
<script src="/assets/js/wow.min.js"></script>
<script src="/assets/js/slidebars.min.js"></script>
<script src="/assets/js/jquery.bxslider.min.js"></script>
<script src="/assets/js/holder.js"></script>
<script src="/assets/js/buttons.js"></script>
<script src="/assets/js/jquery.mixitup.min.js"></script>
<script src="/assets/js/circles.min.js"></script>
<script src="/assets/js/masonry.pkgd.min.js"></script>
<script src="/assets/js/jquery.matchHeight-min.js"></script>
-->

<script src="/assets/js/vendors.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>

<!-- Syntaxhighlighter -->
<script src="/assets/js/syntaxhighlighter/shCore.js"></script>
<script src="/assets/js/syntaxhighlighter/shBrushXml.js"></script>
<script src="/assets/js/syntaxhighlighter/shBrushJScript.js"></script>

<script src="/assets/js/DropdownHover.js"></script>
<script src="/assets/js/app.js"></script>
<script src="/assets/js/holder.js"></script>
<script src="/assets/js/home_info.js"></script>
<script src="/assets/js/active_page.js"></script>
<script src="/assets/js/login_facebook.js"></script>

</body>
</html>