-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Време на генериране: 24 ное 2021 в 08:57
-- Версия на сървъра: 10.3.25-MariaDB
-- Версия на PHP: 7.3.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данни: `enevsoft_academy`
--

-- --------------------------------------------------------

--
-- Структура на таблица `ms_keys`
--

CREATE TABLE `ms_keys` (
  `key_id` int(10) UNSIGNED NOT NULL,
  `key_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `platform_id` int(10) UNSIGNED NOT NULL,
  `activated_on` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `ms_keys`
--

INSERT INTO `ms_keys` (`key_id`, `key_code`, `platform_id`, `activated_on`) VALUES
(1, 'ASDF-ASDF', 1, NULL),
(2, 'WINA-CTVK', 1, '2021-11-24 08:08:56'),
(3, 'IOSA-CTVK', 2, NULL),
(4, 'ANDR-ACTV', 3, NULL);

-- --------------------------------------------------------

--
-- Структура на таблица `ms_platforms`
--

CREATE TABLE `ms_platforms` (
  `platform_id` int(10) UNSIGNED NOT NULL,
  `platform_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `ms_platforms`
--

INSERT INTO `ms_platforms` (`platform_id`, `platform_name`) VALUES
(1, 'windows'),
(2, 'ios'),
(3, 'android');

--
-- Indexes for dumped tables
--

--
-- Индекси за таблица `ms_keys`
--
ALTER TABLE `ms_keys`
  ADD PRIMARY KEY (`key_id`),
  ADD KEY `platform_id` (`platform_id`);

--
-- Индекси за таблица `ms_platforms`
--
ALTER TABLE `ms_platforms`
  ADD PRIMARY KEY (`platform_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ms_keys`
--
ALTER TABLE `ms_keys`
  MODIFY `key_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ms_platforms`
--
ALTER TABLE `ms_platforms`
  MODIFY `platform_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
