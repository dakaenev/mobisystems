<?php
	$admin_menu = array();
	$is_ajax = 'xmlhttprequest' == strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ?? '' ); // Това не е грешка, а нов оператор в РНР 7: https://www.php.net/manual/en/migration70.new-features.php
	$is_empty_layout = (strtolower($_REQUEST['layout']) == 'empty');

	if(session_status() == PHP_SESSION_NONE)
	{
		session_start();
	}


	ob_start();

	$_SESSION['SHOW_ADS'] = false;
	$_SESSION['FIX_FOOTER'] = true;
	$_SESSION['SHOW_SITEMAP'] = false;

	// Глобални дефиниции и променливи
	require __DIR__.'/vendor/autoload.php';
	//use Spipu\Html2Pdf\Html2Pdf;
	//use Spipu\Html2Pdf\Exception\Html2PdfException;
	//use Spipu\Html2Pdf\Exception\ExceptionFormatter;

	//$html2pdf = new Html2Pdf();
	//$html2pdf = new Html2Pdf('L', 'A4', 'en');
	//$html2pdf->setDefaultFont('Arial');
	//$html2pdf->setModeDebug();

	// Да погледна дали wp_compatibility.php не може да се инклудва след definitions.php за да може да ползва неговите константи
	include('config/wp_compatibility.php');
	include('config/functions.php');
	include('config/definitions.php');
	include_all('helpers/');
	construct_messages();
	set_language();
	//include('test.php');
	//pre_print($_POST);

	$_SESSION['SYSTEM_SETTINGS'] = array(
		'email' => 'contacts@plovdiv-web.com',
		'phone' => translate('SYSTEM_SETTINGS_PHONE'),
		'address' => translate('SYSTEM_SETTINGS_ADDRESS'),
		'map_src' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2958.067690379327!2d24.73420851544975!3d42.14883827920169!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3c2c3254e6eae079!2sPlovdiv%20Web!5e0!3m2!1sbg!2sbg!4v1583832632346!5m2!1sbg!2sbg',
		'work_time_start' => '10',
		'work_time_end' => '22',
		'work_time_break' => '17:00-18:00',
		'company_name' => 'Plovdiv Web Community, Inc.',
		'legal_company' => 'еНеВ Софт ЕООД',
//		'bank_name' => 'Райфайзен Банк',
//		'bank_iban' => 'BG86 RZBB 9155 1011 6443 10',
		'bank_name' => 'Уникредит Булбанк',
		'bank_iban' => 'BG06 UNCR 9660 0044 6838 16',
		'bank_bic' => 'UNCRBGSF'
	);

	if(!is_user_logged_in() && isset($_COOKIE['auto_login']))
	{
		$cookie_data = explode('|', $_COOKIE['auto_login']);

		database_open();
		load_user_session($cookie_data[0], $cookie_data[1]);
		database_close();
	}


	//if(!$is_empty_layout){ get_header(); }
	if(is_user_logged_in()) // is_user_logged_in()  || isset($_GET['qa_id'])
	{
		$admins_array = array(1, 44, 88);
		$assistants_array = array(
		);

		$except_users = array(
			1050, 
			882,  
			354,  
			1254, 
			442,  
			1913, 
			274,  
			583,  
			947, 
			773,  
			388,  
			387,  
			8,    
			1327, 
			2557, 
			2555  
		);

		$is_logged_user = true;  	// Това да се върже с WordPress-a
		$user_id = (false ? $_GET['qa_id'] : get_current_user_id());			// Това също да се върже с WordPress-a
		$is_admin_user = in_array($user_id, $admins_array); //is_super_admin() || (in_array($user_id, $admins_array));
		$is_super_admin = $is_admin_user; //(1 == $user_id);
		$controller_functions = array();
    }
	else
	{
		$user_id = 0;
		//wp_redirect( wp_login_url( site_url( $_SERVER['REQUEST_URI'] ) ) );
		//exit;
	}

	if($is_super_admin && false)
	{
		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);
	}

	// Тук са дефинирани всички възможни страници. Ако страница, НЕ е дефинирана тук, системата няма да я разпознае.
	$ALL_PAGES = array('course','exam','question','user', 'logs', 'homeworks', 'certificates', 'mail', 'statistic', 'club', 'page', 'api');

	// =====================================================================================================================================
	// = Програмистът нека да промени този блок код при необходимост                                                                       =
	// =====================================================================================================================================
	//error_reporting(E_ALL);

	// Ако се използват Friendly URLs следва URL-a да се обработи така че $source да бъде масив с ключове 'page', 'id', 'action' и т.н.
	$script_url = ('/' == substr($_SERVER['SCRIPT_URL'], -1) ? substr($_SERVER['SCRIPT_URL'], 0, -1) : $_SERVER['SCRIPT_URL']);

	$friendly = explode('/', $script_url);
	$friendly[CONTROLLER] = $friendly[1];
	$friendly[ACTION] = $friendly[2];
	$friendly[ID] = null;

	if(isset($friendly[3]))
	{
		$id_components = explode('-', $friendly[3]);
		$friendly[ID] = reset($id_components);
	}

	$source = array_merge($friendly, $_GET);

	//$source[PAGE] = $wp_query->query_vars[PAGE];

	define('PAGE_PARSE_START_TIME', get_micro_time());

	if(true) // Log every user activity ;)
	{
		$data_array = array(
			'user_id' => $user_id,
			'request_url' => $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'],
			'ip_address' => $_SERVER['REMOTE_ADDR'],
			'log_time' => (isset($_SERVER['REQUEST_TIME']) ? $_SERVER['REQUEST_TIME'] : strtotime('now')) + TIME_CORRECTION,
			'full_info' => serialize($_SERVER)
		);

		database_open();

		insert_query('user_logs', $data_array);
		//if(!$is_empty_layout){ show_events(($user_id == -1 ? '2016-05-09' : date('Y-m-d'))); }

		database_close();
	}

	load_page(); // Определя как се зареждат страниците. Ако е необходимо внесете корекции. Функцията е дефинирана в "config/functions.php"

//	if(!$is_empty_layout){ $HTML_CONTENT = '<script> //var $ = jQuery.noConflict(); </script>'; }
//	$HTML_CONTENT .= ob_get_contents();
//	ob_end_clean();

	define('PAGE_PARSE_END_TIME', get_micro_time());
	//echo $HTML_CONTENT;
	// =====================================================================================================================================
	// = По другитe файлове няма какво да се променя! =
	// =====================================================================================================================================
?><?php
// Този ред да се коментира, когато се свърже към WordPress-a
//if(!$is_empty_layout){
//include('design.php');
//}else{ echo $HTML_CONTENT; }

$HTML_CONTENT = ob_get_clean();
include_layout();
?><?php //if(!$is_empty_layout){ /* get_footer(); */ } ?>
<?php exit;

