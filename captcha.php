<?php
session_start();

class CaptchaSecurityImages {

var $font = './monofont.ttf';

function CaptchaSecurityImages($width='120',$height='30',$characters='6') {
$_SESSION['code'] = Generate_Captcha_Code($characters);
$code = $_SESSION['code'];

/* font size ще бъде 75% от височината на изображението */
$font_size = $height * 0.95;
$image = @imagecreate($width, $height) or die('Cannot Initialize new GD image stream');

/* задаване на цветове */
$background_color = imagecolorallocate($image, 224, 224, 224);  //    0   0  0 | 255,255,255
$text_color = imagecolorallocate($image, 77, 125, 180);     // 255 255 255 |   0,  0,  0
$noise_color = imagecolorallocate($image, 224, 224, 224);        //  20  40 100 | 100,100,100

/* създаване и добавяне на текст */
$textbox = imagettfbbox($font_size, 0, $this->font, $code);
$x = ($width - $textbox[4])/2;
$y = ($height - $textbox[5])/2;
imagettftext($image, $font_size, 0, $x, $y, $text_color, $this->font , $code);

/* Генериране на случайни точки във фона */
for( $i=0; $i<($width*$height)/3; $i++ ) {
imagefilledellipse($image, mt_rand(0,$width), mt_rand(0,$height), 1, 1, $noise_color);
}

/* Генериране на случайни линии във фона */
for( $i=0; $i<($width*$height)/150; $i++ ) {
imageline($image, mt_rand(0,$width), mt_rand(0,$height), mt_rand(0,$width), mt_rand(0,$height), $noise_color);
}


/* извеждане на изображението дo браузъра */
imagejpeg($image);
imagedestroy($image);
}
}

function Generate_Captcha_Code($characters)
{
	$possible = '0123456789';
	$code = '';
	$i = 0;
	while ($i < $characters) {
		$code .= substr($possible, mt_rand(0, strlen($possible)-1), 1);
		$i++;
	}

	return $code;
}

$width = isset($_GET['width']) ? $_GET['width'] : '120';
$height = isset($_GET['height']) ? $_GET['height'] : '40';
$characters = isset($_GET['characters']) ? $_GET['characters'] : '6';

header('Cache-control: no-cache');
header('Content-Type: image/jpeg');

$captcha = new CaptchaSecurityImages($width,$height,$characters);

?>