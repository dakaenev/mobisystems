<?php
	database_open('enevsoft_wa');
	$max_users = exec_query('SELECT user_id FROM users ORDER BY user_id DESC LIMIT 1');
	$max_users = to_assoc_array($max_users);
	$max_users = reset($max_users)['user_id'];
	database_close();

	$carousel_elements = array(
		'coworking' => array(
			'title' => 'Много повече от <span>споделен офис</span>',
			'items' => array(
				'<strong>Офис и конферентни зали</strong>, тераси, кухня и други!',
				'<strong>Специални намаления</strong> за редовни ползватели!',
				'<strong>Достъпни цени,</strong> съобразени с Вашите нужди!'
			),
			'descr' => 'Всяка зала е напълно оборудвана с бюра и удобни столове. Осигурили сме Ви всичко, за да се чувствате като у дома си!', // Бяла дъска, проектор, екран, - от сокоизтисквачка и мека мебел до китара, IP телевизия, джойстик, бордови игри. Имаме кафе машина,...
			'image' => DIR_IMAGES.'blog/DSC00804.JPG', # /assets/img/demo/mockup2.png
			'order'  => '#',
			'know'  => site_url('co-working/')
		),
		'courses' => array(
			'title' => 'Много повече от <span>Академия</span>',
			'items' => array(
				'Можете да изберете измежду <strong>6  курса</strong> още сега!',
				'Записването е до 30.10.2020, а <strong>цените са достъпни</strong>!',
				'Подготвили сме <strong>атрактивни отстъпки</strong> за най-сериозните!' // най-{какво} редовните, сериозните, запалените ???
			),
			'descr' => '<span style="font-weight: bold; text-decoration:underline; margin:0 5px 0 20px;">Знаете ли, че:</span> можете да изберете <span>онлайн форма на обучение</span>.',
			'image' => DIR_IMAGES.'blog/DSC00837.JPG',
			'order'  => '#',
			'know'  => site_url('courses/')
		),
		'community' => array(
			'title' => str_ireplace('{MEMBERS_COUNT}', round($max_users, -2, PHP_ROUND_HALF_DOWN), translate('MEMBERS_BEHIND_US')), // Ние сме повече от 2600 човека
			'items' => array(
				'Задължително <strong>най-малко 2 събития</strong>, всеки месец.',
				'Специални намаления само за редовни членове. ', //'Всеки семестър безплатен курс само за наши членове. ',
				'Възможност да персонализирате сайта ни и много други...'
			),
			'descr' => 'Станете част от <span>нашата общност</span> и нека заедно да бъдем едно <span>голямо и сплотено семейство</span>. Присъединете се към нас сега!',
			'image' => DIR_IMAGES.'blog/DSC00823.JPG',
			'order' => '#',
			'know'  => site_url('user/register/')
		)
	);

	unset($carousel_elements['community']);
	//unset($carousel_elements['coworking']);
	//unset($carousel_elements['courses']);

	$indicators = '';
	$carousel = '';

	$carousel_elements = array_values($carousel_elements);
	foreach($carousel_elements as $key => $element)
	{
		$items = '';
		foreach($element['items'] as $k => $item)
		{
			$items .= '<li class=""><i class="fa fa-angle-right animated fadeIn animation-delay-'.($k * 4 + 11).'"></i><span class="animated fadeInRightBig animation-delay-'.($k * 3 + 13).'">'.$item.'</span></li>';
		}

		$indicators .= '<li data-target="#carousel-example-ny" data-slide-to="'.$key.'"'.(0 == $key ? ' class="active"' : '').'></li>';
		$carousel .= '<div class="item'.(0 == $key ? '  active' : '').'">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-push-6">
                                '.($element['image'] != '' ? '<img src="'.$element['image'].'" alt="'.$element['image'].'" class="img-responsive animated zoomInUp animation-delay-30 home_carousel_image">' : '').'
                        </div>
                        <div class="col-md-6 col-md-pull-6">
                            <div class="carousel-caption">
                               <h1 class="animated fadeInDownBig animation-delay-7 carousel-title">'.$element['title'].'</h1>
                               <ul class="list-unstyled carousel-list">
                                   '.$items.'
                               </ul>
                               <p class="animated zoomIn animation-delay-20">'.$element['descr'].'</p>
							   <div class="action-zone">
								   <!-- <a href="'.$element['order'].'" class="btn btn-ar btn-transparent-opaque btn-xl animated fadeInUp animation-delay-22"><i class="fa fa-opencart"></i> Purchase Now</a> -->
								   <a href="'.$element['know'].'" class="btn btn-ar btn-transparent-opaque btn-xl animated fadeInUp animation-delay-25 pull-left"><i class="fa fa-search"></i> '.translate('KNOW_MORE').'</a>
							   </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
	}

	$controls = (count($carousel_elements) > 3 ? '<a class="left carousel-control" href="#carousel-example-ny" data-slide="prev"><i class="fa fa-angle-left"></i></a><a class="right carousel-control" href="#carousel-example-ny" data-slide="next"><i class="fa fa-angle-right"></i></a>' : '');
	$indicators = '<ol class="carousel-indicators">'.(count($carousel_elements) > 2 ? $indicators : '').'</ol>';
	$carousel = '<div class="carousel-inner">'.$carousel.'</div>';

	if(count($carousel_elements)) {
?>
<section class="wrap-hero margin-bottom">
    <div id="carousel-example-ny" class="carousel carousel-hero slide" data-ride="carousel" data-interval="16000">
        <!-- Indicators -->
        <?php echo $indicators; ?>

        <!-- Wrapper for slides -->
		<?php echo $carousel; ?>

        <!-- Controls -->
        <?php echo $controls; ?>
    </div>
</section>
	<?php } ?>
<div class="container">
    <section class="margin-bottom">
        <div class="row">
            <div class="col-md-12">
                <h2 class="right-line"><?php echo translate('What_do_we_do'); ?></h2>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="text-icon wow fadeInUp">
                    <span class="icon-ar icon-ar-lg"><i class="fa fa-briefcase"></i></span>
                    <div class="text-icon-content">
                        <h3 class="no-margin">Споделен офис</h3>
                        <p class="align_correct">Нашият <a href="<?php echo site_url('co-working/'); ?>">споделен офис</a> се простира на 120 м<sup>2</sup>, разпределени в кухня, стая за релакс, 3 офис зали, 3 тераси, 2 тоалетни<!--, баня -->. <a href="#" onclick="alert('Свържете се с нас на: <?php echo translate('SYSTEM_SETTINGS_PHONE'); ?>');">Искаш ли място?</a></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="text-icon wow fadeInUp">
                    <span class="icon-ar icon-ar-lg"><i class="fa fa-graduation-cap"></i></span>
                    <div class="text-icon-content">
                        <h3 class="no-margin">Обучения и курсове</h3>
                        <p class="align_correct">В нашите <a href="<?php echo site_url('courses/'); ?>">курсове</a> ще се обучавате в стила на най-добрите практики, чрез много различни примери, поднесени по забавен начин. <i class="fa fa-smile-o"></i></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="text-icon wow fadeInUp">
                    <span class="icon-ar icon-ar-lg"><i class="fa fa-paper-plane-o"></i></span>
                    <div class="text-icon-content">
                        <h3 class="no-margin">Кариерен старт</h3>
                        <p class="align_correct">Участвайки в <a href="<?php echo site_url('#'); ?>">нашите проекти</a>, ще придобиете безценен практически опит, който ще даде летящ старт на Вашето кариерно развитие.</p>
                    </div>
                </div>
            </div>
			<div class="col-lg-4 col-md-6 col-sm-12">
                <div class="text-icon wow fadeInUp">
                    <span class="icon-ar icon-ar-lg"><i class="fa fa-desktop"></i></span>
                    <div class="text-icon-content">
                        <h3 class="no-margin">Отдих и релакс</h3>
                        <p class="align_correct">Нашият офис съчетава в себе си бизнес среда и домашен уют. Тук имаме всичко нужно - от хладилник и кафемашина до IP телевизия</p>
                    </div>
                </div>
            </div>
			<div class="col-lg-4 col-md-6 col-sm-12">
                <div class="text-icon wow fadeInUp">
                    <span class="icon-ar icon-ar-lg"><i class="fa fa-users"></i></span>
                    <div class="text-icon-content">
                        <h3 class="no-margin">Общност</h3>
                        <p class="align_correct">Нашите членове са повече от <?php echo round($max_users, -2, PHP_ROUND_HALF_DOWN); ?> души. Станете част от нашето голямо семейство! <span class="plovdiv theme-color" style="font-size: 16px;">#заедно</span> ще променим Пловдив и региона!</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="text-icon wow fadeInUp">
                    <span class="icon-ar icon-ar-lg"><i class="fa fa-gamepad"></i></span>
                    <div class="text-icon-content">
                        <h3 class="no-margin">Игри и забавления</h3>
                        <p class="align_correct">Ако сте любител на игри, като Counter Strike, Star Craft, Шах, Белот, Activity и други подобни, Вашето място е тук - <a href="<?php echo header_link([CONTROLLER => 'user', ACTION => 'register']);?>">Присъединете се сега!</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>