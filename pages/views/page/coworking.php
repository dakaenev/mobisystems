<?php //$_SESSION['SHOW_ADS'] = false; ?>
<?php $contacts_link = header_link([CONTROLLER => 'page', ACTION => 'contacts']); ?>
<?php
	$carousel_elements = array(
		'conferent' => [ 'image' => DIR_IMAGES.'blog/DSC00801.JPG', 'title' => 'Зала "Конферентна"'	],
		'academy'	=> [ 'image' => DIR_IMAGES.'blog/DSC00837.JPG', 'title' => 'Зала "Академик"'	],
		'private'	=> [ 'image' => DIR_IMAGES.'blog/DSC00872.JPG', 'title' => 'Зала "Частна"'		],
		'cooking'	=> [ 'image' => DIR_IMAGES.'blog/DSC00820.JPG', 'title' => 'Кухня'				],
		'relax'		=> [ 'image' => DIR_IMAGES.'blog/DSC00823.JPG', 'title' => 'Отдих'				]
	);

	//unset($carousel_elements['relax']);

	$active_index = 0;
	$indicators = '';
	$carousel = '';
	$exist = 0;

	$carousel_elements = array_values($carousel_elements);
	foreach($carousel_elements as $key => $element)
	{
		// use substr to remove first '/' from path (/images/...), because file_exist not found it if start with '/'
		if($element['image'] != '' && file_exists(substr($element['image'], 1)))
		{
			$indicators .= '<li data-target="#carousel-example-captions" data-slide-to="'.$key.'"'.($key == $active_index ? ' class="active"' : '').'></li>';
			$carousel .= '<div class="item'.($key == $active_index ? ' active' : '').'">
							<img src="'.$element['image'].'" alt="'.$element['title'].'" class="center">
							<div class="carousel-caption animated fadeInUpBig">
								<h3>'.$element['title'].'</h3>
							</div>
						</div>';
			$exist++;
		}
	}

	$list_items = '<ul class="services-header-list">
						<li class="animated fadeInUp animation-delay-5"><a href="#" class="office-header-link"><i class="fa fa-globe"></i> Отлично местоположение </a></li>
						<li class="animated fadeInUp animation-delay-7"><a href="#" class="office-header-link"><i class="fa fa-tachometer"></i> Бърз и надежден интернет</a></li>
						<li class="animated fadeInUp animation-delay-9"><a href="#" class="office-header-link"><i class="fa fa-building"></i> Удобството на дома в офиса</a></li>
					</ul>';
?>
<header class="services-header background-center">
    <div class="primary-dark-div">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="service-header-text text-center center-block">
                        <h1 class="animated fadeInDown animation-delay-2">Споделен офис</h1>
                        <p class="animated fadeInDown animation-delay-5">Предлагаме възможност за споделено работно пространство на прекрасна локация в град Пловдив – бул. „6-ти септември“ №113</p>
						<?php if($exist){ echo $list_items; } // Show list in left, Carousel Images are on right ?>
                        <p class="animated fadeInDown animation-delay-8"><a href="#" class="btn btn-ar btn-primary btn-lg" onclick="alert('Свържете се с нас на: <?php echo translate('SYSTEM_SETTINGS_PHONE'); ?>');">Запази си място</a></p>
                    </div>
                </div>
                <div class="col-md-6">
					<?php if($exist){  // Show list in left, Carousel Images are on right ?>
					<div id="carousel-example-captions" class="carousel carousel-images slide" data-ride="carousel">
						<div class="carousel-inner">
							<?php echo $carousel; ?>
						</div>
						<ol class="carousel-indicators">
							<?php echo $indicators; ?>
						</ol>
						<?php if($exist > 1) { ?>
						<a class="left carousel-control" href="#carousel-example-captions" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left"></span>
						</a>
						<a class="right carousel-control" href="#carousel-example-captions" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right"></span>
						</a>
						<?php } ?>
					</div>
					<?php } else { echo $list_items; } // Carousel is empty, so show list in right ?>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="container">
    <section>
        <div class="row">
            <div class="col-md-12">
                <h2 class="right-line">Какво Ви предлагаме?</h2>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="text-icon wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                    <span class="icon-ar icon-ar-lg"><i class="fa fa-users"></i></span>
                    <div class="text-icon-content">
                        <h3 class="no-margin">Капацитет</h3>
                        <p class="align_correct">Разполагаме със 120 кв. метра, разпределени в 2 зали за работа, 2 конферентни зали, 1 зала за почивка, 3 тераси, кухня, баня и 2 тоалетни</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="text-icon wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                    <span class="icon-ar icon-ar-lg"><i class="fa fa-home"></i></span>
                    <div class="text-icon-content">
                        <h3 class="no-margin">Търсите ли комфорт?</h3>
                        <p class="align_correct">Осигурили сме Ви всичко необходимо - от сокоизтисквачка, наргиле и мека мебел до китара, IP телевизия, джойстик, board games</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="text-icon wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                    <span class="icon-ar icon-ar-lg"><i class="fa fa-clock-o"></i></span>
                    <div class="text-icon-content">
                        <h3 class="no-margin">Удобно работно време</h3>
                        <p class="align_correct">Можете да ползвате зала Academic всеки делничен ден в диапазона 08:00-18:00 часа, а зала Private всеки ден в диапазона 08:00-22:00</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="text-icon wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                    <span class="icon-ar icon-ar-lg"><i class="fa fa-coffee"></i></span>
                    <div class="text-icon-content">
                        <h3 class="no-margin">Напълно оборудвани зали</h3>
                        <p class="align_correct">Всяка зала е напълно оборудвана с бюра, удобни столове, бяла дъска, проектор, екран. Имаме кафе машина, диспенсър, хладилник... </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="text-icon wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                    <span class="icon-ar icon-ar-lg"><i class="fa fa-graduation-cap"></i></span>
                    <div class="text-icon-content">
                        <h3 class="no-margin">Дом за Вашата академия</h3>
                        <p class="align_correct">Ако искате да стартирате своя академия и търсите оборудвани зали, <a href="<?php echo $contacts_link; ?>">пишете ни</a>, за да обсъдим възможностите за сътрудничество.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="text-icon wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp;">
                    <span class="icon-ar icon-ar-lg"><i class="fa fa-usd"></i></span>
                    <div class="text-icon-content">
                        <h3 class="no-margin">Достъпни цени</h3>
                        <p class="align_correct">Предлагаме всичко това на достъпни цени, като за редовните ползватели има намаления. Не се колебайте, а си <a href="#" onclick="alert('Свържете се с нас на: <?php echo translate('SYSTEM_SETTINGS_PHONE'); ?>');">запазете място при нас!</a></p>
						<!-- Предлагаме всичко това на достъпни цени (около 200 лв намесец), като за редовните  ползватели има атрактивни намаления! Запазете си място или ни пишете! -->
                    </div>
                </div>
            </div>
        </div> <!-- row -->
    </section>
</div>