<?php
	$info_array = array(
		array(
			'name' => 'Кои сме ние?',
			'text' => 'Ние сме малък, но сплотен екип от хора с различна възраст, етнос, религия, обединени от идеята <span class="theme-color">#заедно</span> '
					. 'да спомогнем за подобряване на ИТ образованието в Пловдив и региона, и най-вече да бъдем в помощ на Хората. '
					. 'На един по-късен етап бихме искали да бъдем дом за всички геймъри, програмисти, дизайнери и всякакви други Web & IT хора в Пловдивско!'
		),
		array(
			'name' => 'Нашата мисия?',
			'text' => 'Нашата цел е да бъдем катализатор на образованието в град Пловдив и региона, и по този начин, чрез по-добре обучени кадри и в партньорство с частни, '
					. 'държавни и общински организации да подпомогнем местния пазар на труда, местната икономика, и в частност живота на Хората, които се обучават при нас.'
		),
		array(
			'name' => 'Кой стои зад Нас?',
			'text' => 'Ние принадлежим само и единствено на нашите членове.  Ние няма да изчезнем с банкрута на поредната корпорация / фондация, а ще съществуваме докато нашите членове ни гласуват доверието си!'
					. '<br>Всеки може да ни подкрепи, но никой не може да ни присвои! Нашата адаптивност и възможност да си партнираме с изявени специалисти в съответните области е гаранцията за нашият успех!'
		)
	);

	$list_items = '';
	$panel_items = '';

	foreach($info_array as $k => $info)
	{
		 $list_items .= '<li>'.$info['name'].'<br>'.$info['text'].'</li>';
		$panel_items .= '<div class="panel panel-info">';
		$panel_items .= '<div class="panel-heading panel-heading-link"><a data-toggle="collapse" data-parent="#about_texts" href="#at_'.$k.'" class="collapsed" aria-expanded="false">'.$info['name'].'</a></div>';
		$panel_items .= '<div id="at_'.$k.'" class="panel-collapse collapse" aria-expanded="false"><div class="panel-body">'.$info['text'].'</div></div></div>';
	}
?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h2 class="section-title">
				<span class="pictogram" style="font-size:50px;">flag</span>
				<span class="plovdiv" style="position:relative; top:20px; font-size:40px;"> За нас:</span>
			</h2>

			<ul class="pwc_ul hidden-xs">
				<?php echo $list_items; ?>
			</ul>

			<div class="panel-group hidden-lg hidden-md hidden-sm" id="about_texts">
				<?php echo $panel_items; ?>
			</div>

			<img src="<?php echo DIR_IMAGES; ?>/others/community.png" class="img-responsive center">
		</div>
	</div>
</div>