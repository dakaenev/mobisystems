<?php
	Global $require_code, $default_data;

	$captcha_code_field = '<input id="captcha_code" type="'.($require_code ? 'text' : 'hidden').'" placeholder="Въведете цифрите" name="captcha_code" value="'.$default_data['captcha_code'].'">';
	$form_button = '<button '.($require_code ? 'type="button" data-toggle="modal" data-target="#codeModal"' : 'type="submit"').' class="btn btn-ar btn-primary pull-right">'.translate('BTN_FORM_SEND').'</button>';

	if($require_code)
	{
		$modal_template = '
			<!-- Modal -->
			<div class="modal fade" id="codeModal" tabindex="-1" role="dialog" aria-labelledby="codeModalLabel" aria-hidden="true">
			  <div class="modal-dialog">
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">'.translate('ARE_YOU_HUMAN').'</h4>
				  </div>
				  <div class="modal-body">
					<p>'.translate('PROVE_HUMANITY').'</p>
					' . $captcha_code_field . '
					<img id="captcha_image" src="/captcha.php" alt="captcha" height="30" style="border: 1px solid gray; position: relative; bottom: 1px; left: 4px;">
					<button id="captcha_reload" type="button"><i class="fa fa-refresh"></i></button>
				  </div>
				  <div class="modal-footer">
					<button type="button" class="btn btn-ar btn-default" data-dismiss="modal">'.translate('BTN_CANCEL').'</button>
					<button type="submit" class="btn btn-ar btn-primary">'.translate('BTN_FORM_SEND').'</button>
				  </div>
				</div>
			  </div>
			</div>
			<!-- Modal End Here -->';
	}
	else
	{
		$modal_template = $captcha_code_field;
	}
?>
<form action="<?php echo header_link(array(CONTROLLER => 'page', ACTION => 'contacts')); ?>" method="post" onsubmit="validate();">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="section-title"><?php echo translate('send_message'); ?></h2>
			</div>
			<div class="col-lg-4 col-md-5 col-sm-6">
				<section>
					<div class="panel panel-primary">
						<div class="panel-heading"><i class="fa fa-terminal"></i> <?php echo translate('info'); ?></div>
						<div class="panel-body">
							<h4 class="section-title no-margin-top"><?php echo translate('contacts'); ?></h4>
							<address>
								<strong><?php echo $_SESSION['SYSTEM_SETTINGS']['company_name']; ?></strong><br>
								<abbr title="Адрес"><i class="fa fa-building"></i></abbr> <a href="https://www.google.com/maps/d/u/0/viewer?ie=UTF8&hl=bg&t=h&msa=0&err=1&mid=199j5yi5a8Rs8-ljP4kXsaJMQVfY&ll=42.148689203433676%2C24.74627499999997&z=16" target="_blank"><?php echo $_SESSION['SYSTEM_SETTINGS']['address']; ?></a><br>
								<abbr title="Е-поща"><i class="fa fa-envelope-square"></i></abbr> <a href="#"><?php echo $_SESSION['SYSTEM_SETTINGS']['email']; ?></a><br>
								<abbr title="Телефон"><i class="fa fa-phone-square"></i></abbr> <a href="#" class="showPhone">088****(<?php echo translate('LABEL_SHOW'); ?>)</a><br>
							</address>

							<!-- Business Hours -->
							<h4 class="section-title no-margin-top"><?php echo translate('work_time'); ?></h4>
							<ul class="list-unstyled">
								<li><strong><?php echo translate('monday'); ?> - <?php echo translate('sunday'); ?>:</strong> <?php echo $_SESSION['SYSTEM_SETTINGS']['work_time_start'].':00 - '.$_SESSION['SYSTEM_SETTINGS']['work_time_end'].':00'; ?></li>
								<!--
								<li><strong><?php echo translate('monday'); ?> - <?php echo translate('friday'); ?>:</strong> <?php echo $_SESSION['SYSTEM_SETTINGS']['work_time_start'].':00 - '.$_SESSION['SYSTEM_SETTINGS']['work_time_end'].':00'; ?></li>
								<li><strong><?php echo translate('break_time'); ?>:</strong> <?php echo $_SESSION['SYSTEM_SETTINGS']['work_time_break']; ?></li>
								<li><strong><?php echo translate('saturday'); ?> - <?php echo translate('sunday'); ?>:</strong> <span style="color:red;"><?php echo translate('closed'); ?></span></li>
								-->
							</ul>
						</div>
					</div>
				</section>
				<div class="form-group hidden-xs">
					<?php TC_Checkbox(); ?>
				</div>
			</div>
			<div class="col-lg-8 col-md-7 col-sm-6">
				<section>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="InputName"><?php echo translate('your_name'); ?></label>
								<input type="text" class="form-control" name="person_name" value="<?php echo $default_data['person_name']; ?>">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="InputEmail1"><?php echo translate('email_address'); ?></label>
								<input type="email" class="form-control" name="person_mail" value="<?php echo $default_data['person_mail']; ?>">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label for="InputEmail1">Относно</label>
								<input type="text" class="form-control" name="mail_about" value="<?php echo $default_data['mail_about']; ?>">
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group">
								<label for="InputMessage"><?php echo translate('message'); ?></label>
								<textarea class="form-control" name="mail_text" id="contact_form_message" rows="8"><?php echo $default_data['mail_text']; ?></textarea>
							</div>
						</div>
					</div>
					<div class="form-group">
						<?php TC_Checkbox('hidden-lg hidden-md hidden-sm'); ?>
						<?php echo $form_button; ?>
					</div>

					<div class="clearfix"></div>

					<?php echo $modal_template; ?>
				</section>
			</div>
		</div>

		<hr class="dotted">

		<section>
			<div class="well well-sm">
				<!-- <iframe width="100%" height="350" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=central%2Bpark&amp;ie=UTF8&amp;z=12&amp;t=m&amp;iwloc=near&amp;output=embed"></iframe> -->
				<iframe width="100%" height="350" src="<?php echo $_SESSION['SYSTEM_SETTINGS']['map_src']; ?>" allowfullscreen="allowfullscreen"></iframe>
			</div>
		</section>
	</div>
</form>
<script>
	$(document).ready(function(){
		$(".showPhone").click(function(){
			var d = new Date(<?php echo ((strtotime('now') + TIME_CORRECTION) *1000); ?>);

			if(<?php echo $_SESSION['SYSTEM_SETTINGS']['work_time_start']; ?> <= d.getHours() && d.getHours() <= <?php echo $_SESSION['SYSTEM_SETTINGS']['work_time_end']; ?>){
				$(this).text('<?php echo $_SESSION['SYSTEM_SETTINGS']['phone']; ?>');
				$(this).attr("href", "tel:<?php echo $_SESSION['SYSTEM_SETTINGS']['phone']; ?>");
				$(this).removeClass("showPhone");
			}else{
				alert('<?php echo translate('MESSAGE_WAIT_WORK_TIME'); ?>');
			}
		});
	});




	function validate(){
		//alert('This not work yet! \nIt is in progress!');
		//event.preventDefault();
		//return false;
	}


	$(document).ready(function(){
		$('#captcha_reload').click(function(){
			let captcha_src = '/captcha.php?' + new Date().getTime();
			$('#captcha_image').attr('src', captcha_src);
			//alert(captcha_src);
		});

		//$('#captcha_code').keyup(function(){
		//	$('#form_captcha_code').val($(this).val());
		//});
	});

</script>