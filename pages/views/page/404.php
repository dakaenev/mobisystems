<?php
	$_SESSION['SHOW_ADS'] = false;
	$_SESSION['FIX_FOOTER'] = false;
	$joke_url = 'https://fakti.bg/jokes/random';

	$page = array(
		CONTROLLER => 'page',
		ACTION => 'FileGetContents',
		'layout' => 'empty',
		'url' => $joke_url
	);
?>
<div class="container">
    <div class="row">
        <div class="col-md-12" style="margin-top:14px; margin-bottom:25px;">
            <div class="center-block error-404">
				<section class="text-center">
                    <h2><?php echo translate('Page_not_found_title'); ?></h2>
                    <p class="lead lead-lg plovdiv"><?php echo translate('Page_not_found_subtitle'); ?><i class="fa fa-smile-o"></i></p><!-- The requested URL was not found on this server. That is all we know. -->
                </section>
            </div>
			<section id="joke" class="plovdiv col-sm-12 col-md-7 col-lg-6" style="font-size:16px; overflow: auto; height: 400px;"></section>
			<div class="col-sm-12 col-md-5 col-lg-6">
				<img src="<?php echo DIR_IMAGES; ?>others/404-robot.gif" alt="404 error image" class="pull-right img-responsive hidden-xs">
			</div>
        </div>
    </div>
</div>
<script>
	$(document).ready(function(){
		$('section#joke').load("<?php echo header_link($page); ?> .fun", function( response, status, xhr ) {
			if ( status == "success" ) {
				$('section#joke').append('<br><br><p>Ако искате да се порадвате на още вицове, <a href="<?php echo $joke_url; ?>" target="blank"><b>натиснете тук!</b></a></p>');
			}else{
				$('section#joke').hide();
				console.log( msg + xhr.status + " " + xhr.statusText );
			}
		});
	});
</script>