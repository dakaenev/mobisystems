<?php $_SESSION['SHOW_ADS'] = false; ?>
<div class="container">
    <div class="row">

			<img src="/assets/img/demo/e_product2.jpg" alt="Phone" />

            <form method="post" action="<?php echo $_SERVER['SCRIPT_URI']; ?>" class="pull-right" style="margin-top:40px;">
				<div class="form-group">
					<label>E-Mail <input type="email" name="email" id="email" class="form-control" required="required" value="dakaenev@gmail.com"></label><br>
					<label>Phone <input type="tel" name="phone" id="phone" class="form-control" required="required" value="0885 (34-53-95)"></label><br>
					<input type="hidden" name="action" value="send_otp" />
					<button type="submit" name="action" value="send_otp" id="btn_register">Register me</button><br>

					<div id="otp_validation">
						<label>OTP <input type="password" name="otp" id="otp" value="" class="form-control"></label><br>
						<input type="submit" name="action" value="check_otp" id="btn_verify" value="Verify">
					</div>
				</div>
			</form>

    </div>
</div>
<script>
	function validatePhone(phone){ return true;
		const DEFAULT_PHONE_CODE = <?php echo DEFAULT_PHONE_CODE; ?>; // Взимаме стойността от РНР, за да не стане разминаване между JS и PHP.

		// Премахваме всичко, което не е цифра, за да унифицираме номера
		phone = phone.replace(/\D/gi, "");

		if("0" == phone.substr(0,1)){ // Преобразуваме: 0877... => 359877...
			phone = DEFAULT_PHONE_CODE + phone.substr(1);
		}

		if("359" != phone.substr(0,3)){
			alert("Неразпознат код за държава! \nРаботим само с български номера!");
			return false;
		}

		if(12 != phone.length){
			alert("Телефонния номер има грешна дължина!");
			return false;
		}

		console.log(phone);
		return true;
	}

	$(document).ready(function(){
		$("#otp_validation").addClass("hidden");
	});

	// Ако зависеше от мен, бих забранил всичко, което е различно от цифра, но по условие символите като празен интервал, тире и скоби са разрешени :)
	$("input[type=tel]").keypress(function(event){
		var allowed = "0123456789 (-)";

		//console.log(event.key + ':' + event.which);
		if(-1 == allowed.indexOf(event.key)){
			return false;
		}
		return true;
	});

	$("#btn_register").click(function(event){
		event.preventDefault();

		var phoneValue = $("#phone").val();
		if(validatePhone(phoneValue) == true){
			$.post("<?php echo $_SERVER['SCRIPT_URI']; ?>",
			{
			  action: "send_otp",
			  phone: phoneValue,
			  email: $("#email").val()
			},
			function(data, status){
			  //alert("Data: " + data + "\nStatus: " + status);
			  alert(data);
			  console.log(data);
			  if("success" == status){
				  $("#otp_validation").removeClass("hidden");
			  }
			});
		}
	});

	$("#btn_verify").click(function(){
		event.preventDefault();

		var phoneValue = $("#phone").val();
		if(validatePhone(phoneValue) == true){
			$.post("<?php echo $_SERVER['SCRIPT_URI']; ?>",
			{
			  action: "check_otp",
			  phone: phoneValue,
			  email: $("#email").val(),
			  otp: $("input[name=otp]").val()
			},
			function(data, status){
			  //alert("Data: " + data + "\nStatus: " + status);
			  alert(data);
			  console.log(data);
			  if("success" == status){
				  $("#otp_validation").removeClass("hidden");
			  }
			});
		}
	});
</script>