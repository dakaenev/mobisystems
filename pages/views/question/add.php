<?php
	Global $is_admin_user, $source, $question, $message, $QUESTIONS_GROUPS, $QUESTIONS_TYPES, $default_test_id, $user_id;

	if($is_admin_user)
	{
		if($message != '')
		{
			echo $message;
		}
		else
		{
			//$question = reset($question);
			//pre_print($question);

			echo '<h1>Добавяне на Въпрос</h1>';

			$group_options = '';
			//pre_print($QUESTIONS_GROUPS);
			foreach($QUESTIONS_GROUPS as $k => $v)
			{
				$group_options .= '<option value="'.$k.'">'.$v.'</option>'.PHP_EOL;
			}

			//pre_print($QUESTIONS_TYPES);
			$question_title = '<textarea name="question_title" rows="2" cols="100">'.$question['question_title'].'</textarea>';

			database_open();
			$tests = get_all_tests('1=1 ORDER BY test_name ASC');
			//pre_print($tests);
			$tests_dropdown = ReturnDropDown($tests, max(array_keys($tests)));
			database_close();

			$tests_dropdown = '<select name="test_id">'.$tests_dropdown.'</select>';
?>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<form id="edit_form" method="post">
				<?php
					echo '<a class="fake_tab" href="'.link_to(array(CONTROLLER => 'question', ACTION => 'index')).'"><img src="'.DIR_IMAGES.'ui/preferences_contact_list.png" /> Списък</a>';
					render_table(
						array(
							array(
								'&nbsp;-',
								//'<input type="text" name="question_title" value="'.$question['question_title'].'" style="width:600px;" />',
								$question_title,
								'<span id="question_group_preview" style="padding:0px 7px; margin-left:1px; border:1px solid black;">-</span> <select name="question_group" id="question_group">'.$group_options.'</select>',
								'&nbsp;---',
								'<select name="is_active"><option value="1">Активен</option><option value="0">Неактивен</option></select>',
								'<select name="is_correct"><option value="1">Да</option><option value="0">Не</option></select>',
								$tests_dropdown //$default_test_id
							),
						),
						array(
							array('text' => 'ID', 'attr' => 'width="10"'),
							'Въпрос',
							array('text' => 'Група', 'attr' => 'width="10"'),
							array('text' => 'Тип', 'attr' => 'width="10"'),
							array('text' => 'Статус', 'attr' => 'width="10"'),
							array('text' => 'Верен', 'attr' => 'width="10"'),
							'Тест'
						)
						//,'border="1" width="100%"'
					);

					echo '<a href="'.link_to(array(CONTROLLER => 'question')).'"><button><img src="'.DIR_IMAGES.'ui/0.png"></button></a>';
					echo '<a href="#" onclick="document.getElementById(\'edit_form\').reset();"><button><img src="'.DIR_IMAGES.'ui/r.png" height="22"></button></a>';
					echo '<a href="#" onclick="document.getElementById(\'edit_form\').submit();"><button type="submit"><img src="'.DIR_IMAGES.'ui/1.png"></button></a>';
				?>
			</form>
		</div>
	</div>
</div>
<script>
	$('#question_group').change(function(){
		$('#question_group_preview').css('background-color', $("#question_group option:selected").text());
		$('#question_group_preview').text($(this).val());
	});
</script>
<?php
		}
	}
	else
	{
		page_not_access();
	}
?>