<?php Global $is_admin_user, $questions, $tests_dropdown, $user_id; ?>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<form method="GET" id="search_question_form">
				<div class="form-group">
					<div class="row">

						<input type="hidden" name="<?php echo CONTROLLER; ?>" value="<?php echo $source[CONTROLLER]; ?>" />
						<input type="hidden" name="<?php echo ACTION; ?>" value="<?php echo $source[ACTION]; ?>" />

						<!--
						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
							<label for="group_number">
								Група
							</label>

							<select name="group_number" class="form-control">
								<option value="null" <?php if(!empty($_GET['group_number']) && $_GET['group_number'] == null || empty($_GET['group_number'])) echo 'selected'; ?>> - Без филтър - </option>
								<option value="0" <?php if(!empty($_GET['group_number']) && $_GET['group_number'] == 0) echo 'selected'; ?>>Онлайн</option>
								<option value="1" <?php if(!empty($_GET['group_number']) && $_GET['group_number'] == 1) echo 'selected'; ?>>Група 1</option>
								<option value="2" <?php if(!empty($_GET['group_number']) && $_GET['group_number'] == 2) echo 'selected'; ?>>Група 2</option>
							</select>
						</div>
						-->
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
							<label for="group_number">
								Дума
							</label>
							<input name="word" class="form-control" value="<?php echo (isset($source['word']) ? $source['word'] : ''); ?>" />
						</div>
						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
							<label for="course_id">
								Тест
							</label>

							<select name="test_id" class="form-control">
								<option value="-1">-- Без значение --</option>
								<?php
									echo $tests_dropdown;
								?>
							</select>
						</div>
						<input type="hidden" name="show_answers" id="show_answers" value="0" />
						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
							<button type="submit" class="btn btn-large btn-success" style="margin-top:30px" id="submit">
								<span class="glyphicon glyphicon-filter"></span> Филтрирай
							</button>
							<button type="submit" class="btn btn-large btn-danger" style="margin-top:30px;" id="show_answers_btn">
								<span title="Homeworks" class="glyphicon glyphicon-check"></span> Провери
							</button>
						</div>
					</div>
				</div>
			</form>
		</div>
<?php
	if($is_admin_user)
	{
		//pre_print($questions);
		//echo '<div class="container">';
		//echo '<div class="row">';
		echo '<h1>Управление на Въпроси</h1>';
		echo '<a class="fake_tab'.(isset($source[VIEW]) && $source[VIEW] == 'all' ? ' fake_tab_active' : '').'" href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element(VIEW, $_SERVER['QUERY_STRING']).'&' : '').VIEW.'=all"><img src="'.DIR_IMAGES.'ui/preferences_contact_list.png" /> Всички</a>';
		echo '<a class="fake_tab'.(isset($source[VIEW]) && $source[VIEW] == 'wrong' ? ' fake_tab_active' : '').'" href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element(VIEW, $_SERVER['QUERY_STRING']).'&' : '').VIEW.'=wrong"><img src="'.DIR_IMAGES.'ui/0.png" height="16" /> Грешни</a>';
		echo '<a class="fake_tab'.(isset($source[VIEW]) && $source[VIEW] == 'reported' ? ' fake_tab_active' : '').'" href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element(VIEW, $_SERVER['QUERY_STRING']).'&' : '').VIEW.'=reported"><img src="'.DIR_IMAGES.'ui/icon_mail.gif" height="16" /> Докладвани</a>';

		if(isset($source['test_id']) && $source['test_id'] >= 0)
		{
			echo '<a class="fake_tab fake_tab_remove" title="Кликнете тук за да премахнете филтъра!" href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element('test_id', $_SERVER['QUERY_STRING']).'&' : '').'">Филтър: test_id = '.$source['test_id'].'</a>';
		}

		if(isset($_REQUEST['word']) && $REQUEST['word'] != '')
		{
			echo '<a class="fake_tab fake_tab_remove" title="Кликнете тук за да премахнете филтъра!" href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element('word', $_SERVER['QUERY_STRING']).'&' : '').'">Филтър: word = %'.$_REQUEST['word'].'%</a>';
		}

		$columns = array(
			array('text' => '<a href="'.link_to(array(CONTROLLER => 'question', ACTION => 'add')).'" style="margin-left:2px;"><button style="margin-bottom:2px;"><img src="'.DIR_IMAGES.'ui/add_page_16.gif" alt="" /> New</button></a>', 'key' => 'functions', 'attr' => 'width="71"', 'style' => 'padding:0;'),
			array('text' => 'ID', 'field' => 'question_id'),
			array('text' => 'Тест', 'field' => 'test_questions.test_id'),
			array('text' => 'Въпрос', 'field' => 'question_title'),
			array('text' => 'Тип', 'field' => 'question_type'),
			array('text' => 'Гр', 'field' => 'question_group', 'attr' => 'title="Група"'),
			array('text' => 'Ст', 'field' => 'is_active', 'attr' => 'title="Статус"'),
			array('text' => 'ОК', 'field' => 'is_correct', 'attr' => 'title="Коректен"'),
		);
		render_table($questions, $columns);
		//echo '</div></div>';
	}
	else
	{
		page_not_access();
	}
?>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('.delete_btn').click(function(){
			if(confirm("Сигурен ли сте?!") != true)
			{
				return false;
			}
		});

		$('#show_answers_btn').click(function(){
			$('#show_answers').val('1');
		});
	});
</script>