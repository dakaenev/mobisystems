<?php
	Global $is_admin_user, $source, $question, $message, $QUESTIONS_GROUPS, $QUESTIONS_TYPES, $default_test_id, $user_id;
//echo serialize(array(0, 15,10,5));
?>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
<?php
	if($is_admin_user)
	{
		show_all_messages();
		if(has_error_messages())
		{
			clear_all_messages();
		}
		else
		{
			//$question = reset($question);
			//pre_print($question);

			if(isset($source[ID]))
			{
				$page_title = 'Редакция на Въпрос #'.$source[ID];
			}
			else
			{
				$page_title = 'Добавяне на Въпрос';
			}

			echo '<h1>'.$page_title.'</h1>';

			$group_options = '';
			//pre_print($QUESTIONS_GROUPS);
			foreach($QUESTIONS_GROUPS as $k => $v)
			{
				$group_options .= '<option value="'.$k.'"'.($k == $question['question_group'] ? ' selected="selected"' : '').'>'.$v.'</option>'.PHP_EOL;
			}

			//pre_print($QUESTIONS_TYPES);
			if(substr($question['question_title'], 0, 4) == 'img=')
			{
				$question_title = '<a href="'.DIR_IMAGES.substr($question['question_title'],4).'" target="_blank"><img src="'.DIR_IMAGES.substr($question['question_title'],4).'" style="border:1px solid black; height:50px;"></a>';
				$is_image = true;
			}
			else
			{
				$question_title = '<textarea name="question_title" rows="2" cols="100">'.$question['question_title'].'</textarea>';
				$is_image = false;
			}

			database_open();
			$tests = get_all_tests('1=1 ORDER BY test_name ASC');
			//pre_print($tests);
			if(isset($question['test_id']))
			{
				$default_test_id = $question['test_id'];
			}
			else
			{
				$default_test_id = max(array_keys($tests));
			}
			$tests_dropdown = ReturnDropDown($tests, $default_test_id);
			database_close();

			$tests_dropdown = '<select name="test_id">'.$tests_dropdown.'</select>';
?>
			<form id="edit_form" method="post">
				<input type="hidden" name="question_id" value="<?php echo $source[ID]; ?>" />
				<?php
					echo '<a class="fake_tab" href="'.link_to(array(CONTROLLER => 'question', ACTION => 'index')).'"><img src="'.DIR_IMAGES.'ui/preferences_contact_list.png" /> Списък</a>';
					echo '<a class="fake_tab" href="'.link_to(array(CONTROLLER => 'question', ACTION => 'add')).'"><img src="'.DIR_IMAGES.'ui/add_page_16.gif" /> Добави нов</a>';
					render_table(
						array(
							array(
								$question['question_id'],
								//'<input type="text" name="question_title" value="'.$question['question_title'].'" style="width:600px;" />',
								stripslashes($question_title),
								'<span id="question_group_preview" style="padding:0px 7px; margin-left:1px; border:1px solid black; background-color:'.$QUESTIONS_GROUPS[$question['question_group']].';">'.$question['question_group'].'</span><select name="question_group" id="question_group">'.$group_options.'</select>',
								(isset($QUESTIONS_TYPES[$question['question_type']]) ? $QUESTIONS_TYPES[$question['question_type']] : '---'),
								'<select name="is_active"><option value="1"'.($question['is_active'] == 1 ? ' selected="selected"' : '').'>Активен</option><option value="0"'.($question['is_active'] == 0 ? ' selected="selected"' : '').'>Неактивен</option></select>',
								'<select name="is_correct"><option value="1"'.($question['is_correct'] == 1 ? ' selected="selected"' : '').'>Да</option><option value="0"'.($question['is_correct'] == 0 ? ' selected="selected"' : '').'>Не</option></select>',
								$tests_dropdown
							),
						),
						array(
							array('text' => 'ID', 'attr' => 'width="10"'),
							'Въпрос',
							array('text' => 'Група', 'attr' => 'width="120"'),
							array('text' => 'Тип', 'attr' => 'width="10"'),
							array('text' => 'Статус', 'attr' => 'width="10"'),
							array('text' => 'Верен', 'attr' => 'width="10"'),
							'Тест'
						)
						//,'border="1" width="100%"'
					);

					database_open();
					$answer_index = 0;
					$answers = get_all_answers('question_id = '.$source[ID]);
					foreach($answers as $k => $v)
					{
						$answers[$k]['answer_id'] = '<button class="del_answer" title="'.$v['answer_id'].'"><img src="'.DIR_IMAGES.'ui/0.png" width="16"></button>';
						$answers[$k]['answer_title'] = '<input type="text" name="answers['.$answer_index.'][title]" value="'.htmlspecialchars(stripslashes($v['answer_title'])).'" size="113" />';
						$answers[$k]['is_correct'] = '<input type="radio" name="correct_answer_index" value="'.$answer_index.'" '.((int) $v['is_correct'] == 1 ? ' checked="checked"' : '').'/>'; //'<img src="images/ui/'.(int) $v['is_correct'].'.png" alt="'.$v['is_correct'].'" />';
						$answers[$k]['answer_value'] = '<input type="number" name="answers['.$answer_index.'][value]" min="0" max="10" value="'.$v['answer_value'].'">';
						$answers[$k]['answer_type'] = $QUESTIONS_TYPES[$v['answer_type']];

						$answer_index++;
					}
					database_close();
					render_table(
						$answers,
						array(
							array('text' => '<button id="add_answer">+</button>', 'attr' => ' style="text-align:center;"'),
							'Въпрос',
							'Отговор',
							array('text' => 'Точки', 'key' => 'answer_value'),
							array('text' => 'Верен', 'key' => 'is_correct'),
							'Тип'
						),
						'border="1" id="answers_table"',
						'question_edit'
					);
				?>
			</form>

			<br><br>
			<a href="<?php echo link_to(array(CONTROLLER => 'question')); ?>"><button><img src="<?php echo DIR_IMAGES; ?>ui/0.png"></button></a>
			<a href="#" onclick="document.getElementById('edit_form').reset();"><button type="reset"><img src="<?php echo DIR_IMAGES; ?>ui/r.png" height="22"></button></a>
			<a href="#" onclick="check_submit_form();"><button><img src="<?php echo DIR_IMAGES; ?>ui/1.png"></button></a>

			<script>
				var answer_index = <?php echo $answer_index; ?>;
				$('#question_group').change(function(){
					$('#question_group_preview').css('background-color', $("#question_group option:selected").text());
					$('#question_group_preview').text($(this).val());
				});

				$(document).ready(function(){
					$('#add_answer').click(function(){

						var code = 	'<tr><td align="center"><button class="del_answer"><img src="<?php echo DIR_IMAGES; ?>ui/0.png" width="16"></button></td><td align="center">---</td><td><input type="text" name="answers['+answer_index+'][title]" value="" placeholder="Въведете текст на отговора" /></td><td><input type="number" name="answers['+answer_index+'][value]" min="0" max="10" value="0"></td><td align="center"><input type="radio" name="correct_answer_index" value="'+answer_index+'" /></td><td><?php echo $QUESTIONS_TYPES[$question['question_type']]; ?></td></tr>';

						if($('#answers_table .no_records_found').length == 1)
						{
							$('#answers_table tbody').html(code);
						}
						else
						{
							$('#answers_table tr:last').before(code);
						}

						answer_index++;
						return false;
					});

					$('#answers_table').on('click', '.del_answer', function() {
						$(this).parent().parent().remove();
					});
				});

				function check_submit_form()
				{
					var rowCount = $('#answers_table tbody tr').length - $('#answers_table .no_records_found').length;

					if(rowCount > 0 && $("input[name='correct_answer_index']:checked").val() == undefined )
					{
						alert('Изберете поне 1 верен отговор!');
						return false;
					}
					else
					{
						document.getElementById('edit_form').submit();
					}
				}
			</script>
<?php
		}
	}
	else
	{
		page_not_access();
	}
?>
		</div>
	</div>
</div>