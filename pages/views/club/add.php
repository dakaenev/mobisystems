<?php
	Global $source, $is_admin_user, $is_super_admin, $vip_prices, $payment_methods, $default, $payment_id, $course_exists;

	$search_params = remove_url_elements(array(CONTROLLER, ACTION, ID), $_SERVER['QUERY_STRING']);

	if($is_admin_user)
	{
		database_open();
?>
<div class="container">
	<div class="row">
		<?php
		$is_editable = !has_error_messages();
		$form_title = ($payment_id > 0 ? 'Редактиране на запис #'.$payment_id : 'Добавяне на нов запис');
		$form_title = '<a href="'.header_link(array(CONTROLLER => 'club', ACTION => 'add')).'"><img src="'.DIR_IMAGES.'ui/add_page_16.gif" alt="Add" /></a> '.$form_title.'';

		if(have_messages())
		{
			show_all_messages();
			clear_all_messages();
		}

		if($is_editable)
		{
			$period_radio_inputs = '';
			foreach($vip_prices as $month => $price)
			{
				$month = abs($month);
				$period_radio_inputs .= '<label class="col-sm-'.floor(12 / count($vip_prices)).'" style="padding:5px; margin:0; border:1px dotted lightblue;">';
				$period_radio_inputs .= '<input type="radio" name="period" class="period_radio" data-money="'.($month * $price).'" value="'.$month.'"'.($default['period'] == $month ? ' checked="checked"' : '').'>'.$month.' м.</label> ';
			}

			// Prepare <option> tags for Course Select field
			$courses = get_all_courses('1=1 AND (start_date > '.get_current_period(false).' > end_date ) ORDER BY course_name ASC', 'course_id, start_date, course_name');

			if($default['course_id'] > 0 && !isset($courses[$default['course_id']]) && $course_exists)
			{
				$courses = get_all_courses('course_id = '.$default['course_id'], 'course_id, start_date, course_name');
				$is_editable = false;
			}
			else
			{
				$is_editable = true;
			}

			$courses[-1] = array('course_id' => -1, 'course_name' => ' Добави сега ');

			$courses_dropdown = ReturnDropDown($courses, $default['course_id'], false);
			$courses_dropdown = '<option value="0"> - Изберете курс - </option>'.$courses_dropdown;

			// Use this for datalist functionality
			$last = to_assoc_array(exec_query('SELECT DISTINCT user_id FROM user_logs ORDER BY log_id DESC LIMIT 350'));
			$month_time = strtotime('-1 month');
			$phone_users_query = 'SELECT CONCAT(first_name, " ", last_name, "|", user_id) as full_name FROM users WHERE (first_name != "" AND last_name != "" AND aim != "") ORDER BY first_name, last_name ASC'; //  [1574]
			$named_users_query = 'SELECT CONCAT(first_name, " ", last_name, "|", user_id) as full_name FROM users WHERE (first_name != "" AND last_name != "") ORDER BY first_name, last_name ASC'; //  [1938]
			  $all_users_query = 'SELECT CONCAT(first_name, " ", last_name, "|", user_id) as full_name FROM users ORDER BY first_name, last_name ASC'; //[2520]
			 $last_users_query = 'SELECT CONCAT(first_name, " ", last_name, "|", user_id) as full_name FROM users WHERE (first_name != "" AND last_name != "") AND user_id IN ('.implode(',', array_column($last,'user_id')).') ORDER BY first_name, last_name ASC'; //  [1574]

			$users = to_assoc_array(exec_query($named_users_query));
			$users = array_column($users, 'full_name');
			//pre_print($users,1);

			foreach($users as $key => $value)
			{
				$value = explode('|', $value);
				$users[$key] = '<option value="'.$value[0].' #'.$value[1].'"></option>';
			}
			?>
			<br />
			<div class="panel panel-info">
				<div class="panel-heading"><span style="font-style: italic; font-weight: 600;"><?php echo $form_title; ?></span></div>
				<div class="panel-body">
					<form action="<?php echo header_link(array(CONTROLLER => 'club', ACTION => 'add', ID => $payment_id)); ?>" class="form-horizontal" role="form" novalidate="" method="post">
					  <div class="form-group col-lg-6">
						<label for="full_name" class="col-sm-2 control-label">Курсист</label>
						<div class="col-sm-7">
						  <input type="text" name="full_name" id="full_name" class="form-control" value="<?php echo $default['full_name']; ?>" placeholder="Име на кирилица"<?php echo ($payment_id > 0 ? ' readonly="readony"' : ''); ?> list="users_list" />
						  <datalist id="users_list"><?php echo implode('', $users); ?></datalist>
						</div>
						<div class="col-sm-3">
						  <input type="number" name="user_id" id="user_id" class="form-control" value="<?php echo $default['user_id']; ?>" placeholder="User ID"<?php echo ($payment_id > 0 ? ' readonly="readony"' : ''); ?> />
						</div>
					  </div>
					  <div class="form-group col-lg-6">
						<label for="course_id" class="col-sm-2 control-label">Курс</label>
						<div class="col-sm-10">
						  <select name="course_id" id="course_id" class="form-control" required="required">
							<?php echo $courses_dropdown; ?>
						  </select>
						</div>
					  </div>
					  <div class="form-group col-lg-6"><label for="inputPassword3" class="col-sm-2 control-label">Период</label>
						  <div class="col-sm-10"><?php echo $period_radio_inputs; ?></div>
					  </div>
					  <div class="form-group col-lg-6">
						<label for="amount" class="col-sm-2 control-label">Сума</label>
						<div class="col-sm-5">
						  <input type="number" class="form-control" id="amount" name="amount" value="<?php echo $default['amount']; ?>" placeholder="Платена сума">
						</div>
						<div class="col-sm-5">
							<select class="form-control" name="method" id="method" required="required">
							<?php
								$methods_dropdown = ReturnDropDown($payment_methods, $default['method']);
								$methods_dropdown = '<option value="-1"> - Плащане - </option>'.$methods_dropdown;
								echo $methods_dropdown;
							?>
							</select>
						</div>
					  </div>
					<div class="form-group col-lg-6">
						<label for="start_date" class="col-sm-2 control-label">Начало</label>
						<div class="col-sm-10">
						  <input type="date" class="form-control" id="start_date" name="start_date" value="<?php echo $default['start_date']; ?>">
						</div>
					  </div>
					<div class="form-group col-lg-6">
						<label for="end_date" class="col-sm-2 control-label">Край</label>
						<div class="col-sm-10">
						  <input type="date" class="form-control" id="end_date" name="end_date"value="<?php echo $default['end_date']; ?>">
						</div>
					  </div>
					  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="center">
						<?php
						if(true) // $is_editable
						{
							echo '<a href="'.header_link(array(CONTROLLER => 'club', ACTION => 'index')).($search_params != '' ? '&'.$search_params : '').'"><button type="button"><img src="'.DIR_IMAGES.'ui/0.png"></button></a>';
							echo '<button type="reset"><img src="'.DIR_IMAGES.'ui/r.png" height="22"></button>';
							echo '<button type="submit"><img src="'.DIR_IMAGES.'ui/1.png"></button></a>';
						}
						else
						{
							show_message('Не можете да променяте изпит свързан с приключил курс!');
						}
						?>
						</div>
					</div>
					</form>
				</div>
			</div>
			<?php
		}
		?>
	</div>
</div>
<script>
	function applyPeriodDates(){
		var  period_val = $('input[name=period]:checked').val();
		var start_value = $('#start_date').val();
		var  start_date = new Date(start_value);
		var    end_date = new Date(start_date.getTime() + (60*60*24*31*period_val*1000));

		$('#end_date').val( end_date.toISOString().substr(0, 10));
	}

	$(document).ready(function(){

		$('.period_radio').change(function() {
			if('' == $('#amount').attr('value')){
				$('#amount').val($(this).data('money'));
			}
			applyPeriodDates();
		});

		$('#full_name').keyup(function(){
			var thisVal = $('#full_name').val();
			$('#full_name').val( thisVal.replace('№', '#') );
		});

		$('#full_name').change(function(){
			var splitVal = $(this).val().split(' #');

			$('#full_name').val(splitVal[0]);
			$('#user_id').val(splitVal[1]);
		});

		$('#start_date').change(applyPeriodDates);

		$('#course_id').change(function(){
			if($('#course_id :selected').val() < 0){
				var new_val = prompt('Добавете ИД на курс');

				$('#course_id').append('<option value="' + new_val + '"> Добавен: Курс #' + new_val + ' </option>');
				$('#course_id').val(new_val);
			}else{
				$('#start_date').val($('#course_id :selected').data('start_date'));
				applyPeriodDates();
			}
		});
	});
</script>
<?php }	else { page_not_access(); } ?>