<?php Global $user, $course, $url_ok, $vip_info, $vip_columns, $vip_prices, $course_data; ?>
<?php $panel_class = 'primary'; ?>
<div class="container" style="margin-top: 40px;">
    <div class="row">
        <div class="col-md-12">
            <div class="title-logo animated fadeInDown animation-delay-5">Plovdiv Web <span>Premium</span></div>
			<?php
				if(have_messages())
				{
					show_all_messages();
					clear_all_messages();
				}
			?>
        </div>
        <div class="col-md-7">
            <h2 class="right-line">Какво представлява?</h2>
            <p>Нашите курсове са и остават безплатни! Въвеждането на премиум достъп е нашия опит да отсеем сериозните курсисти (които наистина искат да научат нещо) от другите, които просто са дошли да видят що е то Програмиране.</p>
            <p>Премиум достъп се изисква за всичко, което е свързано с нашите зали, т.е. присъствено обучение, консултации в залата (в диапазона 13:00-17:00), видео от лекциите, връчване на сертификати на хартия, явяване на финален изпит и други!</p>
            <p>Премиум достъп НЕ Е задължителен, но е силно препоръчителен и занапред все повече функционалности ще бъдат обвързани с него! Закупувайки премиум достъп получавате безплатно членство в Plovdiv Web и събитията достъпни само за членове на клуба.</p>
        </div>
        <div class="col-md-5">
            <h2 class="right-line">Често задавани въпроси</h2>
            <div class="panel-group" id="accordion">
              <div class="panel panel-<?php echo $panel_class; ?>">
                <div class="panel-heading panel-heading-link">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed">
                      <i class="fa fa-desktop"></i> Видео
                    </a>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse">
                  <div class="panel-body">
                    <p>Очаквайте скоро</p>
                  </div>
                </div>
              </div>
              <div class="panel panel-<?php echo $panel_class; ?>">
                <div class="panel-heading panel-heading-link">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="collapsed">
						<i class="fa fa-usd"></i> Как мога да платя и има ли отстъпка?
                    </a>
                </div>
                <div id="collapseThree" class="panel-collapse collapse">
                  <div class="panel-body">
                    <p>Можете да платите както чрез системата на ePay, така и с превод към посочения по-долу IBAN. <!-- Също така, ако сте от Пловдив и имате път към офиса можете да платите на място! --></p>
                    <p>Въведената сума е символична, и за момента няма специални намаления, и такива не се предвиждат. Разбира се, плащайки за по-дълъг период месечната Ви вноска е по-ниска и така също икономисвате!</p>
                  </div>
                </div>
              </div>
			  <div class="panel panel-<?php echo $panel_class; ?>">
                <div class="panel-heading panel-heading-link">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                      <i class="fa fa-lightbulb-o"></i> Какво ще получа?
                    </a>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in">
                  <div class="panel-body">
                    <ul type="disc">
						<li>Повече материали</li>
						<li>Повече домашни</li>
						<li>Видео лекции</li>
						<li>Консултации</li>
						<li>Вижте повече информация в таблицата по-долу</li>
					</ul>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div> <!-- row -->
	<div class="row">
		<div class="col-lg-7">
			<span id="payment_div">
			<?php
				$message = '';
				if($course_data == false)
				{
					$message = ($course > 0 ? 'Такъв курс не съществува!' : '');
				}
				else
				{
					if($course_data['end_date'] < date('Y-m-d'))
					{
						$message = 'Този курс вече е приключил!';
					}
				}

				if($message == '')
				{
					echo '';
					echo return_table($vip_info, $vip_columns, ' class="table table-condensed table-striped table-hover table-bordered"');
					echo 'Можете да платите: <!-- на място директно в <a href="/forum/viewtopic.php?f=27&t=221" target="_blank">нашият офис</a>, --><br />през системата на ePay (чрез бутоните по-горе), или чрез банков превод: <br /><b>IBAN:</b> '.WA_BANK_IBAN.', <b>BIC:</b> '.WA_BANK_BIC.', <b>Получател:</b> '.WA_CARD_NAME ;
				}
				else
				{
					echo '<div class="alert alert-danger"><b>'.$message.'</b></div>';
				}
			?>
			</span>
		</div>
		<div class="col-lg-5">
			<table class="table table-condensed table-striped table-hover table-bordered">
				<tr class="warning">
					<th>&nbsp;</th>
					<th>Безплатен</th>
					<th>Премиум</th>
				</tr>
				<tr>
					<td>Лекции (PDF файл)</td>
					<th><img src="<?php echo DIR_IMAGES; ?>ui/1.png" alt="Status" class="center" style="width:16px;" /></th>
					<th><img src="<?php echo DIR_IMAGES; ?>ui/1.png" alt="Status" class="center" style="width:16px;" /></th>
				</tr>
				<tr>
					<td>Материали от упражнения (ZIP файл)</td>
					<th><img src="<?php echo DIR_IMAGES; ?>ui/1.png" alt="Status" class="center" style="width:16px;" /></th>
					<th><img src="<?php echo DIR_IMAGES; ?>ui/1.png" alt="Status" class="center" style="width:16px;" /></th>
				</tr>
				<tr>
					<td>Брой Домашни</td>
					<th><span class="center" title="Броят на домашните зависи от конкретния курс!">&le; 4</span></th>
					<th><span class="center" title="Броят на домашните зависи от конкретния курс!">&ge; 8</span></th>
				</tr>
				<tr>
					<td>Видео Лекции</td>
					<th><img src="<?php echo DIR_IMAGES; ?>ui/0.png" alt="Status" class="center" style="width:16px;" /></th>
					<th><img src="<?php echo DIR_IMAGES; ?>ui/1.png" alt="Status" class="center" style="width:16px;" /></th>
				</tr>
				<tr>
					<td>Сертификат (PDF файл)</td>
					<th><img src="<?php echo DIR_IMAGES; ?>ui/1.png" alt="Status" class="center" style="width:16px;" /></th>
					<th><img src="<?php echo DIR_IMAGES; ?>ui/1.png" alt="Status" class="center" style="width:16px;" /></th>
				</tr>
				<tr>
					<td>Сертификат (на хартия)</td>
					<th><img src="<?php echo DIR_IMAGES; ?>ui/0.png" alt="Status" class="center" style="width:16px;" /></th>
					<th><img src="<?php echo DIR_IMAGES; ?>ui/1.png" alt="Status" class="center" style="width:16px;" /></th>
				</tr>
				<tr>
					<td>Консултации в залата (13:00-17:00)</td>
					<th><img src="<?php echo DIR_IMAGES; ?>ui/0.png" alt="Status" class="center" style="width:16px;" /></th>
					<th><img src="<?php echo DIR_IMAGES; ?>ui/1.png" alt="Status" class="center" style="width:16px;" /></th>
				</tr>
				<tr>
					<td>Такса за явяване на финален изпит</td>
					<th><span class="center red"><?php echo reset($vip_prices); ?> лв.</span></th>
					<th>Безплатно</th>
				</tr>
			</table>
		</div>
	</div>
</div> <!-- container -->
