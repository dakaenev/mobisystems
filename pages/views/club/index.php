<div class="container">
<?php
	Global $source, $payments, $payment_methods, $payment_icons, $query_string, $users, $one_type_only, $is_super_admin;

	$search_params = remove_url_elements(array(CONTROLLER, ACTION), $_SERVER['QUERY_STRING']);

	$payments_array = array();
	$users = array();
	$sum_total = 0;

	$courses_short_names = array(
		'Front End - Level 1' => 'Front End за начинаещи',
		'Front End - Level 2' => 'Front End за напреднали',
		'Java Script - Level 1' => 'Програмиране с JavaScript'
	);

	if(count($payments))
	{
		$payment_methods[-1] = '- ЛИПСВА -';
		foreach($payments as $key => $payment)
		{
			//pre_print($payment,1);
			$tmp = array();

			//$tmp['id'] = $payment['id'];
			//$edit_button = '<a href="'.link_to( array(CONTROLLER => 'club', ACTION => 'add', ID => $payment['id'], 'OPTION_SET_CLASS' => false) ).'" title="Кликнете тук за да редактирате"><button><img src="'.DIR_IMAGES.'ui/application_form_edit.png" alt="" /></button></a>';
			$edit_button = '<a href="'.header_link( array(CONTROLLER => 'club', ACTION => 'add', ID => $payment['id']) ).($search_params != '' ? '&'.$search_params : '').'" class="center" title="Кликнете тук за да редактирате"><img src="'.DIR_IMAGES.'ui/application_form_edit.png" alt="" /></a>';
			  $edit_user = '<a href="'.header_link( array(CONTROLLER => 'user', ACTION => 'edit', ID => $payment['user_id']) ).'" title="Управление на потребителя" style="margin-left:3px; float:right; display:none;" class="user_options"><span class="glyphicon glyphicon-cog"></span></a> ';
			  $view_user = '<a href="'.header_link( array(CONTROLLER => 'user', ACTION => 'view', ID => $payment['user_id']) ).'" title="Преглед на потребителя" style="margin-left:3px; float:right; display:none;" class="user_options"><span class="glyphicon glyphicon-eye-open"></span></a> ';

			$tmp['id'] = '<span class="center">'.$payment['id'].'</span>';
			$tmp['user_id'] = '&nbsp;<a href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element('user_id', $_SERVER['QUERY_STRING']).'&' : '').'user_id='.$payment['user_id'].'" class="show_options">'.('' != $payment['full_name'] ? $payment['full_name'].' <span style="float: right; color: silver; font-style: italic;">'.$payment['user_id'].'</span>'.$edit_user.$view_user : '<span class="red">Неизвестен потребител #'.$payment['user_id'].'</span>').'</a>&nbsp;';
			$tmp['course_id'] = '&nbsp;<a href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element('course_id', $_SERVER['QUERY_STRING']).'&' : '').'course_id='.$payment['course_id'].'">'.('' != $payment['course_name'] ? str_replace($courses_short_names, array_keys($courses_short_names), $payment['course_name']).' <span style="float: right; color: silver; font-style: italic;">'.$payment['course_id'].'</span>' : '<span class="red">Неизвестен курс #'.$payment['course_id'].'</span>').'</a>&nbsp;';
			$tmp['amount'] = '<span class="center'.(0 >= $payment['amount'] ? ' red' : '').'">'.$payment['amount'].'</span>';
			$tmp['method'] = (0 > $payment['method'] ? '<span class="red center"><i class="fa fa-warning"></i>'.$payment_methods[$payment['method']].'</span>' : $payment_icons[$payment['method']].'&nbsp;<span class="hidden-sm hidden-xs">'.$payment_methods[$payment['method']].'</span>');
			$tmp['start_date'] = ($payment['start_date'] > $payment['end_date'] ? '<span class="red">'.$payment['start_date'].'</span>' : $payment['start_date']);
			$tmp['end_date'] = ($payment['start_date'] > $payment['end_date'] ? '<span class="red">'.$payment['end_date'].'</span>' : $payment['end_date']);
			$tmp['operations'] = $edit_button;

			$payments_array[] = $tmp;
			$sum_total += $payment['amount'];
			$users[] = '<option value="'.$payment['full_name'].'">'.$payment['user_id'].'</option>';
		}

		unset($payment_methods[-1]);
		$users = array_unique($users);
		usort($users, function($a,$b){ return mb_strtoupper($a) > mb_strtoupper($b); } );
	}

	database_open();

	$template_data = array(
		'COURSE_ID' => 65,
		'COURSE_NAME' => 'FE1',
		'EXPIRE' => '',
		'EXPIRE_DATE' => '2019-03-11',
		'PAY_NOW' => '<a href="'.header_link(array(CONTROLLER => 'club', ACTION => 'premium', 'course_id' => 65)).'">плати сега</a>'
	);

	$premium_expire = replace_template_data( getTemplateData(5), $template_data);

	$mail_data = array(
		'mail_content' => $premium_expire['template_content'],
		'mail_subject' => $premium_expire['template_about'],
		'created_from' => 0, // This is System Message
	);
	pre_print($premium_expire, 1);
	//insert_mail($mail_data, $mail_recipients);
	$col_size = 12;

	if(!$is_super_admin)
	{
		add_error_message('Преглед на плащанията е разрешена само за Супер Администратор!');
	}
	else
	{
		add_success_message('Общо '.count($payments).' плащания направени от '.count($users).' различни курсиста на стойност '.round($sum_total,2).' лв.!<br /><a href="https://www.epay.bg/v3main/report/mvmnts" target="_blank">Провери отново в ePay <span class="glyphicon glyphicon-new-window"></span></a>');
	}

	// Courses Drop Down Menu
	$courses_optgroups = array();
	$courses_ancestors = get_all_ancestors();

	$courses_array = get_all_courses('descendant_of > 0 AND end_date <= "'.date('Y-m-d', strtotime('-1 week')).'" ORDER BY descendant_of ASC, course_name ASC');

	foreach($courses_array as $course)
	{
		$courses_optgroups[$course['descendant_of']][] = '<option value="'.$course['course_id'].'"'.($source['course_id'] == $course['course_id'] ? ' selected="selected"' : '').'>'.$course['course_name'].' ['.$course['course_id'].']</option>';
	}

	foreach($courses_optgroups as $ancestor_id => $options)
	{
		$courses_optgroups[$ancestor_id] = '<optgroup id="ancestor_'.$ancestor_id.'" label="'.$courses_ancestors[$ancestor_id]['ancestor_name'].'">'.implode($options).'</optgroup>';
	}

	$courses_optgroups = implode(PHP_EOL, $courses_optgroups);

	if(have_messages())
	{
		show_all_messages();
		clear_all_messages();
	}

	if(SHOW_DEBUG_INFO){
?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel-group" id="accordion1" style="margin-top:20px;">
				<div class="panel panel-<?php echo (count($payments) != 0 ? 'info' : 'danger'); ?>">
					<div class="panel-heading panel-plus-link">
							<a class="accordion-toggle<?php echo (count($payments) != 0 ? ' collapsed' : ''); ?>" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne1" aria-expanded="<?php echo (count($payments) != 0 ? 'false' : 'true'); ?>">
								<b><h5 style="display:inline;">Управление на Плащания</h5></b>
							</a>
					</div>
					<div id="collapseOne1" class="panel-collapse collapse<?php echo (count($payments) != 0 ? '' : ' in'); ?>" aria-expanded="<?php echo (count($payments) != 0 ? 'false' : 'true'); ?>">
						<div class="panel-body">
							<?php echo SQL_DEBUG($query_string); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php } ?>
	<div class="row">
		<div class="col-lg-12">
			<form method="GET" action="<?php echo header_link(array(CONTROLLER => 'club', ACTION => 'index')); ?>">
				<div class="form-group">
					<div class="row">

						<input type="hidden" name="<?php echo CONTROLLER; ?>" value="club" />
						<input type="hidden" name="<?php echo ACTION; ?>" value="index" />

						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
							<label for="ancestor_id">
								Курс Прародител<!-- (Родоначалник) -->
							</label>

							<select name="ancestor_id" id="ancestor_id" class="form-control">
								<option value="-1">- Без Филтър -</option>
								<?php foreach($courses_ancestors as $v){
									echo '<option value="'.$v['ancestor_id'].'"'.($source['ancestor_id'] == $v['ancestor_id'] ? ' selected="selected"' : '').'>'.$v['ancestor_id'].'. '.$v['ancestor_name'].'</option>';
								} ?>
							</select>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
							<label for="course_id">
								Курс
							</label>

							<select name="course_id" id="course_id" class="form-control">
								<option value="0">- Без Филтър -</option>
								<?php echo $courses_optgroups; ?>
							</select>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
							<label for="method">
								Източник
							</label>

							<select name="method" id="method" class="form-control">
							<?php
								$methods_dropdown = ReturnDropDown($payment_methods, $source['method']);
								$methods_dropdown = '<option value="-1">- Без филтър -</option>'.$methods_dropdown;
								echo $methods_dropdown;
							?>
							</select>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
							<label for="full_name">
								Курсист
							</label>

							<input type="hidden" name="not_pay" id="not_pay" value="2" />
							<input type="text" name="full_name" id="full_name" class="form-control" value="<?php echo (isset($_GET['full_name']) ? $_GET['full_name'] : ''); ?>" placeholder="Име на кирилица" list="users_list" />
							<datalist id="users_list"><?php echo implode('', $users); ?></datalist>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
							<button type="submit" style="margin-top:29px; border-top-right-radius: 0; border-bottom-right-radius: 0; padding: 5px 12px;" class="pull-left btn btn-success" id="submit_button"><span class="glyphicon glyphicon-filter"></span> Филтрирай</button>
							<a href="<?php echo header_link(array(CONTROLLER => 'club', ACTION => 'export')); ?>"><button type="button" class="pull-right btn btn-primary" style="margin-top:29px;">Export <span class="glyphicon glyphicon-export"></span></button></a>
							<button type="button" style="margin-top:29px;" class="pull-left" id="not_pay_btn"><span class="glyphicon glyphicon-certificate wa-certificate-icon-<?php echo abs($source['not_pay']-1); ?>"></span></button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-<?php echo ($col_size+1); ?> col-lg-<?php echo $col_size; ?>">
<?php
	if($is_admin_user)
	{
		//pre_print($questions);
		//echo '<a class="fake_tab'.(isset($source[VIEW]) && $source[VIEW] == 'all' ? ' fake_tab_active' : '').'" href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element(VIEW, $_SERVER['QUERY_STRING']).'&' : '').VIEW.'=all"><img src="'.DIR_IMAGES.'ui/preferences_contact_list.png" /> Всички</a>';

		$columns = array(
			//array('text' => '<a href="'.link_to(array(CONTROLLER => 'question', ACTION => 'add')).'"><button><img src="'.DIR_IMAGES.'ui/add_page_16.gif" alt="" /></button></a>', 'key' => 'functions'),
			//array('text' => 'ID', 'field' => 'question_id'),
			array('text' => '<a href="'.header_link(array(CONTROLLER => 'club', ACTION => 'add')).($search_params != '' ? '&'.$search_params : '').'"><button><img src="'.DIR_IMAGES.'ui/add_page_16.gif" alt="" /></button></a>', 'key' => 'id', 'attr' => 'width="10"'),
			array('text' => '&nbsp;Курсист', 'field' => 'full_name', 'key' => 'user_id', 'attr' => 'width="423"'),
			array('text' => '&nbsp;Курс', 'field' => 'c.course_name', 'key' => 'course_id', 'attr' => 'width="226"'),
			array('text' => '&nbsp;<span class="glyphicon glyphicon-usd"></span>&nbsp;', 'field' => 'up.amount', 'key' => 'amount', 'attr' => 'width="25" title="Платена сума"'),
			array('text' => '&nbsp;Източник &nbsp;', 'field' => 'up.method', 'key' => 'method', 'attr' => 'width="170"'),
			array('text' => '&nbsp;Начална', 'field' => 'up.start_date', 'key' => 'start_date'),
			array('text' => '&nbsp;Крайна', 'field' => 'up.end_date', 'key' => 'end_date'),
			array('text' => '&nbsp;', 'field' => 'up.id', 'key' => 'operations', 'attr' => 'width="25"')
		);

		$filter_columns = array('course_id', 'user_id');

		foreach($filter_columns as $filter)
		{
			if(isset($source[$filter]) && $source[$filter] != '')
			{
				echo '<a class="fake_tab fake_tab_remove" title="Кликнете тук за да премахнете филтъра!" href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element($filter, $_SERVER['QUERY_STRING']).'&' : '').'">Филтър: '.$filter.' = '.(is_array($source[$filter]) ? implode(',', $source[$filter]) : $source[$filter]).'</a>';
			}
		}

		echo '<div class="table-responsive">'.return_table($payments_array, $columns, 'class="table-w100 table-striped table-hover table-bordered"').'</div>';
		echo (5 == $col_size ? '</div><div class="col-xs-12 col-sm-12 col-md-6 col-lg-7"><iframe name="result_frame" id="result_frame" width="100%" height="600" src="" style="border: 1px solid black; zoom: 0.75; -moz-transform: scale(0.75); -moz-transform-origin: 0 0;">Your browser not understand from iframe</iframe>' : '');
	}
	else
	{
		page_not_access();
	}
?>
		</div>
		<div class="col-lg-12">
			<div id="chart_container" style="border:3px double #2CB3DC; margin-top:10px; "></div>
		</div>
	</div>
</div>

<script>

	$(document).ready(function(){

		$('#ancestor_id').change(function(){
			var selected_ancestor = $(this).val();
			if(selected_ancestor == -1)
			{
				$('#course_id optgroup').prop('disabled', false);
			}
			else
			{
				$('#course_id').val(0);
				$('#course_id optgroup').prop('disabled', true);
				$('#ancestor_'+selected_ancestor).prop('disabled', false);
			}
		});

		$('#not_pay_btn').click(function(){
			$('#not_pay').val(<?php echo abs($source['not_pay']-1); ?>);
			$('#submit_button').trigger('click');
		});

		$('.show_options').mouseover(function(){
			$('.user_options').hide();
			$(this).nextAll('.user_options').show();
		});

		// Set up the chart
		var chart = new Highcharts.Chart({
			chart: {
				renderTo: 'chart_container',
				type: 'column',
				height: 550,
				options3d: {
					enabled: true,
					alpha: 15,
					beta: 15,
					depth: 100,
					viewDistance: 25
				}
			},
			credits: {
				enabled: false,
				href: "Plovdiv Web",
				text: "Plovdiv Web"
			},
			exporting: {
				filename: "chart_payments_<?php echo date('Y-m-d'); ?>"
			},
			subtitle: {
				text: "Курс №<?php echo $course['course_id']; ?> <?php echo $course['course_name']; ?> (<?php echo $middle_date['semester']; ?>)"
			},
			title: {
				text: "Диаграма с присъствия"
			},
			xAxis: {
				min: 0,
				max: <?php echo count($visibility_array)-1; ?>, // 14
				categories: [<?php echo implode(', ', $chart_dates); ?>]
			},
			yAxis: {
				min: 0,
				title: {
				  text: ""
				}
			},
			plotOptions: {
				column: {
					depth: 25
				}
			},
			series: [{
				name: "В залата",
				data: [<?php echo implode(', ', array_column($visibility_array, 'users_count')); ?>]
			}]
		});
	});
</script>