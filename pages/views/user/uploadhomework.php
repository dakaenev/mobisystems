<?php
Global $upload, $data, $homework, $is_admin_user;
database_open();

if(!empty($data)) {
	$now = strtotime('now');

	$ip_check = ( ($homework['for_office_only'] == 1 && is_office_ip() ) || $homework['for_office_only'] == 0);

    if($is_admin_user || ($homework['start_date'] <= $now && $now <= $homework['end_date'] && $ip_check))
	{
    ?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row wppb-user-forms">

                        <?php
                            if(!empty($upload) && $upload != false) {

                                $message; $type;

                                switch($upload) {
                                    case 'Invalid parameters.':
                                        $message = 'Невалиден файл.';
                                        $type = 0;
                                    break;

                                    case 'No file sent.':
                                        $message = 'Няма избран файл';
                                        $type = 0;
                                    break;

                                    case 'Exceeded filesize limit.':
                                        $message = 'Файлът надвишава допустимия лимит.';
                                        $type = 0;
                                    break;

                                    case 'Unknown errors.':
                                        $message = 'Получи се грешка при качването на файла.';
                                        $type = 0;
                                    break;

                                    case 'Invalid file format.':
                                        $message = 'Невалиден файлов формат. Архивирайте си домашната и качете Архива - rar, zip, tar.gz ';
                                        $type = 0;
                                    break;

                                    case 'Failed to move uploaded file.':
                                        $message = 'Неуспешно записване на качения файл.';
                                        $type = 0;
                                    break;

                                    case 'Could not save homework to databse.':
                                        $message = 'Домашната не беше записана в базата данни.';
                                        $type = 0;
                                    break;

                                    case 'File is uploaded successfully.':
                                        $message = 'Домашната е качена успешно. <br />Може да се върнете <a href="' . header_link([CONTROLLER => 'user', ACTION => 'view']) . '">към профила си</a> <br />';
                                        $type = 1;
                                    break;

                                    default:
                                        $message = 'Няма избран файл';
                                        $type = 0;
                                    break;
                                }

                                if(!empty($message) && isset($type)) {
                                    if($type == 0) {
                                        ?>
                                        <div class="alert alert-border alert-info" style="background-color: #f6f6f6">
                                            <?php echo $message; ?>
                                        </div>
                                <?php
                                    } elseif($type == 1) {
                                ?>
                                        <div class="alert alert-border alert-success" style="background-color: #f6f6f6">
                                            <?php echo $message; ?>
                                        </div>
                        <?php
                                    }

                                }
                            }

							//echo '<a href="/forum/viewtopic.php?f=19&t=7" target="_blank" style="float:right;">Как се качва / замества домашна?</a><br />';
                            //echo '<a href="/forum/viewtopic.php?f=19&p=705" target="_blank" style="float:right;">Архивиране на финален изпит !</a><br />';

                            if($data['allow'] === false) {
                        ?>
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="alert alert-border" style="margin-top:42px; background-color:#f6f6f6;">
                                            <span style="font-family: 'Open Sans' !important; font-size: 13px !important; font-weight: 600 !important; color: #000000 !important;"><img src="<?php bloginfo('template_url'); ?>/img/caution_icon.png" /></span>
                                            Невалидна домашна работа.
                                            <br />
                                            <a href="<?php echo site_url('/course/'); ?>" style="text-indent: 20px; display: block">
                                                Назад към профила
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php
                            } else {
                                if($data['have_homeworks'] === true) {
                                    $btn_text = 'Замести домашна';
									$form_caption = '<span class="green"><strong>Вече имате успешно качена домашна!</strong></span><br /><br />В случай, че сте качили грешен файл имате възможност да презапишете домашната си преди тя да бъде оценена. <br />
                                                    След оценяване на качена домашна тя се заключва и не може да бъде променяна!<br /><br />';
                                } else {
                                    $btn_text = 'Качи домашна';
									$form_caption = '';
                                }

								echo $form_caption;
                        ?>

                                <form method="POST" enctype="multipart/form-data">
									<?php if(isset($_REQUEST['layout'])){ echo '<input type="hidden" name="layout" value="'.$_REQUEST['layout'].'"';} ?>
                                    <label for="upfile">Избери файл</label>
                                    <input type="file" id="upfile" name="upfile" />
                                    <input type="submit" name="submit" value="<?php echo $btn_text;?>" />
                                </form>

                        <?php } ?>

                </div>
            </div>
        </div>
    </div>
<?php
	}
	else
	{
		echo '<div class="alert alert-border alert-warning">Срока за качване на тази домашна изтече или е ограничена за качване по IP, а вашият IP: '.$_SERVER['REMOTE_ADDR'].' не е сред разрешените!</div>';
	}

} else {
?>
    <div class="alert alert-border alert-warning">Несъществуваща домашна работа!</div>
<?php
}
?>