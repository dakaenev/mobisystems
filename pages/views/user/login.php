

<script async defer crossorigin="anonymous" src="https://connect.facebook.net/bg_BG/sdk.js#xfbml=1&version=v8.0&appId=786317405489365" nonce="kx9uLLSq"></script>
<div class="container">
	<div class="row">
    <div class="center-block logig-form" style="margin-top:20px;">
        <div class="panel panel-primary" >
            <div class="panel-heading hidden-xs" style="background-color: transparent; text-align: center;"><img src="<?php echo DIR_IMAGES; ?>ui/lock_key.png" alt="<?php echo translate('LOGIN', true); ?>" title="<?php echo translate('LOGIN', true); ?>" style="margin:20px auto;"></div>
            <div class="panel-body">
                <form role="form" autocomplete="off" method="post" action="">
                    <div class="form-group">
                        <div class="input-group login-input">
                            <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                            <input type="text" class="form-control" name="user" placeholder="<?php echo translate('LABEL_USER').' / '.translate('EMAIL_ADDRESS'); ?>" value="">
                        </div>
                        <br>
                        <div class="input-group login-input">
                            <span class="input-group-addon"><i class="fa fa-key"></i></span>
                            <input type="password" class="form-control" name="pass" placeholder="<?php echo translate('LABEL_PASS'); ?>" value="">
                        </div>
                        <div class="checkbox col-lg-6 pull-left">
                            <input type="checkbox" id="remember_me" name="remember_me" value="1">
                            <label for="remember_me"><?php echo translate('REMEMBER_ME'); ?></label>
                        </div>
						<div class="col-lg-6" style=" padding: 0;"><a href="<?php echo header_link(array(CONTROLLER => 'user', ACTION => 'forgotpass')); ?>" style="font-style: italic; padding:10px 0; float:right;"><?php echo translate('FORGOT_YOUR_PASSWORD'); ?></a></div>
						<div class="col-lg-12" style="padding: 0; margin-top:15px;">

							<button type="submit" class="btn btn-ar btn-primary btn-block"><?php echo translate('LOGIN'); ?></button>
							<!--
							 <div id="fb-root"></div>
							<div style="background-color: #1877F2" class="btn btn-primary btn-block fb-login-button" onlogin="checkLoginState();"data-size="large" data-button-type="login_with" data-layout="default"   data-auto-logout-link="false" data-width="100%" data-use-continue-as="false"  data-scope="public_profile,email"  ></div>
							-->
						</div>


<!--<script async defer crossorigin="anonymous" src="https://connect.facebook.net/bg_BG/sdk.js#xfbml=1&version=v8.0&appId=786317405489365" nonce="kx9uLLSq"></script>-->
<!--<div  class="btn-block fb-login-button" onlogin="checkLoginState();"data-size="large" data-button-type="login_with" data-layout="default"   data-auto-logout-link="false" data-width="100%" data-use-continue-as="false"  data-scope="public_profile,email"  ></div>-->

    <!--<fb:login-button  onlogin="checkLoginState();" data-auto-logout-link="true" data-size="large" data-scope="public_profile,email" >-->
<!--           <div class="form-group">
        <a href="<?php echo htmlspecialchars($loginUrl); ?>" class="btn btn-primary btn-block"><i class="fab fa-facebook-square"></i> Log in with Facebook!</a>
    </div>
		-->
						<div class="clearfix"></div>
						<!--
						<hr class="color double">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<a href="#" class="btn-social solid button-pill btn-block facebook" style="width:100%;"><i class="fa fa-facebook"></i>Facebook Login</a>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
							<a href="#" class="btn-social solid button-pill btn-block google-plus" style="width:100%;"><i class="fa fa-google-plus"></i>Google+ Login</a>
						</div>
						-->
                    </div>
                </form>
            </div>
			<hr class="dotted margin-10">
			<a href="<?php echo header_link(array(CONTROLLER => 'user', ACTION => 'register')); ?>" class="center" style="font-style: italic;"><?php echo translate('REGISTER_NOW'); ?></a>
        </div>
    </div>
	</div>
</div>