<?php Global $courses_array, $load_user_id; ?>
<?php
	database_open();
	$auto_collapse = true;
	$item_collapse = true;


	//pre_print($courses_array, 1); exit;
	$courses = '';

	if(empty($courses_array))
	{
		$courses = '<div class="alert alert-warning center"><strong>Образованието е пътят към по-добър живот!</strong> <br>Направи първата крачка сега! <a href="'.header_link([CONTROLLER => 'course', ACTION => 'index']).'" style="color:inherit;"><button type="button" class="">Избери си курс</button></a></div>';
	}
	else
	{
		$not_paid = getCoursesNeedMoney();

		if(count($not_paid))
		{
			unset($_SESSION[_APP_][_CLASS_MODALS]);
			$allowed_payments = array(
				'В брой в нашият офис' => '<iframe width="100%" height="350" src="'.$_SESSION['SYSTEM_SETTINGS']['map_src'].'" allowfullscreen="allowfullscreen"></iframe>Заповядайте в нашият офис на адрес <br class="hidden-lg hidden-md hidden-sm">'.$_SESSION['SYSTEM_SETTINGS']['address'].' <br><span class="hidden-xs">всеки ден от 10:00 до 20:00, или се свържете с нас на </span>телефон: '.$_SESSION['SYSTEM_SETTINGS']['phone'],
				//'Наложен платеж' => '',
				//'Плащане през EPay' => 'Заплатете сумата от <span class="course_all">'.$course['course_price'].'</span> лв. като кликнете на бутона по-долу и следвайте инструкциите в сайта на ePay.'.ePay_Button($button_options),
				'Банков превод' => '<span class="payment_bank_template course_name">'.$course['course_name'].'</span>'
								  .'<span class="payment_bank_template course_all" id="final_price">'.$course['course_price'].'</span>'
								  .'<span class="payment_bank_template pay_date">'.date('dmy').'</span>'
								  .'<img src="'.DIR_IMAGES.'/templates/platejno.png" alt="payment_example" style="margin:0 auto;" class="img-responsive hidden-xs">'
								  .'Преведете сумата от <span class="course_all">'.$course['course_price'].'</span> лв. в полза на '.$_SESSION['SYSTEM_SETTINGS']['legal_company'].' по сметка: <br>'
								  .'<strong>IBAN:</strong> '.str_ireplace(' ', '', $_SESSION['SYSTEM_SETTINGS']['bank_iban']).', '
								  .'<strong>BIC:</strong> '.$_SESSION['SYSTEM_SETTINGS']['bank_bic'].', '
								  .'<strong>Банка:</strong> '.$_SESSION['SYSTEM_SETTINGS']['bank_name']
			);

		//	$payment_options = '';
		//	foreach($allowed_payments as $payment_name => $payment_info)
		//	{
		//		$payment_options .= '<div class="col-lg-4"><label><input type="radio"> '.$payment_name.'</label></div>';
		//		$payment_options .= '<div class="col-lg-8">'.$payment_info.'</div>';
		//	}

			$options_counter = 0;
			$payment_options = array('options' => '', 'info' => '');
			foreach($allowed_payments as $payment_name => $payment_info)
			{
				$label_size = floor(12 / count($allowed_payments));
				$payment_options['options'] .= '<label class="col-lg-'.$label_size.' col-md-'.$label_size.' col-sm-'.$label_size.' col-xs-12 payment_labels"><input type="radio" name="payment_selected" value="payment_'.$options_counter.'"> '.$payment_name.'</label>';
				$payment_options['info'] .= '<div id="payment_'.$options_counter.'" class="payment hidden">'.$payment_info.'</div>';
				$options_counter++;
			}
			$payment_options = '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">'.$payment_options['options'].'</div><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align:center; color:red;">'.$payment_options['info'].'</div>';
		}

		foreach($courses_array as $course)
		{
			$lectures = to_assoc_array(exec_query('SELECT * FROM course_dates WHERE course_id =  '.$course['course_id'].' ORDER BY lecture_date ASC'));
			$lectures_list = '';
			$lecture_number = 0;

			if(count($lectures))
			{
				foreach($lectures as $lecture)
				{
					$lecture_number++;
					// '.translate('MONTH_'.date('m', strtotime($lecture['lecture_date']))).'
					$lectures_list .= '
						<tr>
							<td>'.$lecture_number.'</td>
							<td>'.$lecture['lecture_name'].'</td>
							<td>'.date('d.m.Y', strtotime($lecture['lecture_date'])).'</td>
						</tr>';
				}

				$lectures_list = '<div class="table-responsive"><table class="table table-bordered margin-bottom-0"><tr class="warning"><th width="20">#</th><th>Какво</th><th width="90">Кога</th></tr>'.$lectures_list.'</table></div>';
			}
			else
			{
				$lectures_list = '<div class="alert alert-info">Все още няма въведени дати!</div>';
			}

			$course_exams = getTestsPerCourse($course['course_id']);
			$passed_exams = getActiveTestsPerUser($course['course_id'], $load_user_id);
			$input_exams = array();

			//pre_print($course_exams);
			//pre_print($passed_exams);


			$exams_columns = array('#', 'Тест', 'Тип', 'Въпроси', 'Време', '');
			$exam_count = 0;
			foreach($course_exams as $exam)
			{
				if(!isset($passed_exams[$exam['exam_id']]['history_id']))
				{
					$text = 'Реши сега';
				}
				else
				{
					$text = 'Решен -'.$passed_exams[$exam['exam_id']]['score'].'%';
				}

				$tmp = array();
				$tmp[] = ++$exam_count;
				$tmp[] = 'Изпит Програмиране 1';//$exam['test_name'];
				$tmp[] = $exam['exam_type'];
				$tmp[] = $exam['questions_number'];
				$tmp[] = ($exam['test_seconds'] / 60) . ' мин.';
				$tmp[] = '<a href="'.header_link([CONTROLLER => 'exam', ACTION => 'solve', ID => $exam['exam_id']]).'" style="font-weight:bold;">'.$text.'</a>';
				$input_exams[] = $tmp;
			}

			$link_attributes = [];
			$body_attributes = [];

			if($auto_collapse)
			{
				$link_attributes[] = 'data-parent="#courses_drop_down"';
			}

			if($item_collapse)
			{
				$link_attributes[] = 'aria-expanded="false" class="collapsed"';
				$body_attributes[] = 'class="panel-collapse collapse"';
				$body_attributes[] = 'aria-expanded="false" style="height: 0px;"';
			}
			else
			{
				$link_attributes[] = 'aria-expanded="true"';
				$body_attributes[] = 'class="panel-collapse collapse in"';
				$body_attributes[] = 'aria-expanded="true" style=""';
			}

			if(!empty($link_attributes))
			{
				$link_attributes = ' '.implode(' ', $link_attributes);
			}

			//$courses .= '<a href="#" class="list-group-item"><h3>'.$course['course_name'].'</h3><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aequo chremes illis obligantur, metrodorus, graviter restincto permulta asperner, umquam.</p></a>';
			$course_directory = 'uploads/courses/'.(int) $course['course_id'];
			$materials_list = '';
			//$materials = to_assoc_array(exec_query('SELECT * FROM course_files AS cf LEFT JOIN files AS f ON cf.file_id = f.id WHERE cf.course_id = '.(int) $course['course_id']));
			$materials = get_files_for_directory($course_directory);
			sort($materials);
			//pre_print($materials, 1); exit;

			$video_template = '<!-- Button trigger modal -->
				<!-- <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3"></div> -->
					<div class="button-group col-lg-3">
                        <button type="button" class="button" data-toggle="modal" data-target="#FileModal_{FILE_ID}" style="padding:0 10px;">
                          <i class="fa fa-file-video-o"></i> {FILE_NAME}
                        </button>
						<a href="/{FILE_PATH}" download><button type="button" class="button" style="padding:0 10px;"><i class="fa fa-download"></i></button></a>
					</div>
						<!-- Modal -->
                        <div class="modal fade video_modal" id="FileModal_{FILE_ID}" tabindex="-1" role="dialog" aria-labelledby="FileModal_{FILE_ID}" aria-hidden="true" data-id="{FILE_ID}">
                          <div class="modal-dialog">
                            <div class="modal-content">
							  <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title">{FILE_NAME}</h4>
                              </div>
                              <div class="modal-body">
                                <div class="video">
                                    <video id="video_{FILE_ID}" width="100%" controls>
										<source src="/{FILE_PATH}" type="video/mp4">
										Your browser does not support the video tag.
									</video>
                                </div>
                              </div>
                            </div>
                          </div>
						</div>


					';

			$embed_template = '<!-- Button trigger modal --><div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                        <button class="btn btn-ar" data-toggle="modal" data-target="#FileModal_{FILE_ID}">
                          <i class="fa fa-file"></i> {FILE_NAME}
                        </button>
						<!-- Modal -->
                        <div class="modal fade video_modal" id="FileModal_{FILE_ID}" tabindex="-1" role="dialog" aria-labelledby="FileModal_{FILE_ID}" aria-hidden="true" data-id="{FILE_ID}">
                          <div class="modal-dialog">
                            <div class="modal-content">
							  <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title">{FILE_NAME}</h4>
                              </div>
                              <div class="modal-body">
                                <div class="video">
                                    <embed id="embed_{FILE_ID}" src="/{FILE_PATH}" width="100%">
                                </div>
                              </div>
                            </div>
                          </div>
						</div></div>';
			//$embed_template = '';
			if(count($materials))
			{

				foreach($materials as $key => $file)
				{
					$row = array();
					$row['file_name'] = $file;
					$row['file_path'] = $course_directory . DIRECTORY_SEPARATOR . $row['file_name'];
					$row['file_id'] = $course['course_id'].'_'.$key;

					switch(getFileExtension($row['file_name']))
					{
						case 'mp4': $template = $video_template; break;
						case 'pdf': $template = $embed_template; break;
						default: break;
					}

					if(is_file($row['file_path'])){ $materials_list .= str_ireplace(['{FILE_ID}','{FILE_PATH}','{FILE_NAME}'], [$row['file_id'], $row['file_path'],$row['file_name']], $template); }
				}
			}
			else
			{
				$materials_list = 'Няма качени материали!';
			}

			if('candidate' == $course['role']){
			$courses .= '
				<div class="panel panel-danger">
					<div class="panel-heading panel-heading-link">
						<a data-toggle="collapse" href="#course_'.$course['course_id'].'"'.$link_attributes.'><h3 style="display:inline;">'.$course['course_name'].'</h3></a>
					</div>
					<div id="course_'.$course['course_id'].'" '.implode(' ', $body_attributes).'>
						<div class="panel-body">
							<ul class="nav nav-tabs nav-tabs-round">
								<li class="active"><a data-toggle="tab" href="#status_'.$course['course_id'].'"><span class="glyphicon glyphicon-info-sign"></span><span class="hidden-xs"> '.translate('STATUS').'</span></a></li>
								<li><a data-toggle="tab" href="#calendar_'.$course['course_id'].'"><span class="glyphicon glyphicon-calendar"></span><span class="hidden-xs"> '.translate('CALENDAR').'</span></a></li>
								<li><a data-toggle="tab" href="#homeworks_'.$course['course_id'].'"><span class="glyphicon glyphicon-check"></span><span class="hidden-xs"> '.translate('HOMEWORKS').'</span></a></li>
								<li><a data-toggle="tab" href="#exams_'.$course['course_id'].'"><span class="glyphicon glyphicon-list-alt"></span><span class="hidden-xs"> '.translate('EXAMS').'</span></a></li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane active" id="status_'.$course['course_id'].'">
									<p>Запазил сте си място, но трябва да го платите<br>'
									. ' <form action="'.header_link([CONTROLLER => 'course',  ACTION => 'RemoveFromUser']).'" class="col-xs-12 col-lg-10 hidden alert-danger">'
									. '		<strong style="text-decoration: underline;">Моля посочете причината:</strong>'
									. '		<label style="display: block;"><input type="radio" name="why" value="1"> Намерих по-евтин курс</label>'
									. '		<label style="display: block;"><input type="radio" name="why" value="2"> Намерих по-евтин курс</label>'
									. '		<input type="hidden" name="course_id" value="'.$course['course_id'].'">
										</form>
									<!-- <button type="button" class="btn btn-danger pull-right btn_cancel_course">Отказвам се</button> -->
									</p>
								</div>
								<div class="tab-pane" id="calendar_'.$course['course_id'].'">
										'.$lectures_list.'
								</div>
								<div class="tab-pane alert alert-warning" id="homeworks_'.$course['course_id'].'">
									Тук ще се покажат вашите домашни, когато настъпи  времето за тях.
								</div>
								<div class="tab-pane" id="exams_'.$course['course_id'].'">
									'.(count($input_exams) ? return_table($input_exams, $exams_columns, 'class="table courses_sub_table"') : '<div class="alert alert-warning">Тук ще се покажат вашите междинни изпити, финални изпити и анкети, когато настъпи  времето за тях :)</div>').'
								</div>
							</div>
						</div>
					</div>
				</div>';
			}

			if('student' == $course['role']){
			$courses .= '
				<div class="panel panel-default">
					<div class="panel-heading panel-heading-link">
						<a data-toggle="collapse" href="#course_'.$course['course_id'].'"'.$link_attributes.'><h3 style="display:inline;">'.$course['course_name'].'</h3></a>
					</div>
					<div id="course_'.$course['course_id'].'" '.implode(' ', $body_attributes).'>
						<div class="panel-body">
							<ul class="nav nav-tabs nav-tabs-round">
								<li class="active"><a data-toggle="tab" href="#status_'.$course['course_id'].'"><span class="glyphicon glyphicon-info-sign"></span><span class="hidden-xs"> '.translate('STATUS').'</span></a></li>
								<li><a data-toggle="tab" href="#calendar_'.$course['course_id'].'"><span class="glyphicon glyphicon-calendar"></span><span class="hidden-xs"> '.translate('CALENDAR').'</span></a></li>
								<li><a data-toggle="tab" href="#materials_'.$course['course_id'].'"><span class="fa fa-book"></span><span class="hidden-xs"> '.translate('MATERIALS').'</span></a></li>
								<li><a data-toggle="tab" href="#homeworks_'.$course['course_id'].'"><span class="glyphicon glyphicon-check"></span><span class="hidden-xs"> '.translate('HOMEWORKS').'</span></a></li>
								<li><a data-toggle="tab" href="#exams_'.$course['course_id'].'"><span class="glyphicon glyphicon-list-alt"></span><span class="hidden-xs"> '.translate('EXAMS').'</span></a></li>
							</ul>
							<div class="tab-content">
								<div class="tab-pane active" id="status_'.$course['course_id'].'">
									<p>Вие сте успешно записан за курс №'.$course['course_id'].' '.$course['course_name'].'<br>Можете да намерите всичко необходимо за курса в навигацията по-горе<br>Във вашият случай, финалният изпит е в таб: "Изпити"' // <br>Материали:<br><a href="https://plovdiv-web.com/HTML.pdf" target="_blank">Лекции за самоподготовка</a>
									. ' <form action="'.header_link([CONTROLLER => 'course',  ACTION => 'RemoveFromUser']).'" class="col-xs-12 col-lg-10 hidden alert-danger">'
									. '		<strong style="text-decoration: underline;">Моля посочете причината:</strong>'
									. '		<label style="display: block;"><input type="radio" name="why" value="1"> Намерих по-евтин курс</label>'
									. '		<label style="display: block;"><input type="radio" name="why" value="2"> Намерих по-евтин курс</label>'
									. '		<input type="hidden" name="course_id" value="'.$course['course_id'].'">
										</form>
									<!-- <button type="button" class="btn btn-danger pull-right btn_cancel_course">Отказвам се</button>	 -->
									</p>
								</div>
								<div class="tab-pane" id="calendar_'.$course['course_id'].'">
										'.$lectures_list.'
								</div>
								<div class="tab-pane" id="materials_'.$course['course_id'].'">
										'.$materials_list.'
								</div>
								<div class="tab-pane" id="homeworks_'.$course['course_id'].'">
									<!-- <span data-src="'.header_link(array(CONTROLLER => 'user', ACTION => 'homeworks', ID => $course['course_id'])).'">Loading...</span> -->
									<iframe src="'.header_link(array(CONTROLLER => 'user', ACTION => 'homeworks', ID => $course['course_id'])).'?layout=blank" onload="" style="height:100%;width:100%;border:none;overflow:hidden;"></iframe>
								</div>
								<div class="tab-pane" id="exams_'.$course['course_id'].'">
									'.(count($input_exams) ? return_table($input_exams, $exams_columns, 'class="table courses_sub_table"') : '<div class="alert alert-warning">Тук ще се покажат вашите междинни изпити, финални изпити и анкети, когато настъпи  времето за тях :)</div>').'
								</div>
							</div>
						</div>
					</div>
				</div>';
			}
		}
	}
?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<section id="courses_list">
			<h2 class="section-title">Моите курсове</h2>
			<?php
				if(count($not_paid))
				{
					$modal_options = [
						'title' => 'Как ще платите?',
						'body' => '<fieldset>'.$payment_options.'</fieldset>',
						//'link' => '<button type="button">Платете сега!</button>',
						//'footer' => '<button type="button" class="btn btn-ar btn-primary">Потвърждавам!</button>'
					];

					echo add_modal('pay_now', $modal_options);
					$paid_button = add_modal('pay_now', '<button type="button">Платете сега!</button>');
					echo '<div class="alert alert-danger">Запазили сте си място за курса, но същинското записване става когато платите. '.$paid_button.'</div>';
				}
			?>
				<div class="panel-group" id="courses_drop_down">
					<?php echo $courses; ?>
				</div>
			</section>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){ //alert('asd');
		// To do course cancel work here...
		$('.video_modal').on('hidden.bs.modal', function () {
			document.getElementById('video_' + $(this).data('id')).pause();
		});
	});
</script>
<?php
/*
 * Това беше на 112 ред:
 * <div class="vertical-tabs-right"><div class="tab-content" style="width:100%;"><!--  style="width:100%;" -->
									<div class="tab-pane active" id="status_'.$course['course_id'].'">
										<p>Вие сте успешно записан за курс №'.$course['course_id'].' '.$course['course_name'].'<br>Можете да намерите всичко необходимо за курса в навигацията в дясно<br><br>Материали:<br><a href="https://plovdiv-web.com/HTML.pdf" target="_blank">Лекции за самоподготовка</a>'
										. ' <form action="'.header_link([CONTROLLER => 'course',  ACTION => 'RemoveFromUser']).'" class="col-xs-12 col-lg-10 hidden alert-danger">'
										. '		<strong style="text-decoration: underline;">Моля посочете причината:</strong>'
										. '		<label style="display: block;"><input type="radio" name="why" value="1"> Намерих по-евтин курс</label>'
										. '		<label style="display: block;"><input type="radio" name="why" value="2"> Намерих по-евтин курс</label>'
										. '		<input type="hidden" name="course_id" value="'.$course['course_id'].'">
											</form>
										<!-- <hr>	<button type="button" class="btn btn-danger pull-right btn_cancel_course">Отказвам се</button></fieldset> -->
										</p>
									</div>
									<div class="tab-pane" id="calendar_'.$course['course_id'].'">
											'.$lectures_list.'
									</div>
									<div class="tab-pane alert alert-warning" id="homeworks_'.$course['course_id'].'">
										Тук ще се покажат вашите домашни, когато настъпи  времето за тях.
									</div>
									<div class="tab-pane" id="exams_'.$course['course_id'].'">
										'.(count($input_exams) ? return_table($input_exams, $exams_columns, 'class="table courses_sub_table"') : '<div class="alert alert-warning">Тук ще се покажат вашите междинни изпити, финални изпити и анкети, когато настъпи  времето за тях :)</div>').'
									</div>
								</div>
								<div class="vertical-tab-list min-width-xs-0">
									<ul class="nav">
										<li><a data-toggle="tab" href="#status_'.$course['course_id'].'"><span class="glyphicon glyphicon-info-sign"></span><span class="hidden-xs"> '.translate('STATUS').'</span></a></li>
										<li><a data-toggle="tab" href="#calendar_'.$course['course_id'].'"><span class="glyphicon glyphicon-calendar"></span><span class="hidden-xs"> '.translate('CALENDAR').'</span></a></li>
										<li><a data-toggle="tab" href="#homeworks_'.$course['course_id'].'"><span class="glyphicon glyphicon-check"></span><span class="hidden-xs"> '.translate('HOMEWORKS').'</span></a></li>
										<li><a data-toggle="tab" href="#exams_'.$course['course_id'].'"><span class="glyphicon glyphicon-list-alt"></span><span class="hidden-xs"> '.translate('EXAMS').'</span></a></li>
									</ul>
								</div>
 </div>
 */
?>