<?php
Global $courses_array, $columns, $is_logged_user, $message, $load_user_id, $user_id, $is_admin_user, $mail_notifications, $new_mails_count, $wpdb, $have_new_certificate;

	if($is_logged_user)
	{
		database_open();
?>
<div class="container">
	<?php global $current_user;

		if(isset($_GET['user_id']) && $is_admin_user)
		{
			$load_user_id = $_GET['user_id']; //get_query_var('id','2','news');
		}
		else
		{
			//get_currentuserinfo();
			$load_user_id = get_current_user_id();
		}

		$loaded_user = get_userdata($load_user_id);
		//echo '<pre>'.print_r($loaded_user, true).'</pre>';

		if($have_new_certificate && ($loaded_user->user_firstname == '' || $loaded_user->user_lastname == ''))
		{
			$error_message  = 'За да Ви създадем сертификат се нуждаем от Вашето име и фамилия! <br />';
			$error_message .= '<a href="'.site_url('/edit-profile/').(isset($_GET['user_id']) && $is_admin_user ? '?user_id='.$_GET['user_id'] : '').'">Редактирайте профила си!</a>';

			add_error_message($error_message);
		}
	?>
	<?php

	$candidate_courses = $wpdb->get_results('SELECT course_id FROM course_members WHERE role="candidate" AND user_id = '.$load_user_id);

	if(count($candidate_courses) > 0)
	{
		foreach($candidate_courses as $course)
		{
			$required_tests = $wpdb->get_results('SELECT rt.*, t.* FROM required_tests AS rt LEFT JOIN tests as t ON rt.test_id = t.test_id  WHERE rt.course_id = '. $course->course_id , OBJECT_K);
			$user_tests = $wpdb->get_results('SELECT rt.*, t.*, eh.score, eh.is_pass, eh.history_id FROM required_tests AS rt LEFT JOIN tests as t ON rt.test_id = t.test_id LEFT JOIN exams_history AS eh ON rt.exam_id = eh.exam_id WHERE rt.course_id = '. $course->course_id.' AND eh.user_id = '.$load_user_id.' GROUP BY eh.exam_id' );

			//echo '<pre>'.print_r($required_tests,true).'</pre>';
			//echo '<pre>'.print_r($user_tests, true).'</pre>';

			$estimated_tests_count = count($required_tests) - count($user_tests);
			if($estimated_tests_count > 0)
			{
				foreach($user_tests as $test)
				{
					if(isset($required_tests[$test->exam_id]))
					{
						unset($required_tests[$test->exam_id]);
					}
				}

				$next_test = reset($required_tests);
				$next_test_id = $next_test->exam_id;
				break;
			}
			else
			{
				//
			}

			if(isset($next_test_id))
			{
				break;
			}
		}

		if(!isset($next_test_id))
		{
			$next_test_id = 0;
		}
	}
	else
	{
		$next_test_id = -1;
	}

	$uncomplete_profile = ($loaded_user->user_lastname == '' || $loaded_user->aim == '' || $loaded_user->skype == '' || $loaded_user->twitter == '');
	if($uncomplete_profile || $next_test_id >= 0 || have_messages()){ ?>
	<div class="row">
		<div class="col-md-12">
			<?php
				if(have_messages())
				{
					show_all_messages();
					clear_all_messages();
				}
			?>
			<div class="alert alert-border" style="margin-top:42px; background-color:#f6f6f6;">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<?php if($next_test_id > 0){ ?>
					<span style="font-family: 'Open Sans' !important; font-size: 13px !important; font-weight: 400 !important; color: #000000 !important;"><img src="<?php bloginfo('template_url'); ?>/img/caution_icon.png" /> Не забравяйте тестовете!</span>
					За да участвате в крайното класиране е нужно да решите <a href="<?php echo site_url('/course/?c=exam&a=solve&id='.$next_test_id); ?>" style="font-family: 'Open Sans' !important; font-size: 15px !important; font-weight: 550 !important;" title="Кликнете тук за да решите следващия нерешен тест.">входните тестове</a>
				<?php }elseif($next_test_id == 0){ ?>
					<span style="font-family: 'Open Sans' !important; font-size: 13px !important; font-weight: 600 !important; color: #000000 !important;"><img src="<?php bloginfo('template_url'); ?>/img/caution_icon.png" /> Благодарим Ви, че решихте входните тестове!</span>
					Крайното класиране ще бъде обявено на <b>13.05.2019, след 13:00 часа. Очаквайте обаждане от нас.</b>
				<?php }else{ ?>
					<span style="font-family: 'Open Sans' !important; font-size: 13px !important; font-weight: 400 !important; color: #000000 !important;"><img src="<?php bloginfo('template_url'); ?>/img/caution_icon.png" /> Вашият профил е незавършен!</span>
					<a href="<?php echo site_url('/edit-profile/'); ?><?php if(isset($_GET['user_id']) && $is_admin_user){ echo '?user_id='.$_GET['user_id']; } ?>" style="font-family: 'Open Sans' !important; font-size: 15px !important; font-weight: 550 !important; color: #5f6060;">Редактирай профил</a>
				<?php } ?>
			</div>
		</div>
	</div>
	<?php } ?>
	<div class="row" style="margin-top:24px;">
		<div class="col-xs-12 col-sm-9 col-md-9 col-lg-10">
			<div class="col-xs-5 col-sm-3 col-md-3 col-lg-2">
				<div class="row">
					<div class="col-lg-12">
						<?php echo get_avatar($loaded_user->user_email, 130); ?>
					</div>
				</div>
				<span id="btn_alerts">
				<?php
				if(true) // show mails ?
				{
					//$btn_alerts = '<button type="button" class="btn btn-danger btn-ar btn-block button button-rounded btn-edit-profile" data-toggle="collapse" data-target="#div_alerts" style="margin-top:5px;"><span title="Mail" class="glyphicon glyphicon-envelope"></span> Поща <span class="badge">'.count($alerts_array).'</span></button>';
					//echo '<a href="#" data-toggle="modal" data-target="#mails_modal"><img src="http://gifgifs.com/animations/computers-technology/email/flying_envelope_2.gif" alt="flying_envelope"  style="position:fixed;top:275px; left:49%; transform: translate(-50%, -50%); height:20%;" /></a>';

					echo '<a href="#" data-toggle="modal" data-target="#mails_modal">'
					. '<button type="button" id="mailbox_button" class="btn btn-ar btn-block button button-rounded btn-edit-profile" style="margin-top:5px; width:130px;"><span title="Mail" class="glyphicon glyphicon-envelope"></span> Поща <span class="badge" id="badge_mails_count"></span></button>'
					. '</a>';
//'.$new_mails_count.''.$mail_notifications.'
					echo '<form id="mail_form">
						<div class="modal fade" id="mails_modal" tabindex="-1" role="dialog" aria-labelledby="MailsModalLabel" aria-hidden="true">
							<div class="modal-dialog" style="width: 750px;">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										<h4 class="modal-title" id="MessagesLabel">Съобщения</h4>
									</div>
									<div class="modal-body" id="MessagesBody">

									</div>
									<div class="modal-footer" style="padding:2px;">
									'.($is_admin_user ? '<a href="'.header_link(array(CONTROLLER => 'mail', ACTION => 'create')).'" class="normal_link" id="btn_create_mail"><button type="button" style="width:75px; font-weight:bold;"><img src="/images/ui/add_page_16.gif" alt="New Message"> Ново</button></a>' : '').'
									<!--
										<a href="#" class="normal_link" id="btn_create_alert"><button type="button" class="btn btn-danger btn-sm" style="font-weight:bold;"><span class="glyphicon glyphicon-exclamation-sign"></span> Оплакване</button></a>
										<input type="submit" value="Разбрах" />
									-->
									</div>
								</div>
							</div>
						</div></form>';
				}
				?>
				</span>
			</div>
			<div class="col-xs-7 col-sm-9 col-md-9 col-lg-10">
				<div class="row">
					<div class="col-lg-12">
						<!--
						<div class="page-title  profile-email" style="margin-top:8px; margin-bottom:17px;"><?php echo date('ymd', strtotime($loaded_user->user_registered)).'-'.$load_user_id.''; //echo $loaded_user->user_login; ?></div>
						-->
						<div class="course_title"><?php echo $loaded_user->user_firstname; ?> <?php echo $loaded_user->user_lastname.($is_admin_user ? ' <a href="'.site_url('/course/?c=user&a=edit&id=').$load_user_id.'" title="Виж информация от регистрацията"><span class="glyphicon glyphicon-cog"></span></a>' : ''); ?></div>
						<div style="margin-top:13px; margin-bottom:10px;"><img src="<?php bloginfo('template_url'); ?>/img/mail_icon.png" style="margin-right:10px !important;" /> <a href="mailto:<?php echo $loaded_user->user_email; ?>" class="link"><?php echo $loaded_user->user_email; ?></a></i></div>
						<div class="course_date"><img src="<?php bloginfo('template_url'); ?>/img/phone_icon.png" style="margin-right:10px !important;" /> <?php echo $loaded_user->aim; ?></div>
						<div class="course_date"><span class="glyphicon glyphicon-user" style="margin-right:10px; margin-top:10px; color:gray; font-size:20px;"></span> <?php echo 'wa-'.date('ymd', strtotime($loaded_user->user_registered)).'-'.$load_user_id.''; //echo $loaded_user->user_login; ?></div>
						<!-- <div><img src="<?php bloginfo('template_url'); ?>/img/phone_icon.png" style="margin-right:10px !important;" /> <a href="skype:<?php echo $loaded_user->skype; ?>?chat" class="link"><?php echo $loaded_user->skype; ?></a></div> -->
						<!-- <div style="margin-top:8px;" class="course_date"><img src="<?php bloginfo('template_url'); ?>/img/calendar_icon.png" style="margin-right:10px !important;" /> <?php $registered = ($loaded_user->user_registered . "\n"); echo date('d/m/Y', strtotime($registered)); //echo date(get_option('date_format'), strtotime($registered)); ?></div> -->
						<div style="margin-top:8px;" class="course_date"><img src="<?php bloginfo('template_url'); ?>/img/calendar_icon.png" style="margin-right:10px !important;" /> <?php echo my_date_format(getUserLastOnline($load_user_id)); ?></div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-3 col-md-3 col-lg-2 profile-courses-info">
			<?php
				$courses = $wpdb->get_results('SELECT * FROM course_members AS cm LEFT JOIN courses AS c ON cm.course_id = c.course_id WHERE role <> "rejected" AND user_id = '.$load_user_id);
				$courses_count = count($courses);

				$finished = $wpdb->get_results('SELECT * FROM user_certificates WHERE user_id = '.$load_user_id);
				$finished = count($finished);

				//echo '<pre>'.print_r($courses, true).'</pre>';
				/*
				foreach($courses as $course)
				{
					if($course->role == 'finished')
					{
						$finished++;
					}
				}
				*/

				$courses_count -= $finished;
			?>
			<div class="row">
				<div class="col-lg-12">
					<a href="<?php echo site_url('/edit-profile/'); ?><?php if(isset($_GET['user_id']) && $is_admin_user){ echo '?user_id='.$_GET['user_id']; } //echo get_edit_user_link(); ?>"
					   class="btn btn-ar btn-block button button-border-primary button-rounded upload_button btn-edit-profile"
					   style="margin-bottom:23px;"
					>
						Редактирай профил
					</a>
					<!--
					<a href="<?php echo wp_logout_url(home_url()); ?>"class="btn btn-ar btn-block button button-border-caution button-rounded upload_button" style="margin-bottom:20px;">Изход</a>
					<hr style="margin-top:6px; margin-bottom:6px;">
					-->
					<div class="content-box box-default col-md-6 col-xs-6  _min_height" style="border-right:1px solid white !important;">
						<span class="content-box-title _number">
							<?php echo $courses_count; ?>
						</span>
						<p class="_text"><?php echo($courses_count == 1 ? 'записан курс' : 'записани курса'); ?></p>
					</div>
					<div class="content-box box-default col-md-6 col-xs-6  _min_height" style="border-left:1px solid white !important;">
						<span class="content-box-title _number">
							<?php echo $finished; ?>
						</span>
						<p class="_text"><?php echo ($finished == 1 ? 'завършен курс' : 'завършени курса'); ?></p>
					</div>
					<button type="button" class="btn btn-info btn-ar btn-block button button-rounded btn-edit-profile" data-toggle="collapse" data-target="#exams">Дати за курсовете</button>
				</div>
			</div>
		</div>
	</div>
	<div class="row" style="margin-top:59px;">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<?php //echo '<div id="div_alerts" class="collapse panel panel-danger">'.$mail_notifications.'</div>'; ?>
			<div id="exams" class="collapse table-responsive">
				<?php show_dates_table($load_user_id); ?>
			</div>
		</div>

		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<section>
				<!-- <h2 class="section-title">Моите курсове</h2> -->
				<div class="table-responsive">
					<?php
					//$args = array(
					//    'user_email' => $loaded_user->user_email,
					//    'number' => '3'
					//);
					//$comments = get_comments($args);
					//foreach($comments as $comment) :
					?>
						<!-- <a href="<?php echo get_page_link($comment->comment_post_ID); ?>" class="list-group-item">
							<h3><?php echo get_the_title($comment->comment_post_ID); ?></h3>
							<?php echo $comment->comment_content; ?>
						</a> -->
					<?php //endforeach; ?>
					<?php

		if(isset($message) && $message != '')
		{
			echo '<div class="row">
		<div class="col-md-12">
			<div class="alert alert-border" style="margin-top:42px; background-color:#f6f6f6;">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'.$message.'</div></div></div>';
		}

		//echo '<div id="filter_area" class="hidden row"><div class="col-lg-3"><label>Отхвърлени <input type="checkbox" name="filter_by" value="" /></label></div><div class="col-lg-3"><label>Отхвърлени <input type="checkbox" name="filter_by" value="" /></label></div><div class="col-lg-3"><label>Отхвърлени <input type="checkbox" name="filter_by" value="" /></label></div><div class="col-lg-3"><label>Отхвърлени <input type="checkbox" name="filter_by" value="" /></label></div></div>';
		render_table($courses_array, $columns, ' class="table "', 'mycourses'); //  _margin_top

		if(is_tester($load_user_id))
		{
			//echo '<a href="'.link_to(array(CONTROLLER => 'logs', ACTION => 'distinct')).'">Васко, не кликай тук :)</a>';
		}

		if(is_admin_user($load_user_id)) // Администраторите виждат предназначеното за тях
		{
			$alerts_array = to_assoc_array(exec_query('SELECT * FROM alerts WHERE read_date = 0 AND id != 5 AND to_user = '.$user_id));
			database_close();

			$alerts = array();
			foreach($alerts_array as $alert_record)
			{
				$alert_record['about'] = decrypt($alert_record['about']);
				$alert_record['description'] = decrypt($alert_record['description']);
				$alert_record['send_date'] = date('Y-m-d H:i:s', $alert_record['send_date']);

				$alerts[] = array(
					'',
					$alert_record['id'],
					'<b>Оплакване от '.$alert_record['from_user'].' относно '.$alert_record['about'].'</b>'.$alert_record['description'],
					'<a href="'.header_link(array(CONTROLLER => 'user', ACTION => 'view', ID => $alert_record['author_id'])).'" title="View User '.$alert_record['author_id'].'"><span class="glyphicon glyphicon-user"></span></a>',
					$alert_record['send_date']
				);
			}

			//$alerts = $alerts_array;
			$alerts = return_table($alerts, array('&nbsp;','ID','Тема','От','Изпратено',));

			$div_alerts = '
				<div class="panel-heading">Подадени оплаквания</div>
				<div class="panel-body">'.$alerts.'</div>
				<div class="panel-footer">
					<form method="post" action="'.site_url('/course/').'">
						От кого / какво се оплаквате:
						<select name="from" disabled="disabled">
							<option value="0">--- Изберете ---</option>
							<optgroup label="Лектори">
								<option value="1">Йордан Енев</option>
								<option value="9">Чанита Попова</option>
								<option value="68">Борислав Илийков</option>
								<option value="225">Красимир Тодоров</option>
							</optgroup>
							<optgroup label="Проблеми">
								<option value="-8">Конкурси</option>
								<option value="-3">Домашни</option>
								<option value="-2">Сайтът</option>
								<option value="-1">Друго</option>
							</optgroup>
						</select>
						Относно: <input type="text" name="about" style="width:400px;" disabled="disabled" />
						<input type="submit" value="Търси оплакване" class="btn btn-danger pull-right" disabled="disabled" /><br />
					</form>
				</div>';
		}
		else // Курсистите виждат това
		{
			$div_alerts = '
				<div class="panel-heading">Форма за подаване на оплаквания</div>
				<div class="panel-body">
					<form method="post" action="'.site_url('/course/').'">
						От кого / какво се оплаквате:
						<select name="from">
							<option value="0">--- Изберете ---</option>
							<optgroup label="Лектори">
								<option value="1">Йордан Енев</option>
								<option value="9">Чанита Попова</option>
								<option value="68">Борислав Илийков</option>
								<option value="225">Красимир Тодоров</option>
							</optgroup>
							<optgroup label="Проблеми">
								<option value="-8">Конкурси</option>
								<option value="-3">Домашни</option>
								<option value="-2">Сайтът</option>
								<option value="-1">Друго</option>
							</optgroup>
						</select>
						Относно: <input type="text" name="about" style="width:400px;" />
						<input type="submit" value="Изпрати оплакване" class="btn btn-danger pull-right" /><br />
						Съобщение: <textarea name="descr" style="border:1px solid silver; display:block; width:100%;"></textarea>
					</form>
				</div>
				<div class="panel-footer">
					* За възражения от оценка на домашна НЕ използвайте тази форма, а вместо това използвайте съответната форма за докладване на домашна.<br />
					** Използвайте тази форма за важни оплаквания - оплаквания от лектори, сгрешени домашни / изпити, открити бъгове по сайта и други. <br />
					*** Анонимността Ви е гарантирана!<input type="button" value="!" class="btn btn-danger pull-right" />
				</div>';
		}

?>
	<script type="text/javascript">
		// Редът маркиран с !!! създава проблеми при курсовете по които се кандидатства в момента.
			var $ = jQuery.noConflict();

			$(document).ready(function(){
				function LoadMails(){
					let myFirstPromise = new Promise((resolve, reject) => {
						$("#MessagesBody").load("<?php echo header_link(array(CONTROLLER => 'mail', ACTION => 'show', ID => $load_user_id)); ?> #show_mails_table");
						setTimeout(function(){ resolve($("#new_mails_count").text()); }, 1000);
					});

					myFirstPromise.then((successMessage) => { // successMessage ???
						successMessage = $("#new_mails_count").text(); // successMessage ???
						//console.log('Promise successMessage: '+ successMessage);
						$("#badge_mails_count").text(successMessage);
						if(0 == successMessage){
							$("#mailbox_button").removeClass('btn-danger');
						}else{
							$("#mailbox_button").addClass('btn-danger');
						}
					});
				}

				$(".filter_courses").click(function(){
					$("."+$(this).val()).parent().parent().prop("hidden", !$(this).prop( "checked" )); // !!!
				});

				//$("#course_rejected").trigger("click");
				//$("#course_finished").trigger("click");

				//$("#btn_create_mail").click(function(){
					//$("#MessagesBody").load("<?php echo header_link(array(CONTROLLER => 'mail', ACTION => 'create')); ?> #mail_form");
				//});

				$("#btn_create_alert").click(function(){
					$("#MessagesBody").load("<?php echo header_link(array(CONTROLLER => 'mail', ACTION => 'create', 'mode' => 'alert')); ?> #mail_form");
				});

				//$("#btn_delete_mails").click(function(){
				$("body").on("click", "#btn_delete_mails", function(){
					var chkbox_id = "input.del_chkbox:checked";

					if($(chkbox_id).length > 0){
						var answer = confirm("Наистина ли искате да изтриете тези съобщения?");

						if(answer == true){
							$.post( "<?php echo header_link(array(CONTROLLER => 'mail', ACTION => 'delete', 'layout' => 'empty')); ?>", $( "#mail_form" ).serialize() );
							$(chkbox_id).parent().parent().remove();
						}
					}else{
						alert("Първо изберете съобщенията, които искате да изтриете!");
					}
				});

				$("body").on("click", ".restore_message", function(){
					var mail_id = $(this).data('mail_id');
					var this_row = $(this).parent().parent();

					$.post( "<?php echo header_link(array(CONTROLLER => 'mail', ACTION => 'restore', 'layout' => 'empty')); ?>" + '&mr_id='+mail_id, function( data ) {
						if(data == 'success'){
							$('*', this_row).removeClass('deleted_mail');
							$(this).parent().html('<input type="checkbox" class="del_chkbox" name="del_chkbox[]" value="' + mail_id + '" />');
						}else{
							alert('Error! :(');
						}
					});
				});

				$("body").on("click", "a.refresh_messages", function(){
					var myParrent = $(this).closest('table').parent();

					myParrent.html('<span id="status"></span>');
					myParrent.load("<?php echo header_link(array(CONTROLLER => 'mail', ACTION => 'show', ID => $load_user_id)); ?> #show_mails_table");
				});



				var intervalID = setInterval( LoadMails, 60000 );
				setTimeout(LoadMails, 500);
			});

		//'.(true ? '$("#btn_alerts").html(\''.$btn_alerts.'\');' : '').'
		//'.(true ? '$("#div_alerts").html(\''.str_ireplace(PHP_EOL, ' ', $div_alerts).'\');' : '').'
	</script>
				</div> <!--list-group -->
			</section>
		</div>
	</div>
</div> <!-- container  -->
<?php
	}
	else
	{
		page_not_access();
	}
?>