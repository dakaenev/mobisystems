<?php
	Global $source;
?>
<div class="container">
    <div class="row">
		<div class="col-md-12">
			<form role="form" action="<?php echo header_link(array(CONTROLLER => 'user', ACTION  => 'user-forgotpass', ID => look_for_user())); ?>" method="post" id="change_pass_form">
                <h3 class="section-title"><?php echo translate('LABEL_UPDATE_PASS'); ?></h3>
				<div class="row">
                   

					<div class="col-md-6">
						<div class="form-group">
							<label for="email"><?php echo translate('EMAIL'); ?><sup>*</sup></label>
							<input type="email" class="form-control" id="email" name="email" value="<?php echo $default_data['email']; ?>">
              <hr>
              <button  name="submit" class="btn btn-ar btn-success btn-block"><?php echo translate('BTN_FORM_SEND'); ?></button>
						</div>
					</div>
					
					
						
				
				</div>
			</form>
		</div>
    </div>
</div>