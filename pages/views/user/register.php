<?php
	Global $default_data;

	$submit_button = '<button type="submit" class="btn btn-ar btn-primary pull-right%s">'.translate('REGISTER').'</button>';
	$come_from_array = array(	'-' => translate('DEFAULT_SELECT_LABEL') );
	$come_from_options = array('Ads', 'FaceBook', 'Friend', 'Google', 'Other');

	foreach($come_from_options as $option)
	{
		$come_from_array[$option] = translate('COME_FROM_'.$option);
	}

	if(isset($_GET['come_from']))
	{
		$is_editable = false;
		setcookie("come_from", $_GET['come_from'], time() + 60*60*24*30); // Cookies expire after 30 days
		$_GET['come_from'] = explode('_', $_GET['come_from']);
		$default['come_from'] = $_GET['come_from'][0];
		$default['explain_field'] = $_GET['come_from'][1];
	}
	else
	{
		$is_editable = true;
		$default['come_from'] = array_keys($come_from_array)[0];
		$default['explain_field'] = '';
	}
?>
<div class="container">
	<div class="row">
        <form role="form" autocomplete="off" action="<?php echo header_link(array(CONTROLLER => 'user', ACTION  => 'register')); ?>" method="post" style="margin-top:20px; margin-bottom:20px;">
			<div class="row">
				<div class="col-md-4 col-sm-6">
					<div class="img-caption-ar pwc_reg_caption_ar">
						<img src="<?php echo $default_data['avatar']; ?>" class="img-responsive pwc_regform_avatar profile-avatar" id="avatar_preview" alt="Image">
						<div class="caption-ar">
							<label class="caption-content" for="avatar_file">
								<a class="animated fadeInDown"><i class="fa fa-folder-open-o"></i><?php  echo translate('UPLOAD'); ?></a>
								<h4 class="caption-title"><?php  echo translate('CLICK_TO_UPLOAD'); ?></h4>
							</label>
						</div>
					</div>
					<?php TC_Checkbox('hidden-sm hidden-xs hidden-lg', 'margin-top:35px;'); ?>
				</div>
				<div class="col-md-8  col-sm-6">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="first_name"><?php echo translate('FIRST_NAME'); ?><sup>*</sup></label>
								<input type="text" class="form-control accept_<?php echo LANG_CODE; ?>_only" id="first_name" name="first_name" value="<?php echo $default_data['first_name']; ?>">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="last_name"><?php echo translate('LAST_NAME'); ?><sup>*</sup></label>
								<input type="text" class="form-control accept_<?php echo LANG_CODE; ?>_only" id="last_name" name="last_name" value="<?php echo $default_data['last_name']; ?>">
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label for="pass1"><?php echo translate('LABEL_PASS'); ?><sup>*</sup></label>
								<input type="password" class="form-control" id="pass1" name="pass1" value="<?php echo $default_data['pass1']; ?>">
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label for="pass2"><?php echo translate('LABEL_PASS_AGAIN'); ?><sup>*</sup></label>
								<input type="password" class="form-control" id="pass2" name="pass2" value="<?php echo $default_data['pass2']; ?>">
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-8  col-sm-12">
					<div class="row">
						<div class="col-md-6 col-sm-6">
							<div class="form-group">
								<label for="phone"><?php echo translate('PHONE'); ?><sup>*</sup></label>
								<input type="tel" class="form-control" id="phone" name="phone" value="<?php echo $default_data['phone']; ?>">
							</div>
						</div>

						<div class="col-md-6 col-sm-6">
							<div class="form-group">
								<label for="email"><?php echo translate('EMAIL_ADDRESS'); ?><sup>*</sup></label>
								<input type="email" class="form-control" id="email" name="email" value="<?php echo $default_data['email']; ?>">
							</div>
						</div>

						<div class="col-md-6 col-sm-6">
							<div class="form-group">
								<label for="facebook"><?php echo translate('FB_PROFILE_URL'); ?> [<a href="#"><?php echo translate('NOT_USE_FB_URL'); ?></a>]</label>
								<input type="url" class="form-control" id="facebook" name="facebook" placeholder="https://www.facebook.com/tiriram.shushukov" value="<?php echo $default_data['facebook']; ?>">
							</div>
						</div>

						<div class="col-md-6 col-sm-6">
							<div class="form-group">
								<label for="InputPassword"><?php echo translate('COME_FROM'); ?></label>
								<select name="come_from" id="come_from" class="form-control"<?php echo ($is_editable ?  '' : ' readonly="readonly" disabled="disabled"'); ?>>
									<?php RenderDropDown($come_from_array, $default_data['come_from']); ?>
								</select>
								<input type="text" class="form-control hidden" id="explain_field" name="explain_field" placeholder="" value="<?php echo $default_data['explain_field']; ?>" list=""<?php echo ($is_editable ?  '' : ' readonly="readonly"'); ?>>
								<datalist id="users">
									<option value="Internet Explorer">
									<option value="Firefox">
									<option value="Chrome">
									<option value="Opera">
									<option value="Safari">
								</datalist>
							</div>
						</div>

						<div class="col-md-6 col-sm-6">
							<div class="form-group">
								<label for="born_date"><?php echo translate('BORN_DATE'); ?></label>
								<input type="date" class="form-control" id="born_date" name="born_date" value="<?php echo $default_data['born_date']; ?>" style="height:36px;">
							</div>
						</div>

						<div class="col-md-6 col-sm-6">
							<span id="sex_chooser">
								<?php echo translate('LABEL_SEX'); ?><br>
								<?php printf($submit_button, '  hidden-sm hidden-lg'); ?>
								<span class="radio sex_radio">
									<input type="radio" name="sex" id="sex_1" value="1"<?php echo (1 == $default_data['sex'] ? ' checked="checked"' : ''); ?>>
									<label for="sex_1"> <?php echo translate('SEX_MALE'); ?></label>
								</span>
								<span class="radio sex_radio">
									<input type="radio" name="sex" id="sex_0" value="0"<?php echo (0 == $default_data['sex'] ? ' checked="checked"' : ''); ?>>
									<label for="sex_0"> <?php echo translate('SEX_FEMALE'); ?></label>
								</span>
								<span class="radio sex_radio">
									<input type="radio" name="sex" id="sex_2" value="2"<?php echo (2 == $default_data['sex'] ? ' checked="checked"' : ''); ?>>
									<label for="sex_2"> <?php echo translate('SEX_OTHER'); ?></label>
								</span>
							</span>
						</div>
					</div>
				</div>
			</div>

			<div class="row hidden-md">
				<div class="col-md-4 col-sm-6">
					<?php TC_Checkbox(); ?>
				</div>
				<div class="col-md-8 col-sm-6">
					<?php printf($submit_button, ' hidden-xs'); ?>
				</div>
			</div>
			<input type="hidden" name="avatar" id="avatar_address" value="<?php echo $default_data['avatar']; ?>">
        </form>
		<form id="avatarUploadForm" action="<?php echo header_link([CONTROLLER => 'user', ACTION  => 'uploadavatar']); ?>" method="post" enctype="multipart/form-data">
			<input type="hidden" name="layout" value="empty">
			<input type="file" name="avatar_file" class="hidden" id="avatar_file">
		</form>
	</div>
</div>
<script>
	$(document).ready(function(){
		<?php echo AJAX_Avatar_Upload(); ?>

		// Validate Email, Name, Phone ...
		<?php JS_Validate_Digits_Only(); ?>
		<?php JS_Validate_English_Only(); ?>

		<?php if( is_default_language() ){ echo PHP_EOL; ?>
		// ==============================| Validations for Bulgarian Language Only |==============================
		$('#last_name').change(function(){
			var nameSuffix = $('#last_name').val();
			var nameLength = nameSuffix.length;
			var mSuffix = nameSuffix.substr(nameLength-2).toLowerCase();
			var fSuffix = nameSuffix.substr(nameLength-3).toLowerCase();

			if('ов' == mSuffix || 'ев' == mSuffix){
				$('#sex_1').prop("checked", true);
				return true;
			}

			if('ова' == fSuffix || 'ева' == fSuffix){
				$('#sex_0').prop("checked", true);
				return true;
			}

			$('#sex_2').prop("checked", true);
			alert("<?php echo translate('MSG_UNKNOWN_SEX'); ?>");
			return false;
		});

		<?php JS_Validate_Bulgarian_Only(); ?>
		<?php JS_Validate_Bulgarian_Phone(); ?>
		// ==============================|   End of Bulgarian Validations Code   |================================
		<?php } ?>

		$('#facebook').change(function(){
			$('#fb_preview').attr('href', $(this).val());
		});

		$('#fb_preview').click(function(){
			if('#' == $(this).attr('href')){
				alert("<?php echo translate('MSG_FB_PREVIEW_INFO'); ?>");
				return false;
			}
		});

		$('#come_from').change(function(){
			let come = '#come_from';
			let explain = '#explain_field';
			let cf_value = $(come).val().toLowerCase();

			if(cf_value == 'friend' || cf_value == 'other'){

				$(come).removeClass('form-control');
				$(explain).removeClass('hidden');

				if(cf_value == 'friend'){
					$(explain).attr('placeholder', "<?php echo translate('FRIEND_NAME'); ?>");
					$(explain).attr('list', 'users');
				} else {
					$(explain).attr('placeholder', "<?php echo translate('PLEASE_EXPLAIN'); ?>");
					$(explain).attr('list', '');
				}

			} else {
				$(come).addClass('form-control');
				$(explain).addClass('hidden');
			}
		});

		<?php echo ($is_editable ?  '' : "$('#come_from').trigger('change');" ); ?>
	});
</script>