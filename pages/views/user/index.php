<?php database_open(); ?>
<?php Global $is_admin_user, $source; ?>
<?php $is_observer = is_observer($source['course_id']); ?>

<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<form method="GET" action="./?c=user&a=index">
				<div class="form-group">
					<div class="row">

						<input type="hidden" name="<?php echo CONTROLLER; ?>" value="<?php echo $source[CONTROLLER]; ?>" />
						<input type="hidden" name="<?php echo ACTION; ?>" value="<?php echo $source[ACTION]; ?>" />

						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
							<label for="course_id">
								Курс
							</label>

							<select class="form-control" name="course_id<?php echo (isset($source['course_id']) && is_array($source['course_id']) ? '[]" multiple="multiple" size="1' : ''); ?>">
								<option value="">- Без Филтър -</option>
								<?php

									if(!$is_admin_user) // This is Observer
									{
										$courses_scope = 'course_id IN ('.implode(', ', getObservedCourses()).')';
									}
									else
									{
										$courses_scope = null;
									}

									$archive = '';
									$current = '';
									foreach(get_all_courses($courses_scope) as $course)
									{
										$course['end_date'] = strtotime($course['end_date']) + (0 * 7 * 24 * 3600); // курса се брои за активен до 2 седмици след неговия край ;-)
										$which_courses = ($course['start_date'] <= date('Y-m-d') && strtotime(date('Y-m-d')) <= $course['end_date'] ? 'current' : 'archive');
										$$which_courses .= '<option value="'.$course['course_id'].'"'.($source['course_id'] == $course['course_id'] || in_array($course['course_id'], $source['course_id']) ? ' selected="selected"' : '').'>'.$course['course_id'].'. '.$course['course_name'].'</option>';
										// Тук използваме $$ за да укажем, коя променлива искаме да променяме. Така избягваме 1 if, или пък двойното писане на един и същи код.
									}

									$archive = '<optgroup label="Архивни">'.$archive.'</optgroup>';
									$current = '<optgroup label="Текущи">'.$current.'</optgroup>';

									echo $current.$archive;
								?>
							</select>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
							<label for="group_number">
								Обучение
							</label>

							<select name="group_number" class="form-control">
								<option value="-1" <?php if(!isset($_GET['group_number']) && $_GET['group_number'] == -1){ echo 'selected'; } ?>> - Без филтър - </option>
								<optgroup label="Системни">
									<option value="online" <?php if(isset($_GET['group_number']) && $_GET['group_number'] == 'online'){ echo 'selected'; } ?>>Онлайн</option>
									<option value="onlive" <?php if(isset($_GET['group_number']) && $_GET['group_number'] == 'onlive'){ echo 'selected'; } ?>>Присъствено</option>
									<option value="strange" <?php if(isset($_GET['group_number']) && $_GET['group_number'] == 'strange'){ echo 'selected'; } ?>>Странни</option>
								</optgroup>
								<optgroup label="Групи (Потоци)">
									<option value="1" <?php if(isset($_GET['group_number']) && $_GET['group_number'] == 1){ echo 'selected'; } ?>>Група 1</option>
									<option value="2" <?php if(isset($_GET['group_number']) && $_GET['group_number'] == 2){ echo 'selected'; } ?>>Група 2</option>
								</optgroup>
							</select>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
							<label for="role">
								Тип
							</label>

							<select name="role" class="form-control">
								<option value="all" <?php if(!empty($_GET['role']) && $_GET['role'] == null || empty($_GET['role'])) echo 'selected'; ?>> - Без филтър - </option>
								<optgroup label="Системни">
									<option value="candidate" <?php if(!empty($_GET['role']) && $_GET['role'] == 'candidate') echo 'selected'; ?>>Кандидати</option>
									<option value="student" <?php if(!empty($_GET['role']) && $_GET['role'] == 'student') echo 'selected'; ?>>Курсисти</option>
									<option value="rejected" <?php if(!empty($_GET['role']) && $_GET['role'] == 'rejected') echo 'selected'; ?>>Некласирани</option>
								</optgroup>
								<optgroup label="Съсловия"><!-- Касти -->
									<option value="white" <?php if(!empty($_GET['role']) && $_GET['role'] == 'white') echo 'selected'; ?>>Бяло</option>
									<option value="neutral" <?php if(!empty($_GET['role']) && $_GET['role'] == 'neutral') echo 'selected'; ?>>Неутрално</option>
									<option value="black" <?php if(!empty($_GET['role']) && $_GET['role'] == 'black') echo 'selected'; ?>>Черно</option>
								</optgroup>
							</select>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
							<label for="full_name">
								Курсист
							</label>

							<input type="text" name="full_name" id="full_name" class="form-control" value="<?php echo (isset($_GET['full_name']) ? $_GET['full_name'] : ''); ?>" placeholder="Име на кирилица" />
						</div>
						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
							<input type="hidden" name="is_send" value="1" />
							<button type="submit" class="btn btn-large btn-success" style="margin-top:30px"><span class="glyphicon glyphicon-filter"></span> Филтрирай</button>
							<button type="button" id="getRandomUser" class="btn btn-large btn-success" style="margin-top:30px"><span class="glyphicon glyphicon-random"></span></button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="users_main_div">
			<form method="post" action="" id="checkbox_form">
<?php
	if($is_admin_user || $is_observer)
	{
		$users = array();
		$emails = array();

		$columns = array(
			array('text' => '<i class="fa fa-file-pdf-o center" id="btn_pdf" style="cursor:pointer;"></i>', 'attr' => 'width="10"'),
			array('text' => 'Аватар', 'attr' => 'width="145"'),
			array('text' => 'Факултетен Номер', 'attr' => 'width="145"'),
			//'Парола',
			array('text' => 'Потребител', 'attr' => 'width="180	"'),
			array('text' => 'E-mail', 'attr' => 'width="110"'),
			array('text' => 'GSM', 'attr' => 'width="145"'),
			array('text' => '<img src="'.DIR_IMAGES.'ui/icon_mail.gif" alt="Mail" id="btn_mail" style="cursor:pointer;" />', 'attr' => 'width="116"'),
			array('text' => 'Обучение', 'attr' => 'width="10"'),
			array('text' => 'Група', 'attr' => 'width="10"'),
			array('text' => 'Facebook', 'attr' => 'width="96"'),
//			array('text' => '<img src="'.DIR_IMAGES.'ui/facebook.png" alt="Facebook" id="btn_facebook" style="cursor:pointer;" />', 'attr' => 'width="116"'),
			//array('text' => '&nbsp;', 'attr' => 'width="10"'),
		);

		if(isset($_GET['is_send']))
		{
			$query_string = '';
			if(isset($source['course_id']) && $source['course_id'] > 0)
			{
				$query_string .= ' AND cm.course_id = '. (int) $source['course_id'];
			}

			if(isset($source['role']))
			{
				switch($source['role'])
				{
					case 'student': $query_string .= ' AND cm.role = "student"'; break;
					case 'rejected': $query_string .= ' AND cm.role = "rejected"'; break;
					case 'candidate': $query_string .= ' AND cm.role = "candidate"'; break;
					case 'black'	: $query_string .= ' AND us.user_caste = "-1"'; break;
					// case 'neutral'	: $query_string .= ' AND us.user_caste = "0"'; break;
					case 'white'	: $query_string .= ' AND us.user_caste = "1"'; break;
				}
			}

			if(isset($source['form_id']))
			{
				$query_string .= ' AND cm.form_id = "'. (int) $source['form_id'].'"';
			}

			if(isset($source['group_number']) && $source['group_number'] != -1)
			{
				switch($source['group_number'])
				{
					case       '1': $query_string .= ' AND (cm.form_id = 1 AND cm.group_number = 1)'; break;
					case       '2': $query_string .= ' AND (cm.form_id = 1 AND cm.group_number = 2)'; break;
					case  'online': $query_string .= ' AND (cm.form_id = 2 AND cm.group_number = 0)'; break;
					case  'onlive': $query_string .= ' AND (cm.form_id = 1 AND cm.group_number > 0)'; break;
					case 'strange': $query_string .= ' AND (cm.form_id NOT IN (1,2) OR cm.group_number NOT IN (0,1,2) OR (cm.form_id = 1 AND cm.group_number <= 0) OR (cm.form_id = 2 AND cm.group_number >= 1))'; break;
				}
			}

			if(isset($_GET['full_name']) && trim($_GET['full_name']) != '')
			{
				if(!is_numeric($_GET['full_name']))
				{
					$query_string .= ' AND '.like_trans(array('u.first_name', 'u.last_name', 'u.user_email', 'u.phone' ), $_GET['full_name']);
				}
				else
				{
					$query_string .= ' AND u.user_id = '.$_GET['full_name'];
				}
			}

			if(isset($_GET['users']))
			{
				if(!is_array($_GET['users']))
				{
					$_GET['users'] = explode(',', $_GET['users']);
					foreach($_GET['users'] as $k => $v)
					{
						if($v == '')
						{
							unset($_GET['users'][$k]);
						}
					}
				}

				$query_string .= ' AND u.user_id IN ('.implode(',',$_GET['users']).')';
			}

			$query_string = 'SELECT u.*, cm.course_id, cm.role, cm.form_id, cm.group_number, us.user_caste FROM users AS u '.($query_string == '' ? 'LEFT JOIN' : 'LEFT JOIN').' course_members AS cm ON u.user_id = cm.user_id LEFT JOIN user_settings AS us ON u.user_id = us.user_id WHERE 1=1'.$query_string;

			$query_string .= ' GROUP BY cm.user_id'; if(!isset($source[ORDER_BY])){ $source[ORDER_BY] = 'u.first_name, u.last_name'; } // Временно решение. Да го премахна в скоро време...
			if(isset($source[ORDER_BY]))
			{
				$query_string .= ' ORDER BY '.$source[ORDER_BY].' ASC';
			}

			//echo ''.PHP_EOL.sql_debug($query_string).PHP_EOL.'';

			$query_users = to_assoc_array(exec_query($query_string));
			$online_users = array();

			if(isset($source['course_id']) && $source['course_id'] > 0)
			{
				// Prepare Course Activity
			}

			foreach( $query_users as $k => $user )
			{
				//pre_print($user, true);

				$users[$k]['id']  = '<a href="'.header_link( array(CONTROLLER => 'user', ACTION => 'view', ID => $user['user_id']) ).'"><button style="width:30px;" type="button"><span class="glyphicon glyphicon-user"></span></button></a>';
				$users[$k]['id'] .= '<a href="'.header_link( array(CONTROLLER => 'user', ACTION => 'manage', ID => $user['user_id']) ).'" title="Управление на потребителя"><button style="width:30px;" type="button"><span class="glyphicon glyphicon-cog"></span></button></a>';

				if($user['twitter'] != '')
				{
					$user['twitter'] = explode(':', $user['twitter']);
					if($user['twitter'][0] != 'http' && $user['twitter'][0] != 'https')
					{
						$user['twitter'][0] = 'https://www.facebook.com/'.$user['twitter'][0];
					}
					$user['twitter'] = implode(':', $user['twitter']);
					$users[$k]['id'] .= ' <a href="'.$user['twitter'].'" title="Виж FB" target="_blank"><button style="width:30px;" type="button">FB</button></a>';
				}

				$last_online = 0;
				$last_online = getUserLastOnline($user['user_id']);

//				$avatar = get_avatar($user['user_id']);
				//unset($users[$k]['id']);

				//$users[$k]['avatar'] = '<a href="' . str_replace('"',"'",$avatar_url) . '" title="Последно на линия: '.($last_online == 0 ? 'Неизвестно' : date('Y-m-d H:i:s', $last_online + TIME_CORRECTION)).'" class="avatar-full" target="_blank">'.get_avatar($user['user_id'], '128', 'Avatar', '',  array('height' => 'auto', 'class'=>'img-responsive avatar', 'force_default' => true)).'</a>';
				//$users[$k]['avatar'] = htmlspecialchars(str_replace('"',"'",$avatar_url));

				$reg_date = $user['user_registered'] ;



				$reg_date = strtotime($reg_date);
				$reg_date = date('ymd', $reg_date);

				$users[$k]['avatar'] = '<img src="'.$user['avatar'].'" alt="avatar" class="img-responsive" />';
				$users[$k]['avatar'] .= '<span style="position:relative; top:-20px; right:-110px; background-color:white; border-radius:50%; font-size:20px;'.( (strtotime('now') - (60 * 60 * 3)) <= getUserLastOnline($user['user_id']) ? 'color:green;" data-user="'.$user['user_id'].'-'.$user['first_name'].' '.$user['last_name'].'" class="glyphicon glyphicon-ok-circle">' : 'color:red;" class="glyphicon glyphicon-ban-circle">').'</span>';

				$users[$k]['fak_num'] .= 'wa-'.$reg_date.'-'.$user['user_id'].' <!-- '.hex2bin($user['meta_value']).' -->';
				if(isset($source['course_id']) && $source['course_id'] > 0)
				{
					$users[$k]['fak_num'] .= showActivityTable($user['user_id'], $source['course_id']);
				}

				$users[$k]['user'] = $user['first_name'].' '.$user['last_name'];
				$users[$k]['mail'] = $user['user_email'] ;
//	     		$users[$k]['aim'] = '<a href="#">' .$user['phone']. '</a>';
				$users[$k]['aim'] = '<a href="tel:'.$user['phone'].'">' .my_phone_format($user['phone']).'</a>';
				$users[$k]['send_mail'] = '<input type="checkbox" name="send_to['.$user['user_id'].']" value="'.$user['user_id'].'" checked="checked" />';

				if(stripos($users[$k]['avatar'], 'glyphicon-ok-circle'))
				{
					$online_users[] = $user['user_id'];
				}
				$users[$k]['form'] =  $user['form_id'] ;

				$users[$k]['group'] = $user['group_number'] ;

				$users[$k]['facebook'] = '<a href="'.$user['facebook'].'"><img src="'.DIR_IMAGES.'ui/facebook.png" alt="Facebook" style="height:16px";></a>';

//				$users[$k]['registered'] =  $user['user_registered'] ;


				switch($users[$k]['form']) {
					case 1:
						$users[$k]['form'] = 'Присъствена';
					break;

					case 2:
						$users[$k]['form'] = 'Онлайн';
					break;

					default:
						$users[$k]['form'] = '---';
				}

				if($users[$k]['form'] != '---') {
					switch ($users[$k]['group']) {
						case 0:
							$users[$k]['group'] = 'Онлайн';
							break;

						case 1:
							$users[$k]['group'] = 'Първа група';
							break;

						case 2:
							$users[$k]['group'] = 'Втора група';
							break;

						default:
							$users[$k]['group'] = '---';
							break;
					}
				} else {
					$users[$k]['group'] = '---';
				}


//				Pre_Print($user,1);


//				$users[$k]['aim'] = 'Tel: '.$users[$k]['aim'];
//				unset($users[$k]['avatar']);

//				unset($users[$k]['id']);
//				unset($users[$k]['form']);
//				unset($users[$k]['group']);
//				unset($columns[0]);
//				unset($columns[1]);
//				unset($columns[6]);
//				unset($columns[7]);

				$emails[] = $user['user_email'] ;


			}
			database_close();

			$table_classes = 'class="table table-striped table-bordered table-condensed table-responsive"';

			render_table($users, $columns, $table_classes);

			$emails = implode(', ', $emails);
			echo '<textarea cols="96" style="margin-top:10px; border:5px ridge lightblue;">'.$emails.'</textarea>';
		}
	}
	else
	{
		page_not_access();
	}
?>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
	function getRandomInt(min, max)
	{
	  return Math.floor(Math.random() * (max - min + 1)) + min;
	}

	$(document).ready( function () {
		//alert('Users Online: ' + $( ".glyphicon-leaf" ).length);

		if($('.avatar').length > 0) {
			$('.avatar:visible').each( function () {
				$(this).css('height', 'auto');

				var avatar_url = $(this).attr('src');
				try{
					var cut = avatar_url.lastIndexOf("-");
					var ext = avatar_url.substring(avatar_url.length -4);
					avatar_url = avatar_url.substring(0, cut) + ext;
				}catch(e){
					avatar_url = avatar_url;
				}
//				console.log(avatar_url);
				$(this).closest('.avatar-full').attr('href', avatar_url);
				$(this).removeAttr('srcset');
				$(this).error( function() {
					$(this).hide();
					$(this).closest('.avatar-full').hide();
				});
			});
		}

		$('#getRandomUser').click(function(){
			var id = (getRandomInt(0, $( ".glyphicon-ok-circle" ).length - 1));
			var online_users = $.makeArray($( ".glyphicon-ok-circle" ));
			alert($(online_users[id]).data('user'));
		});

		$('#btn_mail').click(function(){
			var checkboxForm = $('#checkbox_form');
			checkboxForm.attr('action', "<?php echo header_link( array(CONTROLLER => 'mail', ACTION => 'create')); ?>");
			checkboxForm.submit();
		});

		$('#btn_pdf').click(function(){
			var checkboxForm = $('#checkbox_form');
			checkboxForm.attr('action', "<?php echo header_link( array(CONTROLLER => 'user', ACTION => 'pdf')); ?>");
			checkboxForm.submit();
		});
	});
</script>
