<?php
	Global $source, $user_data, $is_ajax;
	//pre_print($user_data);
// - Видимо на публичната Ви страница, достъпно за всеки
// - Видимо за логнати потребители
// - Видимо само за вашите приятели
// - Видимо само за Вас и никой друг

	//pre_print($user_data);
	$privacy_icons_array = array();
	$faq_elements = array(
		[
			'title' => 'Какво значат означенията?',
			'text' => '<li><i class="fa fa-globe"></i> - '.translate('PRIVACY_4_XS').translate('PRIVACY_4_LG').'</li>'
					. '<li><i class="fa fa-unlock-alt"></i> - '.translate('PRIVACY_3_XS').translate('PRIVACY_3_LG').'</li>'
					. '<li><i class="fa fa-smile-o"></i> - '.translate('PRIVACY_2_XS').translate('PRIVACY_2_LG').'</li>'
					. '<li><i class="fa fa-eye-slash"></i> - '.translate('PRIVACY_1_XS').translate('PRIVACY_1_LG').'</li>'
			// Тук липсват <ul> тагове. Да се добавят и да се прегледа отново дизайна.
		],
		['title' => 'Защо ми трябва FB акаунт?', 'text' => 'Очаквайте скоро...'],
		['title' => 'Защо ми трябва рождена дата?', 'text' => 'Очаквайте скоро...']
	);

	$faq_section = '';
	foreach($faq_elements as $k => $element)
	{
		$faq_section .= '
			<div class="panel panel-info">
				<div class="panel-heading panel-heading-link">
					<a data-toggle="collapse" data-parent="#faq_edit" href="#ans_edit_'.$k.'" class="collapsed" aria-expanded="false">'.$element['title'].'</a>
				</div>
				<div id="ans_edit_'.$k.'" class="panel-collapse collapse" aria-expanded="false">
					<div class="panel-body">
					  '.$element['text'].'
					</div>
				</div>
			</div>';
	}
	$faq_section = '<hr class="dashed"><h4>Разберете повече:</h4><div class="panel-group" id="faq_edit">'.$faq_section.'</div>';

	function prepare_row($field, $input)
	{
		Global $user_data, $privacy_icons_array;

		$field = strtolower($field);

		$row = array();
		$row[] = translate($field);
		$row[] = $input;

		if(!isset($user_data['privacy'][$field]))
		{
			$user_data['privacy'][$field] = 1;
		}

		for($i = 4; $i >= 1; $i--)
		{
			$row[] = '<input type="radio" id="privacy-'.$field.'-'.$i.'" class="center privacy-'.$i.' privacy-'.$field.'" name="privacy['.$field.']" value="'.$i.'"'.($i == $user_data['privacy'][$field] ? ' checked="checked"' : '').'>';
		}

		return $row;
	}

	//pre_print($user_data);
	$fields = array(
		'first_name', 'last_name', 'phone', 'email', 'facebook'
	);

	$form_data = array();
	foreach($fields as $field)
	{
		$form_data[] = prepare_row($field, '<input type="'.('email' == $field ? 'email' : 'text').'" name="'.$field.'" value="'.$user_data[$field].'" style="width:100%;"'.('' == $user_data[$field] ? ' class="need_attention"' : '').'>');
	}

	// Custom Fields
	$sex = '<span class="radio d-inline"><input type="radio" name="sex" id="sex_1" value="1"'.(1 == $user_data['sex'] ? ' checked="checked"' : '').'><label for="sex_1"> '.translate('SEX_MALE').'</label></span>
			<span class="radio d-inline"><input type="radio" name="sex" id="sex_0" value="0"'.(0 == $user_data['sex'] ? ' checked="checked"' : '').'><label for="sex_0"> '.translate('SEX_FEMALE').'</label></span>
			<span class="radio d-inline"><input type="radio" name="sex" id="sex_2" value="2"'.(2 == $user_data['sex'] ? ' checked="checked"' : '').'><label for="sex_2"> '.translate('SEX_OTHER').'</label></span>';

	$form_data[] = prepare_row('label_sex', $sex);
	$form_data[] = prepare_row('born_date', '<input type="date" name="born_date" value="'.substr($user_data['born_date'],0,10).'" style="width:100%;">');
	$form_data[] = prepare_row('avatar', '<label for="avatar_file">Прикачи нова</label><label class="pull-right hidden">Избери от галерията</label>');

	$columns = array(
		'Поле',
		'Стойност',
		'<i data-target="privacy-4" class="center select-all fa fa-globe" title="'.translate('PRIVACY_GLOBAL', true).'"></i>',
		'<i data-target="privacy-3" class="center select-all fa fa-unlock-alt" title="'.translate('PRIVACY_LOGGED', true).'"></i>',
		'<i data-target="privacy-2" class="center select-all fa fa-smile-o" title="'.translate('PRIVACY_FRIENDS', true).'"></i>',
		'<i data-target="privacy-1" class="center select-all fa fa-eye-slash" title="'.translate('PRIVACY_ME', true).'"></i>'
	);

	$avatar_classes = 'img-responsive pwc_regform_avatar profile-avatar';
	if($user_data['avatar'] == '' || $user_data['avatar'] == DEFAULT_MISSING_AVATAR)
	{
		$user_data['avatar'] = DEFAULT_MISSING_AVATAR;
		$avatar_classes .= ' need_attention';
	}
//	else
//	{
//		$user_data['avatar'] = $user_data['avatar'];
//	}

	if(!has_error_messages()){
?>

<div class="container">
    <div class="row">
		<div class="col-lg-4 col-md-5 col-sm-6">
            <section>
                <img src="<?php echo $user_data['avatar']; ?>" class="<?php echo $avatar_classes; ?>" id="avatar_preview" alt="Image" style="margin-top:20px;">
			</section>
        </div>
        <div class="col-lg-8 col-md-7 col-sm-6">
			<div id="edit_profile_form">
				<form  role="form" action="<?php echo header_link(array(CONTROLLER => 'user', ACTION  => 'edit', ID => look_for_user())); ?>" method="post" autocomplete="off">
					<h3 class="section-title"><?php echo translate('LABEL_UPDATE_PROFILE'); ?></h3>
					<?php render_table($form_data, $columns, 'class="table-w100 table-striped table-hover table-bordered margin-bottom-10"'); ?>
					<input type="hidden" name="avatar" id="avatar_address" value="<?php echo $user_data['avatar']; ?>">
					<input type="hidden" name="privacy[full_name]" id="privacy-full_name" value="<?php echo $user_data['privacy']['full_name']; ?>">
					<button type="reset" class="btn btn-ar btn-danger pull-left"><?php echo translate('BTN_FORM_RESET'); ?></button>
					<button type="submit" class="btn btn-ar btn-success pull-right"><?php echo translate('BTN_FORM_SEND'); ?></button>
				</form>
				<form id="avatarUploadForm" action="<?php echo header_link([CONTROLLER => 'user', ACTION  => 'uploadavatar']); ?>" method="post" enctype="multipart/form-data">
					<input type="hidden" name="layout" value="empty">
					<input type="file" name="avatar_file" class="hidden" id="avatar_file">
				</form>
				<br>
				<!-- Страницата е заредена чрез AJAX.load() метод, затова показваме FAQ секцията тук -->
				<?php echo ($is_ajax ? $faq_section : ''); ?>
			</div>
		</div>
		<!-- Страницата е заредена директно, затова показваме FAQ секцията тук -->
		<?php echo ($is_ajax ? '' : '<div class="col-md-12">'.$faq_section.'</div>'); ?>
    </div>
</div>
<script>
	$(document).ready(function(){
		<?php echo JS_Select_All(); ?>
		<?php echo AJAX_Avatar_Upload(); ?>
		<?php echo JS_Name_Privacy_Change(); ?>
	});
</script>
	<?php } ?>