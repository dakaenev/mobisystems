<?php
	Global $source;
?>
<div class="container">
    <div class="row">
		<div class="col-md-12">
			<form role="form" action="<?php echo header_link(array(CONTROLLER => 'user', ACTION  => 'change-pass', ID => look_for_user())); ?>" method="post" id="change_pass_form">
                <h3 class="section-title"><?php echo translate('LABEL_UPDATE_PASS'); ?></h3>
				<div class="row">
                    <div class="col-md-6">
						<div class="form-group">
							<label for="pass1"><?php echo translate('LABEL_PASS'); ?><sup>*</sup></label>
							<input type="password" class="form-control" id="pass1" name="pass1" value="<?php echo $default_data['pass1']; ?>">
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label for="pass2"><?php echo translate('LABEL_PASS_AGAIN'); ?><sup>*</sup></label>
							<input type="password" class="form-control" id="pass2" name="pass2" value="<?php echo $default_data['pass2']; ?>">
						</div>
					</div>
					<div class="col-md-6">
						<button type="reset" class="btn btn-ar btn-danger btn-block"><?php echo translate('BTN_FORM_RESET'); ?></button>
					</div>
					<div class="col-md-6">
						<button type="submit" class="btn btn-ar btn-success btn-block"><?php echo translate('BTN_FORM_SEND'); ?></button>
					</div>
				</div>
			</form>
		</div>
    </div>
</div>