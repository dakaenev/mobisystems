<?php
	Global $source, $user, $look_for_user, $button_left, $button_right, $show_tabs, $scripts, $friendship, $friend_level;
	//$user['data']['score'] = 10;
	//pre_print($user, true);

	$sex_icons = array(0 => 'female', 1 => 'male', 2 => 'child');

	$allowed_tabs = array(
		array('icon' => 'fa fa-book', 'title' => 'courses'),
		//array('icon' => 'fa fa-gear', 'title' => 'friends'),
		//array('icon' => 'fa fa-gear', 'title' => 'badges'),
		//array('icon' => 'fa fa-gear', 'title' => 'projects'),
		array('icon' => 'fa fa-gear', 'title' => 'settings')
	);

	foreach($allowed_tabs as $tab)
	{
		$tabs_links .= '<li><a href="#'.$tab['title'].'" data-toggle="tab" aria-expanded="false"><i class="'.$tab['icon'].'"></i> <span class="hidden-xs">'.translate('MENU_'.$tab['title']).'</span></a></li>';
	}
	$tabs_links = ($tabs_links != '' ? '<ul class="nav nav-tabs nav-tabs-ar nav-tabs-ar-white" style="margin-top:20px;">'.$tabs_links.'</ul>' : '');

	if(isset($user))
	{
		///pre_print($_SESSION[CURRENT_USER], 1);
?>
<div class="container">
    <div class="row">
		<div class="col-lg-4 col-md-5 col-sm-6 col-xs-12">
            <section>
                <img src="<?php echo ($user['data']['avatar'] != '' ? $user['data']['avatar'] : DEFAULT_MISSING_AVATAR); ?>" class="img-responsive pwc_regform_avatar profile-avatar" id="avatar_preview" alt="Image" style="margin-top:20px;">
				<br>
				<?php echo $button_left; ?>
				<?php echo $button_right; ?>
			</section>
        </div>
        <div class="col-lg-8 col-md-7 col-sm-6 col-xs-12">
			<section>
                <div class="panel panel-primary" style="margin-top:20px; margin-bottom:10px;">
                    <div class="panel-heading">
						<i class="fa fa-terminal hidden-xs"></i> <?php echo translate('INFO'); //' <i class="pull-right fa fa-'.$sex_icons[$user['data']['sex']].'"></i>'; ?>
						<span class="pull-right"># <?php echo $user['data']['user_id']; ?></span>
					</div>
                    <table class="table table-striped">
                        <tbody>
							<tr>
								<th><?php echo translate('LABEL_USER'); ?></th>
								<td><?php echo $user['data']['full_name']; ?></td>
							</tr>
							<tr>
								<th><?php echo translate('PHONE'); ?></th>
								<td><?php echo $user['data']['phone']; ?></td>
							</tr>
							<tr>
								<th><?php echo translate('EMAIL'); ?></th>
								<td><?php echo $user['data']['email']; ?></td>
							</tr>
							<tr>
								<th><?php echo translate('FACEBOOK'); ?></th>
								<td><?php echo $user['data']['facebook']; ?></td>
							</tr>
							<!--
							<tr>
								<th><?php echo translate('PUBLIC_PROFILE'); ?></th>
								<td><i>скоро</i> :-) <a href="#"><?php echo translate('COMMING_SOON'); ?></a></td>
							</tr>
							 -->
							<tr>
								<th><?php echo translate('BORN_DATE'); ?></th>
								<td><?php echo $user['data']['born_date']; ?></td>
							</tr>
							<tr>
								<th><?php echo translate('REGISTERED_ON'); ?></th>
								<td><?php echo $user['data']['reg_date']; ?></td>
							</tr>
							 <tr>
								<th><?php echo translate('LAST_LOGIN'); ?></th>
								<td><?php echo $user['data']['last_login']; ?></td>
							</tr>
							<!--
							<tr>
								<th>Ранг</th>
								<td>
									<a href="#"><?php echo translate('ROLE_'.$user['data']['role_id']); ?></a>
								</td>
							</tr>
							-->
						</tbody>
					</table>
                </div>
				<?php
				$user_contribution = '';
				$contribution_modal = [
					'footer' => '<button type="button" class="btn btn-ar btn-primary" data-dismiss="modal" aria-hidden="true">Разбрах</button>',
					'body' => 'Приносът се изчислява като процент спрямо максималния възможен. <br>Например: ако сте попълнили 5 полета от 10 възможни това е 50% принос, <br>ако обаче броя на възможните полета нарасне до 20, то 5 полета от 20 максимално възможни, са 25% принос',
					'title' => translate('YOUR_CONTRIBUTION_MODAL_TITLE'),
					'link' => translate('YOUR_CONTRIBUTION_MODAL_LINK')
				];

				$user_contribution .= '<div class="progress" style="margin-bottom:0;">';
				$user_contribution .= '<div class="progress-bar" role="progressbar" aria-valuenow="'.$user['data']['score'].'" aria-valuemin="0" aria-valuemax="100" style="width:'.$user['data']['score'].'%;">';
				$user_contribution .= $user['data']['score'].'%</div></div>';

				if(true)
				{
					echo translate('YOUR_CONTRIBUTION').add_modal('contibution_modal', $contribution_modal);
					echo $user_contribution;
				}
				?>
            </section>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
		<?php
		if(!$show_tabs) { echo '&nbsp;'; } // Гледаме чужд профил, затова НЕ показваме табовете
		else {	echo $tabs_links;
		?>
			<div class="tab-content" style="margin-bottom: 20px;">
				<div class="tab-pane" id="courses">
					<span data-src="<?php echo header_link(array(CONTROLLER => 'user', ACTION => 'mycourses', ID => $look_for_user)); ?> #courses_list">Loading...</span>
				</div>
				<div class="tab-pane" id="friends">FRIENDS Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, at, laboriosam, possimus, nam rem quia reiciendis sit vel totam id eum quasi aperiam officiis omnis ipsum quo praesentium quaerat unde mollitia maiores. Dignissimos, deleniti, eos, quibusdam quae voluptatibus obcaecati voluptatum iure quas voluptates cupiditate incidunt voluptate dolorem delectus exercitationem earum?</div>
				<div class="tab-pane" id="badges">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, at, laboriosam, possimus, nam rem quia reiciendis sit vel totam id eum quasi aperiam officiis omnis ipsum quo praesentium quaerat unde mollitia maiores. Dignissimos, deleniti, eos, quibusdam quae voluptatibus obcaecati voluptatum iure quas voluptates cupiditate incidunt voluptate dolorem delectus exercitationem earum?</div>
				<div class="tab-pane" id="projects">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt, ipsam, distinctio, ea quas quam mollitia enim dolorem aliquam laboriosam exercitationem ullam iste quis possimus aut atque est beatae temporibus impedit deleniti explicabo ratione esse molestias maiores minima odit? Tempore, omnis, possimus praesentium minus iusto laboriosam vitae officiis deserunt voluptate odio.</div>
				<div class="tab-pane" id="settings">
					<div class="vertical-tabs-left">
						<div class="vertical-tab-list min-width-xs-0">
							<ul class="nav">
								<li><a href="#profile" data-toggle="tab"><i class="fa fa-user"></i><span class="hidden-xs"> Профил</span></a></li>

								<li><a href="#passwrd" data-toggle="tab"><i class="fa fa-key"></i><span class="hidden-xs"> Парола</span></a></li>

								<!--
								<li><a href="#gamepad" data-toggle="tab"><i class="fa fa-gamepad"></i><span class="hidden-xs"> Интереси</span></a></li>
								<li><a href="#privacy" data-toggle="tab"><i class="fa fa-eye-slash"></i><span class="hidden-xs"> Поверителност</span></a></li>
								<li><a href="#general" data-toggle="tab"><i class="fa fa-gears"></i> Други</a></li>
								<li><a href="#messages2" data-toggle="tab"><i class="fa fa-envelope-o"></i> Съобщения</a></li>
								<li><a href="#settings2" data-toggle="tab"><i class="fa fa-gear"></i> Други</a></li>
								-->

							</ul>
						</div>
						<div class="tab-content" style="width:100%; padding: 0 20px;">
							<div class="tab-pane" id="profile">
								<span data-src="<?php echo header_link(array(CONTROLLER => 'user', ACTION => 'edit', ID => $look_for_user)); ?> #edit_profile_form">Loading...</span>
							</div>
							<div class="tab-pane" id="privacy">
								Privacy<br>
								<i class="fa fa-eye"></i>Всички <i class="fa fa-smile-o"></i>Приятели <i class="fa fa-eye-slash"></i>Никой
							</div>
							<div class="tab-pane" id="gamepad">
								GamePad Игри и интереси
							</div>
							<div class="tab-pane" id="passwrd">
								<span data-src="<?php echo header_link(array(CONTROLLER => 'user', ACTION => 'change-pass', ID => $look_for_user)); ?> #change_pass_form">Loading...</span>
							</div>
							<div class="tab-pane" id="general">
								General Не знам какво да пиша тук
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
		</div>
    </div>
</div>
<?php echo ($look_for_user == $user_id ? Show_Mails_Modal() : Create_Mails_Modal($look_for_user)); ?>
<script>
	$(document).ready(function(){
		// Select first tab no matter who is it
		$('.nav li:nth-child(1) a').trigger('click');
		$('[data-src]').each(function (){console.log('load...'+$(this).data('src'));
			if($(this).parent().hasClass('tab-pane')){
				var baseURL = $(this).data('src').split(' ');
				$(this).parent().prepend('<a target="_blank" href="'+baseURL[0]+'" class="open_in_new_tab"><?php echo translate('OPEN_IN_NEW_TAB'); ?> <i class="fa fa-external-link"></i></a>');
			}
			$(this).load($(this).data('src'));
		});

		$('.size-toggle').click(function(){
			$(this).parent().parent().parent().toggleClass('modal-max');
		});

		$("#btn_friend_toggle").mouseover(function(){
			$(this).html("<?php echo $friendship[$friend_level]['h_text']; ?>");
			$(this).removeClass("<?php echo $friendship[$friend_level]['n_class']; ?>");
			$(this).addClass("<?php echo $friendship[$friend_level]['h_class']; ?>");
		});

		$("#btn_friend_toggle").mouseout(function(){
			$(this).html("<?php echo $friendship[$friend_level]['n_text']; ?>");
			$(this).removeClass("<?php echo $friendship[$friend_level]['h_class']; ?>");
			$(this).addClass("<?php echo $friendship[$friend_level]['n_class']; ?>");
		});

		$('body').on('click', '.btn_cancel_course', function(){
			var form_element = $(this).prev();

			if(form_element.hasClass('hidden')){
				form_element.removeClass('hidden');
			}else{
				if($('input[name=why]:checked').val() === undefined){
					alert('Моля изберете причината!');
				}else{
					var answer = confirm('ВНИМАНИЕ - Това е необратимо!!!\n\tНаистина ли искате да се откажете от курса?');
					if(answer){
						form_element.submit();
					}
				}
			}
		});

		<?php echo implode('', $scripts); ?>
		<?php echo JS_Name_Privacy_Change(); ?>
		<?php echo AJAX_Avatar_Upload(); ?>
		<?php echo JS_Select_All(); ?>
	});

	<?php //JS_Location_Functions(); ?>

	//getLocation();
</script>
<script type="text/javascript">
			$(document).ready(function(){


				//$(".filter_courses").click(function(){
				//	$("."+$(this).val()).parent().parent().prop("hidden", !$(this).prop( "checked" )); // !!!
				//});

				//$("#course_rejected").trigger("click");
				//$("#course_finished").trigger("click");

				//$("#btn_create_mail").click(function(){
				//	$("#MessagesBody").load("<?php echo header_link(array(CONTROLLER => 'mail', ACTION => 'create')); ?> #mail_form");
				//});

				//$("#btn_create_alert").click(function(){
				//	$("#MessagesBody").load("<?php echo header_link(array(CONTROLLER => 'mail', ACTION => 'create', 'mode' => 'alert')); ?> #mail_form");
				//});
			});

		//'.(true ? '$("#btn_alerts").html(\''.$btn_alerts.'\');' : '').'
		//'.(true ? '$("#div_alerts").html(\''.str_ireplace(PHP_EOL, ' ', $div_alerts).'\');' : '').'
	</script>
<?php } ?>
<!--
Админ - Вижда всичко!
Собственик - Вижда и редактира всичко
Друг - Вижда каквото му е разрешено...
Гост - Не вижда нищо...

Приоритети:
1. Импорт на потребители
2. Регистрация, Логин, Логаут
3. Абоут ус, хоум пейдж
4. Контакти, Съобщения
5. Приятели, Интереси
6. Плащания, Каса, Графика
7. Видимост, Редакция на профил
8. Мейл темплейти, Забравена парола
9. User Journey
10. Пояснителни страници
-->