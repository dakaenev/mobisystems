<?php Global $is_admin_user, $user_id; ?>
<?php if(!isset($_POST['submit']))  { ?>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
<?php
	if($is_admin_user)
	{
		echo '<h1>Декларация за безопасност на труда</h1>';

		if(isset($_REQUEST['send_to']))
		{
			$recipients = $_REQUEST['send_to'];
		}
		else
		{
			$recipients = array();
		}

		$recipients_list = '';
		$recipients_count = count($recipients);

		database_open();

		if($recipients_count > 0)
		{
			$existing_users = to_assoc_array(exec_query('SELECT user_id, first_name, last_name FROM users WHERE user_id IN('.implode(',', $recipients).')'), 'user_id');

			foreach($recipients as $recipient_id => $recipient_mail)
			{
				$label_style = 'class="col-lg-3 col-md-4 col-sm-6 col-xs-12" style="border:1px dotted silver;';

				if(isset($existing_users[$recipient_mail]))
				{
					$user = $existing_users[$recipient_mail];
					$trans_name = transliterate(null, $user['first_name'].' '.$user['last_name']);
					$checkbox_element = '<input type="checkbox" class="send_to_chk" name="send_to[-'.$recipient_id.']" value="'.$trans_name.'" checked="checked" /> ';
					$checkbox_element .= '<a href="'.header_link(array(CONTROLLER => 'user', ACTION => 'view', ID => $recipient_id)).'"><span class="glyphicon glyphicon-user"></span></a> '.$trans_name;
					$label_style .= '" title="'.$user['first_name'].' '.$user['last_name'].'"';
				}

				$recipients_list .= '<label '.$label_style.'>'.$checkbox_element.'</label>';
			}
		}
		database_close();
		echo '<form method="post" id="mail_form"><input type="hidden" name="layout" value="empty" /><table class="table table-striped table-bordered table-condensed table-responsive">';


		echo '<tr><th><details '.($recipients_count < 4 ? '' : 'open="open" ').'style="width: 100%;"><summary>'.($is_admin_user ? '<input type="submit" name="submit" value="Изпрати" /><input type="button" value="+" id="btn_add" style="margin-right:5px;" />' : '').' Получатели (<span id="recipients_count">'.$recipients_count.'</span>):</summary>'.$recipients_list.'</details></th></tr>';
		echo '</table>';
		?>
			<script>
				$(document).ready(function(){
					$('#btn_add').click(function(){
						var mail_value = prompt('Въведете име и фамилия:');
						if(mail_value.indexOf(' ') != -1)
						{
							$(this).closest('summary').after('<label class="col-lg-3" style="border:1px dotted silver;"><input type="checkbox" class="send_to_chk" checked="checked" name="send_to[]" value="' + mail_value + '" /> ' + mail_value + '</label>');
							$('#recipients_count').text($('.send_to_chk:checked').length);
						}
						else
						{
							alert('Некоректно име! Въведете име и фамилия! \nВъведохте: ' + mail_value);
						}
					});



					$('details').on('click', 'label', function(){
						$('#recipients_count').text($('.send_to_chk:checked').length);
					});

				});
			</script>
		<?php
		echo '</form>';
	}
	else
	{
		page_not_access();
	}
?>
		<br />
		</div>
	</div>
</div>
<?php } ?>