<?php database_open(); ?>
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="user_edit_div">
		<?php
			Global $is_admin_user, $source, $courses_array, $settings_array, $questions_array, $load_user_id, $user_info, $is_super_admin, $story_data;

			if(true)
			{
				if( 0 == count($story_data))
				{
					// Set  Default Data
					$default_data = array(
						'user_id' => $load_user_id,
						'story_data' => '',
						'story_type' => 'week',
						'created_on' => date('Y-m-d'),
						'published_on' => '0000-00-00'
					);

					$story_id = insert_query('stories', $default_data);
					$story_data = get_user_story($load_user_id, 'week');
				}

				if(Only_One_Exists($story_data))
				{
					$story_data = reset($story_data);
					$story_data['is_editable'] = ($story_data['published_on'] == '' || $story_data['published_on'] == '0000-00-00') || $is_admin_user;
				}
				else
				{
					add_warning_message('Story data is corrupted!');
				}

				if(have_messages())
				{
					show_all_messages();
				}

				if(!has_error_messages())
				{
					// Prepare courses dropdown
					$current_date = date('Y-m-d');
					if(!$is_super_admin)
					{
						$courses = get_all_courses('start_date <= "'.$current_date.'" AND "'.$current_date.'" <= end_date ORDER BY course_id DESC', 'course_id, course_name');
						$courses_dropdown = ReturnDropDown($courses, $default_course);

					}
					else
					{
						$archives = '';
						$currents = '';
						$upcoming = '';

						foreach(get_all_courses() as $course)
						{
							$course['end_date'] = strtotime($course['end_date']) + (0 * 7 * 24 * 3600); // курса се брои за активен до 2 седмици след неговия край ;-)
							if($course['start_date'] <= date('Y-m-d') && strtotime('now') <= $course['end_date']) // ???
							{
								$which_courses = 'currents';
							}
							else
							{
								if(date('Y-m-d') > $course['start_date'])
								{
									$which_courses = 'archives';
								}
								else
								{
									$which_courses = 'upcoming';
								}
							}

							$$which_courses .= '<option value="'.$course['course_id'].'"'.($source['course_id'] == $course['course_id'] ? ' selected="selected"' : '').'>'.$course['course_id'].'. '.$course['course_name'].'</option>';
							// Тук използваме $$ за да укажем, коя променлива искаме да променяме. Така избягваме 1 if, или пък двойното писане на един и същи код.
						}

						$archives = '<optgroup label="Архивни">'.$archives.'</optgroup>';
						$currents = '<optgroup label="Текущи ">'.$currents.'</optgroup>';
						$upcoming = '<optgroup label="Бъдещи ">'.$upcoming.'</optgroup>';
						$courses_dropdown = $upcoming.$currents.$archives;
					}

					$courses_dropdown = '<option value="0"> - Изберете курс - </option>'.$courses_dropdown;

					$roles_array = array('0' => '- Изберете -', 'student' => 'Студент', 'candidate' => 'Кандидат', 'rejected' => 'Отхвърлен', 'teacher' => 'Лектор', 'observer' => 'Наблюдател' );
					$forms_array = array('0' => '- Изберете обучение -', '1' => 'Присъствено', '2' => 'Онлайн', '-1' => 'Странна' );
					$erase_button = '<button type="button" class="erase_course courses_set_row" [{cm_id}]><span class="glyphicon glyphicon-minus"></span></button>';

					echo '<form id="edit_form" name="edit_form" method="post">';

					if($user_info['yahoo'] != '' && is_numeric($user_info['yahoo']) && $is_admin_user)
					{
						echo '<fieldset style="margin-top:20px;"><legend>Лична информация</legend><span id="personal_data"></span></fieldset>';
					}

					if( isset($questions_array) ) // Show Week Story
					{
						$public_on_field = ($is_admin_user ? '<input type="date" name="published_on" value="'.$story_data['published_on'].'" />' : '');
						echo '<fieldset style="margin-top:20px;"><legend>Стани курсист на седмицата! Попълни своите данни сега!<span id="week_story_data" style="float:right;">'.$public_on_field.'</span></legend>';
						if($story_data['is_editable'])
						{
							$story_data = unserialize($story_data['story_data']);
							foreach($questions_array as $question)
							{
								$default_type = ('URL към твоя снимка' == $question['question_title'] ? 'url' : 'text');
								//$default_hint = ('URL към твоя снимка' == $question['question_title'] ? $avatar_url : '');
								echo '<b>'.$question['question_title'].'</b><input type="'.$default_type.'" class="form-control" name="week_question['.$question['question_id'].']" value="'.htmlspecialchars(stripcslashes($story_data[$question['question_id']])).'" />';
							}
						}
						else
						{
							echo 'Вашата история вече е публикувана и можете да я видите <a href="'.site_url('./our-stories/?story_id='.$story_data['story_id']).'">тук</a>!';
						}
						echo '</fieldset>';
					}

					if( isset($courses_array) ) // Show Courses Table
					{
						$courses_table = array();
						$columns = array(
							array('text' => 'ID', 'attr' => 'width="10"' ),
							array('text' => 'Курс'),
							array('text' => 'Статус', 'attr' => 'width="120"'),
							array('text' => 'Обучение', 'attr' => 'width="190"'),
							array('text' => 'Присъствия в залата', 'attr' => 'width="320"'),
							array('text' => '<button type="button" id="add_course"><span class="glyphicon glyphicon-plus"></span></button>', 'attr' => 'width="10"'),
						);

						//pre_print($courses_array);
						if(count($courses_array))
						{
							$user_payments = getVipCoursesPerUser($load_user_id);
							$user_certificates = getCertificates(null, $load_user_id, 'course_id');
							//pre_print($user_certificates);

							foreach($courses_array as $course)
							{
								$course_form = -1;

								if($course['form_id'] == 1 && $course['group_number'] == 1)
								{
									$course_form = 1;
								}

								if($course['form_id'] == 2 && $course['group_number'] == 0)
								{
									$course_form = 2;
								}

								if(isset($user_certificates[$course['course_id']])) // Никой не може да трие курсист ако има сертификат за даден курс.
								{
									$can_erase = false;
									$course_name_style = ' style="font-weight: bold;"';

									if(file_exists($user_certificates[$course['course_id']]['file_path']))
									{
										$course_certificate_icon = '<a target="result_frame" href="'.site_url($user_certificates[$course['course_id']]['file_path']).'"><span title="" class="glyphicon glyphicon-certificate wa-certificate-icon-0"></span></a>';
									}
									else
									{
										$course_certificate_icon = '<span title="" class="glyphicon glyphicon-certificate wa-certificate-icon-1"></span>';
									}

									$erase_code = '<span class="courses_set_row" style="text-align:center; display:block;">'.$course_certificate_icon.'</span>';
								}
								else
								{
									$course_name_style = '';
									$course_certificate_icon = '';

									if(!$is_super_admin) // Супер администратор може да трие независимо от датата
									{
										$can_erase = true;
										$erase_code = str_ireplace('[{cm_id}]', 'data-cm_id="'.$course['id'].'"', $erase_button);
									}
									else // Обикновен администратор може да трие само ако курса не е приключил
									{
										if(date('Y-m-d') < $course['end_date'])
										{
											$can_erase = true;
											$erase_code = str_ireplace('[{cm_id}]', 'data-cm_id="'.$course['id'].'"', $erase_button);
										}
										else
										{
											$can_erase = false;
											$erase_code = '<span class="courses_set_row">&nbsp;</span>';
										}
									}
								}

								$tmp = array();
								$tmp[] = $course['course_id'];
								$tmp[] = ($course['end_date'] < date('Y-m-d') ? ' <i'.$course_name_style.'>'.$course['course_name'].'</i>' : $course['course_name']).(isset($user_payments[$course['course_id']]) ? '<span class="glyphicon glyphicon-usd red pull-right"></span>' : '');
								$tmp[] = '<select name="cm['.$course['id'].'][role]"'.($course['end_date'] < date('Y-m-d') ? ' disabled="disabled"' : '').'>'.ReturnDropDown( $roles_array, $course['role']).'</select>';
								$tmp[] = '<select name="cm['.$course['id'].'][form]"'.($course['end_date'] < date('Y-m-d') ? ' disabled="disabled"' : '').'>'.ReturnDropDown($forms_array, $course_form).'</select>';
								$tmp[] = showActivityTable($load_user_id, $course['course_id']);;
								//$tmp[] = (!$can_erase ? '<span class="courses_set_row">&nbsp;</span>' : str_ireplace('[{cm_id}]', 'data-cm_id="'.$course['id'].'"', $erase_button) );
								$tmp[] = $erase_code;
								$courses_table[] = $tmp;
							}
						}

						//echo '<fieldset style="margin-top:20px;"><legend>Курсове</legend><details><summary>Vix</summary>';
						//render_table($courses_table, $columns, 'id="courses_set" class="table table-striped table-bordered table-condensed table-responsive"');
						//echo '</details></fieldset>';

						echo '<fieldset style="margin-top:20px;"><legend>Курсове</legend>';
						render_table($courses_table, $columns, 'id="courses_set" class="table table-striped table-bordered table-condensed table-responsive"');
						echo '</fieldset>';
					}

					if(Only_One_Exists($settings_array))
					{
						$settings_array = reset($settings_array);

						echo '<fieldset><legend>Настройки</legend>';
						echo '<div class="col-lg-12"><label for="user_descr">Личностна характеристика</label><textarea id="user_descr" name="settings[user_descr]" style="width:100%; height:150px; border:1px solid silver;">'.stripcslashes($settings_array['user_descr']).'</textarea></div>';
						echo '<div class="col-lg-3"><label for="user_caste">Каста</label> <select name="settings[user_caste]" id="user_caste" class="form-control" style="width:50%; display:inline;">'.ReturnDropDown(array('1' => 'White', '0' => 'Neutral', '-1' => 'Black' ), $settings_array['user_caste']).'</select></div>';
						echo '<div class="col-lg-3"><label for="show_events">Показвай събития</label> <input type="hidden" name="settings[show_events]" value="0" /><input type="checkbox" name="settings[show_events]" id="show_events" value="1"'.($settings_array['show_events'] ? ' checked="checked"' : '').' /></div>';
						echo '<div class="col-lg-3"><label for="show_jokes">Показвай шеги</label> <input type="hidden" name="settings[show_jokes]" value="0" /><input type="checkbox" name="settings[show_jokes]" id="show_jokes" value="1"'.($settings_array['show_jokes'] ? ' checked="checked"' : '').' /></div>';
						echo '<div class="col-lg-3"><label for="show_hints">Показвай уведомления</label> <input type="hidden" name="settings[show_hints]" value="0" /><input type="checkbox" name="settings[show_hints]" id="show_hints" value="1"'.($settings_array['show_hints'] ? ' checked="checked"' : '').' /></div>';
						echo '</fieldset>';
					}
					else
					{
						//echo '<details><summary>Unknown problem while loading settings</summary><pre>'.($is_admin_user ? print_r($settings_array, true) : '').'</pre></details>';
					}

					// Use NULL as first param to return array!
					$current_period = get_current_period(null);

					echo '<fieldset style="margin-top:20px;"><legend>Статистика</legend>';
					echo 'Проверени домашни общо: <a href="'.header_link(array(CONTROLLER => 'homeworks', ACTION => 'index', VIEW => 'evaluated', 'teacher_id' => $load_user_id)).'">'.count(get_user_homeworks('teacher_id = '.$load_user_id)).'</a><br />';
					echo 'Проверени този семестър: <a href="'.header_link(array(CONTROLLER => 'homeworks', ACTION => 'index', VIEW => 'evaluated', 'teacher_id' => $load_user_id, 'score_on[min]' => $current_period['begin_time'], 'score_on[max]' => $current_period['end_time'])).'">'.count(get_user_homeworks('teacher_id = '.$load_user_id.' AND (score_on >= '.$current_period['begin_time'].' AND '.$current_period['end_time'].' >= score_on)')).'</a><br />';
					echo 'Последно онлайн: <a href="'.header_link(array(CONTROLLER => 'logs', ACTION => 'index', 'user_id' => $load_user_id)).'">'.my_date_format(getUserLastOnline($load_user_id)).'</a><br />';
					echo '</fieldset>';

					echo '<br />';
					echo '<a href="#" onclick="document.getElementById(\'edit_form\').reset();"><button><img src="'.DIR_IMAGES.'ui/r.png" height="22"></button></a>';
					echo '<a href="#" onclick="document.getElementById(\'edit_form\').submit();"><button type="submit"><img src="'.DIR_IMAGES.'ui/1.png"></button></a>';
					echo '</form><br />';
				}

				clear_all_messages();
			}
			else
			{
				page_not_access();
			}
		?>
		</div>
	</div>
</div>
<script type="text/javascript">
	function change_count(){
		$("#courses_set").find('.items_count').text( "Общ брой: " + ($(".courses_set_row").length) );
	}

	$(document).ready( function () {
		var new_course_index = 0;
		$('#personal_data').load('<?php echo header_link(array(CONTROLLER => 'user', ACTION => 'info', ID => $user_info['yahoo'])); ?>' + ' #user_info');

		// Тук трябва да е с ON а не просто с CLICK, за да работи този код и за новосъздадените динамично бутони
		$('#courses_set').on('click','.del_course', function() {
			$(this).parent().parent().remove();
			change_count();
		});

		$('#courses_set').on('click','.erase_course', function() {
			var val = $(this).data('cm_id');
			var hdn = '<input type="hidden" name="cm['+val+'][delete]" value="'+val+'" />';
			var btn = '<button type="button" class="restore_course courses_set_row" data-cm_id="'+val+'"><span class="glyphicon glyphicon-ban-circle"></span></button>';

			$(this).parent().parent().css({'color':'red', 'text-decoration' : 'line-through'});
			$(this).parent().html(hdn + btn);
		});

		$('#courses_set').on('click','.restore_course', function() {
			var erase_button = '<?php echo $erase_button; ?>';
			$(this).parent().parent().css({'color':'black', 'text-decoration' : 'none'});
			$(this).parent().html(erase_button.replace('[{cm_id}]', 'data-cm_id="' + parseInt($(this).data('cm_id')) + '"'));
		});

		$('#add_course').click(function(){

			var code;

			code = '<tr>';
			code = code + '<td>---</td>';
			code = code + '<td><select name="new_course[' + new_course_index + '][course]"><?php echo $courses_dropdown; ?></select></td>';
			code = code + '<td><select name="new_course[' + new_course_index + '][role]"><?php RenderDropDown($roles_array, '0'); ?></select></td>';
			code = code + '<td><select name="new_course[' + new_course_index + '][form]"><?php RenderDropDown($forms_array, '0'); ?></select></td>';
			code = code + '<td><i>За да видите датите, запазете промените!</i></td>';
			code = code + '<td><button type="button" class="del_course courses_set_row"><span class="glyphicon glyphicon-minus"></span></button></td>';
			code = code + '</tr>';

			//$('#courses_set table tbody tr:last').before(code);
			$('#courses_set tr:last').before(code);
			new_course_index++;
			change_count();

			return false;
		});

		$('#courses_set').on('click','.refresh_visibility', function() {

			var parent_element = $(this).parent();
			var current_html = parent_element.html();

			parent_element.html('<span class="glyphicon glyphicon-time"></span>');
			$.ajax({
				dataType: 'json',
				url: "<?php echo header_link(array(CONTROLLER => 'logs', ACTION => 'SetPresentedUsers', 'layout' => 'empty', ID => '')); ?>" + $(this).data('id'),
				success: function(data){
					console.log(data);

					if(data.users_count == 0){
						parent_element.html(current_html);
					}else{
						parent_element.html('');

						if(data.users_list.indexOf(',<?php echo $load_user_id; ?>,') == -1)
						{
							parent_element.css('background-color', 'red');
						}
						else
						{
							parent_element.css('background-color', 'lime');
						}
					}
				}
			});
		});
	});
</script>
<?php database_close(); ?>

