<?php database_open(); ?>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="users_main_div">
			<form method="post" action="" id="import_form">
				<div class="col-lg-4">
					<label for="course_id">
						Добави към курс
					</label>

					<select class="form-control" name="course_id<?php echo (isset($source['course_id']) && is_array($source['course_id']) ? '[]" multiple="multiple" size="1' : ''); ?>">
						<option value="">- Без Филтър -</option>
						<?php

							if(!$is_admin_user) // This is Observer
							{
								$courses_scope = 'course_id IN ('.implode(', ', getObservedCourses()).')';
							}
							else
							{
								$courses_scope = null;
							}

							$archive = '';
							$current = '';
							foreach(get_all_courses($courses_scope) as $course)
							{
								$course['end_date'] = strtotime($course['end_date']) + (0 * 7 * 24 * 3600); // курса се брои за активен до 2 седмици след неговия край ;-)
								$which_courses = ($course['start_date'] <= date('Y-m-d') && strtotime(date('Y-m-d')) <= $course['end_date'] ? 'current' : 'archive');
								$$which_courses .= '<option value="'.$course['course_id'].'"'.($source['course_id'] == $course['course_id'] || in_array($course['course_id'], $source['course_id']) ? ' selected="selected"' : '').'>'.$course['course_id'].'. '.$course['course_name'].'</option>';
								// Тук използваме $$ за да укажем, коя променлива искаме да променяме. Така избягваме 1 if, или пък двойното писане на един и същи код.
							}

							$archive = '<optgroup label="Архивни">'.$archive.'</optgroup>';
							$current = '<optgroup label="Текущи">'.$current.'</optgroup>';

							echo $current.$archive;
						?>
					</select>
				</div>
				<div class="col-lg-4">
					<label for="course_id">
						Премахни от курс
					</label>

					<select class="form-control" name="course_id<?php echo (isset($source['course_id']) && is_array($source['course_id']) ? '[]" multiple="multiple" size="1' : ''); ?>">
						<option value="">- Без Филтър -</option>
						<?php

							if(!$is_admin_user) // This is Observer
							{
								$courses_scope = 'course_id IN ('.implode(', ', getObservedCourses()).')';
							}
							else
							{
								$courses_scope = null;
							}

							$archive = '';
							$current = '';
							foreach(get_all_courses($courses_scope) as $course)
							{
								$course['end_date'] = strtotime($course['end_date']) + (0 * 7 * 24 * 3600); // курса се брои за активен до 2 седмици след неговия край ;-)
								$which_courses = ($course['start_date'] <= date('Y-m-d') && strtotime(date('Y-m-d')) <= $course['end_date'] ? 'current' : 'archive');
								$$which_courses .= '<option value="'.$course['course_id'].'"'.($source['course_id'] == $course['course_id'] || in_array($course['course_id'], $source['course_id']) ? ' selected="selected"' : '').'>'.$course['course_id'].'. '.$course['course_name'].'</option>';
								// Тук използваме $$ за да укажем, коя променлива искаме да променяме. Така избягваме 1 if, или пък двойното писане на един и същи код.
							}

							$archive = '<optgroup label="Архивни">'.$archive.'</optgroup>';
							$current = '<optgroup label="Текущи">'.$current.'</optgroup>';

							echo $current.$archive;
						?>
					</select>
				</div>
				<div class="col-lg-4">
					<label for="course_id">
						Импортиране на файл
					</label>
					<input type="submit" value="Import" class="pull-right">
					<input type="file" name="users_file">
				</div>
			</form>
		</div>
	</div>
</div>