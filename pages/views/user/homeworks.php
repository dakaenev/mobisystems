<?php
	Global $user_id, $course, $now;

	if(isset($course) && is_array($course) && !empty($course) > 0)
	{
		//Pre_print($course);

		$link_class = 'course_title_active';
		$selected_course_title = $course['course_name'];
		$selected_course_url = $course['course_after_url'];
		$selected_course_id = $course['course_id'];
		$selected_course_description = $course['course_description'];
		$selected_course_start = $course['start_date'];
		$selected_course_end = $course['end_date'];

		$script_text = '';

		if(isset($source['user_id']) && $is_admin_user)
		{
			$load_user_id = $source['user_id'];
			$script_text = '<script>$(document).ready(function(){ $(".decrypt_link").click(function() { if($(this).hasClass( "decrypt_link" )){ $(this).load("'.header_link(array(CONTROLLER => 'logs', ACTION => 'decrypt')).'&'.VIEW.'=" + $(this).text() + " #decrypted_value", function(responseTxt, statusTxt, xhr){ if(statusTxt == "success" && $.isNumeric($(this).text())){ $(this).removeClass("decrypt_link"); $(this).attr("href", "'.header_link(array(CONTROLLER => 'user', ACTION => 'view')).'&'.ID.'=" + $(this).text()); $(this).attr("target", "_blank"); } }); } }); }); </script>';
		}
		else
		{
			$load_user_id = $user_id;
		}
?>
<!--
<header class="main-header" style="padding-top:0px; background-color:#F1F1F1 !important; border-bottom: 1px solid #ddd !important; margin-bottom:27px !important;">
    <div class="">
		<div class="container">
			<h3 class="page-title _title">
				<?php echo $selected_course_title; ?>
			</h3>

			<ol class="breadcrumb pull-right">
				<?php if(function_exists('bcn_display_list')) {
					//bcn_display_list();
				} ?>
			</ol>
			<div>

			<ul class="nav nav-tabs nav-tabs-ar">
				<li class="active"><a href="#home3" data-toggle="tab">Материали</a></li>
				<li><a href="#homeworks" data-toggle="tab">Домашни</a></li>
			</ul>
			</div>

		</div>
	</div>
	<div class="container" style="margin-top:16px !important;">
		<a href="<?php echo SITE_URL.$course['course_after_url'].(isset($source['user_id']) && $is_admin_user ? '?user_id='.$source['user_id'] : ''); ?>" class="fake_tab">Материали</a>
		<a href="#" class="fake_tab_active" style="margin-left:40px !important;">Домашни</a>
		<?php echo (is_assistant($load_user_id) ? '<a class="" href="'.link_to( array('OPTION_SET_CLASS' => false, CONTROLLER => 'homeworks', ACTION => 'index') ).'"> <button>Проверка</button> </a>': ''); ?>
	</div>
</header>
 -->
<div class="">
	<div class="row">
		<?php
			Global $hw_array, $columns, $is_logged_user, $course_title, $user_id;

			if($is_logged_user)
			{
				if(have_messages())
				{
					show_all_messages();
					clear_all_messages();
				}

				//echo '<div class="col-md-12 hidden-xs"><h1 class="">'.$selected_course_title.'</h1></div>';
			?>
			<!--
				<div class="col-md-4" style="margin-top:6px !important; margin-bottom:20px !important;">
					<section>
						<div class="content-box box-default col-md-6 col-xs-6  _padding" style="border-right:1px solid white !important;">
							<span class="_number"><?php echo date('d/m/Y', strtotime($selected_course_start)); ?></span>
							<p class="_text">начало на курса</p>
						</div>
						<div class="content-box box-default col-md-6 col-xs-6  _padding" style="border-left:1px solid white !important;">
							<span class="_number"><?php echo date('d/m/Y', strtotime($selected_course_end)); ?></span>
							<p class="_text">край на курса</p>
						</div>
					</section>
				</div>
				<div class="col-md-9">&nbsp;</div>
			-->
			<?php
				echo '<div class="col-md-12">';
				//echo '<span class=" hidden-xs">'.$selected_course_description.'</span>';
				//pre_print($courses_array);
				echo '<div class="table-responsive" id="homeworks_table">';
				render_table($hw_array, $columns, ' class="table "', 'homeworks');
				echo '</div></div>';

				echo $script_text;
			}
			else
			{
				echo '<div class="col-md-12">';
				page_not_access();
				echo '</div>';
			}
		?>
	</div>
</div>
<div class="modal fade" id="NonActiveHWModal" tabindex="-1" role="dialog" aria-labelledby="NonActiveHWModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Неактивна домашна</h4>
			</div>
			<?php
				$ip = $_SERVER['REMOTE_ADDR'];
				$add_ip = ($is_admin_user ? ' <a href="'.header_link(array(CONTROLLER => 'logs', ACTION => 'AddIpAddress', 'ip' => $ip)).'">Добави IP адреса</a>' : '');
			?>
			<div class="modal-body">
				<span style="font-family: 'Open Sans' !important; font-size: 13px !important; font-weight: 400 !important; color: #000000 !important;">
					<b>Качването на решение за тази домашна <?php echo (/* !isset($course['candidate_until']) || $course['candidate_until'] == '0000-00-00 00:00:00' */ false ? 'все още не е започнало.' : 'е приключило.'); ?></b><br>
					<b>Вече е: <?php echo date('d.m.Y H:i:s',$now).', а Вие сте с IP: '.$ip.$add_ip; ?></b><br /><br />
				</span>
			</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>
<?php
	}
	else
	{
?>
<div class="container">
	<div class="row">
	<?php
		page_not_access();
	?>
	</div>
</div>
<?php
	}
?>