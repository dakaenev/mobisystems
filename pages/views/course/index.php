<?php
	Global $courses, $columns, $is_admin_user, $user_id, $source, $_connection, $query_string, $ALLOWED_ORDERS;

	database_open();
	$search_params = remove_url_elements(array(CONTROLLER, ACTION), $_SERVER['QUERY_STRING']);

	//pre_print($courses);
	$courses_array = ''; 
	$courses_array .= '<h1 class="center hidden-sm hidden-md hidden-lg">Промоция: -10% <br>само до {COURSES_BEGIN_DATE}</h1>';
	$courses_array .= '<h1 class="center hidden-xs hidden-md hidden-lg">Запишете се до {COURSES_DISCOUNT_UNTIL} с 10% отстъпка!</h1>';
	$courses_array .= '<h1 class="center hidden-xs hidden-sm hidden-lg">Получавате -10% отстъпка ако се запишете до {COURSES_DISCOUNT_UNTIL}</h1>';
	$courses_array .= '<h1 class="center hidden-xs hidden-sm hidden-md">Следващите ни курсове започват на {COURSES_BEGIN_DATE}.<br> Запишете се до {COURSES_DISCOUNT_UNTIL} с 10% отстъпка!</h1>';
	$courses_array .= '<br>';

	if(count($courses))
	{
		$courses_begin = strtotime(min(array_column($courses,'start_date')));
		$discount_date = $courses_begin - 60*60*24*7; // 1 Week before start, discounts end.
		$courses_begin = date('d', $courses_begin).' '.mb_strtolower(translate('MONTH_'.date('m', $courses_begin)));
		$discount_date = date('d', $discount_date).' '.mb_strtolower(translate('MONTH_'.date('m', $discount_date)));
		$courses_array = str_ireplace(['{COURSES_BEGIN_DATE}', '{COURSES_DISCOUNT_UNTIL}'], [$courses_begin, $discount_date], $courses_array);
		//pre_print($courses_begin, true); exit;
		foreach($courses as $k => $course)
		{
			//pre_print($course, true); exit;
			if(true)
			{
				$now = strtotime('now');
				if(strtotime($course['candidate_from']) <= $now && $now <= strtotime($course['candidate_to']))
				{
					$button_add_class = 'pull-right btn btn-sm btn-success AddToUser_Button';
					$button_add_text = '<span class="glyphicon glyphicon-plus"></span> Запиши се';
					$button_add_url = '#';
				}
				else
				{
					$button_add_class = 'pull-right btn btn-sm btn-primary CommingSoon_Button';
					$button_add_text = '<span class="glyphicon glyphicon-time"></span> Очаквайте';
					$button_add_url = '#';
				}


				$button_add_class = 'pull-right btn btn-sm btn-primary hidden-xs';
				$button_add_url = header_link( [CONTROLLER => 'course', ACTION => 'view', ID => $course['course_id']] );
				$button_add_text = 'Виж още';
			}
			else
			{
				$button_add_class = 'pull-right btn btn-sm btn-info';
				$button_add_text = '<span class="glyphicon glyphicon-ok"></span> Записан';
				$button_add_url = '#';
			}

			$course['full_name'] = $course['first_name'].' '.$course['last_name'];
			$course['button_add'] = '<a href="'.$button_add_url.'" data-id="'.$course['course_id'].'" data-candidate-from="'.$course['candidate_from'].'"><button type="button" class="'.$button_add_class.'">'.$button_add_text.'</button></a>';
			$course['course_link'] = header_link( [CONTROLLER => 'course', ACTION => 'view', ID => $course['course_id']] );

			if(!isset($course['course_image']) || '' == $course['course_image'])
			{
				$course['course_image'] = DIR_IMAGES.'courses/5.png';
			}
			else
			{
				$course['course_image'] = DIR_IMAGES.'courses/'.$course['course_image'];
			}

			if($course['course_price'] == '')
			{
				$course['course_price'] = '<span class="red">??? лв.</span>'; //Неизвестна <span class="red">??? лв.</span>
				$original_price = -1;
				$cp_index = -1;
			}
			else //0 == $course['course_price'] | 1 == $course['course_id'] % 3
			{
				$original_price = (0 == $course['course_price'] ? 0 : $course['course_price']);
				$cp_index = (0 == $course['course_price'] ? 0 : 1);
				$course['course_price'] = (0 == $course['course_price'] ? '<span style="font-weight: bold; color: red; border: 2px dashed red; padding:3px; text-transform: uppercase;">Безплатно</span>' : '<span style="font-weight: bold; color: green;">&le; '.$course['course_price'].' лв.</span>');
			}

			$members_count = getMembersCount($course['course_id']);
			if($members_count <= 4)
			{
				$members_title = 'Оставащи места';
				$members_count = '<span style="color:red;" title="'.$members_title.'">'.($members_count += 4).'</span>';
			}
			else
			{
				$members_title = 'Брой записани';
				$members_count ='<span style="color:black;" title="'.$members_title.'">'. $members_count.'</span>';
			}

			$tmp = '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 course_cell course_price_-1 course_price_'.$cp_index.' course_teacher_'.$course['teacher_id'].'" data-course_price="'.$original_price.'" data-start_date="'.date('Ymd', strtotime($course['start_date'])).'" data-full_name="'.str2int($course['full_name']).'">
						<div class="panel panel-default">
							<div class="panel-body">
								<a href="'.$course['course_link'].'">
									<img class="course_index_image img-responsive col-xs-12 col-sm-6 col-md-12 col-lg-6" src="'.$course['course_image'].'" alt="Course Image">
									'.$course['course_name'].'
								</a>
								<span class="pull-right">'.$course['course_price'].'</span><br>
								<a href="'.header_link([ CONTROLLER => 'user', ACTION => 'view', ID => $course['teacher_id']]).'">'.$course['full_name'].'</a>, <span class="pull-right"><i class="fa fa-calendar"></i> '.date('d.m.Y г.', strtotime($course['start_date'])).'</span><br>
								<span class="align_correct" style="display: block;">'.excerpt_text($course['course_description'], 140).' <a href="'.$course['course_link'].'">...</a></span>
							</div>
							<div class="panel-footer">
								<i class="fa fa-group" title="'.$members_title.'"></i>'.$members_count.',
								<i class="fa fa-clock-o" title="Продължителност"></i> '.$course['course_horarium'].' часа,
								<span title="Рейтинг">'.prepare_course_stars($course['course_rating']).'</span>
								'.$course['button_add'].'
							</div>
						</div>
					</div>';


			$courses_array .= $tmp;
		}
	}
	else
	{
		//$courses_array .= '<h1 class="center">'.translate('MSG_EMPTY_RESULT').'</h1><br>';
	}

	// Use this for datalist functionality
	$teachers = array();
	foreach(get_all_teachers() as $key => $teacher)
	{
		$teachers[$key] = $teacher['full_name'].'|'.$teacher['user_id'];
	}
	$teachers = array_unique($teachers);
	usort($teachers, function($a,$b){ return mb_strtoupper($a) > mb_strtoupper($b); } );

	foreach($teachers as $key => $value)
	{
		$value = explode('|', $value);
		$teachers[$key] = '<option value="'.$value[1].'"'.(in_array($value[1], $_GET['teachers']) ? ' selected="selected"' : '').'>'.$value[0].'</option>';
	}
?>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<button id="filter_summary_trigger" type="button" class="pull-right"><span class="glyphicon glyphicon-filter"></span> Филтри</button>
			<h2 class="section-title">Нашите курсове</h2>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<details><summary class="hidden" id="filter_summary">Филтри</summary>
			<form method="GET" name="filter_courses_form" action="<?php echo header_link(array(CONTROLLER => 'course', ACTION => 'index')); ?>">
				<div class="row">
				<div class="col-xs-6 col-sm-6 col-md-3">
					<label>Категории <span style="font-style: italic;">(oчаквайте)</span></label>
					<label class="form-control"><input type="checkbox" disabled> Front End курсове</label>
					<label class="form-control"><input type="checkbox" disabled> Back End курсове</label>
					<label class="form-control"><input type="checkbox" disabled> Soft Skills курсове</label>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3">
					<label>Цена</label>
					<label class="form-control"><input type="radio" class="live_filter" name="price" value="-1"<?php echo (-1 == $_GET['price'] ? ' checked="checked"' : ''); ?>> Всички курсове</label>
					<label class="form-control"><input type="radio" class="live_filter" name="price" value="1"<?php	 echo ( 1 == $_GET['price'] ? ' checked="checked"' : ''); ?>> Платени курсове</label>
					<label class="form-control"><input type="radio" class="live_filter" name="price" value="0"<?php  echo ( 0 == $_GET['price'] ? ' checked="checked"' : ''); ?>> Безплатни курсове</label>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3">
					<label>Лектор</label>
					<select name="teachers[]" id="teachers_selector" multiple="multiple" size="5" class="form-control live_filter" style="height: 118px;">
						<?php echo implode('', $teachers); ?>
					</select>
				</div>
				<div class="col-xs-6 col-sm-6 col-md-3">
					<label>Подреди по:</label>
					<select name="order" class="form-control live_filter">
						<?php RenderDropDown($ALLOWED_ORDERS, $_GET['order']); ?>
					</select>
					<button type="submit" class="btn btn-sm btn-info btn-block pull-left" style="text-transform:uppercase; margin-top:17px; "><span class="glyphicon glyphicon-filter"></span> Филтрирай</button>
					<button type="reset" class="btn btn-sm btn-info btn-block pull-right" style="text-transform:uppercase;"><span class="glyphicon glyphicon-trash"></span> Изчисти</button>
				</div>
			</div>
			</form><hr class="double">
		</details>

		</div>
	</div>
	

	<div class="row" id="wrapper">
		<?php echo $courses_array; ?>
	</div>
</div>

<form method="post" action="<?php echo header_link([CONTROLLER => 'course', ACTION => 'AddToUser']); ?>" style="display: none;">
	<input type="text" name="<?php echo ID; ?>" value="0" id="AddToUser_Course">
	<input type="text" name="to_user_id" value="<?php echo $user_id; ?>">
	<input type="text" name="form_id" value="1">
	<input type="submit" value="AddToUser" id="AddToUser_Submit">
</form>
<script>
$(document).ready(function(){
	$('.live_filter').change(function(){
		var duration = 0;
		var teachers = $('#teachers_selector option:selected');
		var price_class = '.course_price_' + $('input[name=price]:checked').val();
		var order_by = $('select[name=order]').val();

		if(order_by != ''){
			//var sort_order = $('div').tsort({attr:'data-' + order_by});
			//alert(sort_order);

			var $wrapper = $('#wrapper');
			$wrapper.find('.course_cell').sort(function (a, b) {
				return +a.getAttribute('data-' + order_by) - +b.getAttribute('data-' + order_by);
			})
			.appendTo($wrapper);
		}

		$('.course_cell').hide(duration);
		if(teachers.length == 0){ // No selected teachers
			$(price_class).show(duration);
		}else{
			teachers.each(function(index){
				var author_class = '.course_teacher_' + $(this).val();

				$(price_class + author_class).show(duration);
			});
		}

		if (history.pushState) {
			var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?' + $( "form[name=filter_courses_form]" ).serialize(); //targetName + '=' + targetVal;
			window.history.pushState({path:newurl},'',newurl);
		}
	});

	$('.AddToUser_Button').click(function(e){
		e.preventDefault();
		$('#AddToUser_Course').val( $(this).parent().data('id') );
		$('#AddToUser_Submit').trigger('click');
	});

	$('.CommingSoon_Button').click(function(e){
		e.preventDefault();
		alert( "Записването ще започне на " +  $(this).parent().data('candidate-from') );
	});

	$('#filter_summary_trigger').click(function(){
		$('#filter_summary').trigger('click');
	});
	<?php if(empty($courses)){ ?>
		$('#filter_summary').trigger('click');
	<?php } ?>
});
</script>