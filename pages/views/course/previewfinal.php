<?php
	Global $candidates, $columns, $live_vip_users, $course, $HALL_PLACES;
?>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<?php
			$show_submit_button = true;

			if(have_messages())
			{
				if(has_warning_messages() || has_error_messages())
				{
					$show_submit_button = false;
				}

				show_all_messages();
				clear_all_messages();
			}

			if($show_submit_button){
			?>
			<form method="post" action="" id="PF_Form">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel-group" id="accordion1" style="margin-top:20px;">
							<div class="panel panel-info">
								<div class="panel-heading panel-plus-link">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne1" aria-expanded="true">
											<b><h5 style="display:inline;">Кандидатстване - Крайно класиране (<?php echo $course['course_name']; ?>)</h5><?php echo HelpButton('/viewtopic.php?f=25&t=202 #post_content322 table'); ?></b>
										</a>
								</div>
								<div id="collapseOne1" class="panel-collapse collapse in" aria-expanded="true">
									<div class="panel-body" style="padding:0;">
										<?php
											echo '<div style="padding:15px;"><table width="100%"><tr><td><span class="glyphicon glyphicon-home"></span> Капацитет на залата <input type="number" value="'.$HALL_PLACES.'" id="hall_places" class="pf_input_number" />';
											echo '</td><td align="center"><a href="#" data-toggle="modal" data-target="#LogInfoModal" class="load_users_info"><span class="glyphicon glyphicon-user"></span></a> Брой курсисти <input type="number" id="user_places" value="'.$live_vip_users.'" disabled="disabled" class="pf_input_number" />';
											echo '</td><td align="center"><img src="'.DIR_IMAGES.'ui/1.png" height="16" id="btn_manual_check" style="cursor: pointer;" /> Свободни места <input type="number" id="free_places" value="" disabled="disabled" class="pf_input_number" />';
											echo '</td><td align="center"><span class="glyphicon glyphicon-ban-circle" style="color:red;"></span> Долна граница: <input type="number" id="lowest_value" value="60" min="0" max="100" class="pf_input_number" />';
											echo '</td><td align="right"><input type="reset" name="btn_clear" value="Изчисти">';

											echo '<input type="button" name="btn_submit" id="btn_submit" value="Приложи" />';
											echo '<input type="hidden" name="is_submited" value="1" /></td></tr></table></div>';
										?>
										<div class="panel panel-info" style="text-align:right; padding-right:15px;">
											<strong>За данни за текущото кандидатстване за всички курсове <a href="/course/?c=course&amp;a=howisgoing">кликнете тук!</a></strong>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php
					render_table($candidates, $columns, 'class="table-w100 table-striped table-hover table-bordered"');
				?>
			</form>
			<div class="modal fade" id="LogInfoModal" tabindex="-1" role="dialog" aria-labelledby="LogInfoModalLabel" aria-hidden="true">
				<div class="modal-dialog" style="width:900px;">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title"></h4>
						</div>
						<div class="modal-body">

						</div>
						<div class="modal-footer" style="border-top:0px;">
						</div>
					</div>
				</div>
			</div>
			<script>
				var free_places;

				function render(){
					var lowest_value = $('#lowest_value').val();
					var estimated_places = free_places;
					var info_span;

					$('.my_radio').prop("checked", false);
					$('.info_span').html('');

					$('.column_3').each(function() {
						info_span = '#info_span_' + $(this).data('row');

						if ($(info_span).data('score') < lowest_value && $(info_span).data('paid') == 0) {
							$(this).prop("disabled", false);
							$(this).prop("checked", true);
						}else{
							$(this).prop("disabled", true);

							if($(info_span).data('form') == 2){
								$('#radio_' + $(this).data('row') + '_2').trigger('click');
							}

							if($(info_span).data('form') == 1){
								if(estimated_places > 0){
									$('#radio_' + $(this).data('row') + '_1').trigger('click');
									estimated_places--;
								}else{
									$('#radio_' + $(this).data('row') + '_2').trigger('click');
									$(info_span).html('<span class="glyphicon glyphicon-transfer red"></span>');
								}
							}
						}
					});

					recalculatePlaces();
				}

				function getFreePlaces(){
					$("#free_places").val( $("#hall_places").val() - $("#user_places").val() );
					free_places = $("#free_places").val();

					render();
				}

				function recalculatePlaces(){
					$("#onlive_count").text($('.column_1:checked').length);
					$("#online_count").text($('.column_2:checked').length);
					$("#reject_count").text($('.column_3:checked').length);

					if(parseInt($("#onlive_count").text()) > free_places){
						$("#onlive_count").removeClass('green');
						$("#onlive_count").addClass('red');
					}else{
						$("#onlive_count").removeClass('red');
						$("#onlive_count").addClass('green');
					}
				}

				function checkFormAndSubmit(){
					recalculatePlaces();

					if(parseInt($("#onlive_count").text()) > free_places){
						alert('Надхвърляте броя на свободните места в залата! Внесете корекции!');
						return false;
					}else{
						$('#PF_Form').submit();
					}
				}

				// =======================================================================================
				$(document).ready(function(){

					$('.load_users_info').click(function(){
						$('#LogInfoModal .modal-title').text('Детайлна информация');
						$('#LogInfoModal .modal-body').text('Моля изчакайте, данните се зареждат...');

						$('#LogInfoModal .modal-body').load('<?php echo header_link(array(CONTROLLER => 'user', ACTION => 'index', 'is_send' => '1', 'role' => 'student', 'group_number' => 'onlive', 'course_id' => $course['course_id'])); ?>'+' #users_main_div');
					});

					$('.my_radio').click(function(){

						//if(!$(this).prop("checked"))
						//{
						//	$(this).prop("checked", !$(this).prop("checked"));
						//}
						//else
						//{
							var row = $(this).data('row');

							$('.row_'+row).prop("checked", false);
							$(this).prop("checked", true);
						//}

						recalculatePlaces();
					});

					$('#btn_manual_check').click(function(){
						$('.my_radio').prop("checked", false);
						$('.my_radio').prop("disabled", false);
					});

					$('#lowest_value').change( render );

					$('#hall_places').change( getFreePlaces );

					$('#btn_submit').click( checkFormAndSubmit );
					getFreePlaces();
				});
			</script>
			<?php } ?>
		</div>
	</div>
</div>

