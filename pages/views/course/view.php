<?php
	define_once('DEFAULT_MISSING_AVATAR', DIR_IMAGES.'ui/user-edit-icon-2729.png');
	define_once('DEFAULT_PRIVACY_AVATAR', DIR_IMAGES.'ui/key-png-login-15.png');

	Global $is_admin_user, $source, $course_data, $user_id;

	$mark_exams = true;

	if(has_error_messages())
	{
		return;
	}

	database_open();

	$promo_codes = to_assoc_array(exec_query('SELECT * FROM promo_codes WHERE active_to >= "'.date('Y-m-d H:i:s').'" GROUP BY code_name'), 'code_name');
	//pre_print($promo_codes);exit;
	$course = $course_data;
	if(true)
	{
		if(strtotime($course['candidate_from']) <= strtotime('now') + TIME_CORRECTION)
		{
			$button_add_class = 'radius_tl_50px radius_br_50px btn btn-sm btn-success AddToUser_Button pull-right';
			$button_add_text = '<span class="glyphicon glyphicon-plus"></span> Запиши се';
			$button_add_url = 'submit'; // submit
		}
		else
		{
			$button_add_class = 'btn btn-sm btn-info center CommingSoon_Button';
			$button_add_text = '<span class="glyphicon glyphicon-time"></span> Скоро'; // Кандидатстване от 01.04.2020
			$button_add_url = 'button';
		}
	}
	else
	{
		$button_add_class = 'btn btn-sm btn-info';
		$button_add_text = '<span class="glyphicon glyphicon-ok"></span> Записан';
		$button_add_url = '#';
	}
	$course['button_add'] = '<button type="'.$button_add_url.'" class="'.$button_add_class.'" style="padding: 5px 20px;">'.$button_add_text.'</button>';
	$courses_begin = strtotime($course['start_date']);
	$discount_date = $courses_begin - 60*60*24*7; // 1 Week before start, discounts end. +3600*23
	//$discount_date = strtotime('2021-01-10'); // Set discount date fixed for some reasons.
	//pre_print($course);

	switch($course['course_id'])
	{
		case 1: $course['course_components'] = [
				'HTML' => 'HyperText Markup Language<br>Основния език използван в Уеб сайтовете',
				'CSS' => 'Cascading Style Sheets<br>Език за описание на стилове използван в Уеб',
				'Рязане на дизайн' => 'Как да превърнете картинките в реален уеб сайт',
				'Бонус: Въведение в Java Script' => ''
			];
			break;
		default: $course['course_components'] = []; break;
	}

	$allowed_tabs = array(
		'preview' => array('icon' => 'fa fa-terminal', 'target' => 'info', 'title' => translate('info')),
		'program' => array('icon' => 'fa fa-calendar', 'target' => 'program', 'title' => 'Програма'),
		//'calendar' => array('icon' => 'fa fa-gear', 'target' => 'calendar', 'title' => 'Календар'),
		'members' => array('icon' => 'fa fa-group', 'target' => 'candidates', 'title' => 'Кандидати')
	);

	//PRE_PRINT($members);

	$wallpapers = [
		'' => '/assets/img/demo/bg.jpg',
		 0 => '/assets/img/demo/w8.jpg',	//  assets/img/demo/img08.jpg
		 1 => '/assets/img/demo/img01.jpg',
		 2 => '/assets/img/demo/w9.jpg'		//  assets/img/demo/img09.jpg
	];

	$seasons = [
		'' => '/assets/img/demo/zima.jpg',
		 0 => '/assets/img/demo/prolet.jpg',
		 1 => '/assets/img/demo/liato.jpg',
		 2 => '/assets/img/demo/esen.jpg'
	];

	// ============================================= PREPARE AUTHOR DATA =============================================
	$my_privacy_level = user_privacy_level($course['teacher_id']);

	$author = getAuthorData($course['teacher_id']);
	$author_settings = reset(get_user_settings($course['teacher_id']));
	$author_settings = unserialize($author_settings['user_privacy']);

	if($author_settings['avatar'] < $my_privacy_level)
	{
		$author['avatar'] = DEFAULT_PRIVACY_AVATAR;
	}

	if($author_settings['facebook'] < $my_privacy_level)
	{
		$author['facebook'] = '';
	}

	$author['image'] = '<img class="img-responsive img-circle" src="'.$author['avatar'].'" alt="User #'.$author['user_id'].' Avatar">';
	$author['link_profile'] = '<a id="author_profile_link" href="'.link_to([CONTROLLER => 'user', ACTION => 'view', ID => $author['user_id']]).'"><img src="'.DIR_IMAGES.'ui/views.gif'.'" alt="View User"> Виж профила</a>';
	$author['link_courses'] = '<a id="author_courses_link" href="'.link_to([CONTROLLER => 'course', ACTION => 'index', 'teachers' => $author['user_id']]).'"><img src="'.DIR_IMAGES.'ui/views.gif'.'" alt="View Courses"> Виж курсове <span class="hidden-xs">с този автор</span></a>';
	$author['link_facebook'] = ( !empty($author['facebook']) ? '<a href="'.$author['facebook'].'" target="_blank" class="social-icon-ar sm facebook pull-right" style="margin:0 0 0 10px;"><i class="fa fa-facebook"></i></a>' : '');

	$author['panel'] = '<div class="panel-body">
							<div class="col-xs-12 col-sm-2 col-md-1 col-lg-1 img_container" style="padding:0;">'.$author['image'].'</div>
							<div class="col-xs-12 col-sm-10 col-md-11 col-lg-11 course_author_text">
								<strong>'.$author['full_name'].'</strong>
								<br><span class="align_correct">'.$author['full_text'].'</span><br>
								'.$author['link_profile'].'
								'.$author['link_courses'].'
							</div>
						</div>';
	// ============================================= END OF AUTHOR DATA =============================================

	$course_components = '';
	foreach($course['course_components'] as $k => $v)
	{
		$course_components .= ' <div class="col-lg-12 course_components">
									<span class="glyphicon glyphicon-ok"></span> '.$k.'
									<br>'.$v.'
								</div>';
	}
	$course_components = '<div class="row">'.$course_components.'</div>';

	$members = getCourseMembers($course['course_id']);
	$members_list = '';
	$real_members = 0;

	if($members)
	{
		$members_settings = get_user_settings( array_column($members, 'user_id') );
		$members_settings = array_column($members_settings, 'user_privacy', 'user_id');
	}

	foreach($members as $member)
	{
		$member_privacy = unserialize($members_settings[$member['user_id']]);
		//Pre_Print($member_privacy);

		$icon_links = ['profile' => '', 'phone' => '', 'message' => '', 'facebook' => ''];

		// Временно решение. Тук трябва да се направят  проверки, за приятелство, както и дали някой логнатия потребител не е някой от  мембърите. (той  трябва да  вижда  всичко за себе си)
		if($member_privacy['facebook'] < 3){ unset($icon_links['facebook']); }
		if($member_privacy['avatar'] < 3){ $member['avatar'] = ''; }
		if($member_privacy['phone'] < 3){ unset($icon_links['phone']); }

		$member['avatar'] = ($member['avatar'] != '' ? $member['avatar'] : DEFAULT_MISSING_AVATAR);
		$member['full_name'] = ( $member_privacy['last_name'] < 3 ? 'Потребител №'.$member['user_id'] : $member['first_name'].' '.$member['last_name']);

		if($member['full_name'] != ' ')
		{	$real_members++;
//			PRE_PRINT($member);
//			$members_list .= '<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12"><div class="panel panel-default">
//                    <div class="panel-body" style="display: inline-block;">'
//						. '<img src="'.$member['avatar'].'" alt="User #'.$member['user_id'].' Avatar" style="width:50px; height:50px; border-radius:50%; margin-right: 15px;">'.$member['full_name'].'</div></div></div>';

			foreach($icon_links as $link => $value)
			{
				switch($link)
				{
					case 'profile' : $icon_links[$link] = ( !empty($member['user_id'])	? '<a href="'.link_to([CONTROLLER => 'user', ACTION => 'view', ID => $member['user_id']]).'"><img src="'.DIR_IMAGES.'ui/eye.png" alt="View User"></a>' : ''); break;
					case 'message' : $icon_links[$link] = ( !empty($member['user_id'])	? '<a href="'.header_link(array(CONTROLLER => 'mail', ACTION => 'create', 'send_to['.$member['user_id'].']' => $member['user_id'])).'"><img src="'.DIR_IMAGES.'ui/icon_mail.gif" alt="Send Message"></a>' : ''); break;
					case 'facebook': $icon_links[$link] = ( !empty($member['facebook'])	? '<a href="'.$member['facebook'].'" target="_blank"><img src="'.DIR_IMAGES.'ui/facebook.png" alt="Send Message" style="height:16px;"></a>' : ''); break;
					case 'phone'   : $icon_links[$link] = ( !empty($member['phone'])	? '<a href="#" onclick="alert(\''.$member['phone'].'\');"><img src="'.DIR_IMAGES.'ui/phone-icon-2.png" alt="View Phone"></a>' : ''); break;
				}

				if($icon_links[$link] != '')
				{
					$icon_links[$link] = '<div class="col-xs-{NUM}"><div class="thumbnail">'.$icon_links[$link].'</div></div>';
				}
				else
				{
					unset($icon_links[$link]);
				}
			}
//$wallpapers[$member['sex']]
			$members_list .= '<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">'
				. '<div class="panel panel-default panel-card">
                            <div class="panel-heading">
                                <img src="'.DIR_IMAGES.'others/plovdiv.jpg"><!-- wallpapers[$member["sex"]] -->
                            </div>
                            <div class="panel-figure" style="overflow:hidden;">
                                <img class="img-responsive" src="'.$member['avatar'].'" alt="User #'.$member['user_id'].' Avatar">
                            </div>
                            <div class="panel-body text-center">
                                <h4 class="panel-header"><a href="#">'.$member['full_name'].'</a></h4>
                                <small>Плъзнете за повече информация</small>
                            </div>
                            <div class="panel-thumbnails">
                                <div class="row">
									'.str_ireplace('col-xs-{NUM}', 'col-xs-'.(12 / count($icon_links)), implode('', $icon_links)).'
									<img src="'.DIR_IMAGES.'others/spritzschutz_gras.jpg" style="margin-top:10px;">
                                </div>
                            </div>
                        </div></div>';


		}
	}
	if($real_members < 4){ $members_list = '<div class="col-xs-12"><div class="alert alert-warning">Поради настройките за поверителност на някои потребители те не са показани в списъка!</div></div>' . $members_list; }
	//    <button class="btn btn-primary btn-ar btn-sm" role="button">+</button> Create_Mails_Modal($look_for_user));

	$lectures = to_assoc_array(exec_query('SELECT * FROM course_dates WHERE course_id = '.$course['course_id'].' ORDER BY lecture_date ASC'));
	$lectures_days = [];
	$lectures_list = '';
	$lecture_number = 0;
	$teach_hours = ($course['course_horarium'] / count($lectures));
	$astro_hours = teach2astro($teach_hours);

	foreach($lectures as $lecture)
	{
		if($lecture_number < 0)
		{
			$li_attributes = 'class="wow fadeInRight" style="visibility: visible; animation-name: fadeInRight;"';
		}
		else
		{
			$li_attributes = 'class="animated fadeInRight animation-delay-'.$lecture_number.'"';
		}

		$lecture_time = strtotime($lecture['lecture_date']);
		$lectures_days[] = translate(date('l', $lecture_time)) . ('00:00:00' != substr($lecture['lecture_date'], -8) ? ', '.date('H:i', $lecture_time).'-'.date('H:i', $lecture_time + $astro_hours * 3600) : '');

		if($lecture['lecture_name'] != '' || $lecture['lecture_info'] != '')
		{
			$lecture_number++;
			// '.translate('MONTH_'.date('m', strtotime($lecture['lecture_date']))).'
			$lectures_list .= '
                <li '.$li_attributes.'>
                    <time class="timeline-time" datetime=""><span>#'.$lecture_number.'</span> '.date('d.m.Y', strtotime($lecture['lecture_date'])).'</time>
                    <i class="timeline-2-point"></i>
                    <div class="panel panel-'.(stripos($lecture['lecture_name'], 'изпит') !== false && $mark_exams ? 'primary' : 'default').'">
                        <div class="panel-heading">'.$lecture['lecture_name'].'</div>
                        <div class="panel-body">'.$lecture['lecture_info'].'</div>
                    </div>
                </li>';
		}
	}
	$lectures_days = array_unique($lectures_days);
	$duration = ceil(count($lectures) / count($lectures_days) / 4);
	if(empty($lectures_list)){ unset($allowed_tabs['program']); }
	if(empty($members_list)){ unset($allowed_tabs['members']); }

	foreach($allowed_tabs as $tab)
	{
		$tabs_links .= '<li><a href="#'.$tab['target'].'" id="'.$tab['target'].'_link" data-toggle="tab" aria-expanded="false"><i class="'.$tab['icon'].'"></i> <span class="hidden-xs">'.$tab['title'].'</span></a></li>';
	}

	$edit_link = '<a href="'.header_link([CONTROLLER => 'course', ACTION => 'add', ID => $course['course_id']]).'" class="pull-right" style="padding:7px 11px;  border:1px solid #E5E5E5; background-color:#F0F0F0;"><img src="'.DIR_IMAGES.'ui/application_form_edit.png" alt="Edit Course"></a>';
	$tabs_links = ($tabs_links != '' ? '<ul class="nav nav-tabs nav-tabs-ar nav-tabs-ar-white" style="margin-top:20px;">'.$tabs_links.(user_can('EDIT_COURSE') ? $edit_link : '').'<span class="pull-right" style="padding:8px 9px;">Рейтинг: '.prepare_course_stars($course['course_rating']).'</span></ul>' : '');
	//$course['course_price'] = 400;
	$discounts = array(
		1 => ['text' => '<span class="hidden-xs">Отстъпка за </span>записване до '.date('j', $discount_date).' '.mb_strtolower(translate('MONTH_'.date('m', $discount_date))), 'value' => '-10', 'active' => (int) $discount_date >= strtotime('now')],
		2 => ['text' => '<span class="hidden-xs">Отстъпка за </span>записване на &ge; 2 курсисти', 'value' => '-10', 'active' => 1],
		0 => ['text' => '<span class="hidden-xs">Отстъпка за </span>заплащане наведнъж', 'value' => '-20', 'active' => 1],
		3 => ['text' => '<span class="hidden-xs">Отстъпка за </span>промокод <span id="hide">няма</span><input id="txt" type="text" placeholder="въведете тук" class="auto_grow_input">', 'value' => '-0', 'active' => 1]

//		['text' => 'Отстъпка за заплащане на цялата сума наведнъж', 'value' => '-20'],
//		['text' => 'Отстъпка за ранно записване, до 22.09.2020', 'value' => '-10'],
//		['text' => 'Отстъпка за записване на 2+ човека', 'value' => '-10'],
//		['text' => 'Отстъпка за промокод <input type="text" placeholder="Въведете промокод">', 'value' => '-20'],
	);

//	if('2020-10-05' <= date('Y-m-d')){ $discounts[1]['active'] = 0; }
	$button_options = array(
		'price' => $discount_price,
		'period' => abs($month),
		'url_ok' => $url_ok,
		'descr' => $descr
	);

	$panel_class = 'default';
?>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h2><?php echo $course['course_name']; ?></h2>
			<?php echo $tabs_links; ?>
			<div class="tab-content" style="margin-bottom: 20px;">
				<div class="tab-pane" id="info">
					<div class="row">
						<?php if($course['course_description'] != '') { ?>
						<div class="col-lg-12">
							<h4 class="margin-bottom-10" style="color:black;">Какво ще научите?</h4><!-- <b>За курса на разбираем език</b><br>  -->
							<?php echo $course['course_description']; ?>
							<br><br>
						</div>
						<?php } ?>
						<div class="col-lg-8">
							<h4 class="right-line margin-bottom-10">Колко ще Ви струва?</h4>
							<form method="post" action="<?php echo header_link([CONTROLLER => 'course', ACTION => 'AddToUser']); ?>">
								<input type="hidden" name="<?php echo ID; ?>" value="<?php echo $course['course_id']; ?>">
								<input type="hidden" name="to_course_name" value="<?php echo $course['course_name']; ?>" id="to_course_name">
								<input type="hidden" name="to_course_price" value="0" id="to_course_price">
								<input type="hidden" name="to_user_id" value="<?php echo $user_id; ?>">
								<div class="table-responsive">
									<table class="table table-condensed">
										<thead>
											<tr class="navbar-inverse">
												<th style="padding-left: 10px;" colspan="4" class="radius_tl_50px">Изберете форма на обучение</th>
												<!--
												<th style="vertical-align: middle;"><label><input type="radio" name="form_id" value="1"> Редовнa</label></th>
												<th style="vertical-align: middle;"><label><input type="radio" name="form_id" value="1"> Свободна</label></th>
												<th style="vertical-align: middle;" class="radius_tr_50px"><label><input type="radio" name="form_id" value="0"> Задочнa</label></th>
												-->
												<th style="vertical-align: middle;" class="radius_tr_50px">
													<select name="form_id" class="pull-right">
														<option value="-1">-- Избери --</option>
														<option value="1">Редовна</option>
														<option value="0">Задочна</option>
													</select>
												</th>
											</tr>
											<tr>
												<td><input type="checkbox" checked="checked" disabled="disabled"></td>
												<td colspan="3">Ненамалена цена на курс</td>
												<td><?php echo $course['course_price']; ?> лв.</td>
											</tr>
										</thead>
										<tbody>
											<tr class="active">
												<th>&nbsp;</th>
												<th>Основание за намаление</th>
												<th>&nbsp;</th>
												<th>&nbsp;</th>
												<th>Отстъпка</th>
											</tr>
										<?php
										foreach($discounts as $k => $d)
										{
											$modify = $course['course_price'] * $d['value']/100;

											if($d['active'])
											{
												echo '<tr><td><input type="checkbox" class="course_discount" data-modify="'.$modify.'" id="discount_'.$k.'"></td><td colspan="3"><label for="discount_'.$k.'">'.$d['text'].' (<span id="discount_'.$k.'_value">'.$d['value'].'</span>%)</label></td><td width="100"><span id="discount_'.$k.'_modify">'.$modify.'</span> лв.</td></tr>';
											}
											else
											{
												echo '<tr style="text-decoration: line-through;"><td><input type="checkbox" id="discount_'.$k.'" disabled></td><td colspan="3"><span>'.$d['text'].' ('.$d['value'].'%)</span></td><td width="100">'.$modify.' лв.</td></tr>';
											}

										}
										?>
										</tbody>
										<tfoot>
											<tr class="navbar-inverse">
												<th class="radius_bl_50px" colspan="2" style="padding-left: 10px; vertical-align: middle;">Крайна цена: <span id="priceText"><span id="course_all" class="course_all"><?php echo $course['course_price']; ?></span> лв. (<span id="course_mon"><?php echo $course['course_price'] / $duration; ?></span> лв. / месец)</span></th>
												<th class="radius_br_50px" colspan="3"><?php echo $course['button_add']; ?></th>
											</tr>
										</tfoot>
									</table>
								</div>
							</form>
							<!-- add_modal('test', $course['button_add']);
							<table class="table">
								<thead>
									<tr class="navbar-inverse">
										<th style="padding-left: 40px;" colspan="2" class="radius_tl_50px">Изберете форма на обучение</th>
										<th style="vertical-align: middle;"><label><input type="radio"> Редовнa</label></th>
										<th style="vertical-align: middle;"><label><input type="radio"> Свободна</label></th>Дистанционно
										<th style="vertical-align: middle;" class="radius_tr_50px"><label><input type="radio"> Задочнa</label></th>
									</tr>
									<tr class="active">
										<th>&nbsp;</th>
										<th>Основание за намаление</th>
										<th>Цена / месец</th>
										<th>Цена на курса</th>
										<th>Отстъпка</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><input type="checkbox" checked="checked" disabled="disabled"></td>
										<td>Ненамалена (регулярна) цена на курс</td>
										<td><?php echo $course['course_price'] / 4; ?> лв.</td>
										<td><?php echo $course['course_price']; ?> лв.</td>
										<td>0 лв.</td>
									</tr>
								<?php
								foreach($discounts as $k => $d)
								{
									$modify = $course['course_price'] * $d['value']/100;
									echo '<tr><td><input type="checkbox" class="course_discount" data-modify="'.$modify.'" id="discount_'.$k.'"></td><td colspan="3"><label for="discount_'.$k.'">'.$d['text'].' ('.$d['value'].'%)</label></td><td>'.$modify.' лв.</td></tr>';
								}
								?>
								</tbody>
								<tfoot>
									<tr class="navbar-inverse">
										<th class="radius_bl_50px" colspan="2" style="padding-left: 40px;">Крайна цена:</th>
										<th><span id="course_mon"><?php echo $course['course_price'] / 4; ?></span> лв.</th>
										<th><span id="course_all"><?php echo $course['course_price']; ?></span> лв.</th>
										<th class="radius_br_50px"><?php echo $course['button_add']; ?></th>
									</tr>
								</tfoot>
							</table>
							-->
						</div>
						<div class="col-lg-4">
							<!-- <b>Какво ще научите?</b><br> -->
							<?php //echo $course_components; ?>
							<h4 class="right-line margin-bottom-10">Детайли за курса</h4>
							<table class="table table-bordered table-condensed table-striped">
								<tr>
									<th>Хорариум</th>
									<td><?php echo $course['course_horarium']; ?> учебни часа</td>
								</tr>
								<tr>
									<th>Начална дата</th>
									<td><?php echo date('d.m.Y', strtotime($course['start_date'])); ?> г.</td>
								</tr>
								<tr>
									<th>Крайна дата</th>
									<td><?php echo date('d.m.Y', strtotime($course['end_date'])); ?> г.</td>
								</tr>
								<tr>
									<th>Брой занятия</th>
									<td><?php echo count($lectures); ?> лекции</td>
								</tr>
								<tr>
									<th>Продължителност</th><!-- Броя на всички лекции (в брой дни) / брой лекции в дни за седмица / 4 за да преобразуваме в месец, приемайки, че всеки месец има 4 седмици ... -->
									<td><?php echo $duration; ?> месеца</td>
								</tr>
								<tr>
									<th>Преподавател</th>
									<td><?php echo $author['full_name']; ?></td>
								</tr>
								<tr>
									<th>Провеждане</th>
									<td><?php echo implode('<br>', $lectures_days); ?></td>
								</tr>
								<tr>
									<th>Рейтинг</th>
									<td><?php echo --$course['course_rating']; ?> / 5</td>
								</tr>
								<tr>
									<th>Записване</th>
									<td><?php echo 'до '.date('d.m.Y H:i', strtotime($course['candidate_to'])); // echo date('d.m.Y', strtotime($course['candidate_from'])).' - '?></td>
								</tr>
							</table>
						</div>
						<!--
						<div class="col-lg-12">
							<div class="panel panel-info margin-bottom-0">
								<div class="panel-heading">За Автора <?php echo $author['link_facebook']; ?></div>
								<?php echo $author['panel']; ?>
							</div>
						</div>
						-->
						<div class="col-lg-12">
							<h4 class="right-line margin-bottom-10">Често задавани въпроси</h4>
							<div class="panel-group margin-bottom-0" id="accordion">
              <div class="panel panel-<?php echo $panel_class; ?>">
                <div class="panel-heading panel-heading-link">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed">
                      <i class="fa fa-graduation-cap"></i> Какво представляват различните форми на обучение?
                    </a>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse">
                  <div class="panel-body">
                    <p>
						Формите на обучение, които предлагаме са:
						<dl>
							<dt>
								<i class="fa fa-home"></i>
								<dfn>Редовна</dfn>
							</dt>
							<dd>
								Идвате на място в нашата учебна зала, когато имате лекции съгласно учебната програма<br>
								Участвате в обсъжданията на задачите и лекциите, задавате вашите въпроси директно на преподаваля, решавате задачи в залата
							</dd>
							<dt>
								<i class="fa fa-globe"></i>
								<dfn>Задочна</dfn>
							</dt>
							<dd>Идвате на място в нашата учебна зала, когато имате лекции съгласно учебната програма</dd>
							<dt>
								<i class="fa fa-child"></i> Свободна
							</dt>
							<dd>Обучението е изцяло онлайн, и е съобразено с вашето индивидуално  темпо. Можете да започнете курса по всяко време.</dd>
						</dl>
					</p>
                  </div>
                </div>
              </div>
              <div class="panel panel-<?php echo $panel_class; ?>">
                <div class="panel-heading panel-heading-link">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" class="collapsed">
						<i class="fa fa-money"></i> Как мога да платя?
                    </a>
                </div>
                <div id="collapseThree" class="panel-collapse collapse">
                  <div class="panel-body">
					<p>
						Можете да заплатите по няколко начина:
						<ol>
							<li>На място в брой в наш офис</li>
							<li>Чрез банков превод</li>
						</ol>
						За повече информация, кликнете на бутона "Плащане", който ще се появи във Вашият профил, след като си резервирате място в избрания от Вас курс.
					</p>
					<p></p>
                  </div>
                </div>
              </div>
			  <div class="panel panel-<?php echo $panel_class; ?>">
                <div class="panel-heading panel-heading-link">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                      <i class="fa fa-user-secret"></i> Кой е авторът на курса?
                    </a>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in">
                  <?php echo $author['panel']; ?>
                </div>
              </div>
            </div>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="program">
					<div class="row">
						<div class="col-lg-12">
							<h3>Курсът се състои от <?php echo $lecture_number; ?> занятия, разпределени както следва:</h3>
							<ul class="timeline-2"><?php echo $lectures_list; ?></ul>
						</div>
						<!-- <div class="col-lg-3"><?php show_ads(); show_ads(); show_ads(); ?></div> -->
					</div>
				</div>
				<div class="tab-pane" id="calendar">Lorem ipsum</div>
				<div class="tab-pane" id="candidates">
					<div class="row">
						<?php echo $members_list; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	var old_discount = 0;

	function recalculate(){
		var price = <?php echo $course['course_price']; ?>;
		var new_price = price;

		$('input[type=checkbox].course_discount:checked').each(function () {
				new_price = parseInt(new_price) + $(this).data('modify');
		});

		$('.course_all').text(new_price);
		$('#course_mon').text( parseInt($('#course_all').text() ) / <?php echo $duration; ?>);
		$('#to_course_price').val($('.course_all').text());
	}

	$(function(){
  $('#hide').text($('#txt').val());
  $('#txt').width($('#hide').width());
}).on('keyup', 'input#txt',function(){

  $('#hide').text($('#txt').val());
  $('#txt').width($('#hide').width());

  switch($('#txt').val().toUpperCase()){
	  <?php foreach($promo_codes as	$row){
		echo 'case "'.mb_strtoupper($row['code_name']).'": discount = '.$row['code_value'].'; break; ';
	  } ?>
	default: discount = 0;
  }
  if(discount != old_discount){
				old_discount = discount;
	setInterval(blinkText(3, 250, '#priceText'), 500);
	}
  $('#discount_3_value').text('-' + discount);
  $('#discount_3_modify').text('-' + <?php echo $course['course_price']; ?> * discount / 100);
  $('#discount_3').data('modify', -(<?php echo $course['course_price']; ?> * discount / 100));

///$('#discount_3').trigger('click');
	if(discount){
		$('#discount_3').prop('checked', true);
		$('#discount_3').parent().parent().removeClass('discount_off');
		$('#discount_3').parent().parent().addClass('discount_on');
	}else{
		$('#discount_3').prop('checked', false);
		$('#discount_3').parent().parent().removeClass('discount_on');
		$('#discount_3').parent().parent().addClass('discount_off');
	}
	recalculate();
	function blinkText(times, delay, element) {

					for (let i = 0; i < times; i++ ) {
						$(element).fadeOut(delay);
						$(element).fadeIn(delay);
					}

			}

});

$('#txt').keypress(function(event){
	var ew = event.which;

if(13 == ew || ew == 10) {event.preventDefault(); return false;}
});

	$(document).ready(function(){
		// Select first tab no matter who is it
		$('.nav li:nth-child(1) a').trigger('click');

		$('.nav-tabs a').on('shown.bs.tab', function(event){ // get target tab
			var target_hash = event.target.hash;

			if (history.pushState) {
				var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + target_hash;
				window.history.pushState({path:newurl},'',newurl);
			}
		});

		if('' != window.location.hash){
			var target_link = window.location.hash + '_link';
			$(target_link).trigger('click');
		}

		var clickTime = new Date();

		$('.course_discount').click(function(){

			function blinkText(times, delay, element) {
				var nowTime = new Date();

				if(1000 < nowTime.getTime() - clickTime.getTime()){
					for (let i = 0; i < times; i++ ) {
						$(element).fadeOut(delay);
						$(element).fadeIn(delay);
					}
				}
				clickTime = nowTime;
			}

			var price = parseInt($('#course_all').text());
			var modify = $(this).data('modify');

			if($(this).prop('checked')){
				$(this).parent().parent().removeClass('discount_off');
				$(this).parent().parent().addClass('discount_on');

				//$('.course_all').text(price + modify);
				setInterval(blinkText(3, 250, '#priceText'), 500);
			}else{
				$(this).parent().parent().removeClass('discount_on');
				$(this).parent().parent().addClass('discount_off');

				//$('.course_all').text(price - modify);
				setInterval(blinkText(3, 250, '#priceText'), 500);
			}

			recalculate();
		});

		$('.AddToUser_Button ').click(function(){
			var course_form = $('select[name=form_id]').val();
			//alert(course_form);

			if(course_form === undefined || course_form < 0){
				alert('Изберете форма на обучение!');
				return false;
			}
		});


//		$('.auto_grow_input').keyup(function(){
//			$(this).attr('size', $(this).val().length);
//		});

		$("#LogInfoModal").draggable({ handle: ".modal-header" });
		$("#test_Modal").draggable({ handle: ".modal-header" });
	});
</script>
<?php return; ?>





































	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="panel-group" id="Course_accordion_<?php echo $course['course_id']; ?>" style="margin-top:20px;">
					<div class="panel panel-info">
						<div class="panel-heading panel-plus-link">
							<a data-toggle="collapse" data-parent="#Course_accordion_<?php echo $course['course_id']; ?>" href="#Course_collapseOne_<?php echo $course['course_id']; ?>" class="accordion-toggle<?php echo ($is_collapsed ? ' collapsed' : ''); ?>" aria-expanded="<?php echo ($is_collapsed ? 'false' : 'true'); ?>">
								<b><h5 style="display:inline;"><?php echo $caption; ?></h5><?php echo HelpButton(); ?></b><!-- <button id="add_hw_visibility" type="button" style="position: relative; left:-230px;">+</button> -->
							</a>
						</div>
						<div id="Course_collapseOne_<?php echo $course['course_id']; ?>" class="panel-collapse collapse<?php echo ($is_collapsed ? '' : ' in'); ?>"<?php echo ($is_collapsed ? ' aria-expanded="false" style="height: 0px;"' : ' aria-expanded="true"'); ?>>
							<div class="panel-body">
								<!-- <input type="hidden" name="edit_course" value="<?php echo $course['course_id']; ?>" /> -->
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<label for="course_name">
										Име на курс
									</label>

									<input type="text" name="course_name" id="course_name" class="form-control" value="" />
								</div>
								<?php if($is_admin_user){ ?>
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<label for="ancestor_id">
										Курс Прародител
									</label>

									<select name="ancestor_id" id="ancestor_id" class="form-control">
										<?php echo ReturnDropDown(get_all_ancestors(null, 'ancestor_id, ancestor_name'), $course['descendant_of'])?> ?>
									</select>
								</div>
								<?php } ?>
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<label for="course_after_url">
										URL за материали
									</label>

									<input type="text" name="course_after_url" id="course_after_url" class="form-control" value="<?php echo $course['course_after_url']; ?>" />
								</div>
								<?php if($is_admin_user){ ?>
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<label for="course_url">
										URL за кандидатстване
									</label>

									<input type="text" name="course_url" id="course_url" class="form-control" value="<?php echo $course['course_url']; ?>" />
								</div>
								<?php } ?>
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<label for="start_date">
										Начална дата
									</label>

									<input type="date" name="start_date" id="start_date" class="form-control" value="<?php echo $course['start_date']; ?>" />
								</div>
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<label for="end_date">
										Крайна дата
									</label>

									<input type="date" name="end_date" id="end_date" class="form-control" value="<?php echo $course['end_date']; ?>" />
								</div>
								<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
									<label for="teacher_id">
										Лектор
									</label>

									<select name="teacher_id" id="teacher_id" class="form-control choose_other_teacher">
										<?php
										$teachers = array_merge($admins_array, $assistants_array);
										if(!in_array($course['teacher_id'], $teachers))
										{
											$teachers[] = $course['teacher_id'];
										}
										$teachers = implode(',', array_unique($teachers));
										$teachers_query = exec_query('SELECT user_id, CONCAT(first_name, " ", last_name," (", user_id, ")") AS user_full_name FROM users WHERE user_id IN ('.$teachers.') ORDER BY user_full_name');
										$teachers_list = array(0 => '-- Моля изберете --') + to_dropdown_array($teachers_query, 'user_id', 'user_full_name') + array(-1 => '-- Друг --');
										echo ReturnDropDown($teachers_list, $course['teacher_id'])?> ?>
									</select>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
									<label for="horarium">
										Хорариум
									</label>

									<input type="number" name="horarium" id="horarium" class="form-control" value="<?php echo $course['horarium']; ?>" />
								</div>
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<label for="candidate_until">
										Кандидатстване до:
									</label>
									<input type="datetime" name="candidate_until" id="candidate_until" class="form-control" value="<?php echo $course['candidate_until']; ?>" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php
		if(count($courses) == 1)
		{
			$course = reset($courses);
			$middle_date = get_current_period(null, $course['start_date']);
	?>
		<div class="row">
			<div class="col-lg-12">
				<div class="panel-group" id="Course_accordion2" style="margin-top:20px;">
					<div class="panel panel-info">
						<div class="panel-heading panel-heading-link">
							<a data-toggle="collapse" data-parent="#Course_accordion2" href="#Course_collapseDates" class="accordion-toggle<?php echo (!$is_collapsed ? ' collapsed' : ''); ?>"  aria-expanded="<?php echo (!$is_collapsed ? 'false' : 'true'); ?>">
								<i>Дати на занятия</i>
								<?php if($is_editable){ echo '<button id="add_date" type="button" style="position: absolute; right:4px; top:2px;">+</button>'; } ?>
							</a>
						</div>
						<div id="Course_collapseDates" class="panel-collapse collapse<?php echo (!$is_collapsed ? '' : ' in'); ?>"<?php echo (!$is_collapsed ? ' aria-expanded="false" style="height: 0px;"' : ' aria-expanded="true"'); ?>>
							<div class="panel-body">
								<div class="col-lg-6" id="visibility_div" style="position:relative; left:-25px;">
									<span class="col-lg-12">
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3" >
											Ден
										</div>
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
											Дата
										</div>
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
											Действия
										</div>
									</span>

									<?php
										$chart_dates = array();
										$visibility_array = $course['dates_array'];
										$existing_homeworks = getHomeworksVisibility('course_id = '.$course['course_id'].' GROUP BY homework_id', 'lecture_date');
										//pre_print($existing_homeworks,1);

										foreach($visibility_array as $k => $visibility)
										{
											$chart_dates[] = '"'.date('j M', strtotime($visibility['lecture_date'])).'"';

											$icon = (isset($existing_homeworks[$visibility['lecture_date']]) ? 'h' : 'n');
											$link = header_link(array(CONTROLLER => 'homeworks', ACTION => 'add'));
											$link .= (isset($existing_homeworks[$visibility['lecture_date']]) ? '&'.ID.'='.$existing_homeworks[$visibility['lecture_date']]['homework_id'] : '&default_course='.$course['course_id'].'&default_start='.$visibility['lecture_date']);
									?>
										<span class="col-lg-12">
											<div class="col-xs-12 col-sm-12 col-md-6 col-lg-2" >
												<input type="text" class="form-control" value="<?php echo ($k+1); ?>" disabled="disabled" />
											</div>
											<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5">
												<input name="visibility[<?php echo $k; ?>][lecture_date]" id="vld<?php echo $k; ?>" data-key="<?php echo $k; ?>" type="date" class="form-control visibility_date" value="<?php echo $visibility['lecture_date']; ?>"<?php if(!$is_editable){ echo ' disabled="disabled"'; } ?> />
											</div>
											<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5">
												<button type="button" class="btn-danger<?php echo (!$is_editable ? '" onclick="alert(\'Не можете! Курсът вече е приключил или нямате права!\');"' : ' del_hw_visibility"'); ?>><span class="glyphicon glyphicon-trash"></span></button>
												<div class="btn-group" role="group" aria-label="...">
													<!--
													<a style="float:right;" href="#" data-toggle="modal" data-target="#LogInfoModal" class="new_homework" data-key="<?php echo $k; ?>">
														<button type="button" style="padding:1px;">
															<img class="wpba-document" src="<?php echo SITE_URL.'/'.LOCAL_PATH.'/images/ui/document_icon_n.png'; ?>" />
														</button>
													</a>
													-->
													<a style="float:right;" href="<?php echo $link; ?>" target="_blank" class="new_homework" data-key="<?php echo $k; ?>">
														<button type="button" style="padding:1px;">
															<img class="wpba-document" src="<?php echo SITE_URL.'/'.LOCAL_PATH.'/images/ui/document_icon_'.$icon.'.png'; ?>" />
														</button>
													</a>
													<button style="float:right;" type="button" style="padding:1px;"><img src="<?php echo SITE_URL.'/'.LOCAL_PATH.'/images/ui/test_icon.png'; ?>" /></button>
												</div>
												<div class="btn-group" role="group" aria-label="...">
													<button style="float:right;" type="button" class="refresh_visibility" data-id="<?php echo $visibility['id']; ?>" data-key="<?php echo $k; ?>"><span class="glyphicon glyphicon-refresh"></span></button>
													<a style="float:right;" href="#" data-toggle="modal" data-target="#LogInfoModal" class="load_log_info" data-users="<?php echo $visibility['present_users']; ?>">
														<button type="button" id="btn_u_<?php echo $k; ?>" style="width:46px; text-align: left;"><span class="glyphicon glyphicon-user"></span><?php echo ($visibility['users_count'] < 10 ? '_' : '').$visibility['users_count']; ?></button>
													</a>
													<!--
													<button style="float:right; padding: 1px 0;" type="button"><span class="glyphicon glyphicon-plus-sign"></span></button>
													-->
												</div>
											</div>
										</span>
									<?php } ?>
									<span><!-- Не премахвайте този span, иначе добавянето на нови редове няма да работи <img src="<?php echo DIR_IMAGES; ?>ui/r.png" height="22"> --></span>
								</div>
								<div class="col-lg-6">
									<div id="chart_container" style="border:3px double #2CB3DC; margin-top:10px; "></div>
									<!--
									<svg style="width:100%; height:550px; border:3px double #2CB3DC; margin-top:10px;">
										<text x="50" y="50" fill="red">Тук може да има графика, след като FE2 я направят... :)</text>
									</svg>
									-->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>
	</div>
