<?php
	Global $courses, $columns, $is_admin_user, $source, $_connection, $query_string;


	$search_params = remove_url_elements(array(CONTROLLER, ACTION), $_SERVER['QUERY_STRING']);

	
	$courses_array = array();
	foreach($courses as $k => $course)
	{
		$tmp = array('id', 'course_name', 'teacher_name', 'semester', 'actions'); // 'horarium',
		$tmp = array_flip($tmp);

		if(true) //$view == 'all'
		{
			//$courses[$k]['course_name'] = '<a href="'.link_to( array(CONTROLLER => 'course', ACTION => 'required', ID => $course['course_id']) ).'">'.$course['course_name'].'</a>';
			$tmp['id'] = '<div style="text-align:center;"><span class="show_checkbox">'.$course['course_id'].'</span><input type="checkbox" name="courses[]" value="'.$course['course_id'].'" style="display: none;" /></div>';

			if($course['teacher_id'] == 0)
			{
				$tmp['teacher_name'] = '<span class="red">[НЯМА ИНФОРМАЦИЯ]</span>';

				$teachers[] = 'НЯМА ИНФОРМАЦИЯ|'.$course['teacher_id'];
			}
			else
			{
				if($course['first_name'] == '' && $course['last_name'] == '')
				{
					$tmp['teacher_name'] = '<span class="red">[НЕПОЗНАТ ПОТРЕБИТЕЛ #'.$course['teacher_id'].']</span>';

					$teachers[] = 'НЕПОЗНАТ ПОТРЕБИТЕЛ #'.$course['teacher_id'].'|'.$course['teacher_id'];
				}
				else
				{
					$tmp['teacher_name'] = '<a href="'.header_link( array(CONTROLLER => 'user', ACTION => 'edit', ID => $course['teacher_id']) ).'" title="Управление на потребителя" style="margin-left:3px;"><span class="glyphicon glyphicon-cog"></span></a> ';
					$tmp['teacher_name'] .= $course['teacher_name'];

					$teachers[] = $course['teacher_name'].'|'.$course['teacher_id'];
				}
			}

			//$middle_date = (strtotime($course['end_date']) - strtotime($course['start_date'])) / 2 + strtotime($course['start_date']);
			$middle_date = get_current_period(null, $course['start_date']); // get_current_period(null, $middle_date);

			$tmp['course_name'] = '<a href="'.Prepare_URL($course['course_url']).'">'.$course['course_name'].'</a>'.($course['is_visible'] ? '<i class="fa fa-eye pull-right"></i>' : '<i class="fa fa-eye-slash pull-right red"></i>');
			$tmp['semester'] = '<div style="text-align:center;">'.$middle_date['semester'] . ' <span class="course_dates" style="display:none;">(' . my_date_format($course['start_date'],'d.m.Y') . ' - ' . my_date_format($course['end_date'],'d.m.Y') . ')</span></div>';
			//$tmp['horarium'] = '<div style="text-align:center;">'.$course['horarium'].'</div>';
			$tmp['actions'] = '<a href="'.link_to( array(CONTROLLER => 'exam', ACTION => 'manage', 'course_id' => $course['course_id']) ).'" title="Виж тестовете към този курс"><button><span class="glyphicon glyphicon-question-sign"></span></button></a>';
			//$courses[$k]['actions'] .= '<a href="'.link_to( array(CONTROLLER => 'course', ACTION => 'AddToUser', ID => $course['course_id'], 'OPTION_SET_CLASS' => false) ).'"><button>Кандидатствай</button></a>';

			if($is_admin_user)
			{
				$now = date('Y-m-d');

				// Без значение какъв е курса, винаги се нуждаем от списък с участниците в него... Дали ще показвам кандидати или курсисти зависи от това дали курсът е започнал
				$user_button = '<button><span class="glyphicon glyphicon-user"></span></button>';

				// Времето за кандидатстване е изтекло, но курсът не е почнал все още
				if($course['candidate_until'] <= $now && $now <= $course['start_date'])
				{
					$sort_button = '<button class="btn-info"><span class="glyphicon glyphicon-sort"></span></button>';
					$sort_button = '<a href="'.link_to( array(CONTROLLER => 'course', ACTION => 'PreviewFinal', ID => $course['course_id'], 'OPTION_SET_CLASS' => false) ).'" title="Кликнете тук за да обявите класиране на кандидатите">'.$sort_button.'</a>';

					// Показваме абсолютно всички потребители към този курс
					$user_button = '<a href="'.link_to( array(CONTROLLER => 'user', ACTION => 'index', 'course_id' => $course['course_id'], 'OPTION_SET_CLASS' => false, 'is_send' => 1, MOP_PARAM => 0) ).'">'.$user_button.'</a>';
				}
				else
				{
					$sort_button = '<button class="disabled_link btn-default" disabled="disabled"><span class="glyphicon glyphicon-sort"></span></button>';

					if($now < $course['candidate_until']) // Времето за кандидатстване не е приключило още. Показваме само кандидатите за този курс
					{
						$user_button = '<a href="'.link_to( array(CONTROLLER => 'user', ACTION => 'index', 'course_id' => $course['course_id'], 'OPTION_SET_CLASS' => false, 'is_send' => 1, MOP_PARAM => 0, 'role' => 'candidate') ).'">'.$user_button.'</a>';
					}
					else // Курсът вече е започнал... Показваме всички приети курсисти
					{
						$user_button = '<a href="'.link_to( array(CONTROLLER => 'user', ACTION => 'index', 'course_id' => $course['course_id'], 'OPTION_SET_CLASS' => false, 'is_send' => 1, MOP_PARAM => 0, 'role' => 'student') ).'">'.$user_button.'</a>';
					}
				}

				// Курсът вече е започнал
				if($course['start_date'] < $now)
				{
					$hwrk_button = '<a href="'.link_to( array(CONTROLLER => 'homeworks', ACTION => 'index', 'course_id' => $course['course_id'], VIEW => 'all') ).'"><button><span class="glyphicon glyphicon-check"></span></button></a>';
				}
				else
				{
					$hwrk_button = '<a href="'.link_to( array(CONTROLLER => 'homeworks', ACTION => 'manage', 'course_id' => $course['course_id'], 'OPTION_SET_CLASS' => false) ).'"><button><span class="glyphicon glyphicon-check"></span></button></a>';
				}

				// Курсът е приключил
				if($course['end_date'] < $now)
				{
					$flag_button = '<button class="btn-info"><span class="glyphicon glyphicon-flag"></span></button>';
					$flag_button = '<a href="'.link_to( array(CONTROLLER => 'course', ACTION => 'Result', ID => $course['course_id'], 'OPTION_SET_CLASS' => false, MOP_PARAM => 0) ).'" title="Кликнете тук за да приключите курса и раздадете сертификати за него">'.$flag_button.'</a>';
					$cert_button = '<a href="'.link_to( array(CONTROLLER => 'certificates', ACTION => 'index', 'course_id' => $course['course_id'], 'OPTION_SET_CLASS' => false, MOP_PARAM => 0) ).'"><button><span class="glyphicon glyphicon-certificate"></span></button></a>';
				}
				else
				{
					$flag_button = '<button class="disabled_link btn-default" disabled="disabled"><span class="glyphicon glyphicon-flag"></span></button>';
					$cert_button = '<button class="disabled_link"><span class="glyphicon glyphicon-certificate"></span></button>';
				}

				$edit_button = '<a href="'.link_to( array(CONTROLLER => 'course', ACTION => 'add', ID => $course['course_id'], 'OPTION_SET_CLASS' => false) ).'" title="Кликнете тук за да редактирате курса"><button><img src="'.DIR_IMAGES.'ui/application_form_edit.png" alt="" /></button></a>';
				$stat_button = '<a href="'.link_to( array(CONTROLLER => 'course', ACTION => 'howisgoing', ID => $course['course_id'], 'OPTION_SET_CLASS' => false) ).'" title="Кликнете тук за да видите статистика за курса"><button class="btn-warning"><span class="glyphicon glyphicon-stats"></span></button></a>';

				// Задаваме подредбата на елементите
				$tmp['actions'] .= $edit_button;
				$tmp['actions'] .= $user_button;
				$tmp['actions'] .= $hwrk_button;
				$tmp['actions'] .= $cert_button;
				$tmp['actions'] .= $stat_button;
				$tmp['actions'] .= $sort_button;
				$tmp['actions'] .= $flag_button;
			}

			$courses_array[] = $tmp;
		}

		if($view == 'main')
		{
			$courses[$k]['course_name'] = '<a href="'.Prepare_URL($course['course_after_url']).'">'.$course['course_name'].'</a>';
			//$courses[$k]['actions'] = '';
		}

		//unset($courses[$k]['course_url']);
		//unset($courses[$k]['course_after_url']);
		//unset($courses[$k]['course_description']);
		//unset($courses[$k]['start_date']);
		//unset($courses[$k]['end_date']);
	}

	// Use this for datalist functionality
	$teachers = array_unique($teachers);
	usort($teachers, function($a,$b){ return mb_strtoupper($a) > mb_strtoupper($b); } );

	foreach($teachers as $key => $value)
	{
		$value = explode('|', $value);
		$teachers[$key] = '<option value="'.$value[0].'">'.$value[1].'</option>';
	}

	$columns = array(
		array(
			'text' => '&nbsp;ID',
			'field' => 'c.course_id',
			'key' => 'id'
			),
		array(
			'text' => 'Курс',
			'field' => 'c.course_name',
			'key' => 'course_name'
			),
		array(
			'text' => 'Лектор',
			'field' => 'teacher_name',
			'key' => 'teacher_name'
			),
		array(
			'text' => 'Семестър <img id="btn_calendar" src="'.DIR_IMAGES.'/ui/calendar_icon.png" alt="Calendar" title="Кликни тук за да се покажат начална и крайна дата" height="20" style="position:relative; top:-1px;" />',
			'attr' => 'id="semester_cell" style="width:100px;"'
		),
		//array('text' => '<span class="glyphicon glyphicon-time"></span>', 'attr' => 'width="20" title="Брой учебни часа"'),
		array('text' => 'Операции <img src="'.DIR_IMAGES.'ui/copy.png" alt="Копирай избрания курс" height="24" id="btn_copy" style="display:none;" />', 'attr' => 'width="243"')
	);
	//render_table($courses_array, $columns);
?>
<div class="container">
<?php
	if(SHOW_DEBUG_INFO){
?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel-group" id="accordion1" style="margin-top:20px;">
				<div class="panel panel-<?php echo (count($courses) != 0 ? 'info' : 'danger'); ?>">
					<div class="panel-heading panel-plus-link">
							<a class="accordion-toggle<?php echo (count($courses) != 0 ? ' collapsed' : ''); ?>" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne1" aria-expanded="<?php echo (count($courses) != 0 ? 'false' : 'true'); ?>">
								<b><h5 style="display:inline;">Управление на Курсове</h5><?php echo HelpButton(); ?></b>
							</a>
					</div>
					<div id="collapseOne1" class="panel-collapse collapse<?php echo (count($courses) != 0 ? '' : ' in'); ?>" aria-expanded="<?php echo (count($courses) != 0 ? 'false' : 'true'); ?>">
						<div class="panel-body">
							<?php echo SQL_DEBUG($query_string); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php } ?>
	<div class="row">
		<div class="col-lg-12">
<?php
		database_open();
		$active_key = 'A';
		$courses_optgroups = array();
		$courses_ancestors = get_all_ancestors();
		$courses_ancestors[$active_key] = array('ancestor_name' => 'Текущи', 'ancestor_id' => $active_key);
		ksort($courses_ancestors);

		$all_courses = get_all_courses('1=1 ORDER BY descendant_of ASC, course_name ASC');

		$current_courses = getCurrentCourses();
		//$current_courses[0] = array('course_id' => 0, 'course_name' => 'Всички активни курсове');
		//ksort($current_courses); // Sort by course_id (it is array key) to make "Всички активни курсове" on top

		foreach($current_courses as $tmp_course)
		{
			$all_courses[] = array('descendant_of' => $active_key, 'course_id' => $tmp_course['course_id'], 'course_name' => $tmp_course['course_name']);
		}

		foreach($all_courses as $course)
		{
			$courses_optgroups[$course['descendant_of']][] = '<option value="'.$course['course_id'].'"'.($source['course_id'] == $course['course_id'] ? ' selected="selected"' : '').'>'.$course['course_name'].' ['.$course['course_id'].']</option>';
		}

		foreach($courses_optgroups as $ancestor_id => $options)
		{
			$courses_optgroups[$ancestor_id] = '<optgroup id="ancestor_'.$ancestor_id.'" label="'.$courses_ancestors[$ancestor_id]['ancestor_name'].'">'.implode($options).'</optgroup>';
		}

		$current = $courses_optgroups[$active_key];
		$unknown = $courses_optgroups[0];

		//unset($courses_ancestors[$active_key]);
		unset($courses_optgroups[$active_key]);
		unset($courses_optgroups[0]);

		$courses_optgroups = $current . implode(PHP_EOL, $courses_optgroups) . $unknown;

		if(have_messages())
		{
			show_all_messages();
			clear_all_messages();
		}
?>
			<form method="GET" action="<?php echo header_link(array(CONTROLLER => 'homeworks', ACTION => 'index')); ?>">
				<div class="form-group">
					<div class="row">

						<input type="hidden" name="<?php echo CONTROLLER; ?>" value="<?php echo $source[CONTROLLER]; ?>" />
						<input type="hidden" name="<?php echo ACTION; ?>" value="<?php echo $source[ACTION]; ?>" />

						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
							<label for="ancestor_id">
								Курс Прародител<!-- (Родоначалник) -->
							</label>

							<select name="ancestor_id" id="ancestor_id" class="form-control">
								<option value="-1">- Без Филтър -</option>
								<?php foreach($courses_ancestors as $v){
									echo '<option value="'.$v['ancestor_id'].'"'.($source['ancestor_id'] == $v['ancestor_id'] ? ' selected="selected"' : '').'>'.$v['ancestor_id'].'. '.$v['ancestor_name'].'</option>';
								} ?>
							</select>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-4">
							<label for="course_id">
								Курс
							</label>

							<select name="course_id" id="course_id" class="form-control">
								<option value="-1">- Без Филтър -</option>
								<option value="0"<?php if(0 == $source['course_id']) { echo ' selected="selected"'; } ?>>Всички активни курсове</option>
								<?php echo $courses_optgroups; ?>
							</select>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-3">
							<label for="teacher_name">
								Лектор
							</label>

							<input type="text" list="teachers_list" name="teacher_name" id="teacher_name" class="form-control" value="<?php echo (isset($_GET['teacher_name']) ? $_GET['teacher_name'] : ''); ?>" placeholder="Име на Лектор"  /></label>
							<datalist id="teachers_list"><?php echo implode('', $teachers); ?></datalist>
						</div>

						<input type="hidden" name="check_them" id="check_them" value="0" />
						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
							<button type="submit" style="margin-top:30px; vertical-align: bottom;"><span class="glyphicon glyphicon-filter"></span> Филтрирай</button>
							<button type="submit" style="height:32px;" id="check_btn">
								<span title="Homeworks" class="glyphicon glyphicon-check"></span> Провери
							</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<?php render_table($courses_array, $columns, 'class="table-w100 table-striped table-hover table-bordered"'); ?>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		$('.show_checkbox').mouseover(function(){
			$(this).hide();
			$(this).next('input[type=checkbox]').show();
		});

		$('input[type=checkbox]').mouseout(function(){
			if(!$(this).prop('checked')){
				$(this).hide();
				$(this).prev('.show_checkbox').show(750);
			}
		});

		$('input[type=checkbox]').click(function(){
			if($('input[type=checkbox]:checked').length > 0){
				$('#btn_copy').show();
			}else{
				$('#btn_copy').hide();
			}
		});

		$('#btn_copy').click(function(){
			var courses_to_copy = $('input[type=checkbox]:checked').serialize();

			window.location.assign('<?php echo header_link(array(CONTROLLER => 'course', ACTION => 'add')); ?>&' + courses_to_copy);
		});

		$('#btn_calendar').click(function(){
			if('100px' == $('#semester_cell').css('width')){
				$('#semester_cell').animate({ width: "220px" }, 250 );
				$('.course_dates').delay(250).show(750);
			}else{
				$('.course_dates').hide(1000);
				$('#semester_cell').animate({ width: "100px" }, 1000 );
			}
		});

		$('#ancestor_id').change(function(){
			var selected_ancestor = $(this).val();
			if(selected_ancestor == -1)
			{
				$('#course_id optgroup').prop('disabled', false);
			}
			else
			{
				$('#course_id').val(0);
				$('#course_id optgroup').prop('disabled', true);
				$('#ancestor_'+selected_ancestor).prop('disabled', false);
			}
		});
	});
</script>