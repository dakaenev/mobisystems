<?php
	Global $is_admin_user, $source, $edit_course, $courses, $is_collapsed, $admins_array, $assistants_array;

	if($is_admin_user)
	{
		//pre_print($course);
?>
<?php
	if(have_messages())
	{
		echo '<div class="container">';
		show_all_messages();
		echo '</div>';
	}

	if(!has_error_messages())
	{
		database_open();
?>
<form id="Course_EditForm" method="post">
	<div class="container">
		<?php
			foreach($courses as $course)
			{
				$is_editable  = true;
				$caption = 'Копиране на Курс №'.$course['course_id'].': '.$course['course_name'];

		?>
		<div class="row">
			<div class="col-lg-12">
				<div class="panel-group" id="Course_accordion_<?php echo $course['course_id']; ?>" style="margin-top:20px;">
					<div class="panel panel-info">
						<div class="panel-heading panel-plus-link">
							<a data-toggle="collapse" data-parent="#Course_accordion_<?php echo $course['course_id']; ?>" href="#Course_collapseOne_<?php echo $course['course_id']; ?>" class="accordion-toggle<?php echo ($is_collapsed ? ' collapsed' : ''); ?>" aria-expanded="<?php echo ($is_collapsed ? 'false' : 'true'); ?>">
								<b><h5 style="display:inline;"><?php echo $caption; ?></h5><?php echo HelpButton(); ?></b><!-- <button id="add_hw_visibility" type="button" style="position: relative; left:-230px;">+</button> -->
							</a>
						</div>
						<div id="Course_collapseOne_<?php echo $course['course_id']; ?>" class="panel-collapse collapse<?php echo ($is_collapsed ? '' : ' in'); ?>"<?php echo ($is_collapsed ? ' aria-expanded="false" style="height: 0px;"' : ' aria-expanded="true"'); ?>>
							<div class="panel-body">
								<!-- <input type="hidden" name="edit_course" value="<?php echo $course['course_id']; ?>" /> -->
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<label for="course_name">
										Име на курс
									</label>

									<input type="text" name="course_name" id="course_name" class="form-control" value="<?php echo $course['course_name']; ?>" />
								</div>
								<?php if($is_admin_user){ ?>
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<label for="ancestor_id">
										Курс Прародител
									</label>

									<select name="ancestor_id" id="ancestor_id" class="form-control">
										<?php echo ReturnDropDown(get_all_ancestors(null, 'ancestor_id, ancestor_name'), $course['descendant_of'])?> ?>
									</select>
								</div>
								<?php } ?>
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<label for="course_after_url">
										URL за материали
									</label>

									<input type="text" name="course_after_url" id="course_after_url" class="form-control" value="<?php echo $course['course_after_url']; ?>" />
								</div>
								<?php if($is_admin_user){ ?>
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<label for="course_url">
										URL за кандидатстване
									</label>

									<input type="text" name="course_url" id="course_url" class="form-control" value="<?php echo $course['course_url']; ?>" />
								</div>
								<?php } ?>
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<label for="start_date">
										Начална дата
									</label>

									<input type="date" name="start_date" id="start_date" class="form-control" value="<?php echo $course['start_date']; ?>" />
								</div>
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<label for="end_date">
										Крайна дата
									</label>

									<input type="date" name="end_date" id="end_date" class="form-control" value="<?php echo $course['end_date']; ?>" />
								</div>
								<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
									<label for="teacher_id">
										Лектор
									</label>

									<select name="teacher_id" id="teacher_id" class="form-control choose_other_teacher">
										<?php
										$teachers = array_merge($admins_array, $assistants_array);
										if(!in_array($course['teacher_id'], $teachers))
										{
											$teachers[] = $course['teacher_id'];
										}
										$teachers = implode(',', array_unique($teachers));
										$teachers_query = exec_query('SELECT user_id, CONCAT(first_name, " ", last_name," (", user_id, ")") AS user_full_name FROM users WHERE user_id IN ('.$teachers.') ORDER BY user_full_name');
										$teachers_list = array(0 => '-- Моля изберете --') + to_dropdown_array($teachers_query, 'user_id', 'user_full_name') + array(-1 => '-- Друг --');
										echo ReturnDropDown($teachers_list, $course['teacher_id'])?> ?>
									</select>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
									<label for="horarium">
										Хорариум
									</label>

									<input type="number" name="horarium" id="horarium" class="form-control" value="<?php echo $course['horarium']; ?>" />
								</div>
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<label for="candidate_until">
										Кандидатстване до:
									</label>
									<input type="datetime" name="candidate_until" id="candidate_until" class="form-control" value="<?php echo $course['candidate_until']; ?>" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>
	<?php
		if(count($courses) == 1)
		{
			$course = reset($courses);
			$middle_date = get_current_period(null, $course['start_date']);
	?>
		<div class="row">
			<div class="col-lg-12">
				<div class="panel-group" id="Course_accordion2" style="margin-top:20px;">
					<div class="panel panel-info">
						<div class="panel-heading panel-heading-link">
							<a data-toggle="collapse" data-parent="#Course_accordion2" href="#Course_collapseDates" class="accordion-toggle<?php echo (!$is_collapsed ? ' collapsed' : ''); ?>"  aria-expanded="<?php echo (!$is_collapsed ? 'false' : 'true'); ?>">
								<i>Дати на занятия</i>
								<?php if($is_editable){ echo '<button id="add_date" type="button" style="position: absolute; right:4px; top:2px;">+</button>'; } ?>
							</a>
						</div>
						<div id="Course_collapseDates" class="panel-collapse collapse<?php echo (!$is_collapsed ? '' : ' in'); ?>"<?php echo (!$is_collapsed ? ' aria-expanded="false" style="height: 0px;"' : ' aria-expanded="true"'); ?>>
							<div class="panel-body">
								<div class="col-lg-6" id="visibility_div" style="position:relative; left:-25px;">
									<span class="col-lg-12">
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3" >
											Ден
										</div>
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
											Дата
										</div>
										<div class="col-xs-12 col-sm-12 col-md-6 col-lg-3">
											Действия
										</div>
									</span>

									<?php
										$chart_dates = array();
										$visibility_array = $course['dates_array'];
										foreach($visibility_array as $k => $visibility){
											$chart_dates[] = '"'.date('j M', strtotime($visibility['lecture_date'])).'"';
									?>
										<span class="col-lg-12">
											<div class="col-xs-12 col-sm-12 col-md-6 col-lg-2" >
												<input type="text" class="form-control" value="<?php echo ($k+1); ?>" disabled="disabled" />
											</div>
											<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5">
												<input name="visibility[<?php echo $k; ?>][lecture_date]" id="vld<?php echo $k; ?>" data-key="<?php echo $k; ?>" type="date" class="form-control visibility_date" value="<?php echo $visibility['lecture_date']; ?>"<?php if(!$is_editable){ echo ' disabled="disabled"'; } ?> />
											</div>
											<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5">
												<button type="button" class="btn-danger<?php echo (!$is_editable ? '" onclick="alert(\'Не можете! Курсът вече е приключил или нямате права!\');"' : ' del_hw_visibility"'); ?>><span class="glyphicon glyphicon-trash"></span></button>
												<div class="btn-group" role="group" aria-label="...">
													<a style="float:right;" href="#" data-toggle="modal" data-target="#LogInfoModal" class="new_homework" data-key="<?php echo $k; ?>">
														<button type="button" style="padding:1px;">
															<img class="wpba-document" src="<?php echo SITE_URL.'/'.LOCAL_PATH.'/images/ui/document_icon_n.png'; ?>" />
														</button>
													</a>
													<button style="float:right;" type="button" style="padding:1px;"><img src="<?php echo SITE_URL.'/'.LOCAL_PATH.'/images/ui/test_icon.png'; ?>" /></button>
												</div>
												<div class="btn-group" role="group" aria-label="...">
													<button style="float:right;" type="button" class="refresh_visibility" data-id="<?php echo $visibility['id']; ?>" data-key="<?php echo $k; ?>"><span class="glyphicon glyphicon-refresh"></span></button>
													<a style="float:right;" href="#" data-toggle="modal" data-target="#LogInfoModal" class="load_log_info" data-users="<?php echo $visibility['present_users']; ?>">
														<button type="button" id="btn_u_<?php echo $k; ?>" style="width:46px; text-align: left;"><span class="glyphicon glyphicon-user"></span><?php echo ($visibility['users_count'] < 10 ? '_' : '').$visibility['users_count']; ?></button>
													</a>
													<!--
													<button style="float:right; padding: 1px 0;" type="button"><span class="glyphicon glyphicon-plus-sign"></span></button>
													-->
												</div>
											</div>
										</span>
									<?php } ?>
									<span><!-- Не премахвайте този span, иначе добавянето на нови редове няма да работи <img src="<?php echo DIR_IMAGES; ?>ui/r.png" height="22"> --></span>
								</div>
								<div class="col-lg-6">
								<?php
									//$exams = get_all_exams('rt.exam_type = "precondition" AND c.course_id = '.$source[ID]);
									//pre_print($exams, 614);

									//foreach($exams as $exam)
									//{
										//echo '';
									//}
								?>
									<!--
									<div id="chart_container" style="border:3px double #2CB3DC; margin-top:10px; "></div>
									<svg style="width:100%; height:550px; border:3px double #2CB3DC; margin-top:10px;">
										<text x="50" y="50" fill="red">Тук може да има графика, след като FE2 я направят... :)</text>
									</svg>
									-->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div style="text-align:center;">
				<?php
				if($is_admin_user){
					echo '<a href="'.link_to(array(CONTROLLER => 'course', ACTION => 'add')).'"><button type="button" style="width:71px;"><img src="'.DIR_IMAGES.'ui/add_page_16.gif" alt="" /> New</button></a>';
					echo '<a href="'.link_to(array(CONTROLLER => 'course', ACTION => 'index')).'"><button type="button"><img src="'.DIR_IMAGES.'ui/0.png"></button></a>';
					echo '<a href="#" onclick="document.getElementById(\'edit_form\').reset();"><button type="button"><img src="'.DIR_IMAGES.'ui/r.png" height="22"></button></a>';
					echo '<a href="#" onclick="document.getElementById(\'edit_form\').submit();"><button type="submit"><img src="'.DIR_IMAGES.'ui/1.png"></button></a>';
				}
				?>
				</div>
			</div>
			<div class="col-lg-12"><br /></div>
		</div>
	</div>
</form>
<div class="modal fade" id="LogInfoModal" tabindex="-1" role="dialog" aria-labelledby="LogInfoModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="width:900px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">

			</div>
			<div class="modal-footer" style="border-top:0px;">
			</div>
		</div>
	</div>
</div>
<script>
	var group_index = <?php echo (count($visibility_array) + 1); ?>;
	var   key_index = <?php echo (count($visibility_array) - 0); ?>;

	$(document).ready(function(){
		$('.load_log_info').click(function(){
			$('#LogInfoModal .modal-title').text('Детайлна информация');
			$('#LogInfoModal .modal-body').text('Моля изчакайте, данните се зареждат...');

			$('#LogInfoModal .modal-body').load('<?php echo header_link(array(CONTROLLER => 'user', ACTION => 'index', 'is_send' => '1', 'users' => '')); ?>'+$(this).data('users')+' #users_main_div');
		});

		$('.new_homework').click(function(){
			$('#LogInfoModal .modal-title').text('Домашни');
			$('#LogInfoModal .modal-body').text('Моля изчакайте, данните се зареждат...');

			$('#LogInfoModal .modal-body').load('<?php echo header_link(array(CONTROLLER => 'homeworks', ACTION => 'add', 'layout' => 'empty', 'start_date' => '')); ?>'+$("#vld" + $(this).data('key')).val()+' #HW_EditForm');
		});

		$('#add_date').click(function(){

			var code;

			code = '<span class="col-lg-12">';
			code = code + '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-2"><input type="text" class="form-control" value="'+group_index+'" disabled="disabled" /></div>';
			code = code + '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5"><input name="visibility['+key_index+'][lecture_date]" id="vld'+key_index+'" data-key="'+key_index+'" type="date" class="form-control visibility_date" value="<?php echo $default_date; ?>" /></div>';
			code = code + '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-5"><button type="button" class="del_hw_visibility"><img src="<?php echo DIR_IMAGES; ?>ui/0.png"></button></div>';
			code = code + '</span>';

			$('#visibility_div span:last').before(code);

			group_index++;
			key_index++;

			return false;
		});

		// Тук трябва да е с ON а не просто с CLICK, за да работи този код и за новосъздадените динамично бутони
		$('#visibility_div').on('click','.del_hw_visibility', function() {
			$(this).parent().parent().remove();
		});

		$('#visibility_div').on('click','.refresh_visibility', function() {
			var el_id = '#btn_u_' + $(this).data('key');
			var current_html = $(el_id).html();
			var nowDate = new Date();
			var myDate = new Date( $('#vld' + $(this).data('key')).val() );

			if(nowDate.getTime() < myDate.getTime()){
				alert('Не можете да проверявате бъдеща дата!');
			}else{
				$(el_id).html('<span class="glyphicon glyphicon-time"></span>');
				$.ajax({
					dataType: 'json',
					url: "<?php echo header_link(array(CONTROLLER => 'logs', ACTION => 'SetPresentedUsers', 'layout' => 'empty', ID => '')); ?>" + $(this).data('id'),
					success: function(data){
						console.log(data);

						if(data.users_count == 0){
							$(el_id).html(current_html);
						}else{
							$(el_id).html('<span class="glyphicon glyphicon-user">' + data.users_count);
							$(el_id).parent().data('users', data.users_list);
						}
					}
				});
			}
		});

		$('#visibility_div').on('change','.visibility_date', function() {
			var myDate = new Date($(this).val());

			for(var i = $(this).data('key')+1; i <= key_index; i++)
			{
				if (document.getElementById('vld'+i) != null)
				{
					var myDate = new Date(myDate.getTime() + (60*60*24*7*1000));

					$('#vld'+i).val(myDate.toISOString().substr(0, 10));
				}
			}
		});

		$('.choose_other_teacher').change(function(){
			if($(this).val() == -1)
			{
				var new_teacher_id = prompt('Въведете ID на Лектора:', '0');

				if(isNaN(new_teacher_id) || new_teacher_id == null)
				{
					alert('Некоректна стойност!');
				}
				else
				{
					$(this).append('<option value="' + new_teacher_id + '">Потребител (' + new_teacher_id + ')</option>');
					$(this).val(new_teacher_id);
				}
			}
		});

		$('#start_date').change(function(){
			var myDate = new Date($(this).val());
			var myDate = new Date(myDate.getTime() + (60*60*24*7*14*1000)); // 15 weeks later

			$('#end_date').val(myDate.toISOString().substr(0, 10));
			$('#vld0').val($('#start_date').val());
			$('#vld0').trigger('change');
		});

		<?php if( count($visibility_array) > 0 ){ // count($visibility_array) > 0 ?>
		// Set up the chart
		var chart = new Highcharts.Chart({
			chart: {
				renderTo: 'chart_container',
				type: 'column',
				height: 550,
				options3d: {
					enabled: true,
					alpha: 15,
					beta: 15,
					depth: 100,
					viewDistance: 25
				}
			},
			credits: {
				enabled: false,
				href: "PW/",
				text: "PW"
			},
			exporting: {
				filename: "chart_dates_course-<?php echo $course['course_id']; ?>"
			},
			subtitle: {
				text: "Курс №<?php echo $course['course_id']; ?> <?php echo $course['course_name']; ?> (<?php echo $middle_date['semester']; ?>)"
			},
			title: {
				text: "Диаграма с присъствия"
			},
			xAxis: {
				min: 0,
				max: <?php echo count($visibility_array)-1; ?>, // 14
				categories: [<?php echo implode(', ', $chart_dates); ?>]
			},
			yAxis: {
				min: 0,
				title: {
				  text: ""
				}
			},
			plotOptions: {
				column: {
					depth: 25
				}
			},
			series: [{
				name: "В залата",
				data: [<?php echo implode(', ', array_column($visibility_array, 'users_count')); ?>]
			}]
		});
		<?php } ?>
	});
</script>
<?php
			database_close();
		}

		clear_all_messages();
	}
	else
	{
		page_not_access();
	}
?>