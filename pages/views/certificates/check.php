<?php
	Global $estimated_tests_count, $next_test_id, $Requested_From, $is_live_preview, $exam_questions, $exam_id, $_connection, $questions_groups, $is_admin_user, $history_data, $QUESTIONS_GROUPS, $user_id;

	//Pre_Print($exam_questions, true);
	//Pre_Print($questions_groups);
	//Pre_Print($_SESSION['exam_info']);
?>
<div class="row" style="background-color:#3cc954;">
	<div class="col-lg-2"></div>
	<div class="col-lg-8" style="color:white; height:470px; background-image: url('<?php echo get_template_directory_uri(); ?>/img/wa_watermark.png'); background-repeat:no-repeat; background-position: right bottom;">
		<center>
			<br>
			<p style="margin-bottom:20px; background: url('<?php echo get_template_directory_uri(); ?>/img/sprite.png')  no-repeat -770px -180px; width: 84px; height: 84px;">&nbsp;</p>
			<br>
			<span style="font-size:48px; font-family:'Open Sans';">Вашият тест e изпратен успешно!</span><br>
			<br><br>
			<span style="font-size:28px; font-family:'Open Sans'; font-weight:100;">
				<?php if($estimated_tests_count > 0){ ?>
				Не забравяйте да решите <?php echo ($estimated_tests_count == 1 ? 'последния 1 тест' : 'оставащите '.$estimated_tests_count.' теста'); ?>,<br>преди изтичане на крайния срок за кандидатстване.
				<?php }else{ ?>
				Благодарим Ви, че решихте всички тестове! <br><!-- Ние ще се свържем с вас, когато излезе крайното класиране. -->
				<?php } ?>
			</span>
		</center>
	</div>
	<div class="col-lg-2"></div>
</div>
<div class="row">
	<div class="col-lg-12">
		<center>
		<br><br>
		<a href="<?php echo site_url('/profile-2/'); ?><?php if(isset($_GET['user_id']) && is_super_admin()){ echo '?user_id='.$_GET['user_id']; } ?>" class="btn btn-ar button button-border-primary button-rounded upload_button" style="margin-right:40px;">Учебен портал</a>
		<?php if(isset($next_test_id) && $next_test_id > 0){ ?>
		<a href="<?php echo link_to(array( CONTROLLER => 'exam', ACTION => 'solve', ID => $next_test_id, 'OPTION_SET_CLASS' => false)); ?>" class="btn btn-success stepy-finish">Решаване на следващия тест &gt;</a>
		<?php } ?>
		</center>
		<br><br><br><br><br><br><br>
	</div>
</div>

