<div class="container">
<?php
	Global $is_admin_user, $certificates, $source, $query_string, $users, $one_type_only;

	database_open();

	$col_size = (!$one_type_only && count($certificates) >= 9 ? 5 : 12);

	if(SHOW_DEBUG_INFO){
?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel-group" id="accordion1" style="margin-top:20px;">
				<div class="panel panel-<?php echo (count($certificates) != 0 ? 'info' : 'danger'); ?>">
					<div class="panel-heading panel-plus-link">
							<a class="accordion-toggle<?php echo (count($certificates) != 0 ? ' collapsed' : ''); ?>" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne1" aria-expanded="<?php echo (count($certificates) != 0 ? 'false' : 'true'); ?>">
								<b><h5 style="display:inline;">Управление на Сертификати</h5></b>
							</a>
					</div>
					<div id="collapseOne1" class="panel-collapse collapse<?php echo (count($certificates) != 0 ? '' : ' in'); ?>" aria-expanded="<?php echo (count($certificates) != 0 ? 'false' : 'true'); ?>">
						<div class="panel-body">
							<?php echo SQL_DEBUG($query_string); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php } ?>
<?php
	$courses_optgroups = array();
	$courses_ancestors = get_all_ancestors();

	$courses_array = get_all_courses('descendant_of > 0 AND end_date <= "'.date('Y-m-d', strtotime('-1 week')).'" ORDER BY descendant_of ASC, course_name ASC');

	foreach($courses_array as $course)
	{
		$courses_optgroups[$course['descendant_of']][] = '<option value="'.$course['course_id'].'"'.($source['course_id'] == $course['course_id'] ? ' selected="selected"' : '').'>'.$course['course_name'].' ['.$course['course_id'].']</option>';
	}

	foreach($courses_optgroups as $ancestor_id => $options)
	{
		$courses_optgroups[$ancestor_id] = '<optgroup id="ancestor_'.$ancestor_id.'" label="'.$courses_ancestors[$ancestor_id]['ancestor_name'].'">'.implode($options).'</optgroup>';
	}

	$courses_optgroups = implode(PHP_EOL, $courses_optgroups);
?>
	<div class="row">
		<div class="col-lg-12">
			<form method="GET" action="<?php echo header_link(array(CONTROLLER => 'certificates', ACTION => 'index')); ?>">
				<div class="form-group">
					<div class="row">

						<input type="hidden" name="<?php echo CONTROLLER; ?>" value="certificates" />
						<input type="hidden" name="<?php echo ACTION; ?>" value="index" />

						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
							<label for="ancestor_id">
								Курс Прародител<!-- (Родоначалник) -->
							</label>

							<select name="ancestor_id" id="ancestor_id" class="form-control">
								<option value="-1">- Без Филтър -</option>
								<?php foreach($courses_ancestors as $v){
									echo '<option value="'.$v['ancestor_id'].'"'.($source['ancestor_id'] == $v['ancestor_id'] ? ' selected="selected"' : '').'>'.$v['ancestor_id'].'. '.$v['ancestor_name'].'</option>';
								} ?>
							</select>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
							<label for="course_id">
								Курс
							</label>

							<select name="course_id" id="course_id" class="form-control">
								<option value="0">- Без Филтър -</option>
								<?php echo $courses_optgroups; ?>
							</select>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
							<label for="from_city">
								Филиал
							</label>

							<select name="from_city" id="from_city" class="form-control">
								<option value="" <?php if(!empty($_GET['from_city']) && $_GET['from_city'] == '' || empty($_GET['from_city'])) echo 'selected'; ?>> - Без филтър - </option>
								<option value="PLD" <?php if(!empty($_GET['from_city']) && $_GET['from_city'] == 'PLD') echo 'selected'; ?>>Пловдив</option>
								<option value="STZ" <?php if(!empty($_GET['from_city']) && $_GET['from_city'] == 'STZ') echo 'selected'; ?>>Стара Загора</option>
							</select>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-2">
							<label for="full_name">
								Курсист
							</label>

							<input type="hidden" name="not_show" id="not_show" value="2" />
							<input type="text" name="full_name" id="full_name" class="form-control" value="<?php echo (isset($_GET['full_name']) ? $_GET['full_name'] : ''); ?>" placeholder="Име на кирилица" list="users_list" />
							<datalist id="users_list"><?php echo implode('', $users); ?></datalist>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
							<button type="submit" style="margin-top:29px; border-top-right-radius: 0; border-bottom-right-radius: 0; padding: 5px 12px;" class="pull-left btn btn-success" id="submit_button"><span class="glyphicon glyphicon-filter"></span> Филтрирай</button>
							<a href="<?php echo header_link(array(CONTROLLER => 'certificates', ACTION => 'export')); ?>"><button type="button" class="pull-right btn btn-primary" style="margin-top:29px;">Export <span class="glyphicon glyphicon-export"></span></button></a>
							<button type="button" style="margin-top:29px;" class="pull-left" id="not_show_btn"><span class="glyphicon glyphicon-certificate wa-certificate-icon-<?php echo abs($source['not_show']-1); ?>"></span></button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-<?php echo ($col_size+1); ?> col-lg-<?php echo $col_size; ?>">
			<form method="post" action="<?php echo header_link( array(CONTROLLER => 'mail', ACTION => 'create')); ?>">
<?php
	if($is_admin_user)
	{
		//pre_print($questions);
		//echo '<a class="fake_tab'.(isset($source[VIEW]) && $source[VIEW] == 'all' ? ' fake_tab_active' : '').'" href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element(VIEW, $_SERVER['QUERY_STRING']).'&' : '').VIEW.'=all"><img src="'.DIR_IMAGES.'ui/preferences_contact_list.png" /> Всички</a>';

		if(isset($source['test_id']))
		{
			echo '<a class="fake_tab fake_tab_remove" title="Кликнете тук за да премахнете филтъра!" href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element('test_id', $_SERVER['QUERY_STRING']).'&' : '').'">Филтър: test_id = '.$source['test_id'].'</a>';
		}

		$columns = array(
			//array('text' => '<a href="'.link_to(array(CONTROLLER => 'question', ACTION => 'add')).'"><button><img src="'.DIR_IMAGES.'ui/add_page_16.gif" alt="" /></button></a>', 'key' => 'functions'),
			//array('text' => 'ID', 'field' => 'question_id'),
			array('text' => '&nbsp;'),
			array('text' => '&nbsp;Курсист', 'field' => 'full_name', 'key' => 'user_id'),
			array('text' => '&nbsp;Курс', 'field' => 'c.course_name', 'key' => 'course_id'),
			array('text' => '&nbsp;Сериен №&nbsp;', 'field' => 'uc.serial_number'),
			//array('text' => 'Крайно', 'field' => 'is_active'),
			array('text' => '<span class="glyphicon glyphicon-certificate" title="Брой сертификати"></span>', 'field' => 'certificates_count'),
		);

		if($one_type_only)
		{
			$columns[] = array('text' => 'Телефон');
			$columns[] = array('text' => 'Сертификат?'); //'Искате ли сертификат?'
			$columns[] = array('text' => '<img src="'.DIR_IMAGES.'ui/icon_mail.gif" alt="Mail" id="btn_mail" style="cursor:pointer;" />', 'attr' => 'width="16"');
		}

		echo '<div class="table-responsive">'.return_table($certificates, $columns, 'class="table-w100 table-striped table-hover table-bordered"').'</div>';
		echo (5 == $col_size ? '</div><div class="col-xs-12 col-sm-12 col-md-6 col-lg-7"><iframe name="result_frame" id="result_frame" width="100%" height="600" src="" style="border: 1px solid black; zoom: 0.75; -moz-transform: scale(0.75); -moz-transform-origin: 0 0;">Your browser not understand from iframe</iframe>' : '');
		echo 'Общо '.count($certificates).' сертификата взети от '.count($users).' различни курсиста!';
	}
	else
	{
		page_not_access();
	}
?>
			</form>
		</div>
	</div>
</div>

<script>
	function resizeIframe(){
		var resultIframe = $('#result_frame');
		var iframeWidth = resultIframe.outerWidth();

		resultIframe.height(iframeWidth / 1.45);
	}

	$(document).ready(function(){

		resizeIframe();

		$(window).resize(function(){
			resizeIframe();
		});

		$('#ancestor_id').change(function(){
			var selected_ancestor = $(this).val();
			if(selected_ancestor == -1)
			{
				$('#course_id optgroup').prop('disabled', false);
			}
			else
			{
				$('#course_id').val(0);
				$('#course_id optgroup').prop('disabled', true);
				$('#ancestor_'+selected_ancestor).prop('disabled', false);
			}
		});

		$('#not_show_btn').click(function(){
			$('#not_show').val(<?php echo abs($source['not_show']-1); ?>);
			$('#submit_button').trigger('click');
		});

		$('#btn_mail').click(function(){
			$(this).closest("form").submit();
		});
	});
</script>