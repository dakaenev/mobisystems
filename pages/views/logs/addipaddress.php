<?php
	Global $is_admin_user, $source, $form_data, $admins_array, $is_edit;

	if($is_admin_user)
	{
?>
<?php
	if(have_messages())
	{
		echo '<div class="container">';
		show_all_messages();
		echo '</div>';
	}

	if(!has_error_messages())
	{
		database_open();

		$caption = (isset($source[ID]) ? 'Редакция' : 'Добавяне').' на IP адрес';
?>
<form id="edit_form" method="post">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="panel-group" id="form_accordion" style="margin-top:20px;">
					<div class="panel panel-info">
						<div class="panel-heading panel-plus-link">
							<a data-toggle="collapse" data-parent="#form_accordion" href="#ip_form" class="accordion-toggle" aria-expanded="true">
								<b><h5 style="display:inline;"><?php echo $caption; ?></h5><?php echo HelpButton(); ?></b>
							</a>
						</div>
						<div id="ip_form" class="panel-collapse collapse in" aria-expanded="true">
							<div class="panel-body">
								<!-- <input type="hidden" name="edit_course" value="<?php echo $course['course_id']; ?>" /> -->
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<label for="ip_address">
										IP Адрес
									</label>

									<input type="text" name="ip_address" id="ip_address" class="form-control" value="<?php echo $form_data['ip_address']; ?>" placeholder="<?php echo $_SERVER['REMOTE_ADDR']; ?>" />
								</div>
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<label for="description">
										Кратко описание
									</label>

									<input type="text" name="description" id="description" class="form-control" value="<?php echo htmlentities(stripslashes($form_data['description'])); ?>" />
								</div>
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<label for="start_date">
										Начална дата
									</label>

									<input type="date" name="start_date" id="start_date" class="form-control" value="<?php echo $form_data['start_date']; ?>" />
								</div>
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<label for="end_date">
										Крайна дата
									</label>

									<input type="date" name="end_date" id="end_date" class="form-control" value="<?php echo $form_data['end_date']; ?>" />
								</div>
								<?php if(!isset($source[ID])) { ?>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<br />
									<label>
										Добавяне / Заместване
									</label>
									<table class="table table-responsive  table-striped table-condensed table-hover table-bordered">
									<?php
										$look_for_date = date('Y-m-d');
										$ip_array = get_all_addresses('start_date <= "'.$look_for_date.'" AND end_date >= "'.$look_for_date.'" ORDER BY description');

										array_unshift($ip_array, array('id' => 0));
										foreach($ip_array as $record)
										{
											echo '<tr>';
											echo '<td><input type="radio" name="replace_id" id="rid_'.$record['id'].'" value="'.$record['id'].'"'.($form_data['replace_id']  == $record['id'] ? ' checked="checked"' : '').' /></td>';

											if(0 == $record[id])
											{
												echo '<td colspan="2"><label for="rid_'.$record['id'].'"><b>Добавяне на нов запис</b></label></td>';
												echo '<th>Начало</th><th>Край</th>';
											}
											else
											{
												echo '<td><label for="rid_'.$record['id'].'"><b class="red">Замести</b> '.$record['description'].'</label></td>';
												echo '<td><a href="https://www.ip2location.com/'.$record['ip_address'].'" target="_blank">'.$record['ip_address'].'</a></td>';
												echo '<td>'.$record['start_date'].'</td>';
												echo '<td>'.$record['end_date'].'</td>';
											}
											echo '</tr>';
										}
									?>
									</table>
								</div>
								<?php }	?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>

		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div style="text-align:center;">
				<?php
					echo '<a href="'.link_to(array(CONTROLLER => 'logs', ACTION => 'AddIpAddress')).'"><button type="button" style="width:71px;"><img src="'.DIR_IMAGES.'ui/add_page_16.gif" alt="" /> New</button></a>';
					echo '<a href="'.link_to(array(CONTROLLER => 'logs', ACTION => 'index')).'"><button type="button"><img src="'.DIR_IMAGES.'ui/0.png"></button></a>';
					echo '<a href="#" onclick="document.getElementById(\'edit_form\').reset();"><button type="button"><img src="'.DIR_IMAGES.'ui/r.png" height="22"></button></a>';
					echo '<a href="#" onclick="document.getElementById(\'edit_form\').submit();"><button type="submit"><img src="'.DIR_IMAGES.'ui/1.png"></button></a>';
				?>
				</div>
			</div>
			<div class="col-lg-12"><br /></div>
		</div>
	</div>
</form>
<?php
		database_close();

		clear_all_messages();
	}
	else
	{
		page_not_access();
	}
?>