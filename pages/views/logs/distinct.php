<?php Global $is_admin_user, $logs_links; ?>
<div class="container">
	<div class="row">
<?php
	if($is_admin_user || is_tester())
	{
		echo '<h1>Заповядай, Васко</h1>';

		$table_classes = 'class="table table-striped table-bordered"';// table-condensed table-responsive
		$columns = array(
			array('text' => 'URL', 'field' => 'request_url'),
		);

		foreach($logs_links as &$link)
		{
			$link = '<a href="'.$link['request_url'].'" target="_blank">'.$link['request_url'].'</a>';
		}
		render_table($logs_links, $columns, $table_classes);
	}
	else
	{
		page_not_access();
	}
?>
		<br />
	</div>
</div>