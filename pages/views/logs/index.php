<div class="container">
<?php Global $is_admin_user, $logs, $query_string;
if(SHOW_DEBUG_INFO){
?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel-group" id="accordion1" style="margin-top:20px;">
				<div class="panel panel-<?php echo (count($logs) != 0 ? 'info' : 'danger'); ?>">
					<div class="panel-heading panel-plus-link">
							<a class="accordion-toggle<?php echo (count($logs) != 0 ? ' collapsed' : ''); ?>" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne1" aria-expanded="<?php echo (count($logs) != 0 ? 'false' : 'true'); ?>">
								<b><h5 style="display:inline;">Управление на Логове</h5></b>
							</a>
					</div>
					<div id="collapseOne1" class="panel-collapse collapse<?php echo (count($logs) != 0 ? '' : ' in'); ?>" aria-expanded="<?php echo (count($logs) != 0 ? 'false' : 'true'); ?>">
						<div class="panel-body">
							<?php echo SQL_DEBUG($query_string); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php } ?>
	<div class="row">
		<div class="col-lg-12">
			<form method="GET" id="search_question_form">
				<div class="form-group">
					<div class="row">

						<input type="hidden" name="<?php echo CONTROLLER; ?>" value="<?php echo $source[CONTROLLER]; ?>" />
						<input type="hidden" name="<?php echo ACTION; ?>" value="<?php echo $source[ACTION]; ?>" />

						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
							<label for="request_url">
								URL / IP Address
							</label>
							<input id="request_url" name="request_url" class="form-control" value="<?php echo (isset($source['request_url']) ? $source['request_url'] : ''); ?>" />
						</div>
						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
							<label for="from_place">
								Местоположение
							</label>

							<select id="from_place" name="from_place" class="form-control">
								<option value="-1">-- Без значение --</option>
								<option value="0">У дома</option>
								<option value="1">Академията</option>
								<option value="2">Друго място</option>

							</select>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
							<label for="from_date">
								Начална дата
							</label>

							<input type="date" name="from_date" id="from_date" value="<?php echo (isset($_REQUEST['from_date']) ? $_REQUEST['from_date'] : ''); ?>" class="form-control" />
						</div>
						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
							<label for="to_date">
								Крайна дата
							</label>

							<input type="date" name="to_date" id="to_date" value="<?php echo (isset($_REQUEST['to_date']) ? $_REQUEST['to_date'] : ''); ?>" class="form-control" />
						</div>
						<input type="hidden" name="show_answers" id="show_answers" value="0" />
						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
							<button type="submit" class="btn btn-large btn-success" style="margin-top:30px" id="submit">Филтрирай</button>
							<button type="submit" class="btn btn-large btn-danger" style="margin-top:30px;" id="show_answers_btn">
								<!-- <span class="glyphicon glyphicon-trash"></span> -->Провери
							</button>
							<button type="submit" class="btn btn-large btn-info" style="margin-top:30px;" id="date_btn">
								<span class="glyphicon glyphicon-time"></span>
							</button>
						</div>
					</div>
				</div>
			</form>
<?php
	if($is_admin_user)
	{
		//pre_print($questions);
		//echo '<div class="container">';
		//echo '<div class="row">';
		//echo '<h1>Потребителска активност</h1>';
		//echo '<a class="fake_tab'.(isset($source[VIEW]) && $source[VIEW] == 'all' ? ' fake_tab_active' : '').'" href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element(VIEW, $_SERVER['QUERY_STRING']).'&' : '').VIEW.'=all"><img src="'.DIR_IMAGES.'ui/preferences_contact_list.png" /> Всички</a>';
		//echo '<a class="fake_tab'.(isset($source[VIEW]) && $source[VIEW] == 'wrong' ? ' fake_tab_active' : '').'" href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element(VIEW, $_SERVER['QUERY_STRING']).'&' : '').VIEW.'=wrong"><img src="'.DIR_IMAGES.'ui/0.png" height="16" /> Грешни</a>';
		//echo '<a class="fake_tab'.(isset($source[VIEW]) && $source[VIEW] == 'reported' ? ' fake_tab_active' : '').'" href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element(VIEW, $_SERVER['QUERY_STRING']).'&' : '').VIEW.'=reported"><img src="'.DIR_IMAGES.'ui/icon_mail.gif" height="16" /> Докладвани</a>';

		if(isset($source['user_id']) && $source['user_id'] >= 0)
		{
			echo '<a class="fake_tab fake_tab_remove" title="Кликнете тук за да премахнете филтъра!" href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element('user_id', $_SERVER['QUERY_STRING']).'&' : '').'">Филтър: user_id = '.$source['user_id'].'</a>';
		}

		//if(isset($_REQUEST['word']) && $REQUEST['word'] != '')
		//{
		//	echo '<a class="fake_tab fake_tab_remove" title="Кликнете тук за да премахнете филтъра!" href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element('word', $_SERVER['QUERY_STRING']).'&' : '').'">Филтър: word = %'.$_REQUEST['word'].'%</a>';
		//}

		$table_classes = 'class="table table-striped table-bordered"';// table-condensed table-responsive
		$columns = array(
			array('text' => 'ID', 'field' => 'log_id'),
			array('text' => 'User', 'field' => 'user_id'),
			array('text' => 'URL', 'field' => 'request_url'),
			array('text' => 'IP Address', 'field' => 'ip_address'),
			array('text' => 'When', 'field' => 'log_time'),
		);
		render_table($logs, $columns, $table_classes);
		//echo '</div></div>';
	}
	else
	{
		page_not_access();
	}
?>
		<br />
		</div>
	</div>
</div>
<div class="modal fade" id="LogInfoModal" tabindex="-1" role="dialog" aria-labelledby="LogInfoModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="width:900px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">

			</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){

		$('.load_log_info').click(function(){
			$('#LogInfoModal .modal-title').text('Детайлна информация');
			$('#LogInfoModal .modal-body').text('Моля изчакайте, данните се зареждат...');

			$('#LogInfoModal .modal-dialog').load('<?php echo header_link(array(CONTROLLER => 'logs', ACTION => 'see', ID => '')); ?>'+$(this).data('log_id')+' #log_info');
		});
	});
</script>