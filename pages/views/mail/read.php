<div class="container">
<?php 
	Global $is_admin_user, $mail, $user_id, $load_user_id;
?>
	<div class="row">
		<div class="col-lg-12">
			<br />
<?php
	if(!has_error_messages())
	{
		//pre_print($mail);

		if($mail['created_from'] != 0 && $user_id != $mail['created_from'])
		{
			$reply_form  = '<input type="hidden" name="send_to['.$mail['created_from'].']" value="'.$mail['created_from'].'" />';
			$reply_form .= '<input type="hidden" name="about" value="Re: '.$mail['mail_subject'].'" />';
			$reply_form .= '<button type="submit" style="float: right;"><span class="glyphicon glyphicon-share-alt"></span> Отговори</button>';

			$reply_form  = '<form method="post" action="'.header_link(array(CONTROLLER => 'mail', ACTION => 'create')).'" style="display: inline;">'.$reply_form.'</form>';
		}
		else
		{
			$reply_form = '';
		}

		if($mail['created_from'] == 0)
		{
			$sender = 'Plovdiv Web <sup><i>(автоматично генерирано)</i></sup>';
		}
		else
		{
			$sender = $mail['first_name'].' '.$mail['last_name'].' ('.$mail['user_email'].')';
		}

		database_open();
		$TAGS_ARRAY = get_all_tags();
		$template = getTemplateData(1, 'template_content');
		if(stripos($mail['mail_content'], $template) !== false)
		{
			$new_content = '';
			$new_content = 'Доказателство за успешно преминат курс е PDF файлът с вашият сертификат, който е винаги достъпен на сайта ни.
				Освен това ако дойдете до нашият офис за връчването на сертификатите, ще получите и подписан хартиен вариант.
				Моля уточнете желаете ли хартиен вариант, който да получите в нашият офис
				<br />';

			$my_courses = get_user_courses($load_user_id, 'role <> "teacher"', 'course_id');
			$user_certificates = getCertificates(null, $load_user_id, 'course_id'); //'ancestor_id'

			foreach($user_certificates as $certificate)
			{
				$course = $my_courses[$certificate['course_id']];
				if(!file_exists($user_certificates[$course['course_id']]['file_path']))
				{
					$new_content .= '<b>Сертификат '.$course['course_name'].'</b> <select name="cm['.$course['id'].']" style="float:right;">'.ReturnDropDown(array('-1' => '-- Не казвам, тормозете ме още! :)', '1' => 'Да, искам хартиен сертификат, ще дойда.', '0' => 'Достатъчен ми е pdf-а, искам да спася дърво.'), $course['want_certificate']).'</select><div style="clear:both;"></div>';
				}
			}

			$new_content .= '<input type="submit" value="Изпрати" />';
			$new_content = '<form method="post" action="'.header_link(array(CONTROLLER => 'certificates', ACTION => 'setAnswer', ID => $load_user_id)).'">'.$new_content.'</form>';
			$mail['mail_content'] = str_ireplace($template, $new_content, $mail['mail_content']);
		}

		//$mail['mail_content'] = htmlspecialchars(stripslashes($mail['mail_content']));
		//$mail['mail_content'] = str_replace(array('[lt]', '[gt]'), array('<', '>'), $mail['mail_content']);
		$mail['mail_content'] = str_replace( array_column($TAGS_ARRAY, 'tag_name'), array_column($TAGS_ARRAY, 'tag_code'), $mail['mail_content']);
		//$mail['mail_subject'] = excerpt_text($mail['mail_subject'], 45);

		if($mail['mail_subject'] == '')
		{
			$mail['mail_subject'] = '<i>[-- ЛИПСВА ОПИСАНИЕ --]</i>';
		}

		database_close();

		echo '<table id="mail_table" class="table table-bordered table-striped">';
		echo '<tr><th colspan="2">'.$mail['mail_subject'].' '.$reply_form.' </th></tr>';
		echo '<tr><th width="170">Изпратено от: </th><td> '.$sender.' </td></tr>';
		echo '<tr><th width="170">Изпратено на: </th><td>'.date('Y-m-d H:i:s', $mail['created_on']).'</td></tr>';
		echo '<tr><td colspan="2">'.stripslashes(nl2br($mail['mail_content'])).'</td></tr>';
		echo '<tr><td colspan="2">&nbsp;</td></tr>';
		echo '</table>';
	}
	else
	{
		show_all_messages();
		clear_all_messages();
	}
?>
			<br />
		</div>
	</div>
</div>