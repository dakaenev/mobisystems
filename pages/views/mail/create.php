<?php Global $is_admin_user, $user_id; ?>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
<?php
	if($user_id > 0) // $is_admin_user
	{
		echo '<h1>Нов e-mail</h1>';

		if(isset($_GET['mode']))
		{
			switch($_GET['mode'])
			{
				case 'alert': $mode = 'alert'; break;
				default: $mode = 'mail'; break;
			}
		}
		else
		{
			$mode = 'mail';
		}

		if(isset($_REQUEST['send_to']))
		{
			$recipients = $_REQUEST['send_to'];
		}
		else
		{
			$recipients = array();
		}

		if(isset($_REQUEST['about']))
		{
			$about = $_REQUEST['about'];
		}
		else
		{
			$about = '';
		}

		$recipients_list = '';
		$recipients_count = count($recipients);

		database_open();
		$templates = array();
		$templates_array = get_all_templates();

		foreach($templates_array as $template)
		{
			$templates[$template['template_id']] = array($template['template_id'], $template['template_title']);
			echo '<textarea id="template_area_'.$template['template_id'].'" data-hint="'.$template['template_comment'].'" data-about="'.$template['template_about'].'" style="display:none;">'.$template['template_content'].'</textarea>';
		}

		if($recipients_count > 0)
		{
			$existing_users = to_assoc_array(exec_query('SELECT user_id, first_name, last_name, user_email FROM users WHERE user_id IN('.implode(',', $recipients).')'), 'user_id');

			foreach($recipients as $recipient_id => $recipient_mail)
			{
				$label_style = 'class="col-lg-3 col-md-4 col-sm-6 col-xs-12" style="border:1px dotted silver;';

				if(isset($existing_users[$recipient_mail]))
				{
					$user = $existing_users[$recipient_mail];
					$checkbox_element = '<input type="checkbox" class="send_to_chk" name="send_to[-'.$recipient_id.']" value="'.$user['user_email'].'" checked="checked" /> ';
					$checkbox_element .= '<a href="'.header_link(array(CONTROLLER => 'user', ACTION => 'view', ID => $recipient_id)).'"><span class="glyphicon glyphicon-user"></span></a> '.$user['first_name'].' '.$user['last_name'];
					$label_style .= '" title="'.$user['user_email'].'"';
				}
				else
				{
					if(strpos($recipient_mail, '@') === false)
					{
						$label_style .= 'color:red;"';
					}

					$checkbox_element = '<input type="checkbox" class="send_to_chk" name="send_to[]" value="'.$recipient_mail.'" /> '.$recipient_mail;
				}

				$recipients_list .= '<label '.$label_style.'>'.$checkbox_element.'</label>';
			}
		}
		database_close();
		echo '<form method="post" action="'.header_link([CONTROLLER => 'mail', ACTION => 'create']).'" id="mail_form"><input type="hidden" name="mode" value="'.$mode.'" /><table class="table table-striped table-bordered table-condensed table-responsive">';
		if($mode == 'alert')
		{
			echo '<tr><th>
			От кого / какво се оплаквате:
						<select name="alert_from">
							<option value="0">--- Изберете ---</option>
							<optgroup label="Лектори">
								<option value="1">Йордан Енев</option>
								<option value="9">Чанита Попова</option>
								<option value="68">Борислав Илийков</option>
								<option value="225">Красимир Тодоров</option>
							</optgroup>
							<optgroup label="Проблеми">
								<option value="Конкурси">Конкурси</option>
								<option value="Домашни">Домашни</option>
								<option value="Сайтът">Сайтът</option>
								<option value="Лекции">Лекции</option>
								<option value="Видео">Видео</option>
								<option value="Друго">Друго</option>
							</optgroup>
						</select>
						<input type="text" name="about" value="'.$about.'" placeholder="Относно" style="width:50%;" />
						</th></tr>';
		}
		else
		{
			$templates = array('0' => '--- Няма избран шаблон ---') + $templates;
			usort($templates, function($a,$b){ return mb_strtoupper($a) < mb_strtoupper($b); } );

			if(is_admin_user())
			{
				$about_classes = 'col-lg-6 col-md-4 col-sm-12 col-xs-12';
				$add_template_button = '<button type="button" id="btn_add_template"><img src="'.DIR_IMAGES.'ui/add_page_16.gif" alt="New" /></button>';
				$template_dropdown = 'Шаблон: <select id="use_template" style="height:26px;">'.ReturnDropDown($templates, 0).'</select>';
				$pinned_checkbox = '<th width="100"><input type="checkbox" name="is_pinned" id="is_pinned" value="1" /> Закачено</label></th>';

				$templates_section = '<div class="pull-right">'.$template_dropdown.$add_template_button.'</div>';
			}
			else
			{
				$pinned_checkbox = '';
				$templates_section  = '';
				$about_classes = 'col-lg-12 col-md-4 col-sm-12 col-xs-12';
			}

			echo '<tr><th><details '.($recipients_count < 4 ? '' : 'open="open" ').'style="width: 100%;"><summary>'.(false ? '<input type="button" value="+" id="btn_add" style="margin-right:5px;" />' : '').' Получатели (<span id="recipients_count">'.$recipients_count.'</span>):</summary>'.$recipients_list.'</details></th>'.$pinned_checkbox.'</tr>';
			echo '<tr><th colspan="2"><input type="text" name="about" value="'.$about.'" placeholder="Относно" class="'.$about_classes.'" />'.$templates_section.'</th></tr>';
		}
		echo '<tr><th colspan="2"><textarea name="message" placeholder="Съобщение" style="width:100%; height:100px;"></textarea></th></tr>';
		echo '<tr>
				<th colspan="2">
					<input type="reset" value="Изчисти" />
					<button type="button" id="restore_form" style="width:38px;" title="Кликнете тук за да възстановите последните въведени данни!">
						<span class="glyphicon glyphicon-cloud-download"></span>
					</button>
					<input type="submit" name="submit" value="Изпрати" />
					<span id="template_hint" class="pull-right" style="font-style: italic; color: silver; font-weight: 400;"></span>
				</th>
			</tr>';
		echo '</table>';
		?>
			<script>
				$(document).ready(function(){
					$('#btn_add').click(function(){
						var mail_value = prompt('Въведете е-мейл адрес:');
						if(mail_value.indexOf('@') != -1)
						{
							$(this).closest('summary').after('<label class="col-lg-3" style="border:1px dotted silver;"><input type="checkbox" class="send_to_chk" checked="checked" name="send_to[]" value="' + mail_value + '" /> ' + mail_value + '</label>');
							$('#recipients_count').text($('.send_to_chk:checked').length);
						}
						else
						{
							alert('Некоректен е-мейл! Въведохте: ' + mail_value);
						}
					});

					$('#btn_add_template').click(function(){
						var template_id = $('#use_template').val();

						$('[name=about]').val( $('#template_area_' + template_id).data('about') );
						$('[name=message]').val( $('[name=message]').val() +  $('#template_area_' + template_id).html() );
						// На горния ред използваме този код, защото .append() работи с HTML-a на полето, като го унищожава и създава наново.
						// Ако бяхме използввали .append(), тогава <input type="reset" /> нямаше да изчиства новосъздадената <textarea>
					});

					$('#use_template').change(function(){
						var template_id = $('#use_template').val();

						var hint_text = (template_id == 0 ? '' : $('#template_area_' + template_id).data('hint'));
						$('#template_hint').html( hint_text );
					});

					$('details').on('click', 'label', function(){
						$('#recipients_count').text($('.send_to_chk:checked').length);
					});

					$( "#mail_form" ).submit(function( event ) {
						if (typeof(Storage) !== "undefined") {
							var mail_fields = $( "#mail_form" ).serializeArray();

							sessionStorage.setItem("last_mail", JSON.stringify(mail_fields) );
							console.log("Success! Data are saved with Success...");
						} else {
							console.log("Sorry! No Web Storage support..");
						}
					});

					$("#restore_form").click(function(){
						console.log("Reading Session Storage...");

						if (typeof(Storage) !== "undefined") {
							var mail_fields = sessionStorage.getItem("last_mail");
							console.log(mail_fields);

							if(mail_fields != null && mail_fields != "")
							{
								mail_fields = JSON.parse(mail_fields);

								jQuery.each( mail_fields, function( i, field ) {
									document.getElementsByName(field.name)[0].value = field.value;
								});
							}
							else
							{
								alert("Sorry! Session Storage is empty... :(");
							}
						} else {
							alert("Sorry! No Web Storage support..");
						}
					});

					$("#is_pinned").click(function(){
						$('#btn_add').prop('disabled', $(this).prop('checked'));
						$('.send_to_chk').prop('checked', !$(this).prop('checked'));
						$('.send_to_chk').prop('disabled', $(this).prop('checked'));
						$('#recipients_count').text($('.send_to_chk:checked').length);
					});
				});
			</script>
		<?php
		echo '</form>';
	}
	else
	{
		page_not_access();
	}
?>
		<br />
		</div>
	</div>
</div>