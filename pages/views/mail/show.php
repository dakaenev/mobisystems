<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<?php
				Global $is_admin_user, $user_id, $load_user_id, $mail_notifications;

				if(have_messages())
				{
					show_all_messages();
					clear_all_messages();
				}

				$mail_columns = array(
					array('text' => '<a href="#" class="center refresh_messages"><span class="glyphicon glyphicon-refresh"></span></a>' , 'key' => 'icon', 'attr' => 'width="20"'), // 'field' => 'mr.read_status',
					array('text' => 'Относно', 'key' => 'about', 'field' => 'm.mail_subject' ),
					array('text' => 'Подател', 'key' => 'author', 'field' => 'u.first_name', 'attr' => 'width="140"'),
					array('text' => 'Изпратено', 'key' => 'created_on', 'field' => 'm.created_on', 'attr' => 'width="100"'),
					array('text' => '<span class="glyphicon glyphicon-trash" id="btn_delete_mails"></span>', 'attr' => 'width="10"')
				);

				$new_mails_count = 0;
				$mails = array();

				if(count($mail_notifications) > 0)
				{
					//pre_print($mail_notifications);

					foreach($mail_notifications as $mail)
					{
						if($mail['mail_subject'] == '')
						{
							$mail['mail_subject'] = '<i>[-- ЛИПСВА ОПИСАНИЕ --]</i>';
						}

						$mail['mail_subject'] = excerpt_text($mail['mail_subject'], 45);

						$tmp = array();
						$tmp['icon'] = '<img {CLASS_DELETED} alt="mail" title="'.$mail['id'].'" src="'.DIR_IMAGES.'ui/'.($mail['read_status'] == 0 ? 'icon_msg_nonew.gif' : 'mail_icon.png').'" />';
						$tmp['about'] = '<a {CLASS_DELETED} href="'.header_link(array(CONTROLLER => 'mail', ACTION => 'read', ID => $mail['mail_id'], 'user_id' => $load_user_id)).'" target="_blank">'.($mail['read_status'] == 0 ? '<b>'.$mail['mail_subject'].'</b>' : $mail['mail_subject']).'</a>';
						$tmp['author'] = '<span {CLASS_DELETED}>'.$mail['first_name'] . ' ' . $mail['last_name'].'<span>';
						$tmp['created_on'] = '<span {CLASS_DELETED}>'.date('Y-m-d', $mail['created_on']).'<span>';

						if(!$mail['is_pinned'])
						{
							if($mail['is_deleted'])
							{
								$tmp['del_chkbox'] = '<a href="#" class="restore_message" data-mail_id="'.$mail['id'].'"><span class="glyphicon glyphicon-repeat"></span></a>';
							}
							else
							{
								$tmp['del_chkbox'] = '<input type="checkbox" class="del_chkbox" name="del_chkbox[]" value="'.$mail['id'].'" />';
							}
						}
						else
						{
							$tmp['del_chkbox'] = '<span class="glyphicon glyphicon-pushpin"></span>';
						}

						if($mail['is_deleted'])
						{
							if($is_admin_user)
							{
								$tmp = str_ireplace(' {CLASS_DELETED}', ' class="deleted_mail"', $tmp);
								$mails[] = $tmp;
							}
						}
						else
						{
							$tmp = str_ireplace(' {CLASS_DELETED}', '', $tmp);
							$mails[] = $tmp;

							// Съобщението нито е прочетено, нито е изтрито...
							if($mail['read_status'] == 0)
							{
								$new_mails_count++;
							}
						}
					}
				}

				$mail_notifications = return_table($mails, $mail_columns, 'class="table table-bordered table-striped table-hover" style="margin-bottom:0px;"');

				echo '<br /><div id="MessagesBody"><div id="show_mails_table"><span id="new_mails_count" class="hidden">'.$new_mails_count.'</span>'.$mail_notifications.'</div></div><br />';
			?>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		<?php JS_Mails_Functions(); ?>
	});
</script>
