<div class="container">
<?php
	Global $is_admin_user, $mails, $query_string, $columns;

	database_open();
?>
	<div class="row">
		<div class="col-lg-12">
		<?php
			if(have_messages())
			{
				show_all_messages();
				clear_all_messages();
			}

			if($is_admin_user){
		?>
			<form method="GET" action="<?php echo header_link(array(CONTROLLER => 'mail', ACTION => 'index')); ?>">
				<div class="form-group">
					<div class="row">

						<input type="hidden" name="<?php echo CONTROLLER; ?>" value="mail" />
						<input type="hidden" name="<?php echo ACTION; ?>" value="index" />

						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-2">
							<label for="mail_status">
								Статус
							</label>

							<select name="mail_status" id="mail_status" class="form-control">
								<?php
									$status_array = array(
										'-1' => '- Без Филтър -',
										'read' => 'Прочетени',
										'non_read' => 'Непрочетени',
										'sent' => 'Изпратени',
										'non_sent' => 'Неизпратени'
									);

									RenderDropDown($status_array, (isset($_GET['mail_status']) ? $_GET['mail_status'] : -1));
								?>
							</select>
						</div>
						<!--
						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-2">
							<label for="created_from">
								Подател
							</label>

							<input type="text" name="created_from" id="created_from" class="form-control" value="<?php echo (isset($_REQUEST['created_from']) ? $_REQUEST['created_from'] : ''); ?>" placeholder="Име на кирилица" list="users_list" />
							<datalist id="users_list"><?php //echo implode('', $users); ?></datalist>
						</div>
						-->
						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-2">
							<label for="full_name">
								Получател
							</label>

							<input type="text" name="full_name" id="full_name" class="form-control" value="<?php echo (isset($_GET['full_name']) ? $_GET['full_name'] : ''); ?>" placeholder="Име на кирилица" />
						</div>
						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-6">
							<label for="subject">
								Относно
							</label>

							<input type="text" name="subject" id="subject" class="form-control" value="<?php echo (isset($_REQUEST['subject']) ? $_REQUEST['subject'] : ''); ?>" placeholder="Текст на кирилица" />
						</div>
						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-1">
							<button type="submit" style="margin-top:29px; padding: 5px 12px;" class="pull-left btn btn-success" id="submit_button"><span class="glyphicon glyphicon-filter"></span> Филтрирай</button>
						</div>
					</div>
				</div>
			</form>
		<?php } ?>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
<?php
	// <button type="button" class="pull-right btn btn-primary" style="margin-top:29px;"><span class="glyphicon glyphicon-transfer"></span> Маркирай</button>

	if(true) //$is_admin_user
	{
		//pre_print($questions);
		//echo '<div class="container">';
		//echo '<div class="row">';
		//echo '<h1>Потребителска активност</h1>';
		//echo '<a class="fake_tab'.(isset($source[VIEW]) && $source[VIEW] == 'all' ? ' fake_tab_active' : '').'" href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element(VIEW, $_SERVER['QUERY_STRING']).'&' : '').VIEW.'=all"><img src="'.DIR_IMAGES.'ui/preferences_contact_list.png" /> Всички</a>';
		//echo '<a class="fake_tab'.(isset($source[VIEW]) && $source[VIEW] == 'wrong' ? ' fake_tab_active' : '').'" href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element(VIEW, $_SERVER['QUERY_STRING']).'&' : '').VIEW.'=wrong"><img src="'.DIR_IMAGES.'ui/0.png" height="16" /> Грешни</a>';
		//echo '<a class="fake_tab'.(isset($source[VIEW]) && $source[VIEW] == 'reported' ? ' fake_tab_active' : '').'" href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element(VIEW, $_SERVER['QUERY_STRING']).'&' : '').VIEW.'=reported"><img src="'.DIR_IMAGES.'ui/icon_mail.gif" height="16" /> Докладвани</a>';

		if(isset($source['user_id']) && $source['user_id'] >= 0)
		{
			echo '<a class="fake_tab fake_tab_remove" title="Кликнете тук за да премахнете филтъра!" href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element('user_id', $_SERVER['QUERY_STRING']).'&' : '').'">Филтър: user_id = '.$source['user_id'].'</a>';
		}

		//if(isset($_REQUEST['word']) && $REQUEST['word'] != '')
		//{
		//	echo '<a class="fake_tab fake_tab_remove" title="Кликнете тук за да премахнете филтъра!" href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element('word', $_SERVER['QUERY_STRING']).'&' : '').'">Филтър: word = %'.$_REQUEST['word'].'%</a>';
		//}

		$table_classes = 'class="table table-striped table-bordered"';// table-condensed table-responsive

		render_table($mails, $columns, $table_classes);
		//echo '</div></div>';
	}
	else
	{
		page_not_access();
	}
?>
		<br />
		</div>
	</div>
</div>
<div class="modal fade" id="LogInfoModal" tabindex="-1" role="dialog" aria-labelledby="LogInfoModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="width:900px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"></h4>
			</div>
			<div class="modal-body">

			</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>