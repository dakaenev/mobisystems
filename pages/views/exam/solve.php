<?php
	Global $Requested_From, $is_live_preview, $exam_questions, $exam_id, $_connection, $questions_groups, $is_admin_user, $is_exam, $test_seconds, $exam_data;
	//Pre_Print($exam_questions, false);
	//Pre_Print($questions_groups);
	//var_dump($is_exam);

	if($is_exam)
	{
        $is_poll = false;

        $exam_object = 'Exam';
		$title = '';
    }
	else // This is Poll
	{
		$is_poll = true;
		$exam_object = 'Poll';
		$title = '';
	}

    if($Requested_From == 'preview' && !$is_live_preview)
    {
        //$title = 'Preview '.$exam_object.' #'.$exam_id;
    }

	// Преобразуваме максималното време от минути в милисекунди: MAX_TEST_TIME(Времето в минути) * 60 (секунди) * 1000 (защото 1 секунда има 1000 милисекунди)
	$max_test_time = $test_seconds * 1000; //$max_test_time = 5000; // Разкоментирайте това за тестови нужди
?>
<div class="container">
	<div class="row">
<form id="solve_test" method="post" name="form" action="" enctype="multipart/form-data">
<?php if(!$is_live_preview) { ?><input type="hidden" name="exam_id" value="<?php echo $exam_id; ?>"><?php } ?>
<span id="notification" style="display:none;"></span>
<div class="top-button">
    <span class="pull-left"><h2><?php echo $title; ?></h2></span>
    <span class="pull-right">
        <h2>
            <?php  if(isset($percent) && !$is_poll){ echo $percent.'%'; } ?>
        </h2>
        <h3>
            <?php if(isset($_GET['evaluate'])){ echo '<label class="padding_left_right_5px pull-right"><input type="checkbox" id="more_info"> Show more information</label>'; } ?>
        </h3>
    </span>
</div>
<br />
<?php
	if(!has_error_messages())
	{
	if(count($exam_questions) > 0)
	{
		if($Requested_From == 'exam') {
?>
<input type="hidden" name="evaluate" value="true">
<script>
	var show_exit_message = true;

	function sendNow() {
		show_exit_message = false;
		document.getElementById('solve_test').submit();
	}

	$(document).ready(function(){

		$(window).on('beforeunload',function(){
			if(show_exit_message == true)
			{
				return 'Теста все още не е изпратен! \n Моля изпратете го преди да затворите браузъра!';
			}
		});

	});

	window.setTimeout("sendNow()", <?php echo $max_test_time; ?>);

</script>
<?php } ?>
<div class="text-center" style="position:fixed; right:0px;">
	<div class="transparent-div animated animation-delay-8">
		Мислете бързо!<br>До края на теста имате само:
		<div id="getting-started" class="animated fadeInLeft animation-delay-7"></div>
		<input type="button" value="Приключи теста сега" id="btn_submit_trigger" class="btn btn-success stepy-finish">
	</div>
</div>
<script>
$(document).ready(function(){
	$("#getting-started").countdown("<?php echo date('Y/m/d H:i:s', strtotime('now') + TIME_CORRECTION + $test_seconds); ?>", function(event) {
		var H = event.strftime('%H');
		var M = event.strftime('%M');
		var S = event.strftime('%S');

		var make_bold = (M < 1 && S % 2 == 0);

		if(H == 0 && M < 5)
		{
			$(this).html(event.strftime('<ul class="coming-date coming-date-black"><li style="color:red;'+(make_bold ? ' font-weight:bold;' : '')+'"">%M <span style="font-weight:normal;">Минути</span></li><li class="colon" style="color:red; width: 10px;'+(make_bold ? ' font-weight:bold;' : '')+'"">:</li><li style="color:red;'+(make_bold ? ' font-weight:bold;' : '')+'">%S <span style="font-weight:normal;">Секунди</span></li></ul>'));
		}
		else
		{
			$(this).html(event.strftime('<ul class="coming-date coming-date-black"><li>%M <span>Минути</span></li><li class="colon">:</li><li>%S <span>Секунди</span></li></ul>'));
		}
	});

	$("#btn_submit_trigger").click(function(){
		$("#btn_submit").trigger('click');
	});

	$("[name^=questions]").click(function(){
		$("#l" + $(this).data('qid')).addClass('solved_tab');
		//$("#l" + $(this).data('qid')).html('<span class="glyphicon glyphicon-ok"></span>');
	});
});
</script>
				<div class="form-wizzard" id="questions_div"<?php //if(isset($_GET['evaluate'])){ echo ' style="display:none;"'; } ?>>
				<?php
				$i = 0;
				$tabs_names = '';
				$tabs_texts = '';

				foreach($exam_questions as $question)
				{
					++$i;
					$tabs_texts .= '<div class="tab-pane" id="q'.$question['question_id'].'">';
					$tabs_names .= '<li><a id="l'.$question['question_id'].'" href="#q'.$question['question_id'].'" data-toggle="tab" aria-expanded="false">'.$i.'</a></li>';
					$classes = 'question_div padding_bottom_5px margin_bottom_3px';

					if(isset($question['evaluated_data']) && $is_admin_user)
					{
						$classes .= ' '.$question['evaluated_data']['color'];
						//$response = $question['evaluated_data']['message'];
						$response = '';
					}
					else
					{
						$response = '';
					}

					// Check if this is image
					$question['question_title'] = trim($question['question_title']);
					if(substr($question['question_title'], 0, 4) == 'img=')
					{
						$question['question_image'] = '<img src="'.DIR_IMAGES.substr($question['question_title'],4).'" style="border:1px solid black;"><br><br>';
						$question['question_title'] = 'Изберете правилното изображение';
						$is_image = true;
						$answer_style = 'border:1px solid silver; '.(count($question['answers']) <= 6 ? 'max-' : '').'width:'.(int) (1000 / count($question['answers']) - (SHOW_DEBUG_INFO ? 42 : 12)).'px;';
					}
					else
					{
						$question['question_title'] = htmlspecialchars(stripslashes($question['question_title']));
						$question['question_title'] = str_ireplace(array('&lt;b&gt;','&lt;/b&gt;'), array('<b>','</b>'), $question['question_title']);
						$question['question_image'] = '';
						$is_image = false;
						$answer_style = '';
					}

					// Check if question is too long and have more than 1 rows
					if(false) // strpos($question['question_title'], PHP_EOL) !== false
					{
						$question['question_image'] = str_ireplace(PHP_EOL, '<br>', $question['question_title']).'<br><br>';
						$question['question_title'] = '';
					}

					//pre_print($questions_groups[$question['question_group']], true);

					$tabs_texts .= '<fieldset style="margin-left:10px; margin-right:15px;">';
                    //$tabs_texts .= '<legend '.str_ireplace('style="','style="border:1px solid black; padding:5px; ',$questions_groups[$question['question_group']]['group_style']).'>'.(SHOW_DEBUG_INFO ? ' ['.$questions_groups[$question['question_group']]['group_id'].' - '.$questions_groups[$question['question_group']]['group_description'].' ] <br>' : '').(++$i).'. Въпрос №'.$question['question_id'].': '.$question['question_title'].'</legend>';
					$tabs_texts .= '<legend>№'.$question['question_id'].': '.$question['question_title'].'</legend>';
					//<!--
					//<div class="mark_wrong" data-id="'.$question['question_id'].'" title="Кликнете тук, ако смятата че това е некоректен въпрос!">&nbsp;!&nbsp;</div>
					//<div class="show_help" data-id="'.$question['question_group'].'" title="Кликнете тук, ако имате нужда от помощ!">&nbsp;?&nbsp;</div>
					//-->
                    $tabs_texts .= '<div class="'.$classes.'" style="margin-bottom:15px;">'.$response.$question['question_image'];
                    //    <!-- <br /> -->

                              if($question['question_type'] == 1 || $question['question_type'] == 2)
                              {
                                if($question['question_type'] == 2)
                                {
                                    $input_type = 'radio';
                                    $each_on_new_line = true && !$is_image; //Ако е картинка, ще се показват на 1 ред, ако не е, се взема под внимание само първото - True или False
                                }
                                else // so $question['question_type'] == 1
                                {
                                    $each_on_new_line = true;
/*
                                    if(Model_QuestionsAnswers::have_multy_correct_answers($question['answers']))
                                    {
                                        $input_type = 'checkbox';
                                    }
                                    else
                                    {
                                        $input_type = 'radio';
                                    }
*/
                                }
								//Pre_Print($question['answers'], true);

                                foreach($question['answers'] as $answer)
                                {
									//pre_print($answer);

									if(isset($answer['is_checked']))
                                    {
                                        $checked = ' checked="checked"';
                                    }
                                    else
                                    {
                                        $checked = '';
                                    }

									if($is_poll && $answer['is_correct'])
									{
										$checked = ' checked="checked"';
									}

									$debug_info = '';

									if(SHOW_DEBUG_INFO)
                                    {
                                        $debug_info = '<span class="debug '.($answer['is_correct'] == 1 ? 'answer_success' : 'answer_error').'">('.($is_exam ? ($answer['is_correct'] == 1 ? '+' : '-') : '+').$answer['answer_value'].')</span> ';
                                        //Model_HelperFunctions::Pre_Print($answer);
                                    }

									// Check if this is image
									$answer['answer_title'] = htmlspecialchars(stripslashes(trim($answer['answer_title'])));
									if(substr($answer['answer_title'], 0, 4) == 'img=')
									{
										$answer['answer_title'] = '<img src="'.DIR_IMAGES.substr($answer['answer_title'],4).'" alt="'.$answer['answer_id'].':'.substr($answer['answer_title'],4).'" style="'.$answer_style.'">';
									}

									//if($each_on_new_line){ echo '<br>'; if($is_image){ echo '<br>'; } }
									$tabs_texts .= '<label style="margin-left:10px;" class="padding_left_right_5px'.(isset($answer['evaluated_class']) && $is_admin_user ? ' '.$answer['evaluated_class'] : '').'">';
									$tabs_texts .= '<input type="'.$input_type.'" data-qid="'.$question['question_id'].'" name="questions['.$question['question_id'].']'.($input_type == 'checkbox' ? '['.$answer['answer_id'].']' : '').'" value="'.$answer['answer_id'].'"'.(isset($_POST['evaluate']) || $Requested_From == 'view' ? ' disabled="disabled"' : '').$checked.' />';
									$tabs_texts .= $debug_info.$answer['answer_title'];
									$tabs_texts .= '</label>';

									if($each_on_new_line)
									{
										$tabs_texts .= '<br>';
										if($is_image){ $tabs_texts .= '<br>'; }
									}

							 	}
                              }

                              if($question['question_type'] == 3)
                              {
                                $question['answers'] = reset($question['answers']);

                                $tabs_texts .= '<label class="margin_left_right_5px '.(isset($question['answers']['evaluated_class']) ? $question['answers']['evaluated_class'] : '').'"><input type="text" name="questions['.$question['question_id'].']" style="width:777px !important;" class="col-lg-12 '.(isset($question['answers']['evaluated_class']) ? $question['answers']['evaluated_class'] : '').'" value="'.showFieldValue($question['answers']['is_checked']).'"'.(isset($_GET['evaluate']) ? ' disabled="disabled"' : '').' /></label>';
								if(isset($question['answers']['is_checked']) && strtolower(trim($question['answers']['answer'])) != strtolower(trim($question['answers']['is_checked'])))
								{
									$tabs_texts .= '&nbsp;<label class="answer_success">'.$question['answers']['answer'].'</label>';
								}
							  }

                              if($question['question_type'] == 4)
                              {
                                //$question['answers'] = reset($question['answers']);

									$tabs_texts .= '<textarea class="col-lg-12" style="border:1px solid gray; margin-bottom:25px; min-width: 100%;" name="questions['.$question['question_id'].']">'.showFieldValue($question['answers']['is_checked']).'</textarea>';
							  }

                              if($question['question_type'] == 5)
                              {
                                //$question['answers'] = reset($question['answers']);
                                $tabs_texts .= '<input type="file" class="col-lg-12" name="questions['.$question['question_id'].']" />';
							  }

					$tabs_texts .= '</div></fieldset>';
					$tabs_texts .= '</div>';
				}


				$precondition = 'Здравейте,
								Това е нашият входен тест. Трябва да го решите успешно за да участвате в класирането за нашият курс.
								Въз основа на резултатите от входните тестове ще обявим класирането с приети курсисти!
								Решавайте теста бързо, защото времето  е ограничено!

								Желаем Ви късмет!';

					 $current = 'Здравейте,
								Това е нашият междинен изпит. Той има за цел да ви позволи да оцените обективно знанията си към момента.
								Решавайте теста бързо, защото времето  е ограничено!

								Желаем Ви късмет!';

						$pool = 'Здравейте,
								Моля попълнете обективно и откровено нашата анкета!
								Така ни помагате да подобрим себе си!

								Анонимността Ви е гарантирана!';

				$exam_info = nl2br($$exam_data['exam_type']);
			?>
			<ul class="nav nav-tabs nav-tabs-ar">
				<li class="active"><a href="#info" data-toggle="tab" aria-expanded="true"><span class="glyphicon glyphicon-info-sign"></span></a></li>
				  <?php echo $tabs_names; ?>
				<?php if((($Requested_From == 'exam') || ($Requested_From == 'preview' && !$is_live_preview)) && !isset($_GET['evaluate'])) { ?>
					<li style="float:right;"><input type="submit" value="Изпрати" id="btn_submit" class="pull-right btn btn-success stepy-finish"></li>
				<?php } ?>
			</ul>
			<div class="tab-content">
			  <div class="tab-pane active" id="info">
				  <b><i><?php echo $exam_info; ?>Желая Ви късмет! Ако не си изпратите теста до края на времето той ще се изпрати сам :)</i></b>
			  </div>
			  <?php echo $tabs_texts; ?>
			</div>
			<?php if((($Requested_From == 'exam') || ($Requested_From == 'preview' && !$is_live_preview)) && !isset($_GET['evaluate'])) { ?>
                <!--
				<div class="col-lg-12 ">
					<br><input type="submit" value="Изпрати теста" id="btn_submit" class="pull-right btn btn-success stepy-finish"><br>
				</div>
				-->
            <?php } else { echo '<div class="col-lg-12"></div>'; } ?>
			</div>
<script>
	/*
	function showIsActive()
	{
		console.log(window.isActive)
		window.setTimeout("showIsActive()", 2000);
	}
	*/

	$(document).ready(function(){

//		window.isActive = true;
//		$(window).focus(function() { this.isActive = true; });
//		$(window).blur(function() { this.isActive = false; });
//		showIsActive();



		$('#questions_div').click(function(){
			window.setTimeout("fix_sticky_footer()", 1000);
		});

		$('#more_info').click(function(){

			if($(this).prop("checked"))
			{
				$('#questions_div').show();
			}
			else
			{
				$('#questions_div').hide();
			}
		});

		$('#more_info').prop("checked", false); // Set its default value

		$('#btn_submit').click(function(){
			if(show_exit_message == true)
			{
				var answer = confirm("Наистина ли искате да изпратите теста сега?");
				if(answer == true)
				{
					show_exit_message = false;
					form.submit();
				}
				else
				{
					return false;
				}
			}
			else
			{
				form.submit();
			}
		});

		$('.mark_wrong').click(function(){
			var this_item = $(this);
			if(this_item.attr('disabled') != 'disabled')
			{
				var s = prompt('Моля, Въведете описание защо смятате, че въпроса е неправилен','Описание');

				$.ajax({
					url: "<?php echo link_to( array(CONTROLLER => 'question', ACTION => 'mark', 'OPTION_SET_CLASS' => false) ); ?>",
					async: false,
					type: 'post',
					data: 'ID='+this_item.data('id')+'&msg='+s,
					success: function (data)
					{
						//console.log(data);
						//this_item.hide();
						this_item.attr('disabled', true);
						this_item.addClass('mark_wrong_disabled');
						this_item.removeClass('mark_wrong');
						alert('Благодарим Ви! \nВъпросът беше маркиран!');
					},
					error: function ()
					{
						alert('Възникна проблем, при маркирането на въпроса!');
					}
				});
			}
			//alert($(this).data('id'));
		});

		$('.show_help').click(function(){
			var questions_groups = [];
			<?php $questions_groups[1]['group_description'] = 'жз'; $questions_groups[2]['group_description'] = 'де'; $questions_groups[3]['group_description'] = 'вг';$questions_groups[4]['group_description'] = 'аб';?>
			<?php foreach($questions_groups as $group)  { ?>
				questions_groups[<?php echo $group['group_id']-1; ?>] = '<?php echo str_ireplace(PHP_EOL, '\n', $group['group_description']); ?>';
			<?php } ?>

			alert(questions_groups[$(this).data('id')]);
		});
	});
</script>
		<?php
			}
			else
			{
				add_error_message('Няма въпроси свързани с този тест!');
			}
	}
			if(have_messages())
			{
				show_all_messages();
				clear_all_messages();
			}

		?>
</form>
		</div>
</div>
<!--
<button id="opener">open the dialog</button>
<div id="dialog1" title="Dialog Title" hidden="hidden">I'm a dialog</div>
-->