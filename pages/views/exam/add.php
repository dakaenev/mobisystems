<div class="container">
	<div class="row">
		<form id="edit_form" method="post">
	<?php
	Global $source, $exam_types, $is_admin_user, $all_questions, $test_name, $is_super_admin, $highest_score, $lowest_score, $average_score, $users_array;

	$search_params = remove_url_elements(array(CONTROLLER, ACTION, ID), $_SERVER['QUERY_STRING']);

	if($is_admin_user)
	{
		if(true)//has_error_messages())
		{
			show_all_messages();
			clear_all_messages();
		//}
		//else
		//{
			database_open();

			$default_start = date('Y-m-d H:i:s', (isset($_GET['default_start']) ? $_GET['default_start'] : strtotime('now') + TIME_CORRECTION) );
			$default_end = date('Y-m-d', ( isset($_GET['default_start']) ? $_GET['default_start'] + 60*60*24*7 : strtotime('+1 week') ) ).' 23:59:59';

			if(empty($_POST))
			{
				if(!isset($source[ID])) // This is New - Load Default Data
				{
					$default_test = (isset($_GET['default_test']) ? $_GET['default_test'] : -1);
					$default_course = (isset($_GET['default_course']) ? $_GET['default_course'] : 0);
					$default_type = (isset($_GET['default_type']) ? $_GET['default_type'] : 0);
					$default_questions = 0;
					$default_time = 0;
					$default_state_office = 0;
					$default_state_shuffle = 0;

					$visibility_array = array();
					for($i = -1; $i <= 1; $i++)
					{
						$visibility_array[] = array('group_number' => $i, 'start_date' => $default_start, 'end_date' => $default_end);
					}
				}
				else // This is Edit - Load Exam Data
				{
					$exam_id = (int) $source[ID];
					$exam = get_all_exams('exam_id = '.$exam_id);

					if(Only_One_Exists($exam))
					{
						$exam = reset($exam);

						$default_test = $exam['test_id'];
						$default_course = $exam['course_id'];
						$default_type = $exam['exam_type'];
						$default_questions = $exam['questions_number'];
						$default_time = $exam['test_seconds'] / 60;
						$default_state_office = (int) $exam['for_office_only'];
						$default_state_shuffle = (int) $exam['shuffle_exam_components'];

						$visibility_array = get_object_visibility('exam', $exam_id);
					}
					else
					{
						add_error_message('Homework not Exists!');
					}
				}
			}
			else
			{
				$default_test = $_POST['test_id'];
				$default_course = $_POST['course_id'];
				$default_type = $_POST['type'];
				$default_questions = $_POST['questions_number'];
				$default_time = $_POST['test_minutes'];
				$default_state_office = (int) $_POST['office'];
				$default_state_shuffle = (int) $_POST['shuffle'];

				$visibility_array = $_POST['visibility'];
			}
	?>
		<div class="col-lg-6">
			<div class="col-lg-12">
				<h1>Добавяне на изпит</h1>
			</div>
			<label>
				Курс
				<select name="course_id" id="course_id" class="form-control" required="required">
				<?php
					$courses = get_all_courses('1=1 ORDER BY course_name ASC', 'course_id, course_name'); //  AND (start_date > '.get_current_period(false).' > end_date )
					$courses[-1] = array('course_id' => -1, 'course_name' => ' Добави сега ');
					//pre_print($courses,1);
					if($default_course > 0 && !isset($courses[$default_course]))
					{
						$courses = get_all_courses('course_id = '.$default_course, 'course_id, course_name');
						$is_editable = false;
					}
					else
					{
						$is_editable = true;
					}

					$is_editable = true;
					$courses_dropdown = ReturnDropDown($courses, $default_course, false);
					$courses_dropdown = '<option value="0"> - Изберете курс - </option>'.$courses_dropdown;
					echo $courses_dropdown;
				?>
				</select>+
			</label>
			<label>
				Тест
				<select name="test_id" class="form-control" required="required">
				<?php
				$tests = get_all_tests('1=1 ORDER BY test_name ASC');
				$tests_dropdown = ReturnDropDown($tests, $default_test, false); // Със стойност True не работи. Да се оправи!
				$tests_dropdown = '<option value="-1"> - Изберете тест - </option>'.$tests_dropdown;
				echo $tests_dropdown;
				?>
				</select>
			</label>
			<?php //pre_print($tests,1); ?>
			<br />
			<label>
				Тип
				<select name="type" id="exam_type" class="form-control" required="required">
					<?php
					$types_dropdown = ReturnDropDown($exam_types, $default_type);
					$types_dropdown = '<option value="0"> - Изберете тип - </option>'.$types_dropdown;
					echo $types_dropdown;
					?>
				</select>
			</label>
			<label>
				Въпроси <a href="<?php echo header_link(array(CONTROLLER => 'question', ACTION => 'serialize'));?>" target="_blank">?</a>
				<input type="text" value="<?php echo $default_questions; ?>" name="questions_number" class="form-control" required="required" placeholder="a:2:{i:0;i:15;i:1;i:15;}" />
			</label>
			<label>
				Време
				<input type="number" value="<?php echo $default_time; ?>" name="test_minutes" class="form-control" required="required" min="10" max="60" />
			</label><br />
			<label><input type="checkbox" name="shuffle" id="chk_shuffle" value="1"<?php if($default_state_shuffle == 1){ echo ' checked="checked"'; } ?> /> Разбъркай отговорите</label>
			<label><input type="checkbox" name="office" id="chk_office" value="1"<?php if($default_state_office == 1){ echo ' checked="checked"'; } ?> /> Разрешен само в офиса</label>
		</div>
			<div class="col-lg-6">
				<div class="col-lg-12">
					<h1>Задаване на видимост</h1>
				</div>

				<div id="visibility_div">
				<?php foreach($visibility_array as $k => $visibility){ ?>
					<span>
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-2">
							<label for="vgr<?php echo $k; ?>"> Група </label>
							<input name="visibility[<?php echo $k; ?>][group_number]" id="vgr<?php echo $k; ?>" type="number" class="form-control group_field" value="<?php echo $visibility['group_number']; ?>" />
						</div>
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
							<label for="vsd<?php echo $k; ?>"> Начална дата </label>
							<input name="visibility[<?php echo $k; ?>][start_date]" id="vsd<?php echo $k; ?>" type="text" class="form-control start_date_field" value="<?php echo $visibility['start_date']; ?>" data-key="<?php echo $k; ?>" />
						</div>
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
							<label for="ved<?php echo $k; ?>"> Крайна дата </label>
							<input name="visibility[<?php echo $k; ?>][end_date]" id="ved<?php echo $k; ?>" type="text" class="form-control end_date_field" value="<?php echo $visibility['end_date']; ?>" />
						</div>
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-2">
							<label for="" style="visibility: hidden;"> . </label>
							<button type="button" class="form-control del_hw_visibility"><img src="<?php echo DIR_IMAGES; ?>ui/0.png"></button>
						</div>
					</span>
				<?php } ?>
					<span><!-- Не премахвайте този span, иначе добавянето на нови редове няма да работи --></span>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<label for="" style="visibility: hidden;"> . </label>
				<div style="text-align:center;">
				<?php
				if($is_editable)
				{
					echo '<a href="'.header_link(array(CONTROLLER => 'exam', ACTION => 'manage')).($search_params != '' ? '&'.$search_params : '').'"><button type="button"><img src="'.DIR_IMAGES.'ui/0.png"></button></a>';
					echo '<button type="reset"><img src="'.DIR_IMAGES.'ui/r.png" height="22"></button>';
					echo '<button type="submit"><img src="'.DIR_IMAGES.'ui/1.png"></button></a>';

					echo '<a href="#" id="add_hw_visibility"><button type="button">+</button></a>';
				}
				else
				{
					show_message('Не можете да променяте изпит свързан с приключил курс!');
				}
				?>
				</div>
			</div>
			<div class="col-lg-12"><br /></div>
<script>
	var group_index = <?php echo max(array_column($visibility_array, 'group_number')) + 1; ?>;
	var   key_index = <?php echo max(array_keys($visibility_array)) + 1; ?>;

	$(document).ready(function(){
		$('#add_hw_visibility').click(function(){

			var code;

			code = '<span>';
			code = code + '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-2"><label for="vgr'+key_index+'"> Група </label><input name="visibility['+key_index+'][group_number]" id="vgr'+key_index+'" type="number" class="form-control group_field" value="'+group_index+'" /></div>';
			code = code + '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4"><label for="vsd'+key_index+'"> Начална дата </label><input name="visibility['+key_index+'][start_date]" id="vsd'+key_index+'" type="text" class="form-control" value="<?php echo $default_start; ?>" /></div>';
			code = code + '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4"><label for="ved'+key_index+'"> Крайна дата </label><input name="visibility['+key_index+'][end_date]" id="ved'+key_index+'" type="text" class="form-control" value="<?php echo $default_end; ?>" /></div>';
			code = code + '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-2"><label for="" style="visibility: hidden;"> . </label><button type="button" class="form-control del_hw_visibility"><img src="<?php echo DIR_IMAGES; ?>ui/0.png"></button></div>';
			code = code + '</span>';

			$('#visibility_div span:last').before(code);

			group_index++;
			key_index++;

			return false;
		});

		// Тук трябва да е с ON а не просто с CLICK, за да работи този код и за новосъздадените динамично бутони
		$('#visibility_div').on('click','.del_hw_visibility', function() {
			$(this).parent().parent().remove();
		});

		$('#visibility_div').on('change','.start_date_field', function() {
			var start_value = $(this).val();
			var  start_date = new Date(start_value);
			var    end_date = new Date(start_date.getTime() + (60*60*24*7*1000));

			$('#ved' + $(this).data('key')).val( end_date.toISOString().substr(0, 10) + ' 23:59:59');
		});

		$('#exam_type').change(function(){
			var new_val = $(this).val();

			if(new_val == 'poll'){
				$('#chk_shuffle').prop("checked", false);
			}else{
				$('#chk_shuffle').prop("checked", true);
			}

			if(new_val == 'final'){
				$('#chk_office').prop("checked", true);
			}else{
				$('#chk_office').prop("checked", false);
			}
		});

		$('#course_id').change(function(){
			if($('#course_id :selected').val() < 0)
			{
				var new_val = prompt('Добавете ИД на курс');

				$('#course_id').append('<option value="' + new_val + '"> Добавен: Курс #' + new_val + ' </option>');
				$('#course_id').val(new_val);
			}
		});

		$('#teachers').change(function(){
			if($('#teachers :selected').val() < 0)
			{
				var new_teacher_id = prompt('Потребител №');

				$('#temp_teachers').append('<option value="' + new_teacher_id + '">' + 'Потребител №: ' + new_teacher_id + '</option>');
				$('#teachers').val(new_teacher_id);
			}
		});


		$('#edit_form').submit(function() {

			var submit_now = true;

			if($('#exam_type').val() == 'poll'){
				var group_fields = $('.group_field');

				group_fields.each(function() {
					if( $(this).val() == 0){
						if(confirm('Така зададена анкетата да е достъпна и за онлайн курсистите!\nСигурни ли сте, че искате точно това?')){
							submit_now = true;
						}else{
							submit_now = false;
						}
					}
				});
			}

			if(submit_now == false){ return false; }
		});

		$('#exam_type').trigger('change');


	});


</script>

	<?php
		}
	}
	else
	{
		page_not_access();
	}
?>
		</form>
	</div>
</div>