<div class="container">
<?php
	Global $is_admin_user, $source, $_connection, $courses_exams, $query_string;

	if(SHOW_DEBUG_INFO){
?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel-group" id="accordion1" style="margin-top:20px;">
				<div class="panel panel-<?php echo (count($courses_exams) != 0 ? 'info' : 'danger'); ?>">
					<div class="panel-heading panel-plus-link">
							<a class="accordion-toggle<?php echo (count($courses_exams) != 0 ? ' collapsed' : ''); ?>" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne1" aria-expanded="<?php echo (count($courses_exams) != 0 ? 'false' : 'true'); ?>">
								<b><h5 style="display:inline;">Управление на Изпити</h5><?php echo HelpButton(); ?></b>
							</a>
					</div>
					<div id="collapseOne1" class="panel-collapse collapse<?php echo (count($courses_exams) != 0 ? '' : ' in'); ?>" aria-expanded="<?php echo (count($courses_exams) != 0 ? 'false' : 'true'); ?>">
						<div class="panel-body">
							<?php echo SQL_DEBUG($query_string); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php } ?>
	<div class="row">
		<div class="col-lg-12">
<?php
	if($is_admin_user || is_assistant())
	{
		database_open();

		if(have_messages())
		{
			show_all_messages();
			clear_all_messages();
		}
?>
			<form method="GET" action="<?php echo header_link(array(CONTROLLER => 'homeworks', ACTION => 'index')); ?>">
				<div class="form-group">
					<div class="row">

						<input type="hidden" name="<?php echo CONTROLLER; ?>" value="<?php echo $source[CONTROLLER]; ?>" />
						<input type="hidden" name="<?php echo ACTION; ?>" value="<?php echo $source[ACTION]; ?>" />
						<input type="hidden" name="check_them" id="check_them" value="0" />

						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-3">
							<label for="test_id">
								Тест
							</label>
							<select name="test_id" id="test_id" class="form-control" required="required">
							<?php
								$tests = get_all_tests('1=1 ORDER BY test_name ASC');
								unset($tests[0]); // Премахваме Въпросите без отговор
								$tests_dropdown = ReturnDropDown($tests, $source['test_id'], true);
								$tests_dropdown = '<option value="-1"> - Изберете тест - </option>'.$tests_dropdown;
								echo $tests_dropdown;
							?>
							</select>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-4">
							<label for="course_id">
								Курс
							</label>

							<select name="course_id" id="course_id" class="form-control">
								<option value="-1">- Без Филтър -</option>
								<?php
									$courses = get_all_courses();

									$archive = '';
									$current = '';
									foreach($courses as $course)
									{
										$course['end_date'] = strtotime($course['end_date']) + (0 * 7 * 24 * 3600); // курса се брои за активен до 2 седмици след неговия край ;-)
										$which_courses = ($course['start_date'] <= date('Y-m-d') && strtotime(date('Y-m-d')) <= $course['end_date'] ? 'current' : 'archive');
										$$which_courses .= '<option value="'.$course['course_id'].'"'.($source['course_id'] == $course['course_id'] ? ' selected="selected"' : '').'>'.$course['course_id'].'. '.$course['course_name'].'</option>';
										// Тук използваме $$ за да укажем, коя променлива искаме да променяме. Така избягваме 1 if, или пък двойното писане на един и същи код.
									}

									$default = '<option value="0"'.(0 == $source['course_id'] ? ' selected="selected"' : '').'>Всички активни курсове</option>';
									$archive = '<optgroup label="Архивни">'.$archive.'</optgroup>';
									$current = '<optgroup label="Текущи">'.$current.'</optgroup>';

									echo $default.$current.$archive;
								?>
							</select>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
							<label for="exam_type">
								Тип
							</label>

							<select name="exam_type" id="exam_type" class="form-control">
							<?php
								$type_options = array(
									'-' => ' - Без филтър - ',
									'precondition' => 'входни',
									'current' => 'междинни',
									'final' => 'финални',
									'poll' => 'анкети'
								);
								RenderDropDown($type_options, $source['exam_type']);
							?>
							</select>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
							<button type="submit" style="margin-left: 15px; margin-top: 30px; vertical-align: bottom;"><span class="glyphicon glyphicon-filter"></span> Филтрирай</button>
							<button type="submit" style="height:32px;" id="check_btn">
								<span title="Homeworks" class="glyphicon glyphicon-check"></span> Провери
							</button>
							<a href="<?php echo header_link( array(CONTROLLER => 'homeworks', ACTION => 'index') ); ?>" title="Преглед на Домашни">
								<button type="button" class="btn-primary" style="vertical-align: bottom; float:right; margin-top:30px;">
									<span class="glyphicon glyphicon-transfer"></span>
								</button>
							</a>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<?php
		//$emails = array();
		$exams = array();
		$distinct_users = array();

		$total_score = 0;
		$total_count = 0;

		//$query_users = to_assoc_array(exec_query($query_string));
		$search_params = remove_url_elements(array(CONTROLLER, ACTION), $_SERVER['QUERY_STRING']);
		pre_print($courses_exams);
		foreach( $courses_exams as $k => $ch )
		{
			// General Functionality
			$tmp = array();

			$tmp['exam_id'] = $ch['exam_id'];
			$tmp['title'] = $ch['test_name'].' ['.$ch['test_id'].']';
			$tmp['type'] = str_ireplace(array('precondition', 'current', 'poll'), array('входен', 'междинен', 'анкета'), $ch['exam_type']);
			$tmp['weight'] = ($ch['weight'] * 100).' %';
			$tmp['course_id'] = $ch['course_name'].' ['.$ch['course_id'].']';
			$tmp['restrict'] = '<img src="'.DIR_IMAGES.'ui/'.(int) $ch['for_office_only'].'.png" alt="'.$ch['for_office_only'].'" title="For Office Only" style="width:16px;" />';
			$tmp['time'] = ($ch['test_seconds'] / 60);
			$tmp['shuffle'] = ($ch['shuffle_exam_components'] ? 'да' : 'не');
			$tmp['edit'] = '';

			$now = date('Y-m-d');
			$is_active_course = ($ch['start_date'] < $now && $now < $ch['end_date']);
			$is_future_course = ($now <= $ch['start_date']);

			// List exams by exam_id Functionality
			$exam_index_target = array( CONTROLLER => 'exam', ACTION => 'index', 'exam_id' => $ch['exam_id'] );

			$exam_index_button = '<a href="'.header_link( $homework_target_array ).'"><button type="button"><span class="glyphicon glyphicon-list"></span></button></a>';

			// HomeWork Edit Button
			$edit_btn = '<img src="'.DIR_IMAGES.'ui/application_form_edit.png" alt="Edit" />';

			if($is_active_course || $is_future_course)
			{
				$edit_btn = '<button type="button">'.$edit_btn.'</button>';
				$edit_btn = '<a href="'.header_link(array(CONTROLLER => 'exam', ACTION => 'add', ID => $ch['exam_id'])).($search_params != '' ? '&'.$search_params : '').'">'.$edit_btn.'</a>';
			}
			else
			{
				$edit_btn = '<button type="button" style="opacity: 0.5;" onclick="alert(\'Не можете да редактирате домашни свързани с приключил курс!\');">'.$edit_btn.'</button>';
			}

			// Calendar Button
			$calendar_button = '<button type="button"><span class="glyphicon glyphicon-calendar"></span></button>';

			// Statistic Button
			$statistic_button = '<a href="'.link_to( array(CONTROLLER => 'exam', ACTION => 'statistic', ID => $ch['exam_id'], 'OPTION_SET_CLASS' => false, MOP_PARAM => 0) ).'" title="Кликнете тук за да видите статистика за изпита"><button type="button"><span class="glyphicon glyphicon-stats"></span></button></a>'; //

			// Button "See Test Questions"
			$question_button = '<a href="'.header_link( array(CONTROLLER => 'question', ACTION => 'index', 'test_id' => $ch['test_id']) ).'" title="Виж тестовете към този курс"><button><span class="glyphicon glyphicon-question-sign"></span></button></a>';


			$tmp['edit'] .= $edit_btn;
			$tmp['edit'] .= $question_button;
			$tmp['edit'] .= $statistic_button;
			$tmp['edit'] .= $exam_index_button;
			$tmp['edit'] .= $calendar_button;

			$exams[] = $tmp;
		}

		//pre_print($users);
		database_close();

		$columns = array(
			array('text' => ($is_admin_user ? '<a href="'.link_to(array(CONTROLLER => 'exam', ACTION => 'add')).'"><button type="button"><img src="'.DIR_IMAGES.'ui/add_page_16.gif" alt="New" /></button></a>' :  '&nbsp;'), 'attr' => 'width="10"' ),
			array('text' => 'Базиран на тест', 'field' => 'test_name', 'key' => 'title', 'attr' => 'width="250"'),
			array('text' => 'Курс', 'field' => 'c.course_name', 'key' => 'course_id', 'attr' => 'width="390"'),
			array('text' => 'Тип', 'field' => 'exam_type', 'key' => 'type'),
			array('text' => 'Тежест', 'field' => 'weight', 'key' => 'weight'),
			array('text' => '<span class="glyphicon glyphicon-time"></span>', 'field' => 'test_seconds', 'key' => 'time', 'attr' => 'width="20" title="Продължителност в минути"'),
			array('text' => '<span class="glyphicon glyphicon-random"></span>', 'field' => 'shuffle_exam_components', 'key' => 'shuffle', 'attr' => 'width="20" title="Разбъркване на  въпроси / отговори"'),
			array('text' => '<img src="'.DIR_IMAGES.'ui/lock.png" alt="unlock" />', 'field' => 'for_office_only', 'key' => 'restrict', 'attr' => 'width="20" title="Видим само от залата"'),
			array('text' => 'Операции', 'field' => '#', 'key' => 'edit', 'attr' => 'width="163"') // 135
		);

		$table_classes = 'class="table table-striped table-bordered table-condensed table-responsive"';
		//$filter_columns = array('course_id', 'user_id', 'homework_id', 'score');

		//echo '<a class="fake_tab'.(isset($source[VIEW]) && $source[VIEW] == 'all' ? ' fake_tab_active' : '').'" href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element(VIEW, $_SERVER['QUERY_STRING']).'&' : '').VIEW.'=all"><img src="'.DIR_IMAGES.'ui/preferences_contact_list.png" /> Всички</a>';
		//echo '<a class="fake_tab'.(isset($source[VIEW]) && $source[VIEW] == 'non_evaluated' ? ' fake_tab_active' : '').'" href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element(VIEW, $_SERVER['QUERY_STRING']).'&' : '').VIEW.'=non_evaluated"><img src="'.DIR_IMAGES.'ui/0.png" height="16" /> Неоценени</a>';


		/*
		foreach($filter_columns as $filter)
		{
			if(isset($source[$filter]) && $source[$filter] != '')
			{
				echo '<a class="fake_tab fake_tab_remove" title="Кликнете тук за да премахнете филтъра!" href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element($filter, $_SERVER['QUERY_STRING']).'&' : '').'">Филтър: '.$filter.' = '.(is_array($source[$filter]) ? implode(',', $source[$filter]) : $source[$filter]).'</a>';
			}
		}
		*/

		render_table($exams, $columns, $table_classes);

		echo '<b title="'.$total_score.' / '.$total_count.'">Средно: '.($total_count == 0 ? 0 : round($total_score / $total_count, 2)).'%</b>';
		echo '<b title="'.implode(', ', $distinct_users).'" class="pull-right">Оценени от '.count($distinct_users).' различни потребителя</b>';
		//$emails = implode(', ', $emails);
		//echo '<textarea cols="96" style="margin-top:10px; border:5px ridge lightblue;">'.$emails.'</textarea>';
	}
	else
	{
		page_not_access();
	}
?>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready( function () {
		$('#check_btn').click(function(){
			//$('#check_them').val(1);

			$('#course_id').val(-1);
			$('#for_office_only').val(0);
			$('#homework_type').val('final');
			$('#weight').prop('disabled', true);
		});
	});
</script>
