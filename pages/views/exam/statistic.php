<div class="container">
	<div class="row">
	<?php
	Global $source, $is_admin_user, $all_questions, $test_name, $is_super_admin, $highest_score, $lowest_score, $average_score, $users_array, $has_filter_permissions, $except_questions, $except_users, $users_roles, $course_data;

	if(true)
	{
		if(have_messages())
		{
			$have_error = has_error_messages();

			show_all_messages();
			clear_all_messages();
		}
		else
		{
			$have_error = false;
		}

		if(!$have_error)
		{
			$count = 0;
			$points = 0;
			$hidden_fields = '';

			$hidden_fields .= '<input type="hidden" name="'.CONTROLLER.'" value="'.$source[CONTROLLER].'" />';
			$hidden_fields .= '<input type="hidden" name="'.ACTION.'" value="'.$source[ACTION].'" />';
			$hidden_fields .= '<input type="hidden" name="'.ID.'" value="'.$source[ID].'" />';



			//pre_print($all_questions);
			echo '<h1>Статистика за '.$test_name.' <br>(Курс №'.$course_data['course_id'].': '.$course_data['course_name'].')</h1>';
			echo '<form method="get" action="'.header_link(array(CONTROLLER => 'exam', ACTION => 'statistic', ID => $source[ID])).'">';

			//pre_print($all_questions);

			$closed_questions = array();
			$open_questions = array();

			foreach($all_questions as $q_id => $data)
			{
				$value = 0;

				if(isset($data['point']) && isset($data['count']))
				{
					$txt = excerpt_text(htmlspecialchars($data['title']), 150);

					if(!in_array($q_id, $except_questions))
					{
						$value = round($data['point'] / $data['count'], 2);
						$points += round($data['point'] / $data['count'], 2);
						$count++;
					}
					else
					{
						$txt = '<s>'.$txt.'</s>';
					}

					$style = 'color:black;';
					$columns = array('first' => '', 'second' => 'Въпрос<button style="float:right;" id="btn_eye" type="button"><span class="glyphicon glyphicon-eye-close"></span></button>');//, 'third' => '<button style="margin:auto; display:block;" id="btn_eye" type="button"><span class="glyphicon glyphicon-eye-close"></span></button>');

					if($value < 2) // This is %
					{
						$val = $value * 100;
						$columns['first'] = '&nbsp;%';

						if($val == 100){ $style = 'color: silver;';}
						if($val <=  99){ $style = 'color: green;';}
						if($val <=  89){ $style = 'color: blue;';}
						if($val <=  69){ $style = 'color: red;';}
					}
					else // This is between 2 and 6
					{
						$val = $value;
						$columns['first'] = 'Оценка';

						if( ((float)$val - (int) $val) == 0)
						{
							$val = $val.'.00';
						}
						if($val <= 6){ $style = 'color: green;';}
						if($val <= 5.50){ $style = 'color: blue;';}
						if($val <= 5.00){ $style = 'color: red;';}
					}

					//$val = ($value < 2 ? ($value * 100).'%' : 'Оценка: '.$value);
					$val = '<span style="'.$style.' cursor:help; display:block; text-align:center;" title="'.($is_super_admin ? str_replace('<br>', '&#013;', $data['info']) : '').'">'.$val.'</span>';
					$chk = '<input type="checkbox" class="except_this" name="except_questions[]" value="'.$q_id.'"'.(in_array($q_id, $except_questions) ? 'checked="checked"' : '').' />';
					$txt .= $chk;
					$closed_questions[] = array($val, $txt); //, $chk);
				}
				else
				{
					usort($data['info'], function($a,$b){ return mb_strtoupper(trim(end($a))) > mb_strtoupper(trim(end($b))); } );

					$open_questions[] = excerpt_details_summary($data['title'], 155, return_table($data['info']), 'class="statistic"');
				}
			}


			render_table($closed_questions, $columns, 'border="1" style="width:100%;"');
			$value = round($points / $count, 2);
			echo '<b>Средно по въпроси: '.$points.' / '.$count.' = '.($value < 2 ? ($value * 100).'%' : 'Оценка: '.$value).'</b><br><br>';

			if(count($open_questions) > 0)
			{
				render_table($open_questions, array('Отворени Въпроси:'), 'style="width:100%;"');
			}
			else
			{
				echo '<b>Най-нисък резултат: '.$lowest_score.'</b><br>';
				echo '<b>Най-висок резултат: '.$highest_score.'</b><br>';
				echo '<b>Усреднена стойност: '.$average_score.'</b><br>';
			}

			if($has_filter_permissions)
			{
				echo '<br /><fieldset id="fieldset_users" style="display:none;"><legend style="font-weight:bold; font-size:15px;">Игнориране на потребители';
				echo '<button type="submit" style="float:right;"><span class="glyphicon glyphicon-refresh"></span></button>';
				echo '<button type="button" id="btn_globe" style="float:right;"><span class="glyphicon glyphicon-globe"></span></button>';
				echo '<button type="button" id="btn_retweet" style="float:right;"><span class="glyphicon glyphicon-retweet"></span></button>';
				echo '</legend>'.$hidden_fields;
				foreach($users_array as $v)
				{
					$user_class = 'student-unknown';
					if(2 == $users_roles[$v]['form_id'] && 0 == $users_roles[$v]['group_number']){ $user_class = 'student-online'; }
					if(1 == $users_roles[$v]['form_id'] && 0 != $users_roles[$v]['group_number']){ $user_class = 'student-onlive'; }
					echo '<label style="border:1px solid silver; width:63px; margin:1px;"><input type="checkbox" name="except_users[]" class="student '.$user_class.'" style="margin:3px;" value="'.$v.'"'.(in_array($v, $except_users) ? 'checked="checked"' : '').' /> '.$v.'</label>';
				}
				echo '</fieldset>';
			}

			echo '</form>';
		}
	}
	else
	{
		page_not_access();
	}

?>
		<script>
			var check_online = true; // Set Default value for the first click

			$(document).ready(function(){
				$("#btn_eye").click(function(){
				<?php if($has_filter_permissions){ ?>
					$("#fieldset_users").fadeToggle();
					$(".except_this").fadeToggle();
				<?php } else { ?>
					alert('Филтрирането по курсисти е недостъпно за вас!');
				<?php } ?>
				});

				$("#btn_globe").click(function(){
					$('input.student-online').prop("checked", check_online);
					check_online = !check_online;
				});

				$("#btn_retweet").click(function(){
					var checkboxes = $('input.student');
					checkboxes.each(function( index ) {
						$( this ).prop("checked", !$( this ).prop('checked'));
					});
				});

				<?php
				if(isset($_GET['except_users']) || isset($_GET['except_questions']))
				{
					echo '$("#btn_eye").trigger("click");';
				}
				?>
			});
		</script>
		<br />
	</div>
</div>