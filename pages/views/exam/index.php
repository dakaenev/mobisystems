<?php
	Global $is_admin_user, $exams, $source;
    $search_params = remove_url_elements(array(CONTROLLER, ACTION), $_SERVER['QUERY_STRING']);
?>
<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<form method="GET" action="<?php echo header_link(array(CONTROLLER => 'exam', ACTION => 'index')); ?>">
				<div class="form-group">
					<div class="row">

						<input type="hidden" name="<?php echo CONTROLLER; ?>" value="exam" />
						<input type="hidden" name="<?php echo ACTION; ?>" value="index" />

						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
							<label for="course_id">
								Курс
							</label>

							<select name="course_id" class="form-control">
								<option value="-1">- Без Филтър -</option>
								<?php

									$archive = '';
									$current = '';
									foreach(get_all_courses() as $course)
									{
										$course['end_date'] = strtotime($course['end_date']) + (0 * 7 * 24 * 3600); // курса се брои за активен до 2 седмици след неговия край ;-)
										$which_courses = ($course['start_date'] <= date('Y-m-d') && strtotime(date('Y-m-d')) <= $course['end_date'] ? 'current' : 'archive');
										$$which_courses .= '<option value="'.$course['course_id'].'"'.($source['course_id'] == $course['course_id'] ? ' selected="selected"' : '').'>'.$course['course_id'].'. '.$course['course_name'].'</option>';
										// Тук използваме $$ за да укажем, коя променлива искаме да променяме. Така избягваме 1 if, или пък двойното писане на един и същи код.
									}
                                    $default = '<option value="0"'.(0 == $source['course_id'] ? ' selected="selected"' : '').'>Всички активни курсове</option>';
									$archive = '<optgroup label="Архивни">'.$archive.'</optgroup>';
									$current = '<optgroup label="Текущи">'.$current.'</optgroup>';

									echo $current.$archive;
								?>
							</select>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-3">
							<label for="test_id">
								Тест
							</label>
							<select name="test_id" id="test_id" class="form-control" required="required">
							<?php
								$tests = get_all_tests('1=1 ORDER BY test_name ASC');
								unset($tests[0]); // Премахваме Въпросите без отговор
								$tests_dropdown = ReturnDropDown($tests, $source['test_id'], true);
								$tests_dropdown = '<option value="-1"> - Изберете тест - </option>'.$tests_dropdown;
								echo $tests_dropdown;
							?>
							</select>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-3">
							<label for="exam_type">
								Тип
							</label>

							<select name="exam_type" id="exam_type" class="form-control">
							<?php
								$type_options = array(
									'-' => ' - Без филтър - ',
									'precondition' => 'входни',
									'current' => 'междинни',
									'final' => 'финални',
									'poll' => 'анкети'
								);
								RenderDropDown($type_options, $source['exam_type']);
							?>
							</select>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
							<button type="submit" style="margin-top:30px; vertical-align: bottom;"><span class="glyphicon glyphicon-filter"></span> Филтрирай</button>

							<a href="<?php echo header_link( array(CONTROLLER => 'exam', ACTION => 'manage') ).($search_params != '' ? '&'.$search_params : ''); ?>" title="Управление на Изпити">
								<button type="button" class="btn-primary" style="vertical-align: bottom; float:right; margin-top:30px;">
									<span class="glyphicon glyphicon-transfer"></span>
								</button>
							</a>

							<!--
							<a href="<?php echo link_to( array(CONTROLLER => 'homeworks', ACTION => 'clear', 'OPTION_SET_CLASS' => false) ); ?>" style="color:white;">
								<button type="button" class="btn btn-large btn-danger" style="margin-top:30px;">
									<span class="glyphicon glyphicon-trash"></span>
								</button>
							</a>
							-->
						</div>
					</div>
				</div>
			</form>
        </div>
	</div>
</div>
<?php
	if($is_admin_user)
	{
		$min_pass_score = 20;
		echo '<div class="container"><div class="row"><div class="col-lg-12">';

		$exams_array = array();
		//pre_print($exams,1);
		foreach($exams as $exam)
		{
			$tmp = array('history_id' => '', 'courses' => '', 'test_id' => '', 'exam_id' => '', 'user_id' => '', 'score' => '', 'is_pass' => '', 'solved_on_date' => '', 'chk' => '');

			if($exam['full_name'] == '')
			{
				$exam['full_name'] = '<i>User #'.$exam['user_id'].'</i>';
			}

			$tmp['history_id'] = $exam['history_id'];
			$tmp['test_id'] = '<a href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element('test_id', $_SERVER['QUERY_STRING']).'&' : '').'test_id='.$exam['test_id'].'" title="">'.$exam['test_name'].'</a>';
            $tmp['exam_id'] = '<a href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element('exam_id', $_SERVER['QUERY_STRING']).'&' : '').'exam_id='.$exam['exam_id'].'" title="">'.$exam['exam_id'].'</a>';
            $tmp['courses'] = '<a href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element('course_id', $_SERVER['QUERY_STRING']).'&' : '').'course_id='.$exam['course_id'].'" title="">'.$exam['course_name'].'</a>';
			$tmp['user_id'] = '<a href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element('user_id', $_SERVER['QUERY_STRING']).'&' : '').'user_id='.$exam['user_id'].'" title="">'.$exam['full_name'].'</a>';
			$tmp['score'] = '<span title="'.$exam['score'].'%">'.number_format($exam['score'], 2).' % = '.percent2hex($exam['score']).'</span>';
			$tmp['is_pass'] = '<span class="center"><img src="'.DIR_IMAGES.'ui/'.(int) ($exam['score'] >= $min_pass_score) .'.png" alt="'.$exam['is_pass'].'" /></span>';
			$tmp['solved_on_date'] = my_date_format($exam['solved_on_date']);
			$tmp['chk'] = '<input type="checkbox" class="center" name="delete_history[]" value="'.$exam['history_id'].'" />';

			$exams_array[] = $tmp;
		}

		$columns = array(
			array('text' => 'ID', 'field' => 'history_id'),
			array('text' => 'Курс', 'field' => 'courses'),
			array('text' => 'Тест', 'field' => 'test_id'),
            array('text' => 'Изпит','field'=>'exam_id'),
			array('text' => 'Потребител', 'field' => 'user_id'),
			array('text' => 'Оценка', 'field' => 'score'),
			array('text' => 'Статус', 'field' => 'is_pass'),
			array('text' => 'Решен на', 'field' => 'solved_on_date'),
			array('text' => '<span class="glyphicon glyphicon-trash" id="btn_delete_mails"></span>', 'field' => 'chk')
		);

		$filter_columns = array( 'user_id', 'exam_id');
        foreach($filter_columns as $filter)
		{
			if(isset($source[$filter]) && $source[$filter] != '')
			{
				echo '<a class="fake_tab fake_tab_remove" title="Кликнете тук за да премахнете филтъра!" href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element($filter, $_SERVER['QUERY_STRING']).'&' : '').'">Филтър: '.$filter.' = '.(is_array($source[$filter]) ? implode(',', $source[$filter]) : $source[$filter]).'</a>';
			}
		}

		render_table($exams_array, $columns, 'class="table-w100 table-striped table-hover table-bordered"');
		echo '</div></div></div>';

	}
	else
	{
		page_not_access();
	}
?>