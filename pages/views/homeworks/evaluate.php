<?php
	Global $is_admin_user, $source, $user_id, $query_homeworks, $search_params, $new_time, $TIPS_ARRAY, $TAGS_ARRAY;
?>
<div class="container">
	<form method="post" action="./?c=homeworks&a=evaluate&<?php echo ID.'='.$source[ID].($search_params != '' ? '&'.$search_params : ''); ?>" id="evaluate_form">
	<div class="row">
		<div class="col-lg-12">
<?php
	if( $is_admin_user || is_assistant() || (isset($_SESSION['RANDOM_HOMEWORK']) && $_SESSION['RANDOM_HOMEWORK'] == $source[ID]) )
	{
		if(have_messages())
		{
			show_all_messages();
		}
?>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<?php
		if(!has_error_messages())
		{
			$result = array();
			$emails = array();

			if(!has_error_messages())
			{
				foreach( $query_homeworks as $k => $homework )
				{
					//pre_print($homework);

					$download_link = str_replace('wp-content/themes/Artificial-Reason-WP/tests_system//../../../uploads/homeworks/', 'wp-content/uploads/homeworks/', $homework['upload_name']);
					$homework['score'] = ($homework['score'] == -1 ? 0 : $homework['score']);

					$result[$k]['operators'] = '<a href="#" onclick="document.getElementById(\'evaluate_form\').reset();"><button type="reset"><img src="'.DIR_IMAGES.'ui/r.png" height="22"></button></a>';
					$result[$k]['id'] = esc_html( $homework['id'] );
					$result[$k]['homework_id'] = '<a href="'.$homework['homework_url'].'" title="Свали условието"><img src="'.DIR_IMAGES.'ui/document_icon_h.png" alt="Download" /></a>';
					$result[$k]['upload_name'] = '<a class="hidden-xs" href="'.$download_link.'">'.esc_html(str_ireplace('/uploads/homeworks/', '/homeworks/', $download_link)).'</a><a class="hidden-sm hidden-md hidden-lg" href="'.$download_link.'"><span class="glyphicon glyphicon-file"></span></a>';
					$result[$k]['upload_date'] = date('H:i:s', $new_time + TIME_CORRECTION );
					$result[$k]['score'] = $homework['score'].'%';
					$result[$k]['user_id'] = ($is_admin_user ? '<a href="'.link_to(array(CONTROLLER => 'user', ACTION => 'view', ID => $homework['user_id'])).'" target="_blank" title="View User #'.$homework['user_id'].'"><button type="button"><img src="'.DIR_IMAGES.'ui/views.gif" alt="View" /></button></a>' : '<a href="#" data-toggle="modal" data-target="#HelpModalW"><button type="button"><span class="glyphicon glyphicon-question-sign"></span></button></a>');
					if(isset($_POST['score']))
					{
						$homework['score'] = $_POST['score'];
					}

					if(isset($_POST['score_arguments']))
					{
						$homework['score_arguments'] = $_POST['score_arguments'];
					}
				}
			}

	//pre_print($users);

			$columns = array(
				array('text' => ($is_admin_user || is_assistant() ? '<a href="'.header_link(array(CONTROLLER => 'homeworks', ACTION => 'index')).($search_params != '' ? '&'.$search_params : '').'"><button type="button" style="width:38px;" title="Кликнете тук за връщане към общия списък!"><img src="'.DIR_IMAGES.'ui/preferences_contact_list.png" /></button></a>' : ''), 'style' => 'width:30px;'),
				'ID',
				'HW',
				'File',
				'Запазена до:',
				'Оценка',
				($is_admin_user ? HelpButton('/viewtopic.php?f=19&t=6 #post_content7 table') : '') //
			);

			$table_classes = 'class="table table-striped table-bordered table-condensed table-responsive"';

			echo '<br />';

			render_table($result, $columns, $table_classes);

			// ============================= TOOLTIPS Functionality =====================================
			$tooltips_helper = '[TOOLTIPS]';

			$tooltips_helper = '<details class="pull-left"><summary>Съвети:</summary>';
			foreach($TIPS_ARRAY as $k => $v)
			{
				$tooltips_helper .= '<label><input type="checkbox" name="selected_tips['.$k.']" value="'.$k.'"> '.htmlspecialchars(stripslashes($v)).'</label><br />'. PHP_EOL;
			}
			$tooltips_helper .= '</details>';

			$tooltips_helper .= '<details class="pull-right"><summary>Тагове:</summary>';
			foreach($TAGS_ARRAY as $k => $v)
			{
				if($v['is_active'])
				{
					$tooltips_helper .= '<label><input type="checkbox" name="selected_tags['.$k.']" value="'.$k.'"> '.$v['tag_name'].'</label><br />'. PHP_EOL;
				}
			}
			$tooltips_helper .= '</details>';

			echo ($is_admin_user || is_assistant() ? $tooltips_helper : '');
			$placeholder = 'Моля, опишете тук, защо поставяте точно такава оценка! Опишете какво ви харесва и какво не, какво работи и какво не. '.PHP_EOL.'Дайте препоръки на курсиста! Желателно е да използвате повече от 10 символа :)';
			echo '<textarea id="score_arguments" name="score_arguments" style="border:3px solid lightblue; width:100%; height:100px;" placeholder="'.$placeholder.'">'.htmlspecialchars(stripslashes($homework['score_arguments'])).($is_admin_user && strpos($homework['score_arguments'], $TAGS_ARRAY[13]['tag_name']) !== false && strpos($homework['score_arguments'], $TAGS_ARRAY[14]['tag_name']) === false && strpos($homework['score_arguments'], $TAGS_ARRAY[15]['tag_name']) === false ? PHP_EOL.$TAGS_ARRAY[14]['tag_name'].' Моля пишете тук! :) '.$TAGS_ARRAY[17]['tag_name'] : '').'</textarea>';
			echo 'Оценка: <button type="button" id="restore_form" style="width:38px;" title="Кликнете тук за да възстановите последните въведени данни!"><span class="glyphicon glyphicon-cloud-download"></span></button><input type="number" name="score" value="'.abs($homework['score']).'" min="0" max="100"><button type="submit" id="send_form" title="Кликнете тук за да изпратите формата!">Оцени |<span class="glyphicon glyphicon-cloud-upload"></span></button>';
			if($is_admin_user)
			{
				echo '<span class="pull-right">Тип редакция: ';
				echo '<label style="margin:0px 7px;" title="Без промяна самоличността на проверилия домашната"><input type="radio" name="edit_mode" value="'.$TAGS_ARRAY[14]['tag_name'].'" checked="checked" /> Допълване</label>';
				echo '<label style="margin:0px 7px; color:red;" title="Домашната ще се счита за проверена от вас!"><input type="radio" name="edit_mode" value="'.$TAGS_ARRAY[15]['tag_name'].'" /> Ревизия</label>';
				echo '</span>';
			}
		}

		clear_all_messages();
	}
	else
	{
		page_not_access();
	}
?>
			<script>
				$(document).ready(function(){
					console.log("Saving data to local storage...");

					$( "#evaluate_form" ).submit(function( event ) {
						if (typeof(Storage) !== "undefined") {
							var homework_fields = $( "#evaluate_form" ).serializeArray();

							sessionStorage.setItem("last_homework", JSON.stringify(homework_fields) );
							console.log("Success! Data are saved with Success...");
						} else {
							console.log("Sorry! No Web Storage support..");
						}
					});

					$("#restore_form").click(function(){
						console.log("Reading Session Storage...");

						if (typeof(Storage) !== "undefined") {
							var homework_fields = sessionStorage.getItem("last_homework");
							console.log(homework_fields);

							if(homework_fields != null && homework_fields != "")
							{
								homework_fields = JSON.parse(homework_fields);

								jQuery.each( homework_fields, function( i, field ) {
									document.getElementsByName(field.name)[0].value = field.value;
								});
							}
							else
							{
								alert("Sorry! Session Storage is empty... :(");
							}
						} else {
							alert("Sorry! No Web Storage support..");
						}
					});
				});
			</script>
		</div>
	</div>
	<div class="modal fade" id="HelpModalW" tabindex="-1" role="dialog" aria-labelledby="HelpModalLabel" aria-hidden="true">
		<div class="modal-dialog" style="width:925px;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">Помощ</h4>
				</div>
				<div class="modal-body">
					<img src="<?php echo DIR_IMAGES; ?>ui/hw_help.jpg" alt="HW Help" width="900" />
					<br />
					<span style="font-family: 'Open Sans' !important; font-size: 13px !important; font-weight: 400 !important; color: #000000 !important;">
						<b>
							Как се работи със системата?
							<ol>
								<li>Свалете условието с изискванията на домашната от иконката <a href="<?php echo $homework['homework_url']; ?>" title="Свали условието"><img src="<?php echo DIR_IMAGES; ?>ui/document_icon_h.png" alt="Download" /></a></li>
								<li>Свалете решението на курсиста от линка: <a href="<?php echo $download_link; ?>"><?php echo esc_html(str_ireplace('/wp-content/uploads/homeworks/', '/.../', $download_link)); ?></a></li>
								<li>В полето за оценка впишете стойност между 2 и 100; 100 е максималната!</li>
								<li>В полето за описание обяснете вашите мотиви да поставите тази оценка</li>
							</ol>
							<br />
							Не забравяйте че:
							<ul>
								<li>Оценката трябва да бъде между 2 и 100, като 100 е най-високата стойност</li>
								<li>Полето за описание задължително трябва да съдържа конкретен текст за вашите мотиви при поставянето на оценката</li>
								<li>Тази домашна е запазена за вас до <?php echo date('Y-m-d H:i:s', $new_time + TIME_CORRECTION ); ?>. Ако имате нужда от повече време <a href="">презаредете</a> страницата</li>
							</ul>
						</b>
					</span>
				</div>
				<div class="modal-footer">
					Вижте още: <a href="/forum/viewtopic.php?f=19&t=6" target="_blank">"Как се оценява домашна" - Наръчник</a>
				</div>
			</div>
		</div>
	</div>
	</form>
</div>
<script type="text/javascript">
	$(document).ready( function () {
		$('input[name=edit_mode]').change(function(){
			var current_text = $('#score_arguments').val();

			if($(this).val() == "<?php echo $TAGS_ARRAY[15]['tag_name']; ?>")
			{
				current_text = current_text.replace("<?php echo $TAGS_ARRAY[14]['tag_name']; ?>", "<?php echo $TAGS_ARRAY[15]['tag_name']; ?>");
				current_text = current_text.replace("<?php echo $TAGS_ARRAY[17]['tag_name']; ?>", "<?php echo $TAGS_ARRAY[18]['tag_name']; ?>");
			}

			if($(this).val() == "<?php echo $TAGS_ARRAY[14]['tag_name']; ?>")
			{
				current_text = current_text.replace("<?php echo $TAGS_ARRAY[15]['tag_name']; ?>", "<?php echo $TAGS_ARRAY[14]['tag_name']; ?>");
				current_text = current_text.replace("<?php echo $TAGS_ARRAY[18]['tag_name']; ?>", "<?php echo $TAGS_ARRAY[17]['tag_name']; ?>");
			}

			$('#score_arguments').val(current_text);
		});
	});
</script>
