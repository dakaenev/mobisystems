<div class="container">
<?php
	Global $is_admin_user, $source, $_connection, $query_users, $query_string;

	if(SHOW_DEBUG_INFO){
?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel-group" id="accordion1" style="margin-top:20px;">
				<div class="panel panel-<?php echo (count($query_users) != 0 ? 'info' : 'danger'); ?>">
					<div class="panel-heading panel-plus-link">
							<a class="accordion-toggle<?php echo (count($query_users) != 0 ? ' collapsed' : ''); ?>" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne1" aria-expanded="<?php echo (count($query_users) != 0 ? 'false' : 'true'); ?>">
								<b><h5 style="display:inline;">Управление на Домашни</h5><?php echo HelpButton(); ?></b>
							</a>
					</div>
					<div id="collapseOne1" class="panel-collapse collapse<?php echo (count($query_users) != 0 ? '' : ' in'); ?>" aria-expanded="<?php echo (count($query_users) != 0 ? 'false' : 'true'); ?>">
						<div class="panel-body">
							<?php echo SQL_DEBUG($query_string); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php } ?>
	<div class="row">
		<div class="col-lg-12">
<?php
	if($is_admin_user || is_assistant())
	{
		database_open();

		if(have_messages())
		{
			show_all_messages();
			clear_all_messages();
		}

		$search_params = remove_url_elements(array(CONTROLLER, ACTION), $_SERVER['QUERY_STRING']);
?>
			<form method="GET" action="<?php echo header_link(array(CONTROLLER => 'homeworks', ACTION => 'index')); ?>">
				<div class="form-group">
					<div class="row">

						<input type="hidden" name="<?php echo CONTROLLER; ?>" value="homeworks" />
						<input type="hidden" name="<?php echo ACTION; ?>" value="index" />

						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
							<label for="course_id">
								Курс
							</label>

							<select name="course_id" class="form-control">
								<option value="">- Без Филтър -</option>
								<?php

									$archive = '';
									$current = '';
									foreach(get_all_courses() as $course)
									{
										$course['end_date'] = strtotime($course['end_date']) + (0 * 7 * 24 * 3600); // курса се брои за активен до 2 седмици след неговия край ;-)
										$which_courses = ($course['start_date'] <= date('Y-m-d') && strtotime(date('Y-m-d')) <= $course['end_date'] ? 'current' : 'archive');
										$$which_courses .= '<option value="'.$course['course_id'].'"'.($source['course_id'] == $course['course_id'] ? ' selected="selected"' : '').'>'.$course['course_id'].'. '.$course['course_name'].'</option>';
										// Тук използваме $$ за да укажем, коя променлива искаме да променяме. Така избягваме 1 if, или пък двойното писане на един и същи код.
									}

									$archive = '<optgroup label="Архивни">'.$archive.'</optgroup>';
									$current = '<optgroup label="Текущи">'.$current.'</optgroup>';

									echo $current.$archive;
								?>
							</select>
						</div>

						<!--
						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
							<label for="homework_id">
								Домашна
							</label>

							<select name="homework_id[]" class="form-control expand_height" multiple="multiple">
								<option value="" disabled="disabled">- Без Филтър -</option>
								<?php foreach(get_all_homeworks('ch.course_id='.(int) $source['course_id']) as $homework){
									echo '<option value="'.$homework['homework_id'].'"'.($source['homework_id'] == $homework['homework_id'] ? ' selected="selected"' : '').'>'.$homework['homework_title'].'</option>';
								} ?>
							</select>
						</div>
						-->

						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
							<label for="view">
								Тип
							</label>

							<select name="<?php echo VIEW; ?>" id="view" class="form-control">
								<!-- <option value=""<?php if($source[VIEW] == '') echo ' selected'; ?>> - Без филтър - </option> -->
								<optgroup label="Основни">
									<option value="all"<?php if($source[VIEW] == 'all') echo ' selected'; ?>> Всички </option>
									<option value="evaluated"<?php if($source[VIEW] == 'evaluated') echo ' selected'; ?>> Проверени </option>
									<option value="non_evaluated"<?php if($source[VIEW] == 'non_evaluated') echo ' selected'; ?>> Непроверени </option>
									<option value="locked"<?php if($source[VIEW] == 'locked') echo ' selected'; ?>> Заключени </option>
								</optgroup>
								<optgroup label="Възражения">
									<option value="reported_all"<?php if($source[VIEW] == 'reported_all') echo ' selected'; ?>> Получени </option>
									<option value="reported_answered"<?php if($source[VIEW] == 'reported_answered') echo ' selected'; ?>> Отговорени</option>
									<option value="reported_revisited"<?php if($source[VIEW] == 'reported_revisited') echo ' selected'; ?>> Преоценени </option>
									<option value="reported_only"<?php if($source[VIEW] == 'reported_only') echo ' selected'; ?>> Неразгледани </option>
								</optgroup>
							</select>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
							<label for="teachers">
								Проверени от
							</label>

							<select name="teacher_id" id="teachers" class="form-control">
								<option value=""<?php if($source['teacher_id'] == '') echo ' selected'; ?>> - Без филтър - </option>
								<optgroup label="Проверени от...">
									<option value="admins"<?php if($source['teacher_id'] == 'admins') echo ' selected'; ?>> Администратори </option>
									<option value="assist"<?php if($source['teacher_id'] == 'assist') echo ' selected'; ?>> Асистенти </option>
									<option value="member"<?php if($source['teacher_id'] == 'member') echo ' selected'; ?>> Курсисти </option>
									<option value="-1"> Добави сега </option>
								</optgroup>
								<optgroup label="Временни" id="temp_teachers"></optgroup>
							</select>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
							<label for="tags">
								Тагове
							</label>

							<select name="tags" id="tags" class="form-control">
								<option value=""<?php if($source['tags'] == '') echo ' selected'; ?>> - Без филтър - </option>
								<optgroup label="Системни">
									<?php
									foreach(get_all_tags() as $tag)
									{
										//$course['end_date'] = strtotime($course['end_date']) + (0 * 7 * 24 * 3600); // курса се брои за активен до 2 седмици след неговия край ;-)
										//$which_courses = ($course['start_date'] <= date('Y-m-d') && strtotime(date('Y-m-d')) <= $course['end_date'] ? 'current' : 'archive');
										$tmp = '<option value="'.$tag['tag_name'].'"'.($source['tags'] == $tag['tag_name'] ? ' selected="selected"' : '').'>'.$tag['tag_name'].'</option>';
										if($tag['is_active'])
										{
											$tags .= $tmp;
										}
										// Тук използваме $$ за да укажем, коя променлива искаме да променяме. Така избягваме 1 if, или пък двойното писане на един и същи код.
									}

									echo $tags;
									?>
									<option value="-1"> Добави сега </option>
								</optgroup>
								<optgroup label="Временни" id="temp_tags"></optgroup>
							</select>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
							<button type="submit" style="margin-top:30px; vertical-align: bottom;"><span class="glyphicon glyphicon-filter"></span> Филтрирай</button>
							<a href="<?php echo link_to( array(CONTROLLER => 'homeworks', ACTION => 'unlock', 'OPTION_SET_CLASS' => false) ); ?>" style="color:black;">
								<button type="button" style="height:32px;">
									<img src="<?php echo DIR_IMAGES; ?>ui/lock.png" alt="unlock" /> Отключи
								</button>
							</a>
							<a href="<?php echo header_link( array(CONTROLLER => 'homeworks', ACTION => 'manage') ).($search_params != '' ? '&'.$search_params : ''); ?>" title="Управление на Домашни">
								<button type="button" class="btn-primary" style="vertical-align: bottom; float:right; margin-top:30px;">
									<span class="glyphicon glyphicon-transfer"></span>
								</button>
							</a>

							<!--
							<a href="<?php echo link_to( array(CONTROLLER => 'homeworks', ACTION => 'clear', 'OPTION_SET_CLASS' => false) ); ?>" style="color:white;">
								<button type="button" class="btn btn-large btn-danger" style="margin-top:30px;">
									<span class="glyphicon glyphicon-trash"></span>
								</button>
							</a>
							-->
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<?php
		$users = array();
		$emails = array();
		$distinct_users = array();

		$total_score = 0;
		$total_count = 0;

		$query_users = to_assoc_array(exec_query($query_string));

		foreach( $query_users as $k => $user )
		{
			//pre_print($user, true);

			$download_link = '/'.$user['upload_name'];
			$print_link = $download_link;

			$users[$k]['operators'] = '<a href="'.header_link(array(CONTROLLER => 'homeworks', ACTION => 'evaluate', ID => $user['id'])).($search_params != '' ? '&'.$search_params : '').'"><button><img src="'.DIR_IMAGES.'ui/application_form_edit.png" alt="Edit" /></button></a>';
			if($is_admin_user)
			{
				$users[$k]['operators'] .= '<a href="'.link_to(array(CONTROLLER => 'user', ACTION => 'view', ID => $user['user_id'])).'" title="View User"><button><img src="'.DIR_IMAGES.'ui/views.gif" alt="View" /></button></a>';
			}

			$users[$k]['id'] = esc_html( $user['id'] );
			$users[$k]['user_id'] = '<a href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element('user_id', $_SERVER['QUERY_STRING']).'&' : '').'user_id='.$user['user_id'].'" title="">'.$user['user_id'].'</a>';
			$users[$k]['course_id'] = '<a href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element('course_id', $_SERVER['QUERY_STRING']).'&' : '').'course_id='.$user['course_id'].'" title="">'.$user['course_id'].'</a>';
			$users[$k]['homework_id'] = '<a href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element('homework_id', $_SERVER['QUERY_STRING']).'&' : '').'homework_id='.$user['homework_id'].'" title="'.$user['homework_title'].'">'.$user['homework_id'].'</a> <a href="'.$user['homework_url'].'" title="Свали условието" style="float:right;"><img src="'.DIR_IMAGES.'ui/document_icon_h.png" alt="Download" /></a>';
			$users[$k]['upload_name'] = '<a href="'.$download_link.'">'.esc_html($print_link).'</a>';
			$users[$k]['upload_date'] = date('Y-m-d H:i:s', strtotime($user['upload_date']) + TIME_CORRECTION);
			$users[$k]['score'] = ($user['score'] == 0 ? '<span style="color:red; font-weight:bold;">'.$user['score'].'</span>' : $user['score'].'<sup class="pull-right"><a terget="_blank" href="'.link_to(array(CONTROLLER => 'user', ACTION => 'view', ID => $user['teacher_id'])).'" title="View User"><span class="glyphicon glyphicon-eye-open"></span></a></sup><a href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element('teacher_id', $_SERVER['QUERY_STRING']).'&' : '').'teacher_id='.$user['teacher_id'].'" title="'.$user['teacher_id'].'"><span class="pull-right">'.$user['teacher_id'].'</span></a>');

			if($user['score'] <> 0)
			{
				if(!in_array($user['teacher_id'], $distinct_users))
				{echo __LINE__.'<br>';
					$distinct_users[] = $user['teacher_id'];
				}

				$total_score += $user['score'];
				$total_count++;
			}
		}

		//pre_print($users);
		database_close();

		$columns = array(
			array('text' => ($is_admin_user ? '<a href="'.link_to(array(CONTROLLER => 'homeworks', ACTION => 'add')).'"><button style="width:71px;"><img src="'.DIR_IMAGES.'ui/add_page_16.gif" alt="" /> New</button></a>' :  '&nbsp;'), 'key' => 'operators', 'style' => 'width:30px;' ),
			array('text' => 'ID', 'field' => 'uh.id'),
			array('text' => 'User', 'field' => 'uh.user_id'),
			array('text' => 'Course', 'field' => 'ch.course_id'),
			array('text' => 'HW_ID', 'field' => 'uh.homework_id'),
			'Homework File',
			array('text' => 'Качен нa:', 'field' => 'uh.upload_date'),
			array('text' => 'Успех', 'field' => 'uh.score', 'attr' => 'style="width:80px;"' ),
		);

		$table_classes = 'class="table table-striped table-bordered table-condensed table-responsive"';
		$filter_columns = array('course_id', 'user_id', 'homework_id', 'score');

		//echo '<a class="fake_tab'.(isset($source[VIEW]) && $source[VIEW] == 'all' ? ' fake_tab_active' : '').'" href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element(VIEW, $_SERVER['QUERY_STRING']).'&' : '').VIEW.'=all"><img src="'.DIR_IMAGES.'ui/preferences_contact_list.png" /> Всички</a>';
		//echo '<a class="fake_tab'.(isset($source[VIEW]) && $source[VIEW] == 'non_evaluated' ? ' fake_tab_active' : '').'" href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element(VIEW, $_SERVER['QUERY_STRING']).'&' : '').VIEW.'=non_evaluated"><img src="'.DIR_IMAGES.'ui/0.png" height="16" /> Неоценени</a>';


		foreach($filter_columns as $filter)
		{
			if(isset($source[$filter]) && $source[$filter] != '')
			{
				echo '<a class="fake_tab fake_tab_remove" title="Кликнете тук за да премахнете филтъра!" href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element($filter, $_SERVER['QUERY_STRING']).'&' : '').'">Филтър: '.$filter.' = '.(is_array($source[$filter]) ? implode(',', $source[$filter]) : $source[$filter]).'</a>';
			}
		}

		render_table($users, $columns, $table_classes);
		echo '<b title="'.$total_score.' / '.$total_count.'">Средно: '.($total_count == 0 ? 0 : round($total_score / $total_count, 2)).'%</b>';
		echo '<b title="'.implode(', ', $distinct_users).'" class="pull-right">Оценени от '.count($distinct_users).' различни потребителя</b>';
		//$emails = implode(', ', $emails);
		//echo '<textarea cols="96" style="margin-top:10px; border:5px ridge lightblue;">'.$emails.'</textarea>';
	}
	else
	{
		page_not_access();
	}
?>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready( function () {
		$('#tags').change(function(){
			if($('#tags :selected').val() < 0)
			{
				var new_tag = prompt('Въведете нов временен таг');

				$('#temp_tags').append('<option value="' + new_tag + '"> ' + new_tag + ' </option>');
				$('#tags').val(new_tag);
			}
		});

		$('#teachers').change(function(){
			if($('#teachers :selected').val() < 0)
			{
				var new_teacher_id = prompt('Потребител №');

				$('#temp_teachers').append('<option value="' + new_teacher_id + '">' + 'Потребител №: ' + new_teacher_id + '</option>');
				$('#teachers').val(new_teacher_id);
			}
		});
	});
</script>
