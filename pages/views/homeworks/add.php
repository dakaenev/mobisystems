<?php
	Global $is_admin_user, $source, $is_empty_layout;

	if($is_admin_user)
	{
		//$question = reset($question);
		//pre_print($question);

		database_open();

		if(empty($_POST))
		{
			if(!isset($source[ID])) // This is New - Load Default Data
			{
				$default_url = '/uploads/'.date('Y').'/'.date('m').'/';
				$default_title = 'Домашна №';
				$default_course = (isset($_GET['default_course']) ? $_GET['default_course'] : 0);
				$default_start = (isset($source['default_start']) ? strtotime($source['default_start']) : strtotime('now'));
				$default_final_value = 0;
				$default_end = date('Y-m-d ', $default_start + 60 * 60 * 24 * 7).'23:59:59'; // +  1 week after default start date
				$default_start = date('Y-m-d H:i:s', $default_start);

				$visibility_array = array();
				for($i = -1; $i <= 1; $i++)
				{
					$visibility_array[] = array('group_number' => $i, 'start_date' => $default_start, 'end_date' => $default_end);
				}
			}
			else // This is Edit - Load Homework Data
			{
				$homework_id = (int) $source[ID];
				$homework = get_all_homeworks('homework_id = '.$homework_id);

				if(Only_One_Exists($homework))
				{
					$homework = reset($homework);

					$default_url = $homework['homework_url'];
					$default_title = $homework['homework_title'];
					$default_course = $homework['course_id'];
					$default_final_value = ($homework['homework_type'] == 'final' ? 1 : 0);
					$visibility_array = get_object_visibility('homework', $homework_id);
				}
				else
				{
					add_error_message('Homework not Exists!');
				}
			}
/*
			if(empty($visibility_array))
			{
				$visibility_array = array();
				for($i = -1; $i <= 1; $i++)
				{
					$visibility_array[] = array('group_number' => $i, 'start_date' => $default_start, 'end_date' => $default_end);
				}
			}
 */
		}
		else
		{
			$default_url = $_POST['homework_url'];
			$default_title = $_POST['homework_title'];
			$default_course = (int) $_POST['course_id'];
			$default_final_value = (int) $_POST['is_final_exam'];
			$default_start = date('Y-m-d H:i:s');
			$default_end = date('Y-m-d ', strtotime('+1 week')).'23:59:59';

			$visibility_array = $_POST['visibility'];
		}

		$current_date = date('Y-m-d');
		//$courses = get_all_courses('start_date <= "'.$current_date.'" AND "'.$current_date.'" <= end_date ORDER BY course_id DESC', 'course_id, course_name');
		$courses = get_all_courses('1=1 AND (start_date > '.get_current_period(false).' > end_date ) ORDER BY course_name ASC', 'course_id, course_name');

		$courses[-1] = array('course_id' => -1, 'course_name' => ' Добави сега ');
		$courses_dropdown = ReturnDropDown($courses, $default_course);
		$courses_dropdown = '<option value="0"> - Изберете курс - </option>'.$courses_dropdown;

		database_close();
?>
<div class="container">
	<div class="row" id="HW_Manage">
		<div class="col-lg-12">
			<div class="panel-group" id="HW_accordion1" style="margin-top:20px;">
				<div class="panel panel-info">
					<div class="panel-heading panel-plus-link">
						<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#HW_accordion1" href="#HW_collapseOne1" aria-expanded="false">
							<b><h5 style="display:inline;">Управление на Домашни</h5></b>
						</a>
					</div>
					<div id="HW_collapseOne1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
						<div class="panel-body">
							<?php
							/*
								database_open();
								$homeworks = get_all_homeworks('ch.course_id IN (SELECT course_id FROM courses WHERE start_date <= "'.$current_date.'" AND "'.$current_date.'" <= end_date) ORDER BY course_id, homework_title');

								foreach($homeworks as &$homework)
								{
									$homework['homework_url'] = '<a href="'.$homework['homework_url'].'">'.$homework['homework_url'].'</a>';
									$homework['for_office_only'] = '<img src="'.DIR_IMAGES.'ui/'.(int) $homework['for_office_only'].'.png" alt="'.$homework['for_office_only'].'" />';

									$homework = array_merge(array('<a href="'.link_to(array(CONTROLLER => 'homeworks', ACTION => 'add', ID => $homework['homework_id'])).'"><img src="'.DIR_IMAGES.'ui/application_form_edit.png" alt="" /></a>'), $homework);
								}

								render_table($homeworks, array('&nbsp;', 'HW_ID', 'Course', 'Title', 'URL', 'Type', 'Weight', 'Restricted'), 'class="table table-striped table-bordered table-condensed table-responsive"');
								database_close();
							 */
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<form id="HW_EditForm" method="post">
			<?php
				if(have_messages())
				{
					show_all_messages();
					clear_all_messages();
				}
			?>
			<div class="col-lg-<?php echo ($is_empty_layout ? 12 : 6); ?>">
				<div class="col-lg-12">
					<h1>Добавяне на домашна</h1>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-12">
					<label for="homework_title"> Заглавие </label>
					<input name="homework_title" id="homework_title" type="text" class="form-control" value="<?php echo $default_title; ?>" />
				</div>

				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-12">
					<label for="homework_url"> URL на файла със заданието</label> <a href="#" target="_blank"><span class="glyphicon glyphicon-upload"></span></a>
					<input name="homework_url" id="homework_url" type="text" class="form-control" value="<?php echo $default_url; ?>" />
				</div>
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-6">
					<label for="course_id">	Курс </label>
					<select name="course_id" id="course_id" class="form-control">
						<?php echo $courses_dropdown; ?>
					</select>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-6">
					<label for="is_final_exam">	Тип </label>
					<select name="is_final_exam" id="is_final_exam" class="form-control">
						<option value="0"<?php if($default_final_value == 0){ echo ' selected="selected"'; } ?>>Нормална домашна</option>
						<option value="1"<?php if($default_final_value == 1){ echo ' selected="selected"'; } ?>>Финален изпит</option>
					</select>
				</div>
			</div>
			<div class="col-lg-<?php echo ($is_empty_layout ? 12 : 6); ?>">
				<div class="col-lg-12">
					<h1>Задаване на видимост</h1>
				</div>

				<div id="visibility_div">
				<?php foreach($visibility_array as $k => $visibility){ ?>
					<span>
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-2">
							<label for="vgr<?php echo $k; ?>"> Група </label>
							<input name="visibility[<?php echo $k; ?>][group_number]" id="vgr<?php echo $k; ?>" type="number" class="form-control" value="<?php echo $visibility['group_number']; ?>" />
						</div>
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
							<label for="vsd<?php echo $k; ?>"> Начална дата </label>
							<input name="visibility[<?php echo $k; ?>][start_date]" id="vsd<?php echo $k; ?>" type="text" class="form-control" value="<?php echo $visibility['start_date']; ?>" />
						</div>
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4">
							<label for="ved<?php echo $k; ?>"> Крайна дата </label>
							<input name="visibility[<?php echo $k; ?>][end_date]" id="ved<?php echo $k; ?>" type="text" class="form-control" value="<?php echo $visibility['end_date']; ?>" />
						</div>
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-2">
							<label for="" style="visibility: hidden;"> . </label>
							<button type="button" class="form-control del_hw_visibility"><img src="<?php echo DIR_IMAGES; ?>ui/0.png"></button>
						</div>
					</span>
				<?php } ?>
					<span><!-- Не премахвайте този span, иначе добавянето на нови редове няма да работи --></span>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<label for="" style="visibility: hidden;"> . </label>
				<div style="text-align:center;">
				<?php
					echo '<a href="'.link_to(array(CONTROLLER => 'homeworks', ACTION => 'add')).'"><button type="button" style="width:71px;"><img src="'.DIR_IMAGES.'ui/add_page_16.gif" alt="" /> New</button></a>';
					echo '<a href="'.link_to(array(CONTROLLER => 'homeworks', ACTION => 'manage')).'"><button type="button"><img src="'.DIR_IMAGES.'ui/0.png"></button></a>';
					echo '<a href="#" onclick="document.getElementById(\'HW_EditForm\').reset();"><button type="button"><img src="'.DIR_IMAGES.'ui/r.png" height="22"></button></a>';
					echo '<a href="#" onclick="document.getElementById(\'HW_EditForm\').submit();"><button type="submit"><img src="'.DIR_IMAGES.'ui/1.png"></button></a>';
				?>
					<a href="#" id="add_hw_visibility"><button type="button">+</button></a>
				</div>
			</div>
			<div class="col-lg-12"><br /></div>
		</form>
	</div>
</div>
<script>
	var group_index = <?php echo max(array_column($visibility_array, 'group_number')) + 1; ?>;
	var   key_index = <?php echo max(array_keys($visibility_array)) + 1; ?>;

	$(document).ready(function(){
		$('#add_hw_visibility').click(function(){

			var code;

			code = '<span>';
			code = code + '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-2"><label for="vgr'+key_index+'"> Група </label><input name="visibility['+key_index+'][group_number]" id="vgr'+key_index+'" type="number" class="form-control" value="'+group_index+'" /></div>';
			code = code + '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4"><label for="vsd'+key_index+'"> Начална дата </label><input name="visibility['+key_index+'][start_date]" id="vsd'+key_index+'" type="text" class="form-control" value="<?php echo $default_start; ?>" /></div>';
			code = code + '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4"><label for="ved'+key_index+'"> Крайна дата </label><input name="visibility['+key_index+'][end_date]" id="ved'+key_index+'" type="text" class="form-control" value="<?php echo $default_end; ?>" /></div>';
			code = code + '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-2"><label for="" style="visibility: hidden;"> . </label><button type="button" class="form-control del_hw_visibility"><img src="<?php echo DIR_IMAGES; ?>ui/0.png"></button></div>';
			code = code + '</span>';

			$('#visibility_div span:last').before(code);

			group_index++;
			key_index++;

			return false;
		});

		// Тук трябва да е с ON а не просто с CLICK, за да работи този код и за новосъздадените динамично бутони
		$('#visibility_div').on('click','.del_hw_visibility', function() {
			$(this).parent().parent().remove();
		});

		$('#course_id').change(function(){
			if($('#course_id :selected').val() < 0)
			{
				var new_val = prompt('Добавете ИД на курс');

				$('#course_id').append('<option value="' + new_val + '"> Добавен: Курс #' + new_val + ' </option>');
				$('#course_id').val(new_val);
			}
		});
	});
</script>
<?php
	}
	else
	{
		page_not_access();
	}
?>