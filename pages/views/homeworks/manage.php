<div class="container">
<?php
	Global $is_admin_user, $source, $_connection, $courses_homeworks, $query_string;

	if(SHOW_DEBUG_INFO){
?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel-group" id="accordion1" style="margin-top:20px;">
				<div class="panel panel-<?php echo (count($courses_homeworks) != 0 ? 'info' : 'danger'); ?>">
					<div class="panel-heading panel-plus-link">
							<a class="accordion-toggle<?php echo (count($courses_homeworks) != 0 ? ' collapsed' : ''); ?>" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne1" aria-expanded="<?php echo (count($courses_homeworks) != 0 ? 'false' : 'true'); ?>">
								<b><h5 style="display:inline;">Управление на Домашни</h5><?php echo HelpButton(); ?></b>
							</a>
					</div>
					<div id="collapseOne1" class="panel-collapse collapse<?php echo (count($courses_homeworks) != 0 ? '' : ' in'); ?>" aria-expanded="<?php echo (count($courses_homeworks) != 0 ? 'false' : 'true'); ?>">
						<div class="panel-body">
							<?php echo SQL_DEBUG($query_string); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php } ?>
	<div class="row">
		<div class="col-lg-12">
<?php
	if($is_admin_user || is_assistant())
	{
		database_open();

		if(have_messages())
		{
			show_all_messages();
			clear_all_messages();
		}

		$search_params = remove_url_elements(array(CONTROLLER, ACTION), $_SERVER['QUERY_STRING']);
?>
			<form method="GET" action="<?php echo header_link(array(CONTROLLER => 'homeworks', ACTION => 'index')); ?>">
				<div class="form-group">
					<div class="row">

						<input type="hidden" name="<?php echo CONTROLLER; ?>" value="homeworks" />
						<input type="hidden" name="<?php echo ACTION; ?>" value="manage" />

						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-4">
							<label for="course_id">
								Курс
							</label>

							<select name="course_id" id="course_id" class="form-control">
								<option value="-1">- Без Филтър -</option>
								<?php

									$archive = '';
									$current = '';
									foreach(get_all_courses() as $course)
									{
										$course['end_date'] = strtotime($course['end_date']) + (0 * 7 * 24 * 3600); // курса се брои за активен до 2 седмици след неговия край ;-)
										$which_courses = ($course['start_date'] <= date('Y-m-d') && strtotime(date('Y-m-d')) <= $course['end_date'] ? 'current' : 'archive');
										$$which_courses .= '<option value="'.$course['course_id'].'"'.($source['course_id'] == $course['course_id'] ? ' selected="selected"' : '').'>'.$course['course_id'].'. '.$course['course_name'].'</option>';
										// Тук използваме $$ за да укажем, коя променлива искаме да променяме. Така избягваме 1 if, или пък двойното писане на един и същи код.
									}

									$default = '<option value="0"'.(0 == $source['course_id'] ? ' selected="selected"' : '').'>Всички активни курсове</option>';
									$archive = '<optgroup label="Архивни">'.$archive.'</optgroup>';
									$current = '<optgroup label="Текущи">'.$current.'</optgroup>';

									echo $default.$current.$archive;
								?>
							</select>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
							<label for="homework_type">
								Тип
							</label>

							<select name="homework_type" id="homework_type" class="form-control">
							<?php
								$type_options = array(
									'-' => ' - Без филтър - ',
									'current' => 'нормални',
									'final' => 'финални'
								);
								RenderDropDown($type_options, $source['homework_type']);
							?>
							</select>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
							<label for="for_office_only">
								Ограничена
							</label>

							<select name="for_office_only" id="for_office_only" class="form-control">
							<?php
								$restricted_options = array(
									'-1' => ' - Без филтър - ',
									'1' => 'Да',
									'0' => 'Не'
								);
								RenderDropDown($restricted_options, $source['for_office_only']);
							?>
							</select>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-2 col-lg-1">
							<label for="weight">
								Тежест
							</label>

							<input type="number" name="weight" id="weight" class="form-control" value="<?php echo $source['weight']; ?>" />
						</div>
						<input type="hidden" name="check_them" id="check_them" value="0" />
						<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
							<button type="submit" style="margin-top:30px; vertical-align: bottom;"><span class="glyphicon glyphicon-filter"></span> Филтрирай</button>
							<button type="submit" style="height:32px;" id="check_btn">
								<span title="Homeworks" class="glyphicon glyphicon-check"></span> Провери
							</button>
							<a href="<?php echo header_link( array(CONTROLLER => 'homeworks', ACTION => 'index') ).($search_params != '' ? '&'.$search_params : ''); ?>" title="Преглед на Домашни">
								<button type="button" class="btn-primary" style="vertical-align: bottom; float:right; margin-top:30px;">
									<span class="glyphicon glyphicon-transfer"></span>
								</button>
							</a>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
<?php
		//$emails = array();
		$homeworks = array();
		$distinct_users = array();

		$total_score = 0;
		$total_count = 0;

		//$query_users = to_assoc_array(exec_query($query_string));
		//pre_print($courses_homeworks);
		foreach( $courses_homeworks as $k => $ch )
		{
			// General Functionality
			$tmp = array();

			$tmp['homework_id'] = $ch['homework_id'];
			$tmp['course_id'] = $ch['course_name'].' ['.$ch['course_id'].']';
			$tmp['title'] = $ch['homework_title'];
			$tmp['type'] = str_ireplace(array('final', 'current'), array('<b>финална</b>', 'нормална'), $ch['homework_type']);
			$tmp['weight'] = ($ch['weight'] * 100).' %';
			$tmp['restrict'] = '<img src="'.DIR_IMAGES.'ui/'.(int) $ch['for_office_only'].'.png" alt="'.$ch['for_office_only'].'" title="For Office Only" style="width:16px;" />';
			$tmp['edit']  = '';

			$now = date('Y-m-d');
			$is_active_course = ($ch['start_date'] < $now && $now < $ch['end_date']);

			// HomeWork Download Button
			$hw_btn = '<img src="'.DIR_IMAGES.'ui/document_icon_h.png" alt="Изтегляне" style="height:16px;" />';
			$ch['homework_url'] = str_ireplace('/.', '/', $ch['homework_url']);
			$hw_local_url = str_ireplace( array('', ''), '', $ch['homework_url']);
			if(is_file($hw_local_url))
			{
				$hw_btn = '<a href="'.$ch['homework_url'].'" title="Свали условието"><button type="button">'.$hw_btn.'</button></a>';
			}
			else
			{
				$hw_btn = '<button type="button" style="opacity: 0.5;" onclick="alert(\'Файлът '.$hw_local_url.' не е намерен!\');">'.$hw_btn.'</button>';
			}

			// List Homeworks by Course | HW_ID Functionality
			$course_target_array = array( CONTROLLER => 'homeworks', ACTION => 'index', 'course_id' => $ch['course_id'], VIEW => ($is_active_course ? 'non_evaluated' : 'all') );
			$homework_target_array = array_merge($course_target_array, array( 'homework_id' => $ch['homework_id']));

			$list_by_homework = '<a href="'.header_link( $homework_target_array ).'"><button type="button"><span class="glyphicon glyphicon-list"></span></button></a>';
			$list_by_course = '<a href="'.header_link( $course_target_array ).'"><button type="button"><span class="glyphicon glyphicon-th-list"></span></button></a>';

			// HomeWork Edit Button
			$edit_btn = '<img src="'.DIR_IMAGES.'ui/application_form_edit.png" alt="Edit" />';

			if(true) // if($is_active_course)
			{
				$edit_btn = '<button type="button">'.$edit_btn.'</button>';
				$edit_btn = '<a href="'.header_link(array(CONTROLLER => 'homeworks', ACTION => 'add', ID => $ch['homework_id'])).'">'.$edit_btn.'</a>';
			}
			else
			{
				$edit_btn = '<button type="button" style="opacity: 0.5;" onclick="alert(\'Не можете да редактирате домашни свързани с приключил курс!\');">'.$edit_btn.'</button>';
			}

			$tmp['edit'] .= $edit_btn;
			$tmp['edit'] .= $hw_btn;
			$tmp['edit'] .= $list_by_course;
			$tmp['edit'] .= $list_by_homework;

			$homeworks[] = $tmp;
		}

		//pre_print($users);
		database_close();

		$columns = array(
			array('text' => ($is_admin_user ? '<a href="'.link_to(array(CONTROLLER => 'homeworks', ACTION => 'add')).'"><button type="button"><img src="'.DIR_IMAGES.'ui/add_page_16.gif" alt="New" /></button></a>' :  '&nbsp;'), 'attr' => 'width="10"' ),
			array('text' => 'Заглавие', 'field' => 'ch.homework_title', 'key' => 'title'),
			array('text' => 'Курс', 'field' => 'c.course_name', 'key' => 'course_id'),
			array('text' => 'Тип', 'field' => 'ch.homework_type', 'key' => 'type'),
			array('text' => 'Тежест', 'field' => 'ch.weight', 'key' => 'weight'),
			array('text' => '<img src="'.DIR_IMAGES.'ui/lock.png" alt="unlock" />', 'field' => 'ch.for_office_only', 'key' => 'restrict', 'attr' => 'width="20"'),
			array('text' => 'Операции', 'field' => '#', 'key' => 'edit', 'attr' => 'width="135"')
		);

		$table_classes = 'class="table table-striped table-bordered table-condensed table-responsive"';
		//$filter_columns = array('course_id', 'user_id', 'homework_id', 'score');

		//echo '<a class="fake_tab'.(isset($source[VIEW]) && $source[VIEW] == 'all' ? ' fake_tab_active' : '').'" href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element(VIEW, $_SERVER['QUERY_STRING']).'&' : '').VIEW.'=all"><img src="'.DIR_IMAGES.'ui/preferences_contact_list.png" /> Всички</a>';
		//echo '<a class="fake_tab'.(isset($source[VIEW]) && $source[VIEW] == 'non_evaluated' ? ' fake_tab_active' : '').'" href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element(VIEW, $_SERVER['QUERY_STRING']).'&' : '').VIEW.'=non_evaluated"><img src="'.DIR_IMAGES.'ui/0.png" height="16" /> Неоценени</a>';


		/*
		foreach($filter_columns as $filter)
		{
			if(isset($source[$filter]) && $source[$filter] != '')
			{
				echo '<a class="fake_tab fake_tab_remove" title="Кликнете тук за да премахнете филтъра!" href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element($filter, $_SERVER['QUERY_STRING']).'&' : '').'">Филтър: '.$filter.' = '.(is_array($source[$filter]) ? implode(',', $source[$filter]) : $source[$filter]).'</a>';
			}
		}
		*/

		render_table($homeworks, $columns, $table_classes);

		echo '<b title="'.$total_score.' / '.$total_count.'">Средно: '.($total_count == 0 ? 0 : round($total_score / $total_count, 2)).'%</b>';
		echo '<b title="'.implode(', ', $distinct_users).'" class="pull-right">Оценени от '.count($distinct_users).' различни потребителя</b>';
		//$emails = implode(', ', $emails);
		//echo '<textarea cols="96" style="margin-top:10px; border:5px ridge lightblue;">'.$emails.'</textarea>';
	}
	else
	{
		page_not_access();
	}
?>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready( function () {
		$('#check_btn').click(function(){
			//$('#check_them').val(1);

			$('#course_id').val(-1);
			$('#for_office_only').val(0);
			$('#homework_type').val('final');
			$('#weight').prop('disabled', true);
		});
	});
</script>
