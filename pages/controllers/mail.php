<?php
	// Mail Controller
	check_login();

	function action_mail_index()
	{
		Global $source, $_connection, $is_admin_user, $user_id;

		database_open();

		$columns = array(
			array('text' => 'ID'),
			array('text' => 'Относно'),
			array('text' => 'Получател'),
		);

		$query_string = 'SELECT m.mail_id, m.mail_subject, u.first_name, u.last_name, u.phone, u.user_id, m.created_on, mr.read_on, mr.send_on FROM mail_recipients AS mr LEFT JOIN mails AS m ON mr.mail_id = m.mail_id LEFT JOIN users AS u ON mr.user_id =  u.user_id WHERE 1=1';

		if(!$is_admin_user) // Обикновените потребители задължително виждат само изпратените от тях съобщения!
		{
			$_REQUEST['created_from'] = $user_id;
			$_REQUEST['mail_status'] = 'sent_by_me';
		}

		if(isset($_REQUEST['mail_status']) && $_REQUEST['mail_status'] != -1)
		{
			$_REQUEST['mail_status'] = strtolower($_REQUEST['mail_status']);
			switch($_REQUEST['mail_status'])
			{
				case 'read'			: $query_string .= ' AND mr.read_status = 1'; $columns[] = 'Прочетено'; break;
				case 'sent'			: $query_string .= ' AND mr.send_status = 1'; $columns[] = 'Изпратено'; break;
				case 'non_read'		: $query_string .= ' AND mr.read_status = 0'; $columns[] = 'Телефон'  ; break;
				case 'non_sent'		: $query_string .= ' AND mr.send_status = 0'; $columns[] = 'Създадено'; break;
				case 'sent_by_me'	:					  $columns[] = 'Телефон'; $columns[] = 'Създадено'; break;
			}
			// GROUP BY mr.user_id
		}
		else
		{
			$columns[] = 'Телефон';
			$columns[] = 'Създадено';
			$columns[] = 'Прочетено';
			$columns[] = 'Изпратено';
		}

		if(isset($_REQUEST['created_from']))
		{
			$query_string .= ' AND m.created_from = '.(int) $_REQUEST['created_from'];
		}

		if(isset($_REQUEST['full_name']) && trim($_REQUEST['full_name']) != '')
		{
			$query_string .= ' AND '.like_trans(array('u.first_name', 'u.last_name', 'u.user_email' ), $_REQUEST['full_name']);
		}

		if(isset($_REQUEST['subject']) && trim($_REQUEST['subject']) != '')
		{
			$query_string .= ' AND '.like_trans(array('m.mail_subject'), $_REQUEST['subject']);
		}

		if(!isset($_REQUEST[ORDER_BY]))
		{
			$_REQUEST[ORDER_BY] = 'u.first_name, u.last_name';
		}

		if(isset($_REQUEST[ORDER_BY]))
		{
			//$query_string .= ' ORDER BY '.$_REQUEST[ORDER_BY].' LIMIT 10';
			$query_string .= ' ORDER BY '.$_REQUEST[ORDER_BY];
		}

		//echo $query_string;

		$mails = to_assoc_array(exec_query($query_string));
		database_close();

		foreach($mails as $k => $v)
		{
			$mails[$k]['mail_subject'] = '<a href="'.header_link(array(CONTROLLER => 'mail', ACTION => 'read', ID => $v['mail_id'])).'" target="_blank">'.$mails[$k]['mail_subject'].'</a>';
			$mails[$k]['first_name'] = '<a href="'.header_link(array(CONTROLLER => 'user', ACTION => 'view', ID => $v['user_id'])).'" target="_blank">'.($v['first_name'] != '' || $v['last_name'] != '' ? $v['first_name'].' '.$v['last_name'] : $v['user_id']).'</a>';
			$mails[$k]['aim'] = my_phone_format($v['aim']);
			$mails[$k]['read_on'] = date('Y-m-d H:i:s', $v['read_on']);
			$mails[$k]['send_on'] = date('Y-m-d H:i:s', $v['send_on']);
			$mails[$k]['created_on'] = date('Y-m-d H:i:s', $v['created_on']);

			unset($mails[$k]['last_name']);
			unset($mails[$k]['user_id']);

			if('read' == $_REQUEST['mail_status'])
			{
				unset($mails[$k]['aim']);
				unset($mails[$k]['created_on']);
				unset($mails[$k]['send_on']);
			}

			if('non_read' == $_REQUEST['mail_status'])
			{
				unset($mails[$k]['created_on']);
				unset($mails[$k]['read_on']);
				unset($mails[$k]['send_on']);
			}

			if('sent' == $_REQUEST['mail_status'])
			{
				unset($mails[$k]['aim']);
				unset($mails[$k]['created_on']);
				unset($mails[$k]['read_on']);
			}

			if('non_sent' == $_REQUEST['mail_status'])
			{
				unset($mails[$k]['aim']);
				unset($mails[$k]['read_on']);
				unset($mails[$k]['send_on']);
			}

			if('sent_by_me' == $_REQUEST['mail_status'])
			{
				//unset($mails[$k]['aim']);
				unset($mails[$k]['read_on']);
				unset($mails[$k]['send_on']);
			}
		}

		//pre_print($mails,1);
		set('mails', $mails);
		set('columns', $columns);

		return $mails;
	}

	function action_mail_read()
	{
		Global $is_admin_user, $user_id, $source;

		$mail_id = $source[ID];

		if(isset($source['user_id']) && $is_admin_user)
		{
			$load_user_id = $source['user_id'];
		}
		else
		{
			$load_user_id = $user_id;
		}

		if(isset($source[ID]) && is_numeric($source[ID]))
		{
			database_open();

			$mail = getMailData($source[ID]);
			if(Only_One_Exists($mail))
			{
				$mail = reset($mail);
				//pre_print($mail);

				// Важат същите правила, които са и при преоценяване на домашни. Старши администраторите могат да четат мейлите на младоците, но не и обратното!
				$recipient_allow = array_search($load_user_id, array_column($mail['recipients'], 'user_id'));
				$admin_allow = ($is_admin_user && ($user_id <= $mail['created_from'] || $mail['created_from'] == 0)); // тук наистина трябва да бъде $user_id, а не $load_user_id !!!
				$is_pinned = $mail['is_pinned'];
				$is_author = ($user_id == $mail['created_from']);

				if($recipient_allow !== false || $admin_allow || $is_pinned || $is_author)
				{
					$time_now = strtotime('now') + TIME_CORRECTION;
					$is_deleted = $mail['recipients'][$recipient_allow]['is_deleted'];

					if($is_pinned && $recipient_allow === false )
					{
						// Добавяме потребителя към списъка с получатели, за да можем да маркираме, че той е прочел съобщението
						insert_query('mail_recipients', array('read_status' => '1', 'read_on' => $time_now, 'mail_id' => (int) $source[ID], 'user_id' => (int) $user_id));
					}

					if(!$is_deleted)
					{
						update_query('mail_recipients', array('read_status' => '1', 'read_on' => $time_now), 'mail_id = '.(int) $source[ID].' AND user_id = '.(int) $user_id.' AND read_status = 0'); // Тук също трябва да е $user_id
						set('mail', $mail);
						if(isset($source[FILTER_KEY]))
						{
							return $mail;
						}
					}
					else
					{
						add_error_message('Този е-мейл е изтрит от вас!');
					}
				}
				else
				{
					add_error_message('Този е-мейл не е изпратен до вас!');
				}
			}
			else
			{
				add_error_message('Такъв е-мейл не съществува!');
			}

			database_close();
		}
		else
		{
			add_error_message('Не сте указали кой е-мейл искате да достъпите!');
		}

		set('load_user_id', $load_user_id);
	}

	function action_mail_create()
	{
		Global $user_id, $is_admin_user, $source, $admins_array;

		if(!empty($_POST) && isset($_POST['submit'])) // Mail is sent...
		{
			//pre_print($_POST, true);

			if(!isset($_POST['mode']))
			{
				$_POST['mode'] = 'mail';
			}

			if($_POST['mode'] == 'alert')
			{
				if(is_numeric($_POST['alert_from']) && $_POST['alert_from'] > 0)
				{
					$_POST['about'] = 'Потребител '.$_POST['alert_from'].': '.$_POST['about'];
					$_POST['send_to'] = array(1 => '');
				}
				else
				{
					$_POST['about'] = $_POST['alert_from'].': '.$_POST['about'];

					switch($_POST['alert_from'])
					{
						case 'Лекции': $_POST['send_to'] = array(583 => ''); break; // Лекциите отиват при Кремена Желева
						case 'Видео': $_POST['send_to'] = array(9 => ''); break; // Видео въпроси отиват при Чанита Попова

						default: // Всичко друго отива при Йордан Енев, user_id = 1
							$_POST['send_to'] = array(1 => '');
						break;
					}
				}
			}

			database_open();
			$is_pinned = ($is_admin_user ? (int) $_REQUEST['is_pinned'] : 0);
			$required_fields = array('about', 'message');
			check_required_fields($required_fields);

			if(!$is_admin_user)
			{
				// Проверка за неправомерно използване на шаблони!
				$templates = get_all_templates();
				$templates = array_column($templates, 'template_content');
				$use_template = false;

				foreach($templates as $template)
				{
					if(stripos($_POST['message'], $template) !== false)
					{
						$use_template = true;
					}
				}

				if($use_template)
				{
					$_POST['message'] = str_ireplace($templates, '', $_POST['message']);
					add_warning_message('Не Ви е позволено да използвате шаблони!');
				}

				

			// Проверка за получатели
			if(!isset($_POST['send_to']) || empty($_POST['send_to']))
			{
				if(!$is_pinned)
				{
					add_error_message('Нямате нито 1 валиден получател!');
				}
			}


			if(!has_error_messages())
			{
				$data_mail = array(
					'mail_subject' => $_POST['about'],
					'mail_content' => $_POST['message'],
					'created_from' => $user_id,
					'created_on' => strtotime('now') + TIME_CORRECTION,
					'is_pinned' => $is_pinned
				);

				$mail_id = insert_query('mails', $data_mail);
				if($mail_id)
				{
					if(count($_POST['send_to']))
					{
						foreach($_POST['send_to'] as $key => $value)
						{
							$data_array = array(
								'mail_id' => $mail_id,
								'user_id' => ($key < 0 ? abs($key) : 0),
								'email' => $value
							);

							insert_query('mail_recipients', $data_array);

						}
					}

					add_success_message( translate('MSG_MESSAGE_SENT_COMPLETE') );
					header('location:'.$_SERVER['HTTP_REFERER']);
					exit;
				}
				else
				{
					add_error_message( translate('MSG_MESSAGE_NOT_SENT') );
				}
			}


			database_close();

			//add_success_message('Съобщението е изпратено успешно!');
			//header('location:'.header_link(array(CONTROLLER => 'mail', ACTION => 'index')));
			exit;
		}
	}

	function action_mail_sent()
	{
		define_once('MAX_SENT_MAILS', 25);

		database_open();
		$not_sent_mails = to_assoc_array(exec_query('SELECT * FROM mail_recipients AS mr LEFT JOIN mails AS m ON mr.mail_id = m.mail_id WHERE mr.send_status = 0 ORDER BY mr.id'));
		if(count($not_sent_mails) > 0)
		{
		}
		else
		{
			exit;
		}
		database_close();
	}

	function action_mail_delete()
	{
		Global $user_id;

		//pre_print($_POST);

		if(isset($_POST['del_chkbox']) && count($_POST['del_chkbox']) > 0)
		{
			database_open();
			$delete_mails = exec_query('UPDATE mail_recipients SET is_deleted = 1 WHERE user_id = '.$user_id.' AND id IN ('.implode(', ', $_POST['del_chkbox']).')');
			database_close();
		}
	}

	function action_mail_show()
	{
		Global $is_admin_user, $user_id, $source;

		if(isset($source[ID]) && $is_admin_user)
		{
			$load_user_id = $source[ID];
		}
		else
		{
			$load_user_id = $user_id;
		}

		database_open();
		$mail_notifications = getMailForUser($load_user_id);
		database_close();

		set('mail_notifications', $mail_notifications);
		set('load_user_id', $load_user_id);

		return $mail_notifications;
	}

	function action_mail_restore()
	{
		Global $user_id;

		database_open();
		$restore = exec_query('UPDATE mail_recipients SET is_deleted = 0 WHERE user_id = '.$user_id.' AND id = '. (int) $_GET['mr_id']);
		database_close();

		if($restore)
		{
			echo 'success';
		}
		else
		{
			echo 'error';
		}

		exit;
	}

	function action_mail_csv()
	{
		Global $except_users;

		$query = 'SELECT user_id, first_name, last_name, user_email, aim AS phone FROM users AS u WHERE user_id NOT IN ('.implode(', ', $except_users).') AND user_id IN (SELECT DISTINCT user_id FROM course_members)';
		//echo $query;

		database_open();
		$data_array = exec_query($query);
		database_close();
/*
		$tmp = '';
		foreach($data_array as $data)
		{
			$tmp .= implode(',', $data).';';
		}

		file_put_contents('export_'.date('Y-m-d_H-i-s').'.csv', $tmp);
*/
		$fp = fopen(LOCAL_PATH.'exports/csv_'.date('Y-m-d_H-i-s').'.csv', 'w');

		//add BOM to fix UTF-8 in Excel
		fputs($fp, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
		foreach ($data_array as $fields) {
			$fields['first_name'] = transliterate($fields['first_name']);
			$fields['last_name'] = transliterate($fields['last_name']);
			fputcsv($fp, $fields);
		}

		fclose($fp);
	}

	function action_mail_api()
	{
		Global $source;

		if(isset($source[FILTER_KEY]))
		{
			if($source[FILTER_KEY] == md5('qa'))
			{
				if(isset($source[VIEW]))
				{
					$function_name = 'action_mail_'.$source[VIEW];
					if(function_exists($function_name))
					{
						$function_response = $function_name();

						foreach($function_response as $key => $value)
						{
							if(is_array($value))
							{
								foreach($value as $k => $v){

									$v = mb_convert_encoding($v, 'UTF-8', 'UTF-8');
									$value[$k] = strip_tags($v);
									$function_response[$key][$k] = $value[$k];
								}
							}
							else
							{
								$value = mb_convert_encoding($value, 'UTF-8', 'UTF-8');
								$value = strip_tags($value);
								$function_response[$key] = $value;
							}
							//$function_response[$key] = array_values($value);
						}

						//pre_print($function_response,15);
						echo json_encode($function_response, JSON_UNESCAPED_UNICODE);
						//exit;
					}
					else
					{
						add_error_message('View '.$source[VIEW].' NOT exists!');
					}
				}
				else
				{
					add_error_message('Missing GET param '.VIEW.'!');
				}
			}
			else
			{
				add_error_message('GET param '.FILTER_KEY.' have wrong value! It must be: '.md5('qa'));
			}
		}
		else
		{
			add_error_message('Missing GET param '.FILTER_KEY.'!');
		}

		if(have_messages())
		{
			if($source['layout'] == 'empty')
			{
				echo json_encode($_SESSION[_APP_][_CLASS_MESSENGER], JSON_UNESCAPED_UNICODE);
			}
			else
			{
				echo '<div class="container"><div class="row"><div class="col-lg-12">';
				show_all_messages();
				echo '</div></div></div>';
			}

			clear_all_messages();
		}
	}
?>