<?php
	// Course Controller
	function action_course_index()
	{
		$ALLOWED_ORDERS = [
			''				=> translate('DEFAULT_SELECT_LABEL'),
			'course_price'	=> 'Цена',
			//'category_id'	=> 'Категория',
			'full_name'		=> 'Име на лектор',
			'start_date'	=> 'Дата на започване'
		];

		// Да добавя MAX(course_id), DISTINCT, GROUP BY ancestor_id и т.н. Идеята е да изкарам последната инстанцция за всички курсове, които някога ги е имало.
		$query_string = 'is_visible = 1'; // 'NOW() BETWEEN candidate_from AND candidate_to';
		if(isset($_GET['teachers']))
		{
			if(!is_array($_GET['teachers']))
			{
				$_GET['teachers'] = explode(',', $_GET['teachers']);
				foreach($_GET['teachers'] as $k => $v)
				{
					if($v == '')
					{
						unset($_GET['teachers'][$k]);
					}
				}
			}

			$query_string .= ' AND teacher_id IN ('.implode(',',$_GET['teachers']).')';
		}

		if(isset($_GET['price']) && $_GET['price'] >= 0)
		{
			$query_string .= ' AND course_price '.(0 == $_GET['price'] ? '=' : '>').' 0';
		}
		else
		{
			$_GET['price'] = -1;
		}

		if(isset($_GET['order']) && is_string($_GET['order']) && !empty($_GET['order']) )
		{
			$_GET['order'] = strtolower($_GET['order']);

			if(in_array($_GET['order'], array_keys($ALLOWED_ORDERS)))
			{
				$query_string .= ' ORDER BY '.$_GET['order'];
			}
			// else ... value is not allowed, so we ignore order clause
		}

		database_open();
		$courses = get_all_courses($query_string); // NOW() BETWEEN candidate_from AND candidate_to
		database_close();

		set('courses', $courses);
		set('ALLOWED_ORDERS', $ALLOWED_ORDERS);
	}

	function action_course_view()
	{
		define_once('_APP_', 'YBE_SYSTEM');
		define_once('_CLASS_MODALS', 'MODALS');

		Global $source;
		$course_id = $source[ID];

		if(!isset($course_id))
		{
			header('location:'. site_url('courses/'));
			exit;
		}
		$_SESSION['FIX_FOOTER'] = false;
		database_open();
		$course_data = getCourseData($course_id);

		if(!Only_One_Exists($course_data))
		{
			add_error_message( translate('MSG_COURSE_NOT_EXISTS') );
			header('location:'. site_url('courses/'));
			exit;
		}
		else
		{
			$course_data = reset($course_data);
		}

		if(!$course_data['is_visible'])
		{
			add_error_message( translate('MSG_COURSE_NOT_EXISTS') ); // Тук да добавя и обмисля възможности като MSG_COURSE_NOT_VISIBLE, MSG_COURSE_NOT_PUBLIC, MSG_COURSE_IS_PRIVATE и други
			header('location:'. site_url('courses/'));
			exit;
		}

		database_close();

		set('course_data', $course_data);
	}

	function action_course_manage()
	{
		Global $source, $user_id, $is_admin_user, $_connection;

		if(!$is_admin_user)
		{
			echo 'You are not authorized!';
			exit;
		}

		if(!isset($source[VIEW]) || $source[VIEW] == 'all')
		{
			$view = 'all';
		}
		else
		{
			$view = strtolower($source[VIEW]);
		}

		database_open();

		$query = 'SELECT c.*, u.first_name, u.last_name, CONCAT(u.first_name, " ", u.last_name) as teacher_name FROM courses AS c LEFT JOIN users AS u ON c.teacher_id = u.user_id';
		$query_string = '1=1';

		switch($view)
		{
			case 'all':  // Показваме всички курсове
				$table_title = 'Всички курсове';
				$last_column = 'Действия';
				break;

			case 'main': // Показва само курсовете, на които курсиста е член.
				$table_title = 'Моите активни курсове';
				$last_column = 'Бързи връзки';
				$query_string .= ' AND course_id IN (SELECT course_id FROM course_members WHERE user_id = '.$user_id.' AND role="student")';
				break;

			case 'candidate': // Показва курсовете, по които курсиста е кандидатствал.
				$table_title = 'Моите текущи кандидатури';
				$last_column = 'Бал';
				$query = 'SELECT c.*, cm.score FROM courses AS c LEFT JOIN course_members AS cm ON c.course_id = cm.course_id';
				$query_string .= ' AND cm.user_id = '.$user_id.' AND cm.role="candidate"';
				break;

			default: // Пренасочваме към показване на всички курсове
				header('location: ?'.remove_url_element(VIEW, link_to(array('OPTION_SET_CLASS' => false))).'&'.VIEW.'=all');
			break;
		}

		$columns = array('', array('text' => $table_title, 'attr' => ' width="300"'), 'Хорариум', 'Кандидатствай до', $last_column);

		if(isset($source['ancestor_id']) && $source['ancestor_id'] >= 0)
		{
			$query_string .= ' AND c.descendant_of = '.(int) $source['ancestor_id'];
		}

		if(isset($_GET['teacher_name']) && trim($_GET['teacher_name']) != '')
		{
			$query_string .= ' AND '.like_trans(array('u.first_name', 'u.last_name'), $_GET['teacher_name']);
		}

		if(!isset($source['course_id']))
		{
			$source['course_id'] = -1; // Задаваме стойност по подразбиране да бъде "Без филтър"
		}

		if($source['course_id'] >= 0)
		{
			$courses_filter = $source['course_id'];
			if(0 == $courses_filter)
			{
				$courses_filter = array_column(getCurrentCourses(),'course_id');
			}

			if(is_numeric($courses_filter))
			{
				$query_string .= ' AND c.course_id = '.(int) $courses_filter;
			}

			if(is_array($courses_filter))
			{
				$query_string .= ' AND c.course_id IN ('.implode(',', $courses_filter).')';
			}
		}


		if(isset($source[ORDER_BY]) && $source[ORDER_BY] != '')
		{
			$query_string .= ' ORDER BY '.mysqli_real_escape_string($_connection, $source[ORDER_BY]);
		}
		else
		{
			$query_string .= ' ORDER BY c.course_id DESC';
		}

		$query = $query.' '.($query_string != '' ? 'WHERE '.$query_string : '');
		//pre_print($query);

		$courses = to_assoc_array(exec_query($query));
		$query_string = last_query();

		//Pre_Print($courses);
		database_close();
		set('courses', $courses);
		set('query_string', $query_string);
		set('columns', $columns);
	}

	function action_course_required()
	{
		Global $source, $user_id, $is_admin_user;

		if(!$is_admin_user)
		{
			echo 'You are not authorized!';
			exit;
		}

		if(!isset($source[ID]))
		{
			header('location:'.link_to(array(CONTROLLER => 'course', ACTION => 'index', 'OPTION_SET_CLASS' => false)));
			exit;
		}

		$course_id = $source[ID];

		database_open();
		$required_tests = getRequiredTestsPerCourse($course_id);
		database_close();

		set('required_tests', $required_tests);
	}

	function action_course_AddToUser()
	{
		//upload homework
		Global $source, $user_id, $_connection, $is_admin_user;

		check_login();
		database_open();

		if(!isset($_REQUEST[ID]))
		{
			header('location:'.link_to(array(CONTROLLER => 'course', ACTION => 'index', 'OPTION_SET_CLASS' => false)));
			exit;
		}

		if(is_numeric($_REQUEST[ID]))
		{
			$where_clause = 'course_id = "'.mysqli_real_escape_string($_connection, (int) $_REQUEST[ID] ).'"';
		}
		elseif(is_string($_REQUEST[ID]))
		{
			$where_clause = 'course_url = "'.mysqli_real_escape_string($_connection, $_REQUEST[ID] ).'" ORDER BY course_id DESC LIMIT 1';
		}

		$courses = to_assoc_array(exec_query('SELECT * FROM courses AS c LEFT JOIN courses_ancestors AS ca ON c.descendant_of = ca.ancestor_id WHERE '.$where_clause));

		if(count($courses) == 1)
		{
			$course = reset($courses);
			$course_id = $course['course_id'];
		}
		else
		{
			add_unknown_error(__LINE__, __FILE__); // Wrong Course ID, so this course not exists!
			header('location:'.link_to(array(CONTROLLER => 'course', ACTION => 'index', 'OPTION_SET_CLASS' => false)));
			exit;
		}

		if(isset($_REQUEST['to_user_id']) && $is_admin_user)
		{
			$to_user_id = $_REQUEST['to_user_id'];
		}
		else
		{
			$to_user_id = $user_id;
		}


		$role = 'candidate';

		if(!isset($course_id)) 
		{
			add_unknown_error(__LINE__, __FILE__);
		}
		else
		{
			// Записваме потребителя като кандидат за курса.
			if(!has_error_messages())
			{
				$is_already_added = to_assoc_array(exec_query('SELECT * FROM course_members WHERE course_id = '.$course_id.' AND user_id = '.$to_user_id));
				if(count($is_already_added) > 0)
				{
					delete_query('course_members', 'course_id = '.$course_id.' AND user_id = '.$to_user_id); // Да внимавам с този ред, да не би някой, който си е платил, да се самоизтрие!!!
				}
				//else
				//{
				//}

				$insert_data = array('course_id' => $course_id, 'user_id' => $to_user_id, 'role' => $role, 'created_on' => date('Y-m-d H:i:s') + TIME_CORRECTION, 'created_from' => $user_id);
				if(isset($_REQUEST['form_id'])) // isset($form_id)
				{
					$insert_data['form_id'] = $_REQUEST['form_id']; //$form_id;
				}

				if(isset($_REQUEST['group']))
				{
					$insert_data['group_number'] = $_REQUEST['group'];
				}
				else
				{
					if($insert_data['form_id'] ==  1)
					{
						$insert_data['group_number'] = 1;
					}
					else // Online
					{
						$insert_data['group_number'] = 0;
					}
				}

				add_success_message('Успешно си резервирахте място за курса, но за да потвърдите мястото си, е нужно да го заплатите!');
				$required_tests = insert_query('course_members', $insert_data);

				insert_mail(['mail_subject' => 'Остава да платите...', 'mail_content'  => 'Запазихме ви място в курса '.$_REQUEST['to_course_name'].', но трябва да платите за него '.$_REQUEST['to_course_price'].' лв. - вижте как от бутона в червеното каре'], ['-'.$to_user_id => $to_user_id]);
				insert_mail(['mail_subject' => 'Ново записване за курс '.$course_id, 'mail_content'  => 'Потребител <a href="'.header_link([CONTROLLER => 'user', ACTION => 'view', ID => $to_user_id]).'" target="_blank">#'.$_SESSION[CURRENT_USER]['user_id'].' - '.$_SESSION[CURRENT_USER]['first_name'].' '.$_SESSION[CURRENT_USER]['last_name'].'</a>, си запази място в курс №'.$course_id.' - '.$_REQUEST['to_course_name'].' на стойност '.$_REQUEST['to_course_price'].' лв.'], ['-1' => 'dakaenev@gmail.com']);
//				if(isset($source['time']) && isset($source['group']) && $source['group'] > 0) //$source['group'] - Това е избраният град.
//				{
//					$answer = $source['group'] + $source['time'];
//					exec_query('UPDATE anketi SET option_value = option_value + 1 WHERE option_id = '.$answer);
//				}
			}
			database_close();
			header('location:'. header_link( array(CONTROLLER => 'user', ACTION => 'view') ) );
			exit;
			//header('location:'.site_url('/profile-2/').(isset($source['to_user_id']) && $is_admin_user ? '?user_id='.$source['to_user_id'] : ''));
		}
	}

	function action_course_RemoveFromUser()
	{
		Global $user_id;

		check_login();
		database_open();

		$course_id = (int) $_REQUEST['course_id'];
		$reason = escape_string($_REQUEST['why']);

		if(!has_error_messages())
		{
			$is_already_added = to_assoc_array(exec_query('SELECT * FROM course_members WHERE course_id = '.$course_id));

			if(count($is_already_added) > 0)
			{
				insert_mail(['mail_subject' => 'Отказвам се от курс №'.$course_id, 'mail_content' => 'Причина:'.$reason], ['-1' => '-1']);
				delete_query('course_members', 'course_id = '.$course_id.' AND user_id = '.$user_id);
				add_warning_message('Съжаляваме, че се отказахте от курса! <br>Надяваме се да размислите и да се видим отново!');
			}
			else
			{
				add_unknown_error(__LINE__, __FILE__);
			}
		}

		database_close();
		header('location:'. header_link( array(CONTROLLER => 'user', ACTION => 'view') ) );
		exit;
	}

	function action_course_RefreshScores()
	{
		Global $DataBase, $source, $user_id, $is_admin_user;

		if(!$is_admin_user)
		{
			echo 'You are not authorized!';
			exit;
		}

		if(!isset($source['course_id']))
		{
			echo 'Course_ID is required!';
			exit;
		}

		if(!isset($user_id))
		{
			echo 'User_ID is required!';
			exit;
		}

		$new_final_score = 0;

		database_open();
		$candidates_array = getCourseCandidates($source['course_id']);
		//Pre_Print($candidates_array);
		foreach($candidates_array as $candidate)
		{
			refreshUserScore($source['course_id'], $candidate['user_id']);
		}

		database_close();
	}

	function action_course_PreviewFinal()
	{
		Global $is_admin_user, $source, $is_super_admin;
		$HALL_PLACES = 30;

		if($is_admin_user)
		{
			if(isset($source[ID]))
			{
				database_open();

				$course_id = (int) $source[ID];
				$course = get_all_courses('course_id = '.$course_id);

				if(Only_One_Exists($course))
				{
					$course = reset($course);
					pre_print($course);

					$live_vip_users = to_assoc_array(exec_query('SELECT COUNT(*) AS users_count FROM course_members WHERE course_id = '.$course_id.' AND form_id = 1 AND role="student"'));
					if(count($live_vip_users) == 1)
					{
						$live_vip_users = reset($live_vip_users);
						$live_vip_users = $live_vip_users['users_count'];
					}
					else
					{
						$live_vip_users = -1;
					}

					if(!empty($_POST))
					{
						if($is_super_admin)
						{
							if(isset($_POST['candidates']) && is_array($_POST['candidates']))
							{
								if(date('Y-m-d H:i:s', strtotime('now') + TIME_CORRECTION) > $course['candidate_until'])
								{
									foreach($_POST['candidates'] as $who => $records_array)
									{
										switch(strtolower($who))
										{
											case 'onlive': $fields = 'role="student", form_id = 1, group_number = 1'; break;
											case 'online': $fields = 'role="student", form_id = 2, group_number = 0'; break;
											case 'reject': $fields = 'role="rejected"';								  break;

											default: add_error_message('Непознат ключ "'.$who.'" в $_POST["candidates"]');
										}

										$sql = 'UPDATE course_members SET '.$fields.' WHERE course_id = '.(int) $course_id.' AND role="candidate" AND id IN ('.implode(', ', $records_array).')';
										echo $sql.'<br>';

										backup_tables('course_members');
										exec_query($sql);
									}

									add_success_message('Курсистите са приети успешно!');
								}
								else
								{
									$msg  = 'Не можете да обявите класиране преди кандидатстването да е приключило!<br />';
									$msg .= 'Сега е '.date('Y-m-d H:i:s', strtotime('now') + TIME_CORRECTION).', а кандидатстването за <a href="'.header_link(array(CONTROLLER => 'course', ACTION => 'add', ID => $course_id)).'">курса</a> е разрешено до '.$course['candidate_until'];
									add_error_message($msg);
								}
							}
							else
							{
								add_error_message('Нищо не е изпратено!');
							}
						}
						else
						{
							add_error_message('Приемът на курсисти е разрешен само за Супер Администратор!');
						}
					}
					else
					{
						//add_warning_message('За данни за текущото кандидатстване за всички курсове <a href="'.header_link( array(CONTROLLER => 'course', ACTION => 'howisgoing')).'">кликнете тук!</a>');
					}

					$payments = getVipUsersPerCourse($course_id);
					$candidates_array = getCourseCandidates($course_id,'cm.form_id ASC, history.score DESC');
					database_close();

					if(!isset($source[MOP_PARAM]))
					{
						// Щом изпратим параметър MOP_PARAM = 0, все едно изпращаме count($candidates_array)
						header('location:'.header_link(array(CONTROLLER => 'course', ACTION => 'PreviewFinal', ID => $course_id, MOP_PARAM => 0 ) ) );
						exit;
					}

					$i = 1;
					$candidates = array();
					//Pre_Print($candidates_array);
					//pre_print($payments, 1);

					foreach($candidates_array as $k => $v)
					{
						if(isset($v['twitter']) && $v['twitter'] != '')
						{
							$fb_link = '<a target="_blank" href="'.(mb_stripos($v['twitter'], 'http', 0, 'utf-8') === false ? 'https://facebook.com/'.$v['twitter'] : $v['twitter']).'" style="float: right;"><button type="button">FB</button></a>';
						}
						else
						{
							$fb_link = '';
						}

						$dollar = (isset($payments[$v['user_id']]) ? '<span class="glyphicon glyphicon-usd red pull-right"></span>' : '');

						//$candidates[$k]['checkbox'] = '<input type="checkbox" name="students[]" value="'.$v['id'].'"'.($i <= $MAX_CANDIDATES ? ' checked="checked"' : '').'>';
						$candidates[$k]['index'] = $i;
						$candidates[$k]['ID'] = '<a href="'.header_link( array(CONTROLLER => 'user', ACTION => 'edit', ID => $v['user_id']) ).'" title="Управление на потребителя" style="margin-left:3px;"><span class="glyphicon glyphicon-cog"></span></a>';
						$candidates[$k]['name'] = '&nbsp;<a href="'.header_link( array(CONTROLLER => 'user', ACTION => 'view', ID => $v['user_id']) ).'" title="Преглед на потребителя">'.$v['first_name'].' '.$v['last_name'].'</a>'.$fb_link;
						$candidates[$k]['mail'] = '&nbsp;'.$v['user_email'];
						$candidates[$k]['form_id'] = '<span class="red">Странна</span>';
						$candidates[$k]['score'] = round($v['score']).' <span id="info_span_'.$i.'" class="info_span" data-score="'.$v['score'].'" data-paid="'.('' != $dollar ? '1' : '0').'" data-group="'.$v['group_number'].'" data-form="'.$v['form_id'].'"></span>';

						$candidates[$k]['onlive'] = '<label class="pf_color_legend pf_color_legend_onlive"><input type="radio" class="my_radio row_'.$i.' column_1" id="radio_'.$i.'_1" data-row="'.$i.'" value="'.$v['id'].'" name="candidates[onlive]['.$v['id'].']" /></label>';
						$candidates[$k]['online'] = '<label class="pf_color_legend pf_color_legend_online"><input type="radio" class="my_radio row_'.$i.' column_2" id="radio_'.$i.'_2" data-row="'.$i.'" value="'.$v['id'].'" name="candidates[online]['.$v['id'].']" /></label>';
						$candidates[$k]['reject'] = '<label class="pf_color_legend pf_color_legend_reject"><input type="radio" class="my_radio row_'.$i.' column_3" id="radio_'.$i.'_3" data-row="'.$i.'" value="'.$v['id'].'" name="candidates[reject]['.$v['id'].']" />'.$dollar.'</label>';

						if(1 == $v['form_id'] && 1 == $v['group_number'])
						{
							$candidates[$k]['form_id'] = '<span class="green">Редовно</span>';
						}

						if(2 == $v['form_id'] && 0 == $v['group_number'])
						{
							$candidates[$k]['form_id'] = '<span style="color: blue;">Онлайн</span>';
						}

						$i++;
					}

					$columns = array(
						//array('text' => '<input type="checkbox">', 'attr' => 'width="20"'),
						array('text' => '№', 'attr' => 'width="20"'),
						array('text' => 'ID', 'attr' => 'width="20"'),
						array('text' => 'Име', 'attr' => ''),
						array('text' => 'E-mail', 'attr' => ''),
						array('text' => 'Обучение', 'attr' => 'width="50"'),
						array('text' => 'Бал', 'attr' => 'width="50"'),
						array('text' => 'Наживо <span class="info_span green" id="onlive_count"></span>', 'attr' => 'width="100"'),
						array('text' => 'Онлайн <span class="info_span green" id="online_count"></span>', 'attr' => 'width="100"'),
						array('text' => 'Неприети<span class="info_span green" id="reject_count"></span>', 'attr' => 'width="100"')
					);

					set('live_vip_users', $live_vip_users);
					set('HALL_PLACES', $HALL_PLACES);
					set('candidates', $candidates);
					set('columns', $columns);
					set('course', $course);
				}
				else
				{
					add_error_message('Такъв курс не съществува!<br /><a href="'.header_link(array(CONTROLLER => 'course', ACTION => 'index')).'">Връщане назад към списъка с курсове</a>');
					//header('location:'.header_link( array(CONTROLLER => 'course', ACTION => 'index') ) );
					//exit;
				}
			}
			else
			{
				//echo 'Course ID is required!';
				add_error_message('Не е зададен параметър за ID на курса!<br /><a href="'.header_link(array(CONTROLLER => 'course', ACTION => 'index')).'">Връщане назад към списъка с курсове</a>');
			}
		}
		else
		{
			echo 'Не сте оторизиран!';
			exit;
		}
	}

	function action_course_homework()
	{
		if(!$is_admin_user)
		{
			echo 'You are not authorized!';
			exit;
		}
	}

	function action_course_result() // Show Final Result
	{
		Global $source, $user_id, $is_admin_user, $html2pdf;

		if($is_admin_user)
		{
			if(!isset($source[ID]))
			{
				add_error_message('Course ID is required!<br /><a href="'.link_to(array(CONTROLLER => 'course', ACTION => 'index')).'">Go Back</a>');
			}
			else
			{
				$course_id = (int) $source[ID];
			}

			if(isset($course_id))
			{
				if(isset($_POST) && !empty($_POST))
				{
					if(count($_POST['user']) > 0)
					{
						database_open();

						$max_record = to_assoc_array(exec_query('SELECT id FROM user_certificates ORDER BY id DESC LIMIT 1'));
						if(count($max_record) == 0)
						{
							$new_record = 1;
						}
						else
						{
							$max_record = reset($max_record);
							$new_record = $max_record['id'] + 1;
						}

						$course_data = getCourseData($course_id);
						if(Only_One_Exists($course_data))
						{
							$course_data = reset($course_data);
							//pre_print($course_data,1);
						}

						$ancestor_data = get_all_ancestors('ancestor_id = '.$course_data['descendant_of']);
						if(Only_One_Exists($ancestor_data))
						{
							$ancestor_data = reset($ancestor_data);
							$ancestor_data['ancestor_tech_translated'] = '<table border="0" align="center" cellspacing="0"><tr><th>'.str_ireplace(' |##| ', '</th><th valign="middle" class="thp"><img src="'.DIR_IMAGES.'templates/cert_rect.png"></th><th>', $ancestor_data['ancestor_tech_translated']).'</th></tr></table>';
							//pre_print($ancestor_data,1);
						}

						$mail_recipients = array();
						$cert_template = getTemplateData(7, 'template_content');
						$users = to_assoc_array(exec_query('SELECT user_id, CONCAT(first_name, " ", last_name) AS full_name FROM users WHERE user_id IN ('.implode(',', $_POST['user']).')'), 'user_id');
						foreach($_POST['user'] as $key => $u_id)
						{
							$zero_prefix = '';

							if($new_record < 10000){ $zero_prefix .= '0'; }
							if($new_record <  1000){ $zero_prefix .= '0'; }
							if($new_record <   100){ $zero_prefix .= '0'; }
							if($new_record <    10){ $zero_prefix .= '0'; }

							$new_serial = 'PLD'.$zero_prefix.$new_record;
							$latin_name = transliterate($users[$u_id]['full_name']);
							$file_path = 'wp-content/uploads/certificates/'.$course_id.'/';

							if(!file_exists($file_path))
							{
								mkdir($file_path, 0755, true);
							}

							$data_array = array(
								'user_id' => $u_id,
								'course_id' => $course_id,
								'file_path' => $file_path.strtolower(str_ireplace(' ', '_', $latin_name)).'_wa_certificate.pdf',
								//'score_average' => $_POST['score_avg'][$u_id],
								//'score_last' => $_POST['score_lst'][$u_id],
								//'score_final' => $_POST['score_fnl'][$u_id],
								'serial_number' => $new_serial
							);

							insert_query('user_certificates', $data_array);
							$new_record++;

							$mail_recipients[] = $u_id;

							//Certificate Create
							try
							{
								$html2pdf = new Spipu\Html2Pdf\Html2Pdf;
								$date_complete = date('F j, Y', strtotime($course_data['end_date']));
								$html2pdf->pdf->SetDisplayMode('fullpage');
								$template_data = array(
									'STUDENT_NAME' => ucwords(strtolower($latin_name)),
									'COURSE_NAME' => $ancestor_data['ancestor_name_translated'],
									'SERIAL_NUMBER' => $new_serial,
									'TECHNOLOGIES' => $ancestor_data['ancestor_tech_translated'],
									'DATE_COMPLETE' => strtoupper($date_complete),
									'DIR_IMAGES' => DIR_IMAGES
								);


								$page_content = replace_template_data($cert_template , $template_data);

								$html2pdf->writeHTML($page_content);
								$html2pdf->output($data_array['file_path'], 'F');
								//$html2pdf->output($data_array['file_path']);
							}
							catch(HTML2PDF_exception $e)
							{
								echo $e;
							}
						}
						//$html2pdf->output($data_array['file_path']);
						$mail_template = getTemplateData(1); // Load Certificates Template

						$mail_data = array(
							'mail_content' => $mail_template['template_content'],
							'mail_subject' => $mail_template['template_about'],
							'created_from' => 0, // This is System Message
						);

						if(isset($_POST['send_mail']))
						{
							insert_mail($mail_data, $mail_recipients);
						}

						add_success_message('Сертификатите са раздадени успешно!');
						set('file_path', $file_path);
						database_close();

						//header('location: ../'.$file_path);
						//exit;
					}
					else
					{
						add_info_message('Няма избрани потребители!');
					}
				}

				$filters = '';
				$emails = array();
				$certificates_minimum_limit = 60;

				// Може да махна тези 2 реда, когато променя начина по който се вземат заявките.
				if(!isset($source['score']['min'])){ $source['score']['min'] = 0; }
				if(!isset($source['score']['max'])){ $source['score']['max'] = 100; }

				if(isset($source['score']) && $source['score'] != '')
				{
					if(is_array($source['score']))
					{
						//if(!isset($source['score']['min'])){ $source['score']['min'] = 0; }
						//if(!isset($source['score']['max'])){ $source['score']['max'] = 100; }

						//$filters .= ' AND score BETWEEN '.$source['score']['min'].' AND '.$source['score']['max'];

						//unset($source['score']['min']);
						//unset($source['score']['max']);

						if(count($source['score']) > 0)
						{
							//$filters .= ' AND score IN ('.implode(', ', $source['score']).')';
						}
					}
					else
					{
						//$filters .= ' AND score = '.$source['score'];
					}
				}

				if(isset($source[ORDER_BY]) && $source[ORDER_BY] != '')
				{
					//$query_string .= ' ORDER BY '.mysqli_real_escape_string($_connection, $source[ORDER_BY]);
				}
				else
				{
					//$query_string .= ' ORDER BY uh.homework_id';
					$source[ORDER_BY] = 'final_score';
				}

				database_open();

				$Sub_Query = 'SELECT user_id FROM course_members WHERE course_id = '.$course_id.' AND role="student"';

				$Exams_Query = 'SELECT eh.user_id, sum(eh.score * rt.weight) AS result FROM exams_history AS eh
								LEFT JOIN required_tests AS rt ON eh.exam_id = rt.exam_id
								WHERE eh.user_id IN ('.$Sub_Query.') AND (rt.exam_type = "current" OR rt.exam_type = "final") AND rt.weight > 0 '.$filters.' AND rt.course_id = '.$course_id.' GROUP BY eh.user_id';

				$Homeworks_Query = '
								SELECT uh.user_id, sum(uh.score * ch.weight) AS result FROM `user_homeworks` AS uh
								LEFT JOIN course_homeworks AS ch ON uh.homework_id = ch.homework_id
								WHERE uh.user_id IN ('.$Sub_Query.') '.$filters.' AND ch.course_id = '.$course_id.' AND (ch.homework_type = "current" OR ch.homework_type = "final") GROUP BY uh.user_id';

				$Users_Query = 'SELECT * FROM users WHERE user_id IN ('.$Sub_Query.')';

				$exams = to_assoc_array(exec_query($Exams_Query), 'user_id'); // Current and Final Theoretical Exam
				$homeworks = to_assoc_array(exec_query($Homeworks_Query), 'user_id'); // Current and Final Theoretical Exam
				$users = to_assoc_array(exec_query($Users_Query), 'user_id');

				$certificates = getCertificates($course_id, null, 'user_id');
				//$theoretical_exam = to_assoc_array(exec_query('SELECT user_id, score FROM exams_history WHERE score >= '.$certificates_minimum_limit.' AND exam_id = 8'), 'user_id');
				//$practical_exam = to_assoc_array(exec_query('SELECT user_id, score FROM user_homeworks WHERE score >= '.$certificates_minimum_limit.' AND homework_id IN (7,8,9)'), 'user_id');

				if(empty($exams)) // В курса явно няма междинни нито финални  теоретични изпити, така че гледаме само практическия финален изпит
				{
					$Exams_Query = $Homeworks_Query;
					$exams = to_assoc_array(exec_query($Exams_Query), 'user_id');
					add_warning_message('Явно към курса няма добавени никакви изпити! :(');
				}

				$theoretical_exam_id = to_assoc_array(exec_query('SELECT exam_id FROM required_tests WHERE exam_type = "final" AND course_id = '.$course_id));
				if(count($theoretical_exam_id) > 0)
				{
					$theoretical_exam_id = array_column($theoretical_exam_id, 'exam_id');
					$theoretical_exam = to_assoc_array(exec_query('SELECT user_id, score FROM exams_history WHERE score >= '.$certificates_minimum_limit.' AND exam_id IN ('.implode(',', $theoretical_exam_id).')'), 'user_id');

					if(count($theoretical_exam) == 0)
					{
						add_warning_message('Явно никой не е решил успешно теоретичния изпит! :(');
					}
				}
				else
				{
					$theoretical_exam_id = 0;
					$theoretical_exam = array();

					add_warning_message('Не е зададен теоретичен финален изпит! <br /><a href="'.link_to(array(CONTROLLER => 'exam', ACTION => 'manage', 'course_id' => $course_id, 'exam_type' => '-')).'">Редактирайте изпитите...</a>');
				}

				$practical_exam_id = to_assoc_array(exec_query('SELECT homework_id FROM course_homeworks WHERE homework_type = "final" AND course_id = '.$course_id));
				if(count($practical_exam_id) > 0)
				{
					$practical_exam_id = array_column($practical_exam_id, 'homework_id');
					$practical_exam = to_assoc_array(exec_query('SELECT user_id, score FROM user_homeworks WHERE score >= '.$certificates_minimum_limit.' AND homework_id IN ('.implode(',', $practical_exam_id).')'), 'user_id');

					if(count($practical_exam) == 0)
					{
						add_warning_message('Явно никой не се е справил успешно с практичния изпит! :(');
					}
				}
				else
				{
					$practical_exam_id = 0;
					$practical_exam = array();

					add_warning_message('Не е зададен практически финален изпит! <br /><a href="'.link_to(array(CONTROLLER => 'homeworks', ACTION => 'manage', 'course_id' => $course_id)).'">Редактирайте домашните...</a>');
				}

				pre_print($theoretical_exam);
				pre_print($practical_exam);
				pre_print($users);

				database_close();

				$i = 0;
				foreach($exams as $exam)
				{
					$average_val = $exam['result'] + (isset($homeworks[$exam['user_id']]['result']) ? $homeworks[$exam['user_id']]['result'] : 0);

					@ $last_exam_val = ($theoretical_exam[$exam['user_id']]['score'] + $practical_exam[$exam['user_id']]['score']) / 2;
					$certificates_val =  ($last_exam_val + $average_val) / 2;

					$is_passed = ($last_exam_val >= $certificates_minimum_limit) && ($average_val >= $certificates_minimum_limit);

					if($source['score']['min'] <= $average_val && $average_val <= $source['score']['max'])
					{
						$users_results[$exam['user_id']]['id'] = '<input type="checkbox" name="user['.$exam['user_id'].']" value="'.$exam['user_id'].'"'.($is_passed ? ' checked="checked"' : '').(isset($certificates[$exam['user_id']]) ? ' disabled="disabled"' : '').' />';

						$users_results[$exam['user_id']]['name'] = '<a href="'.link_to(array(CONTROLLER => 'user', ACTION => 'view', ID => $exam['user_id'])).'" title="View User #'.$exam['user_id'].'">'.$users[$exam['user_id']]['first_name'].' '.$users[$exam['user_id']]['last_name'].'</a>';
						$users_results[$exam['user_id']]['еmail'] = $users[$exam['user_id']]['user_email'];


						$users_results[$exam['user_id']]['exams' ] = round($exams[$exam['user_id']]['result'], 2);
						$users_results[$exam['user_id']]['hmwrks'] = round($homeworks[$exam['user_id']]['result'], 2);
						$users_results[$exam['user_id']]['t_exam'] = round($theoretical_exam[$exam['user_id']]['score'], 2);
						$users_results[$exam['user_id']]['p_exam'] = round($practical_exam[$exam['user_id']]['score'], 2);

						$users_results[$exam['user_id']]['avg_score'] = round($average_val, 2);
						$users_results[$exam['user_id']]['last_score'] = round($last_exam_val, 2);
						$users_results[$exam['user_id']]['final_score'] = round($certificates_val, 2);
						//$users_results[$exam['user_id']]['course_result'] = ($final_exam_val >= 60 && $average_val >= 60 ? '<span class="certificates_yes">ДА</span>' : '<span class="certificates_no">НЕ</span>');

						if($is_passed)
						{
							$users_results[$exam['user_id']]['id'] .= '<input type="hidden" name="score_avg['.$exam['user_id'].']" value="'.$users_results[$exam['user_id']]['avg_score'].'" />';
							$users_results[$exam['user_id']]['id'] .= '<input type="hidden" name="score_lst['.$exam['user_id'].']" value="'.$users_results[$exam['user_id']]['last_score'].'" />';
							$users_results[$exam['user_id']]['id'] .= '<input type="hidden" name="score_fnl['.$exam['user_id'].']" value="'.$users_results[$exam['user_id']]['final_score'].'" />';
						}

						//$users_results[$exam['user_id']]['last_score'] = '<span title="T:'.$theoretical_exam[$exam['user_id']]['score'].'; P:'.$practical_exam[$exam['user_id']]['score'].'">'.$users_results[$exam['user_id']]['last_score'].'</span>';
					}
				}

				$users_results = array_sort($users_results, $source[ORDER_BY], SORT_DESC);

				foreach($users_results as $k => $v)
				{
					$users_results[$k]['final_score'] = ($v['last_score'] >= $certificates_minimum_limit && $v['avg_score'] >= $certificates_minimum_limit ? '<span class="certificates_yes">' : '<span class="certificates_no">').$v['final_score'].'</span>';

					$emails[] = $v['еmail'];
				}

				$columns = array(
					array('text' => '&nbsp;', 'field' => 'id'),
					array('text' => 'Курсист', 'field' => 'name'),
					array('text' => 'Е-мейл', 'field' => 'еmail'),
					array('text' => 'Домашни', 'field' => 'hmwrks'),
					array('text' => 'Изпити', 'field' => 'exams'),
					array('text' => 'Теория', 'field' => 't_exam'),
					array('text' => 'Практика', 'field' => 'p_exam'),
					array('text' => 'Финален', 'field' => 'last_score'),
					array('text' => 'Ср.Успех', 'field' => 'avg_score'),
					array('text' => 'Крайно', 'field' => 'final_score'),
					//array('text' => 'Взел курса?', 'field' => 'course_result'),
				);

				set('exams', $exams);
				set('emails', $emails);
				set('course_id',$course_id);
				set('homeworks', $homeworks);
				set('users_results', $users_results);
				set('certificates', $certificates);
				set('columns', $columns);
			}
		}
		else
		{
			echo 'Не сте оторизиран!';
			exit;
		}
	}

	function action_course_HowIsGoing()
	{
		Global $DataBase, $source, $user_id, $is_admin_user;

		if(!$is_admin_user)
		{
			echo 'You are not authorized!';
			exit;
		}

		database_open();
		$result_array = array();

		if(!isset($_GET['courses']))
		{
			$courses = get_all_courses('candidate_until >= "'.date('Y-m-d H:i:s').'"');
		}
		else
		{
			if(!is_array($_GET['courses']))
			{
				$_GET['courses'] = array($_GET['courses']);
			}

			$courses = get_all_courses('course_id IN ('.implode(',',$_GET['courses']).')');
		}

		$summary = array(
			'courses' => '<b>Сумарно: '.count($courses).' курса</b>',
			'candidates' => '0',
			'have_all_exam' => '0',
			//'have_one_exam' => '0',
			//'zagora' => '0',
			'have_60_perc' => '0',
			'avg_perc' => '0',
			'vip' => '0'
		);

		foreach($courses as $k => $course)
		{
			$tmp = array();

			$tmp['name'] = $course['course_name'];
			$tmp['candidates'] = '0';
			$tmp['have_all_exam'] = '0';
			//$tmp['have_one_exam'] = '0';
			//$tmp['zagora'] = '0';
			$tmp['have_60_perc'] = '0';
			$tmp['avg_perc'] = '0';
			$tmp['vip'] = '0';

			// All Candidates
			$candidates_sql = 'SELECT COUNT(user_id) AS result FROM course_members WHERE course_id = "'.(int) $course['course_id'].'"';
			$candidates_query = to_assoc_array(exec_query($candidates_sql.' AND role = "candidate"'));
			if(count($candidates_query) == 1)
			{
				$candidates_query = reset($candidates_query);
				$tmp['candidates'] = $candidates_query['result'];
				$summary['candidates'] += $candidates_query['result'];
			}

			/*
			// Zagora Candidates
			$zagora_query = to_assoc_array(exec_query($candidates_sql.' AND role = "candidate" AND group_number = 5'));
			if(count($zagora_query) == 1)
			{
				$zagora_query = reset($zagora_query);
				$tmp['zagora'] = $zagora_query['result'];
				$summary['zagora'] += $zagora_query['result'];
			}
			*/

			// Candidates which have more than 60% in precondition tests
			$tests = getRequiredTestsPerCourse($course['course_id']);
			$sql_query = to_assoc_array(exec_query('SELECT COUNT(exam_id) as result_count FROM exams_history WHERE score >= 60 AND exam_id IN ('.implode(', ', array_keys($tests) ).')'));

			if(count($sql_query) == 1)
			{
				$sql_query = reset($sql_query);
				$tmp['have_60_perc'] = $sql_query['result_count'];
				$summary['have_60_perc'] += $sql_query['result_count'];

				//pre_print($sql_query);
			}

			$sql_query = to_assoc_array(exec_query('SELECT AVG(score) as result_avg FROM exams_history WHERE exam_id IN ('.implode(', ', array_keys($tests) ).')'));

			if(count($sql_query) == 1)
			{
				$sql_query = reset($sql_query);
				$tmp['avg_perc'] = round($sql_query['result_avg']).'%';
				$summary['avg_perc'] += round($sql_query['result_avg']);

				//pre_print($sql_query);
			}

			// Solve All Exams
			$candidates = getCourseCandidates($course['course_id']);
			if(count($candidates) > 0)
			{
				$tmp['have_all_exam'] = count($candidates);
				$summary['have_all_exam'] += count($candidates);
			}

			/*
			// Solve 1 or More Exams
			$tests = getRequiredTestsPerCourse($course['course_id']);

			$sql_query = $candidates_sql.' AND role = "candidate" AND user_id IN (SELECT user_id FROM exams_history WHERE exam_id IN ('.implode(', ', array_keys($tests) ).') )';
			$sql_query = to_assoc_array(exec_query($sql_query));

			if(count($sql_query) == 1)
			{
				$sql_query = reset($sql_query);
				$tmp['have_one_exam'] = $sql_query['result'];
				$summary['have_one_exam'] += $sql_query['result'];
			}
			*/

			// VIP students with certificate
			$sql_query = $candidates_sql.' AND role = "student"';
			$sql_query = to_assoc_array(exec_query($sql_query));

			if(count($sql_query) == 1)
			{
				$sql_query = reset($sql_query);
				$tmp['vip'] = $sql_query['result'];
				$summary['vip'] += $sql_query['result'];
			}

			$result_array[] = $tmp;
		}

		$summary['avg_perc'] = round($summary['avg_perc'] / count($courses));
		$result_array[] = $summary;

		echo '<div class="container"><div class="row"><div class="col-lg-12"><br />';
		render_table($result_array, array('Курс', 'Кандидати общо', 'Решили всичко', 'Над 60%', 'AVG','Вече приети'));
		echo '<br /><b><i><u>Забележки:</u></i></b><br />Кандидати общо - Кандидати (без приети курсисти) независимо от тяхната форма на обучение или град';
		echo '<br />Старозагорци - Кандидати за присъствена форма на обучение в Стара Загора';
		echo '<br />Сертифицирани - Курсисти притежаващи сертификат от предходен курс. Техния брой НЕ влиза в "Кандидати общо", защото те НЕ са кандидати, а са вече приети';
		echo '<br />Решили >= 1 - Кандидати решили поне един от необходимите входни тестове. Виж по-долу';
		echo '<br />Решили всичко - Кандидати решили всички входни тестове. За курсовете различни от "Front End за Начинаещи" числата в "Решили Всичко" и "Решили >=1" съвпадат, защото тези курсове имат само 1 входен тест';
		echo '</div></div></div>';
		database_close();
	}

	function action_course_add()
	{
		Global $source, $_connection, $is_admin_user;

		check_login();
		database_open();

		$is_observer = false;
		if(isset($source[ID]))
		{
			//$is_observer = is_observer($source[ID]);
		}

		if(!$is_admin_user && !$is_observer)
		{
			page_not_access();
			exit;
		}

		if(isset($source[ID])) // This is Edit - Load Course Data
		{
			// Prepare UI Variables
			$edit_course = $source[ID];

			$course_id = (int) $source[ID]; // Временно решение докато направя да работи с мултиедит

			$is_collapsed = true;

			// Loading Data
			$courses = get_all_courses('course_id IN ('.$course_id.')');

			if(count($courses) > 0)
			{
				if(count($courses) == 1)
				{
					$the_key = array_keys($courses);
					$the_key = reset($the_key);

					$courses[$the_key]['dates_array'] = to_assoc_array(exec_query('SELECT * FROM course_dates WHERE course_id = '.$course_id.' ORDER BY lecture_date ASC'));
				}
			}
			else
			{
				add_error_message('Такъв курс не съществува!');
				header('location:'.header_link( array(CONTROLLER => 'course', ACTION => 'index') ) );
				exit;
			}
		}
		else // This is New - Load Default Data
		{
			$course_id = 0;
			$edit_course = 0;
			$is_collapsed = false;

			if(isset($source['courses']))
			{
				$copy_courses = get_all_courses('course_id IN ('.implode(', ', $source['courses']).')');

				if(count($copy_courses) > 0)
				{
					$course = reset($copy_courses);

					$course['start_date'] = date('Y-m-d');
					$course['end_date'] = date('Y-m-d', strtotime('+14 week'));
					$course['candidate_until'] = date('Y-m-d', strtotime('+14 day')).' 23:59:59';
				}
				else
				{
					add_error_message('Курса, който искате да копирате не е намерен!');
				}
			}
			else
			{
				$course = array(
					'descendant_of' => 0,
					'teacher_id' => 0,
					'course_name' => '',
					'course_url' => '',
					'course_after_url' => '',
					'horarium' => 60,
					'start_date' => date('Y-m-d'),
					'end_date' => date('Y-m-d', strtotime('+14 week')),
					'candidate_until' => date('Y-m-d', strtotime('+14 day')).' 23:59:59',
					'dates_array' => array()
				);
			}

			for($i = 1; $i <= 15; $i++)
			{
				$course['dates_array'][] = array('group_number' => $i, 'lecture_date' => date('Y-m-d', strtotime('+'.($i-1).' week')));
			}

			$courses = array($course); // решение за евентуален мултиедит
		}

		if(!empty($_POST)) // Без значение дали е Edit или New, ако имаме изпратени данни зареждаме тях във формата
		{
			$is_collapsed = false;

			$course = array(
				'descendant_of' => (int) $_POST['ancestor_id'],
				'teacher_id' => (int) $_POST['teacher_id'],
				'teacher_commission' => (int) $_POST['teacher_commission'],
				'course_name' => $_POST['course_name'],
				'course_url' => $_POST['course_url'],
				'course_after_url' => $_POST['course_after_url'],
				'course_horarium' => (int) $_POST['course_horarium'],
				'course_price' => (int) $_POST['course_price'],
				'start_date' => $_POST['start_date'],
				'end_date' => $_POST['end_date'],
				'candidate_from' => $_POST['candidate_from'],
				'candidate_to' => $_POST['candidate_to'],
				'dates_array' => array()
			);

			$course['dates_array'] = $_POST['visibility'];

			if(!isset($_POST['teacher_id']) || 0 == $_POST['teacher_id'])
			{
				add_warning_message('Задължително трябва да изберете лектор на курса!');
			}

			if(!isset($_POST['course_name']) || '' == $_POST['course_name'])
			{
				add_warning_message('Не сте въвел име на курса!');
			}

			if(!isset($_POST['course_url']) || '' == $_POST['course_url'])
			{
				add_warning_message('Не сте въвел URL на курса!');
			}

			if($_POST['candidate_until'] > $_POST['start_date'])
			{
				add_warning_message('Не е възможно срока за кандидатстване да е след началната дата на курса!');
			}

			$courses = array($course); // решение за евентуален мултиедит
			if(!has_warning_messages())
			{
				$data_array = $course;
				unset($data_array['dates_array']);

				if(!$edit_course)
				{
					$new_course_id = insert_query('courses', $data_array);

					if($new_course_id > 0)
					{
						add_success_message('Курсът е създаден успешно!');
					}
					else
					{
						add_warning_message('Проблем при създаването на курса!');
					}
				}
				else
				{
					$new_course_id = $edit_course;
					$result = update_query('courses', $data_array, 'course_id = '.$edit_course);

					if($result)
					{
						add_success_message('Промените са запазени успешно!');
						delete_query('course_dates', 'course_id = '.(int) $new_course_id, 0);
					}
					else
					{
						add_warning_message('Неочакван проблем при запазване на промените!');
					}
				}

				if(!has_warning_messages())
				{
					if(isset($_POST['visibility']) && is_array($_POST['visibility']) && count($_POST['visibility']) > 0)
					{
						foreach($_POST['visibility'] as $visibility)
						{
							$data_visibility = array(
								'course_id' => $new_course_id,

								// Set Default Date Values
								'lecture_date' => '0000-00-00',
								'lecture_name' => '',
								'lecture_info' => '',
								'present_users' => ''
							);

							$data_visibility['lecture_date'] = $visibility['lecture_date'];
							$data_visibility['lecture_info'] = $visibility['lecture_info'];
							$data_visibility['lecture_name'] = htmlspecialchars($visibility['lecture_name']);

							$new_visibility_id = insert_query('course_dates', $data_visibility);
						}
					}
				}

				if($new_course_id  > 0)
				{
					header('location:'.header_link( array(CONTROLLER => 'course', ACTION => 'add', ID => $new_course_id) ) );
					exit;
				}
			}
		}

		database_close();
		set('is_collapsed', $is_collapsed);
		set('is_observer', $is_observer);
		set('edit_course', $edit_course);
		set('courses', $courses);
	}

	function action_course_copy()
	{
		Global $source, $_connection, $is_admin_user;

		database_open();

		if(!$is_admin_user)
		{
			page_not_access();
			exit;
		}

		if(isset($source[ID])) // This is Edit - Load Course Data
		{
			// Prepare UI Variables
			$edit_course = $source[ID];
			$course_id = (int) $source[ID];
			$is_collapsed = false;

			// Loading Data
			$copy_course = get_all_courses('course_id IN ('.$course_id.')');

			if(count($copy_course) == 1)
			{
				$course = reset($copy_course);

				$course['start_date'] = date('Y-m-d');
				$course['end_date'] = date('Y-m-d', strtotime('+14 week'));
				$course['candidate_until'] = date('Y-m-d', strtotime('+14 day')).' 23:59:59';
				$course['dates_array'] = array();

				for($i = 1; $i <= 15; $i++)
				{
					$course['dates_array'][] = array('group_number' => $i, 'lecture_date' => date('Y-m-d', strtotime('+'.($i-1).' week')));
				}

				$courses = array($course); // решение за евентуален мултиедит
			}
			else
			{
				add_error_message('Курса, който искате да копирате не е намерен!');
				header('location:'.header_link( array(CONTROLLER => 'course', ACTION => 'index') ) );
				exit;
			}
		}
		else // This is New - Load Default Data
		{
			add_error_message('Параметър '.ID.' е задължителен!');
		}

		if(!empty($_POST)) // Без значение дали е Edit или New, ако имаме изпратени данни зареждаме тях във формата
		{
			$is_collapsed = false;

			$course = array(
				'descendant_of' => (int) $_POST['ancestor_id'],
				'teacher_id' => (int) $_POST['teacher_id'],
				'course_name' => $_POST['course_name'],
				'course_url' => $_POST['course_url'],
				'course_after_url' => $_POST['course_after_url'],
				'horarium' => (int) $_POST['horarium'],
				'start_date' => $_POST['start_date'],
				'end_date' => $_POST['end_date'],
				'candidate_until' => $_POST['candidate_until'],
				'dates_array' => array()
			);

			$course['dates_array'] = $_POST['visibility'];

			if(!isset($_POST['teacher_id']) || 0 == $_POST['teacher_id'])
			{
				add_warning_message('Задължително трябва да изберете лектор на курса!');
			}

			if(!isset($_POST['course_name']) || '' == $_POST['course_name'])
			{
				add_warning_message('Не сте въвел име на курса!');
			}

			if(!isset($_POST['course_url']) || '' == $_POST['course_url'])
			{
				add_warning_message('Не сте въвел URL на курса!');
			}

			if($_POST['candidate_until'] > $_POST['start_date'])
			{
				add_warning_message('Не е възможно срока за кандидатстване да е след началната дата на курса!');
			}

			if(!has_warning_messages())
			{
				$data_array = $course;
				unset($data_array['dates_array']);

				if($edit_course)
				{
					$new_course_id = insert_query('courses', $data_array);

					if($new_course_id > 0)
					{
						add_success_message('Курсът е създаден успешно!');
					}
					else
					{
						add_warning_message('Проблем при създаването на курса!');
					}
				}

				if(!has_warning_messages())
				{
					// Lectures Dates
					if(isset($_POST['visibility']) && is_array($_POST['visibility']) && count($_POST['visibility']) > 0)
					{
						foreach($_POST['visibility'] as $visibility)
						{
							$data_visibility = array(
								'course_id' => $new_course_id,

								// Set Default Date Values
								'lecture_date' => '0000-00-00',
								'present_users' => ''
							);

							$data_visibility['lecture_date'] = $visibility['lecture_date'];

							$new_visibility_id = insert_query('course_dates', $data_visibility);
						}
					}

					$exams = get_all_exams('c.course_id = '.$source[ID]); // rt.exam_type = "precondition" AND
					//pre_print($exams, 614);

					foreach($exams as $exam)
					{
						$exam_data = array(
							'exam_type' => $exam['exam_type'],
							'course_id' => $new_course_id,
							'test_id' => $exam['test_id'],
							'questions_number' => $exam['questions_number'],
							'weight' => $exam['weight'],
							'test_seconds' => $exam['test_seconds'],
							'shuffle_exam_components' => $exam['shuffle_exam_components'],
							'for_office_only' => $exam['for_office_only'],
						);

						$new_exam_id = insert_query('required_tests', $exam_data);

						for($i = -1; $i <= 1; $i++)
						{
							$data_visibility = array(
								'object_id' => $new_exam_id,
								'object_type' => 'exam',
								'group_number' => $i,
								'start_date' => ('precondition' == $exam['exam_type'] ? date('Y-m-d H:i:s') : $_POST['end_date'].' 00:00:00'),
								'end_date' => ('precondition' == $exam['exam_type'] ? $_POST['candidate_until'] : $_POST['end_date'].' 23:59:59')
							);

							$new_visibility_id = insert_query('objects_visibility', $data_visibility);
						}
					}
				}

				if($new_course_id  > 0)
				{
					header('location:'.header_link( array(CONTROLLER => 'course', ACTION => 'add', ID => $new_course_id) ) );
					exit;
				}
			}
		}

		database_close();
		set('is_collapsed', $is_collapsed);
		set('edit_course', $edit_course);
		set('courses', $courses);
	}
?>