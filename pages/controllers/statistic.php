<?php
	// Empty Controller
	function action_statistic_index()
	{
		echo 'Statistic Index';
	}

	function get_avg_val($avg)
	{
		$avg = reset($avg);

		return round($avg['avg_score'],2);
	}

	function action_statistic_birthday()
	{
		database_open();
		$registered_users = count(to_assoc_array(exec_query('SELECT * FROM users')));
		$course_members = count(to_assoc_array(exec_query('SELECT * FROM course_members')));
		$certificated_users = count(to_assoc_array(exec_query('SELECT DISTINCT(user_id) FROM user_certificates')));
		$certificates_count = count(to_assoc_array(exec_query('SELECT * FROM user_certificates')));

		$homeworks_count = count(to_assoc_array(exec_query('SELECT * FROM course_homeworks')));
		$user_homeworks_count = count(to_assoc_array(exec_query('SELECT * FROM user_homeworks')));
		$avg_homeworks = to_assoc_array(exec_query('SELECT AVG(score) AS avg_score FROM user_homeworks WHERE homework_id IN (SELECT homework_id FROM course_homeworks WHERE homework_type <> "final")'));
		$avg_p = to_assoc_array(exec_query('SELECT AVG(score) AS avg_score FROM user_homeworks WHERE homework_id IN (SELECT homework_id FROM course_homeworks WHERE homework_type = "final")'));

		$exams_count = count(to_assoc_array(exec_query('SELECT * FROM required_tests')));
		$avg_exams = to_assoc_array(exec_query('SELECT AVG(score) AS avg_score FROM exams_history WHERE exam_id IN (SELECT exam_id FROM required_tests WHERE exam_type <> "poll" AND weight <> 0)'));
		$avg_t = to_assoc_array(exec_query('SELECT AVG(score) AS avg_score FROM exams_history WHERE exam_id IN (SELECT exam_id FROM required_tests WHERE exam_type = "final")'));

		$user_exams_count = to_assoc_array(exec_query('SELECT COUNT(history_id) as avg_score FROM exams_history'));

		//pre_print($user_exams_count, 1);
		echo '<div class="container"><div class="row"><div class="col-lg-12">';
		echo '<h1>Birthday Statistic</h1>';
		echo '<details><summary>Регистрирани потребители '.$registered_users.'</summary></details>';
		echo '<details><summary>Обучени курсисти '.$course_members.'</summary></details>';

		echo '<details><summary>Брой сертификати: '.$certificates_count.'</summary></details>';
		echo '<details><summary>Взети от брой курсисти: '.$certificated_users.'</summary></details>';

		echo '<details><summary>Брой Домашни към курсове '.$homeworks_count.'</summary></details>';
		echo '<details><summary>Брой качени Домашни от курсисти '.$user_homeworks_count.'</summary></details>';
		echo '<details><summary>Среден успех на домашни '.get_avg_val($avg_homeworks).'</summary></details>';

		echo '<details><summary>Брой Изпити към курсове: '.$exams_count.'</summary></details>';
		echo '<details><summary>Брой Решени Изпити от курсисти: '.get_avg_val($user_exams_count).'</summary></details>';
		echo '<details><summary>Среден успех на изпити: '.get_avg_val($avg_exams).'%</summary></details>';

		echo '<details><summary>Среден успех на Практически изпити: '.get_avg_val($avg_p).'%</summary></details>';
		echo '<details><summary>Среден успех на Теоретически изпити: '.get_avg_val($avg_t).'%</summary></details>';

		echo '</div></div></div>';
		database_close();
	}
?>
<div class="entry-content">
<!-- @TODO -->