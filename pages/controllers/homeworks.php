<?php
	// User Controller
	function action_homeworks_index()
	{
		Global $is_admin_user, $source, $user_id, $_connection, $assistants_array, $admins_array;
		database_open();

		if(!$is_admin_user && !is_assistant())
		{
			$die_now = true;
			if(isset($source[VIEW]) && $source[VIEW] == 'random')
			{
				$user_certificates = getCertificates(null, $user_id, 'ancestor_id'); // Ключовете на масива са ID-тата на курсовете прародители.

				if(true) // Разрешаваме ли всеки курсист да може да проверява домашните на колегите си от същия курс по който се обучава в момента?
				{
					$now = date('Y-m-d');
					$my_current_courses = get_user_courses($user_id, 'role = "student" AND start_date < "'.$now.'" AND "'.$now.'" < end_date');

					foreach(array_column($my_current_courses, 'descendant_of') as $descendant)
					{
						if(!isset($user_certificates[$descendant]))
						{
							$user_certificates[$descendant] = array();
						}
					}

					//pre_print($my_current_courses, 15);
					//pre_print($user_certificates, 15);
					//exit;
				}

				if(count($user_certificates) > 0)
				{
					$die_now = false;

					$courses = get_all_courses('descendant_of IN('.implode(',', array_keys($user_certificates)).') AND start_date <= "'.date('Y-m-d').'" AND "'.date('Y-m-d').'" < end_date');
				}
				else
				{
					add_error_message('Съжаляваме, няма подходяща домашна за теб!');
				}
			}

			if($die_now)
			{
				header('location:'.link_to( array(CONTROLLER => 'user', ACTION => 'view', 'OPTION_SET_CLASS' => false) ));
				exit;
			}
		}

		$query_string = '';

		if(!isset($source[VIEW]))
		{
			// Set Default Value
			$view = 'non_evaluated';
			//$view = 'all';
		}
		else
		{
			$view = strtolower($source[VIEW]);
		}

		  $report_tag = getTagData(13, 'tag_name');
		  $answer_tag = getTagData(14, 'tag_name');
		$revision_tag = getTagData(15, 'tag_name');

		switch($view)
		{
			case 'all': $query_string = '1'; break; // Show All Homeworks
			case 'evaluated': $query_string = 'score > 0'; break; // Show non evaluated homeworks only
			case 'non_evaluated': $query_string = 'score = 0'; break; // Show non evaluated homeworks only
			case 'locked': $query_string = 'score = -1'; break; // Show non evaluated homeworks only
			case 'random':
				$query_string = 'score = 0'.(!$is_admin_user && !is_assistant() ? ' AND ch.homework_type = "current" AND uh.user_id <> '.$user_id : '');
				$source['course_id'] = array_keys($courses);
				$source[ORDER_BY] = 'ch.course_id DESC';
				break;
			case 'reported_all': $query_string = 'score_arguments LIKE "%'.$report_tag.'%"'; break; // Show all reported homeworks, no matter of revision or answer...
			case 'reported_answered': $query_string = 'score_arguments LIKE "%'.$answer_tag.'%"'; break; // Show all reported homeworks with Answer only
			case 'reported_revisited': $query_string = 'score_arguments LIKE "%'.$revision_tag.'%"'; break; // Show all reported homeworks with Answer only
			case 'reported_only': $query_string = 'score_arguments LIKE "%'.$report_tag.'%" AND (score_arguments NOT LIKE "%'.$answer_tag.'%" AND score_arguments NOT LIKE "%'.$revision_tag.'%")'; break; // Show all reported homeworks with Answer only

			default: $query_string = '1'; $view = 'all'; break; // Show All Questions
		}

		if(isset($source['course_id']))
		{
			if(is_numeric($source['course_id']))
			{
				$query_string .= ' AND ch.course_id = '.(int) $source['course_id'];
			}

			if(is_array($source['course_id']))
			{
				$query_string .= ' AND ch.course_id IN ('.implode(',', $source['course_id']).')';
			}
		}

		if(isset($source['user_id']))
		{
			if(strpos($source['user_id'], ',')) // If we have format like homework_id=1,2,3 convert it to array
			{
				$source['user_id'] = explode(',', $source['user_id']);
			}

			if(is_array($source['user_id']))
			{
				$query_string .= ' AND uh.user_id IN ('.implode(', ', $source['user_id']).')';
			}
			else
			{
				$query_string .= ' AND uh.user_id = '.(int) $source['user_id'];
			}
		}

		if(isset($source['homework_id']))
		{
			if(strpos($source['homework_id'], ',')) // If we have format like homework_id=1,2,3 convert it to array
			{
				$source['homework_id'] = explode(',', $source['homework_id']);
			}

			if(is_array($source['homework_id']) && count($source['homework_id']) > 0)
			{
				$query_string .= ' AND uh.homework_id IN ('.implode(', ', $source['homework_id']).')';
			}
			elseif(is_numeric($source['homework_id']))
			{
				$query_string .= ' AND uh.homework_id = '.(int) $source['homework_id'];
			}
		}

		if(isset($source['tags']) && $source['tags'] != '')
		{
			$query_string .= ' AND uh.score_arguments LIKE "%'.$source['tags'].'%"';
		}

		if(isset($source['score']) && $source['score'] != '')
		{
			if(is_array($source['score']))
			{
				if(!isset($source['score']['min'])){ $source['score']['min'] = 0; }
				if(!isset($source['score']['max'])){ $source['score']['max'] = 100; }

				$query_string .= ' AND uh.score BETWEEN '.$source['score']['min'].' AND '.$source['score']['max'];

				unset($source['score']['min']);
				unset($source['score']['max']);

				if(count($source['score']) > 0)
				{
					$query_string .= ' AND uh.score IN ('.implode(', ', $source['score']).')';
				}
			}
			else
			{
				$query_string .= ' AND uh.score = '.$source['score'];
			}
		}

		if(isset($source['score_on']) && $source['score_on'] != '')
		{
			if(is_array($source['score_on']))
			{
				if(!isset($source['score_on']['min'])){ $source['score_on']['min'] = 0; }
				if(!isset($source['score_on']['max'])){ $source['score_on']['max'] = strtotime('now'); }

				if(!is_numeric($source['score_on']['min']))
				{
					$source['score_on']['min'] = strtotime($source['score_on']['min']);
				}

				if(!is_numeric($source['score_on']['max']))
				{
					$source['score_on']['max'] = strtotime($source['score_on']['max']);
				}

				$query_string .= ' AND uh.score_on BETWEEN '.$source['score_on']['min'].' AND '.$source['score_on']['max'];

				unset($source['score_on']['min']);
				unset($source['score_on']['max']);

				if(count($source['score_on']) > 0)
				{
					$query_string .= ' AND uh.score_on IN ('.implode(', ', $source['score_on']).')';
				}
			}
			else
			{
				$query_string .= ' AND uh.score_on = '.$source['score_on'];
			}
		}

		if(isset($source['teacher_id']) && $source['teacher_id'] != '')
		{
			$teachers = null;

			if(is_string($source['teacher_id']))
			{
				$source['teacher_id'] = strtolower($source['teacher_id']);

				switch($source['teacher_id'])
				{
					case 'admins' : $teachers = 'IN ('.implode(',' , $admins_array).')'; break;
					case 'assist' : $teachers = 'IN ('.implode(',' , $assistants_array).')'; break;
					case 'member' : $teachers = 'NOT IN ('.implode(',' , array_merge( array(0), $admins_array, $assistants_array ) ).')'; break;
				}
			}

			if(is_array($source['teacher_id']))
			{
				$teachers = 'IN ('.implode(',' , $source['teacher_id']).')';
			}

			if(is_numeric($source['teacher_id']))
			{
				$teachers = '= '.(int) $source['teacher_id'];
			}

			if(!is_null($teachers))
			{
				$query_string .= ' AND uh.teacher_id '.$teachers;
			}
		}

		if(isset($source[ORDER_BY]) && $source[ORDER_BY] != '')
		{
			$query_string .= ' ORDER BY '.mysqli_real_escape_string($_connection, $source[ORDER_BY]);
		}
		else
		{
			$query_string .= ' ORDER BY uh.homework_id';
		}

		// Clear Homeworks
		exec_query('DELETE n1 FROM `user_homeworks` n1, `user_homeworks` n2 WHERE n1.id < n2.id AND n1.user_id = n2.user_id AND n1.homework_id = n2.homework_id');

		// Unlock some homeworks
		$data_array = array(
			'teacher_id' => 0,
			'score_on' => 0,
			'score' => 0
		);

		update_query('user_homeworks', $data_array, 'score = -1 AND score_on <= "'.strtotime('now').'"');

		// Get User Homeworks
		$query_users = get_user_homeworks($query_string);
		$query_string = last_query();
		//pre_print($query_users);
		database_close();

		if($view == 'random')
		{
			if(count($query_users) > 0)
			{
				$random_hw = rand(0, count($query_users)-1);
				$_SESSION['RANDOM_HOMEWORK'] = $query_users[$random_hw]['id'];

				header('location:'.header_link( array(CONTROLLER => 'homeworks', ACTION => 'evaluate', ID => $query_users[$random_hw]['id']) ));
			}
			else
			{
				add_error_message('Съжалявам, но няма подходяща за вас домашна!');
				header('location:'.header_link( array(CONTROLLER => 'user', ACTION => 'view') ));
			}
			exit;
		}

		set('query_users', $query_users);
		set('query_string', $query_string);
	}

	function action_homeworks_evaluate()
	{
		Global $is_admin_user, $user_id, $source, $admins_array;

		if($is_admin_user || is_assistant() || (isset($_SESSION['RANDOM_HOMEWORK']) && $_SESSION['RANDOM_HOMEWORK'] == $source[ID]) )
		{
			database_open();

			$new_time = strtotime('now') + 30 * 60; // Current Time + 30 min.
			$search_params = remove_url_elements(array(CONTROLLER, ACTION, ID), $_SERVER['QUERY_STRING']);

			$query_homeworks = get_user_homeworks('id='.(int)$source[ID]);

			if(Only_One_Exists($query_homeworks))
			{
				$homework = reset($query_homeworks);

				$TIPS_ARRAY = array(
					'Подреди си кода по-добре!',
					'Забравен задължителен атрибут alt на тага img!',
					'Съгласно xHTML дори единичните тагове се затварят!',
					'Съгласно xHTML <br> се изписва <br />!',
					'Съгласно xHTML всички атрибути трябва да имат стойност - cheched="checked"!',
					'Справил си се чудесно!',
					'Аз вярвам, че можеш!'
				);

				$TAGS_ARRAY = get_all_tags();

				if(isset($_POST) && !empty($_POST))
				{
					//pre_print($_POST, true);

					if(($_POST['score'] <= 1 || $_POST['score'] > 100) && !$is_admin_user)
					{
						add_warning_message('Моля поставете оценка между 2 и 100% !');
					}

					if(!ctype_digit($_POST['score']) && !$is_admin_user)
					{
						add_warning_message('Оценката трябва да бъде цяло число между 2 и 100% !');
					}

					if( mb_strlen($_POST['score_arguments'], 'UTF-8') < 10 && (!$is_admin_user && !is_assistant()) )
					{
						add_warning_message('Моля добавете описание към оценката си!<br />Описанието трябва да бъде по-дълго от 10 символа :)');
					}

					if(!have_messages())
					{
						if(isset($_POST['selected_tips']) && count($_POST['selected_tips']) > 0)
						{
							$_POST['score_arguments'] .= PHP_EOL;
							foreach($_POST['selected_tips'] as $tip_id)
							{
								$_POST['score_arguments'] .= $TIPS_ARRAY[$tip_id].' ';
							}

							unset($_POST['selected_tips']);
						}

						if(isset($_POST['selected_tags']) && count($_POST['selected_tags']) > 0)
						{
							$_POST['score_arguments'] .= PHP_EOL;
							foreach($_POST['selected_tags'] as $tag_id)
							{
								$_POST['score_arguments'] .= $TAGS_ARRAY[$tag_id]['tag_name'].' ';
							}

							unset($_POST['selected_tags']);
						}

						//unset($_POST['score_arguments']);
						//unset($_POST['score']);

						if(in_array($homework['teacher_id'], $admins_array))
						{
							$check_lower = ($user_id <= $homework['teacher_id']);
						}
						else
						{
							$check_lower = true;
						}

						// Ако домашната не е оценена от никого, или правим корекция...
						if( true  ) // ($is_admin_user && $check_lower) || (0 == $homework['teacher_id'] || $user_id == $homework['teacher_id'])
						{
							// Ако домашната е заключена в момента от друг потребител
							if(-1 == $homework['score'] && $user_id != $homework['teacher_id'])
							{
								add_error_message('Съжaлявам! <br>Тази домашна е запазена за потребител №'.$homework['teacher_id'].' до '.date('Y-m-d H:i:s', $homework['score_on'] + TIME_CORRECTION ).'<br /><br /><a href="'.header_link(array(CONTROLLER => 'homeworks', ACTION => 'index', VIEW => 'random')).'" class="btn btn-danger">Разбрах, Дай ми друга!</a>');
							}
							else
							{
								$data_array = array(
									'teacher_id' => $user_id,
									'score_on' => strtotime('now'),
									'score_arguments' => $_POST['score_arguments'],
									'score' => $_POST['score']
								);

								if($is_admin_user && isset($_POST['edit_mode']) && $_POST['edit_mode'] == $TAGS_ARRAY[14]['tag_name']) // Ако администратора е  избрал "Допълване"
								{
									unset($data_array['teacher_id']);
								}

								backup_tables('user_homeworks');
								update_query('user_homeworks', $data_array, 'id = '.(int) $source[ID]);

								add_success_message('Домашната е '.($homework['score'] <= 0 ? 'оценена' : 'променена').' успешно!<br /><br /><a href="'.header_link(array(CONTROLLER => 'homeworks', ACTION => 'index', VIEW => 'random')).'" class="btn btn-success">Супер, Дай ми друга!</a>');

								if($homework['score'] <= 0) // Send Mail ?
								{
									if($homework['score'] == -1)
									{
										$mail = array(
											'mail_subject' => 'Вашата работа "'.$homework['homework_title'].'" бе оценена!',
											'mail_content' => 'Отидете на <a href="'.header_link(array(CONTROLLER => 'user', ACTION => 'homeworks', ID => $homework['course_id'])).'">списъка</a> с домашните и кликнете на оценката за повече информация',
											'created_from' => 0
										);
									}
									else
									{
										$mail = array(
											'mail_subject' => 'Разгледано възражение по домашна №'.$homework['id'],
											'mail_content' => 'Арбитърът отсъди: '.$_POST['score_arguments']
										);
									}

									$recipients = array();
									$recipients[] = $homework['user_id']; // Add User as Recipient

									if($homework['score'] < -1 && $homework['teacher_id'] > 0 && $homework['teacher_id'] != $user_id)
									{
										$recipients[] = $homework['teacher_id']; // Add Teacher as Recipient
									}

									insert_mail($mail, $recipients);
								}
							}
						}
						else
						{
							add_error_message('Не можете да променяте домашни, оценени от по-старши асистент!');
						}
					}
				}
				else
				{
					$data_array = array(
						'teacher_id' => $user_id,
						'score_on' => $new_time,
						'score' => -1
					);

					if(0 == $homework['score'] && ($homework['teacher_id'] == 0 || $homework['teacher_id'] == $user_id) )
					{
						update_query('user_homeworks', $data_array, 'id = '.(int) $source[ID]);
						$homework['teacher_id'] = $user_id;
					}

					if(-1 == $homework['score'])
					{
						if($homework['teacher_id'] == $user_id)
						{
							// Заключваме домашната за текущия потребител за 30 минути напред
							update_query('user_homeworks', $data_array, 'id = '.(int) $source[ID]);
						}
						else
						{
							add_error_message('Не можете да променяте заключени от друг домашни! <br>Тази домашна е запазена за потребител №'.$homework['teacher_id'].' до '.date('Y-m-d H:i:s', $homework['score_on'] + TIME_CORRECTION ).'<br /><br /><a href="'.header_link(array(CONTROLLER => 'homeworks', ACTION => 'index', VIEW => 'random')).'" class="btn btn-danger">Разбрах, Дай ми друга!</a>');
						}
					}
					elseif($user_id != $homework['teacher_id'] && !$is_admin_user)
					{
						add_error_message('Не можете да се бъркате в работата на другите!');
					}
				}

				set('query_homeworks', $query_homeworks);
				set('search_params', $search_params);
				set('TIPS_ARRAY', $TIPS_ARRAY);
				set('TAGS_ARRAY', $TAGS_ARRAY);
				set('new_time', $new_time);
			}
			else
			{
				add_error_message('Такава домашна не съществува!');
			}

			database_close();
		}
		else
		{
			header('location:'.link_to( array(CONTROLLER => 'user', ACTION => 'view', 'OPTION_SET_CLASS' => false, 'RHW' => $_SESSION['RANDOM_HOMEWORK']) ));
			exit;
		}
	}

	function action_homeworks_clear()
	{
		// Изчиства дублираните записи като оставя само последния (т.е. ако някой потребител качи домашната си по няколко пъти.

		Global $is_admin_user;

		if(!$is_admin_user && !is_assistant())
		{
			header('location:'.link_to( array(CONTROLLER => 'user', ACTION => 'view', 'OPTION_SET_CLASS' => false) ));
			exit;
		}
		else
		{
			database_open();
			exec_query('DELETE n1 FROM `user_homeworks` n1, `user_homeworks` n2 WHERE n1.id < n2.id AND n1.user_id = n2.user_id AND n1.homework_id = n2.homework_id');
			database_close();

			add_success_message('Дублиращите домашни бяха изчистени!');
			header('location:'.link_to( array(CONTROLLER => 'homeworks', ACTION => 'index', 'OPTION_SET_CLASS' => false) ));
			exit;
		}
	}

	function action_homeworks_unlock()
	{
		Global $is_admin_user;

		if(!$is_admin_user)
		{
			header('location:'.link_to( array(CONTROLLER => 'user', ACTION => 'view', 'OPTION_SET_CLASS' => false) ));
			exit;
		}
		else
		{
			$data_array = array(
				'teacher_id' => 0,
				'score_on' => 0,
				'score' => 0
			);

			database_open();
			update_query('user_homeworks', $data_array, 'score = -1');
			database_close();

			add_success_message('Всички домашни бяха отключени!');
			header('location:'.link_to( array(CONTROLLER => 'homeworks', ACTION => 'index', 'OPTION_SET_CLASS' => false) ));
			exit;
		}
	}

	function action_homeworks_add()
	{
		Global $source, $_connection, $is_admin_user;

		//$default_test_id = 5;

		if(!$is_admin_user)
		{
			page_not_access();
			exit;
		}

		database_open();

		if(!empty($_POST))
		{
			if(!isset($_POST['course_id']) || 0 == $_POST['course_id'])
			{
				add_error_message('Задължително трябва да изберете курс!');
			}

			if(!isset($_POST['homework_title']) || '' == $_POST['homework_title'])
			{
				add_error_message('Не сте въвел заглавие на домашната!');
			}

			if(!isset($_POST['homework_url']) || '' == $_POST['homework_url'])
			{
				add_error_message('Не сте въвел URL на домашната!');
			}


			if(isset($source[ID]))
			{
				$edit_homework = (int) $source[ID];
			}
			else
			{
				$edit_homework = 0;
			}

			if(!has_error_messages())
			{
				$data_array = array(
					'homework_title' => $_POST['homework_title'],
					'homework_url' => $_POST['homework_url'],
					'course_id' => (int) $_POST['course_id'],
					'for_office_only' => 0,
					'homework_type' => '',
					'weight' => 0,
					//'start_date' => date('Y-m-d H:i:s'),
					//'end_date' => date('Y-m-d H:i:s'),
					//'group_number' => -1,
				);

				if($_POST['is_final_exam'])
				{
					$data_array['weight'] = 0.25;
					$data_array['homework_type'] = 'final';
					$data_array['for_office_only'] = 1;
				}
				else
				{
					$data_array['weight'] = 0;
					$data_array['homework_type'] = 'current';
					$data_array['for_office_only'] = 0;
				}

				//pre_print($_POST);
				backup_tables('course_homeworks');
				backup_tables('objects_visibility');

				if(!$edit_homework)
				{
					$new_homework_id = insert_query('course_homeworks', $data_array);

					if($new_homework_id > 0)
					{
						add_success_message('Домашната е зададена успешно!');
					}
					else
					{
						add_error_message('Проблем при създаването на домашна!');
					}
				}
				else
				{
					$new_homework_id = $edit_homework;
					$result = update_query('course_homeworks', $data_array, 'homework_id = '.$edit_homework);

					if($result)
					{
						add_success_message('Промените са запазени успешно!');
						delete_query('objects_visibility', 'object_type = "homework" AND object_id = '.$source[ID], 0);
					}
					else
					{
						add_error_message('Неочакван проблем при запазване на промените!');
					}
				}

				$course_homeworks = get_all_homeworks('homework_type = "current" AND ch.course_id = '.(int) $_POST['course_id'].'');
				$weight = round( (0.2 / count($course_homeworks)) , 2);
				update_query('course_homeworks', array('weight' => $weight), 'course_id = '.(int) $_POST['course_id'].' AND homework_type = "current"');

				if(!has_error_messages())
				{
					if(isset($_POST['visibility']) && is_array($_POST['visibility']) && count($_POST['visibility']) > 0)
					{
						foreach($_POST['visibility'] as $visibility)
						{
							$data_visibility = array(
								'object_id' => $new_homework_id,
								'object_type' => 'homework',

								// Set Default Date Values
								'start_date' => date('Y-m-d H:i:s'),
								'end_date' => date('Y-m-d ', strtotime('+1 week')).'23:59:59'
							);

							$data_visibility['group_number'] = $visibility['group_number'];
							$data_visibility['start_date'] = $visibility['start_date'];
							$data_visibility['end_date'] = $visibility['end_date'];

							$new_visibility_id = insert_query('objects_visibility', $data_visibility);
						}
					}
				}

				
				if($new_homework_id  > 0)
				{
					header('location:'.header_link( array(CONTROLLER => 'homeworks', ACTION => 'add', ID => $new_homework_id) ) );
					exit;
				}
			}
		}

		database_close();
	}

	function action_homeworks_manage()
	{
		Global $is_admin_user, $source, $user_id, $_connection, $assistants_array, $admins_array;
		database_open();

		if(!$is_admin_user && !is_assistant())
		{
			header('location:'.link_to( array(CONTROLLER => 'user', ACTION => 'view', 'OPTION_SET_CLASS' => false) ));
			exit;
		}

		$query_string = '1=1';

		if(!isset($source['course_id']))
		{
			$source['course_id'] = 0;
		}

		if(isset($source['course_id']) && $source['course_id'] != '-1')
		{
			$courses_filter = $source['course_id'];
			if(0 == $courses_filter)
			{
				$courses_filter = array_column(getCurrentCourses(),'course_id');
			}

			if(is_numeric($courses_filter))
			{
				$query_string .= ' AND ch.course_id = '.(int) $courses_filter;
			}

			if(is_array($courses_filter) && count($courses_filter) > 0)
			{
				$query_string .= ' AND ch.course_id IN ('.implode(',', $courses_filter).')';
			}
		}

		if(isset($source['homework_type']) && $source['homework_type'] != '-')
		{
			$query_string .= ' AND ch.homework_type = "'.mysqli_real_escape_string($_connection, $source['homework_type']).'"';
		}

		if(isset($source['for_office_only']) && $source['for_office_only'] != '-1')
		{
			$query_string .= ' AND ch.for_office_only = '.(int) $source['for_office_only'];
		}
		else
		{
			$source['for_office_only'] = -1;
		}

		if(isset($source['weight']) && $source['weight'] != '')
		{
			$query_string .= ' AND ch.weight = '.($source['weight'] / 100);
		}

		if(isset($source[ORDER_BY]) && $source[ORDER_BY] != '')
		{
			$query_string .= ' ORDER BY '.mysqli_real_escape_string($_connection, $source[ORDER_BY]);
		}
		else
		{
			$query_string .= ' ORDER BY ch.homework_id';
		}

		// Get User Homeworks
		//$homeworks = get_all_homeworks('ch.course_id IN (SELECT course_id FROM courses WHERE start_date <= "'.$current_date.'" AND "'.$current_date.'" <= end_date) ORDER BY course_id, homework_title');
		$courses_homeworks = get_all_homeworks($query_string);
		$query_string = last_query();

		database_close();

		set('courses_homeworks', $courses_homeworks);
		set('query_string', $query_string);
	}

?>