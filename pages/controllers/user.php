<?php
	define_once('DEFAULT_MISSING_AVATAR', DIR_IMAGES.'ui/user-edit-icon-2729.png');
	define_once('DEFAULT_PRIVACY_AVATAR', DIR_IMAGES.'ui/key-png-login-15.png');

	// User Controller
	function action_user_index()
	{
		Global $is_admin_user;

		if(!$is_admin_user)
		{
		header('location:'.link_to(array(CONTROLLER => 'user', ACTION => 'view', 'OPTION_SET_CLASS' => false)));
		exit;
		}
	}

	function action_user_edit()
	{
		$look_for_user = look_for_user();
		if(!empty($_POST))
		{
			$user_data = $_POST;
			$required_fields = array('first_name', 'last_name', 'email', 'phone');
			check_required_fields($required_fields);

			if(!has_error_messages())
			{
				$update_data = array(
					'first_name' => $_POST['first_name'],
					'last_name' => $_POST['last_name'],
					'avatar' => $_POST['avatar'],
					'phone' => $_POST['phone'],
					'email' => $_POST['email'],
					'facebook' => $_POST['facebook'],
					'sex' => $_POST['sex'],
					'born_date' => $_POST['born_date'],
					//,'parent_id' => $_POST['come_from']
				);

				database_open();
				update_query('users', $update_data, set_where('user_id', '=', $look_for_user));
				update_query('user_settings', ['user_privacy' => serialize($_POST['privacy'])], set_where('user_id', '=', $look_for_user));
				add_success_message( translate('MSG_UPDATE_COMPLETE') );
				database_close();

				header('location:'.$_SERVER['HTTP_REFERER']);
				exit;
			}
		}
		else
		{
			// Зареждаме данните от Базата Данни
			database_open();
			$user_data = getUserData($look_for_user);

			if(Only_One_Exists($user_data))
			{
				$user_data = reset($user_data);
				$settings = reset(get_user_settings($look_for_user));
				$user_data['privacy'] = unserialize($settings['user_privacy']);
				//pre_print($user_data);
			}
			else
			{
				add_error_message(translate('MSG_USER_NOT_EXISTS'));
			}
			database_close();
		}

		//pre_print($user_data);
		set('user_data', $user_data);
		set('look_for_user', $look_for_user);
	}

	function action_user_manage()
	{
		Global $source, $is_admin_user, $user_id;

		if($is_admin_user) // $is_admin_user
		{
			database_open();

			//$courses_array = array();

			//if(isset($source[ID]))
			//{
				$load_user_id = ($is_admin_user && isset($source[ID]) ? (int) $source[ID] : $user_id);

				$user_info = to_assoc_array(exec_query('SELECT * FROM users WHERE user_id = '.$load_user_id));
				if(Only_One_Exists($user_info))
				{
					if(!empty($_POST))
					{
						if(isset($_POST['settings']) && is_array($_POST['settings']))
						{
							update_query('user_settings', $_POST['settings'], 'user_id = '.$load_user_id);
						}

						// Update current courses
						if(isset($_POST['cm']) && is_array($_POST['cm']) && count($_POST['cm']) > 0)
						{
							foreach($_POST['cm'] as $cm_id => $cm_data)
							{
								if(isset($cm_data['delete']))
								{
									delete_query('course_members', 'id = '.$cm_id);
								}
								else
								{
									$new_data = array(
										'role' => $cm_data['role'],
										'form_id' => $cm_data['form']
									);

									switch($cm_data['form'])
									{
										case '1' : $new_data['group_number'] =  1; break;
										case '2' : $new_data['group_number'] =  0; break;
										default  : $new_data['group_number'] = -1; break;
									}

									update_query('course_members', $new_data, 'id = '.$cm_id);
								}
								// TO DO: Add User Activity Log...
							}
						}

						// Add New Courses
						if(isset($_POST['new_course']) && is_array($_POST['new_course']) && count($_POST['new_course']) > 0)
						{
							foreach($_POST['new_course'] as $course)
							{
								$new_data = array(
									'role' => $course['role'],
									'user_id' => $load_user_id,
									'form_id' => $course['form'],
									'course_id' => $course['course'],
									'created_on' => date('Y-m-d H:i:s'),
									'created_from' => $user_id
								);

								switch($course['form'])
								{
									case '1' : $new_data['group_number'] =  1; break;
									case '2' : $new_data['group_number'] =  0; break;
									default  : $new_data['group_number'] = -1; break;
								}

								insert_query('course_members', $new_data);

								// TO DO: Add User Activity Log...
							}
						}

						if(isset($_POST['week_question']))
						{
							foreach($_POST['week_question'] as $k => $v)
							{
								$_POST['week_question'][$k] = htmlspecialchars_decode($v);
							}

							$set_array = array('story_data' => serialize($_POST['week_question']), 'created_on' => date('Y-m-d'));
							$where_clause = 'story_type = "week" AND user_id = ' . (int) $load_user_id;

							if($is_admin_user)
							{
								$set_array['published_on'] = $_POST['published_on'] ;
							}
							else
							{
								$where_clause .= ' AND (published_on = "" OR published_on = "0000-00-00")';
							}

							update_query('stories', $set_array, $where_clause);
						}

						add_success_message('Данните са запазени успешно!');
					}

					// Load data
					$user_info = reset($user_info);

					$courses_array = ($is_admin_user ? get_user_courses($load_user_id) : null);
					$settings_array = ($is_admin_user ? get_user_settings($load_user_id) : null);
					$story_data = get_user_story($load_user_id, 'week');
					$questions_array = get_story_questions('week');

					set('user_info', $user_info);
					set('story_data', $story_data);
					set('load_user_id', $load_user_id);
					set('courses_array', $courses_array);
					set('settings_array', $settings_array);
					set('questions_array', $questions_array);

				}
				else
				{
					add_error_message('User Not Exists!');
				}
			//}
			//else
			//{
			//	add_error_message('ID param is required!');
			//}

			database_close();
		}
		else
		{
			page_not_access();
		}
	}

	function action_user_view()
	{
		Global $source, $is_admin_user, $user_id;

		check_login();
		database_open();
		$_SESSION['FIX_FOOTER'] = false;

		if(user_can('SEE_PROFILE_VIEW'))
		{
			$look_for_user = look_for_user();

			// Зареждаме данните
			$user_data = getUserData($look_for_user);

			if(Only_One_Exists($user_data))
			{
				$user = array();

				$user['data'] = reset($user_data);
				//pre_print($user);

				if('0000-00-00 00:00:00' != $user['data']['born_date'])
				{
					$user['data']['born_date'] = date('d F Y', strtotime($user['data']['born_date'])); //'<a href="#" target="_blank">'.date('d F Y', strtotime($user['data']['born_date'])).'</a>';
				}
				else
				{
					$user['data']['born_date'] = '';
				}

				$user['data']['email'] = '<a href="mailto:'.$user['data']['email'].'" target="_blank">'.$user['data']['email'].'</a>';
				$user['data']['facebook'] = (substr($user['data']['facebook'], 0, 4) == 'http' ? '<a href="'.$user['data']['facebook'].'" target="_blank">'.$user['data']['facebook'].'</a>' : '');
				$user['data']['avatar'] = (DEFAULT_MISSING_AVATAR != $user['data']['avatar'] ? $user['data']['avatar'] : '');
				$user['data']['phone'] = ('' != $user['data']['phone'] ? '<a href="tel:'.$user['data']['phone'].'">'.$user['data']['phone'].'</a>' : '');

				$scripts = array();
				if($look_for_user == $user_id)
				{
					$button_left = '<a href="#" data-toggle="modal" data-target="#mails_modal"><button class="w48 btn btn-info pull-left" id="mailbox_button"><i class="glyphicon glyphicon-envelope"></i> Поща <span class="badge" id="badge_mails_count"></span></button></a>';
					$button_right = '<button class="w48 btn btn-success pull-right" id="" onclick="alert(\'Comming Soon\');"><i class="fa fa-search"></i> Приятели</button>';
					$show_tabs = true;

					$scripts[] = JS_Refresh_Mails();
					$scripts[] = JS_Restore_Mails();
					$scripts[] = JS_Delete_Mails();
					$scripts[] = JS_Load_Mails();

					check_user_fields($user);
				}
				else
				{
					$friendship = array(
						0 => ['n_text' => "<i class='fa fa-plus'></i> Добави приятел", 'h_text' => "<i class='fa fa-plus'></i> Добави приятел", 'n_class' => 'btn-success', 'h_class' => 'btn-info'],
						1 => ['n_text' => "Приятели <i class='fa fa-question'></i> ", 'h_text' => "<i class='fa fa-check'></i> Приемане", 'n_class' => 'btn-info', 'h_class' => 'btn-primary'],
						2 => ['n_text' => "<i class='fa fa-check'></i> Приятели", 'h_text' => "<i class='fa fa-minus'></i>  Премахване", 'n_class' => 'btn-primary', 'h_class' => 'btn-danger']
					);

					$friend_level = 0;
					$button_left = '<a href="#" data-toggle="modal" data-target="#mails_modal"><button class="btn btn-info pull-left" style="width:48%;" id="mailbox_button"><i class="fa fa-comment"></i> Пиши ми </button></a>';
					$button_right = '<button class="btn '.$friendship[$friend_level]['n_class'].' pull-right" style="width:48%;" id="btn_friend_toggle">'.$friendship[$friend_level]['n_text'].'</button>';
					$show_tabs = false;

					//$scripts[] = ;

					// Load Privacy
					$settings = reset(get_user_settings($look_for_user));
					$privacy = unserialize($settings['user_privacy']);
				//pre_print($privacy);
				//pre_print($user['data']);

					$my_friends = getUserFriends($look_for_user);
					$is_friend = isset( $my_friends[$user_id] );

					foreach($user['data'] as $field => &$value)
					{
						if(!isset($privacy[$field]))
						{
							$privacy[$field] = 1; // Ако не е зададена такава опция, понеже е нова, я задаваме ръчно = 1, т.е. видимо само за самият потребител
						}

						$pf = abs($privacy[$field]);
						if((1 == $pf) || (!$is_friend && 2 == $pf))
						{
							switch($field)
							{
								//case 'avatar': $value = DEFAULT_PRIVACY_AVATAR; break;
								//default: $value = '<i>'.($pf == 1 ? 'не казвам' : 'само за приятели').'</i>';
							}
						}
					}
				}

				set('user', $user);
				set('$show_tabs', $show_tabs);
				set('$button_left', $button_left);
				set('$button_right', $button_right);
				set('$friend_level', $friend_level);
				set('$friendship', $friendship);
				set('$scripts', $scripts);
			}
			else
			{
				add_error_message(translate('MSG_USER_NOT_EXISTS'));
			}

			set('look_for_user', $look_for_user);
		}
		else
		{
			page_not_access();
		}

	}

	function action_user_info()
	{
		Global $source, $is_admin_user;

		if($is_admin_user)
		{
			if(isset($source[ID]))
			{
				database_open();
				$record = get_registration_info((int) $source[ID]);
				database_close();

				if(Only_One_Exists($record))
				{
					$make_bolder_fields = array(
						'Име',
						'Email',
						'Рождена дата (mm/dd/yy)',
						'Регистриран',
						'IP Address',
						'Снимка',
					);

					$record = reset($record);

                    $record['data'] = unserialize($record['data']);
                    //pre_print($record);

                    $data_array = array();
                    foreach($record['data'] as $row)
                    {
						if(trim($row['value']) != '')
						{
							if(substr($row['value'],0,7) == 'http://')
							{
								$row['value'] = '<a href="'.$row['value'].'">'.$row['value'].'</a>';
							}

							if(in_array(trim($row['name']), $make_bolder_fields))
							{
								$row['name'] = '<b>'.$row['name'].'</b>';
							}

							$data_array[] = array(
								 $row['name'],
								 $row['value'],
							);
						}
                    }

                    $data_array[] = array(
                        '<b>Регистриран</b>',
                        $record['date_submitted']
                    );

                    $data_array[] = array(
                        '<b>IP Address</b>',
                        $record['ip_address']
                    );

                    set('data_array', $data_array);
					set('record', $record);
				}
			}
			else
			{
				echo 'ID е задължително!';
				exit;
			}
		}
		else
		{
			page_not_access();
			exit;
		}
	}

	function action_user_MyCourses()
	{
		Global $source, $is_admin_user, $user_id;

		check_login();
		database_open();

		$courses_array = array();

		if(isset($source[ID]) && $is_admin_user)
		{
			$load_user_id = $source[ID];
		}
		else
		{
			$load_user_id = $user_id;
		}

		$my_courses = get_user_courses($load_user_id, 'role <> "teacher"');
		$courses_array = $my_courses;

		set('have_new_certificate', $have_new_certificate);
		set('mail_notifications', $mail_notifications);
		set('new_mails_count', $new_mails_count);
		set('courses_array', $courses_array);
		set('load_user_id', $load_user_id);
		set('message', $message);
		set('columns', $columns);
		database_close();
	}

	function action_user_MyCoursesOLD()
	{
		Global $source, $is_admin_user, $user_id, $wpdb;

		database_open();

		$courses_array = array();

		if(isset($source[ID]) && $is_admin_user)
		{
			$load_user_id = $source[ID];
		}
		else
		{
			$load_user_id = $user_id;
		}


		$my_courses = get_user_courses($load_user_id, 'role <> "teacher"');
		$as_teacher = get_teacher_courses($load_user_id);
		//override_function('site_url','$url','return $url;');
		//Pre_Print($as_teacher, 1);

		$course_filters = array(
			'currents' => array('enabled' => 0, 'label' => 'Активни'   ),
			'certific' => array('enabled' => 0, 'label' => 'Завършени' ),
			'upcoming' => array('enabled' => 0, 'label' => 'Предстоящи'),
			'finished' => array('enabled' => 0, 'label' => 'Приключили'),
			'rejected' => array('enabled' => 0, 'label' => 'Отхвърлени'),
			'archived' => array('enabled' => 0, 'label' => 'Архивирани')
		);

		$message = '';
		$is_member_of_course = false;
		$user_certificates = getCertificates(null, $load_user_id, 'course_id'); // Ключовете на масива са ID-тата на курсовете прародители. Тук беше: getCertificates(null, $load_user_id, 'ancestor_id')
		$my_vip_courses = getVipCoursesPerUser($load_user_id);
		$notifications = '';

		//pre_print($my_courses, 1);

		$icon_materials = '';
		$icon_homeworks = '';
		$have_new_certificate = false;

		foreach($my_courses as $course)
		{
			if($course['course_id'] == 8)
			{
				$is_member_of_course = true;
			}

			//if($is_admin_user){pre_print($course);}
			// Дефинираме масива предварително, за да сме сигурни, че независимо от статуса, последователността на полетата ще е еднаква.
			$tmp = array('title' => '---', 'status' => '---', 'input_exams' => '---', 'materials' => '---', 'home_works' => '---');

			if($course['role'] == 'candidate') //Кандидат за курса
			{
				$role_status = 'upcoming';
				$course_filters['upcoming']['enabled'] += 1;
				$status_info = 'Няма допълнителна информация';

				if($course['form_id'] == 1)
				{
					$status_info = 'Присъствено обучение';
					if($course['group_number'] == 1)
					{
						//$status_info = 'Присъствено в Пловдив';
					}

					if($course['group_number'] == 5)
					{
						//$status_info = 'Присъствено в Стара Загора';
					}
				}

				if($course['form_id'] == 2 && $course['group_number'] == 0)
				{
					$status_info = 'Онлайн обучение';
				}

				$tmp['title'] = '<a href="'.site_url($course['course_url']).'"><span class="">'.$course['course_name'].'</span></a>';
				$tmp['materials'] = '<span class="">Все още не сте приет!</span>';
				$tmp['home_works'] = '<span class="">Все още не сте приет!</span>';
				$tmp['status'] = (isset($my_vip_courses[$course['course_id']]) ? '<span class="red">СЕРИОЗЕН</span> ' : '').'<span class="green" title="'.$status_info.'">Кандидат '.$status_info.'</span>';

				$required_exams = getRequiredTestsPerCourse($course['course_id']);
				$taken_exams = getRequiredTestsPerUser($course['course_id'], $load_user_id);
				$input_exams = array();

				foreach($required_exams as $exam)
				{//pre_print($exam);
					if(isset($taken_exams[$exam['exam_id']]))
					{
					  //$input_exams[] = '<a target="_blank" href="'.link_to( array('OPTION_SET_CLASS' => false, CONTROLLER => 'exam', ACTION => 'view', ID => $taken_exams[$exam['exam_id']]['history_id']) ).'"><img src="'.DIR_IMAGES.'ui/1.png" width="10" /> '.$exam['test_name'].' - '.$taken_exams[$exam['exam_id']]['score'].'%</a>';
						$input_exams[] = '<a target="_blank" href="'.link_to( array('OPTION_SET_CLASS' => false, CONTROLLER => 'exam', ACTION => 'view', ID => $taken_exams[$exam['exam_id']]['history_id']) ).'" class=""><img src="'.DIR_IMAGES.'ui/test_icon.png" /> '.$exam['test_name'].' - '.$taken_exams[$exam['exam_id']]['score'].'%</a>';
					}
					else
					{
					  //$input_exams[] = '<a href="'.link_to( array('OPTION_SET_CLASS' => false, CONTROLLER => 'exam', ACTION => 'solve', ID => $exam['exam_id']) ).'"><img src="'.DIR_IMAGES.'ui/0.png" width="10" /> '.$exam['test_name'].' - Реши сега!</a>';
						$input_exams[] = '<a href="'.link_to( array('OPTION_SET_CLASS' => false, CONTROLLER => 'exam', ACTION => 'solve', ID => $exam['exam_id']) ).'" class=""><img src="'.DIR_IMAGES.'ui/test_icon.png" /> '.$exam['test_name'].' - Реши сега!</a>';
					}
				}

				$tmp['input_exams'] = implode('<hr style="margin:0px !important; height:8px !important; border:0px;" />', $input_exams);

				//$tmp['materials'] = 'Изберете формата на обучение, която желаете?<br><label class=""><input type="radio" name="form_id['.$course['course_id'].']" value="1"> Присъствена</label><br><label class=""><input type="radio" name="form_id['.$course['course_id'].']" value="2"> Онлайн</label>';

				if( in_array( $course['parent_id'], array_column($user_certificates, 'ancestor_id')) ) // Warning: in_array( $what_value, $into_array)... !!!
				{
					if(true)
					{
						$tmp['input_exams'] = 'Изберете желаната форма на обучение и натиснете бутона.';
						$tmp['materials'] = 'Желана от вас формата<br><select name="form_id['.$course['course_id'].']" id="form_id_'.$course['course_id'].'" class=""><option value="0">--Изберете--</option><option value="1" data-group="1"> Присъствено обучение</option><option value="2" data-group="0"> Онлайн обучение</option></select>';
						$tmp['home_works'] = '<button class=" add_vip_person" data-id="'.$course['course_id'].'">Запази ми място</button>'; // Имам сертификат<br>
					}
				}
			}
			elseif($course['role'] == 'student') //Потребител успешно справил се с входните тестове
			{
				//pre_print($course);
				if($course['teacher_id'] == $load_user_id)
				{
					$course['role'] = 'teacher';
				}
				// =======================================

				$status_info = 'активен';
				if($course['form_id'] == 1) // && $course['group_number'] == 1)
				{
					$status_info = 'Приет присъствено обучение';
				}

				if($course['form_id'] == 2) // && $course['group_number'] == 0)
				{
					$status_info = 'Приет онлайн обучение';
				}

				$status_info .= (isset($my_vip_courses[$course['course_id']]) ? '<br /><span class="red"'.($my_vip_courses[$course['course_id']]['end_date']  < date('Y-m-d') ? ' style="text-decoration: line-through;"' : '').'>СЕРИОЗЕН до '.$my_vip_courses[$course['course_id']]['end_date'].'</span> ' : '');

				if($course['start_date'] <= date('Y-m-d') && date('Y-m-d') <= $course['end_date'])
				{
					$role_status = 'currents';
					$course_filters['currents']['enabled'] += 1;
				}
				else
				{
					if(date('Y-m-d') > $course['end_date'])
					{
						$role_status = 'finished';
						$course_filters['finished']['enabled'] += 1;
					}
					else
					{
						$role_status = 'upcoming';
						$course_filters['upcoming']['enabled'] += 1;
					}
				}

				$tmp['title'] = '<a href="'.site_url($course['course_after_url']).'"><span class="">'.$course['course_name'].'</span></a>';
				$tmp['status'] = '<span class="green" title="Активен курс">'.$status_info.'</span>';

				$required_exams = getTestsPerCourse($course['course_id']);
				$taken_exams = getActiveTestsPerUser($course['course_id'], $load_user_id);
				$input_exams = array();

				//if(is_admin_user()){pre_print($required_exams,1);}

				foreach($required_exams as $exam)
				{
					//pre_print($exam);
					if(have_permissions($load_user_id, $exam['exam_id']))
					{
						if(isset($taken_exams[$exam['exam_id']]))
						{
							$icon_suffix = (int) ($taken_exams[$exam['exam_id']]['score'] >= 60);
							//$input_exams[] = '<a target="_blank" href="'.link_to( array('OPTION_SET_CLASS' => false, CONTROLLER => 'exam', ACTION => 'view', ID => $taken_exams[$exam['exam_id']]['history_id']) ).'"><img src="'.DIR_IMAGES.'ui/1.png" width="10" /> '.$exam['test_name'].' - '.$taken_exams[$exam['exam_id']]['score'].'%</a>';
							$input_exams[] = '<a target="_blank" href="'.link_to( array('OPTION_SET_CLASS' => false, CONTROLLER => 'exam', ACTION => 'view', ID => $taken_exams[$exam['exam_id']]['history_id']) ).'" class=""><img src="'.DIR_IMAGES.'ui/test_icon.png" /> '.$exam['test_name'].($exam['exam_type'] == 'poll' ? ' <img src="'.DIR_IMAGES.'ui/1.png" alt="1.png" width="10" />' : ' - '.$taken_exams[$exam['exam_id']]['score'].'% <img src="'.DIR_IMAGES.'ui/'.$icon_suffix.'.png" alt="status icon" width="10" />' ).'</a>';
						}
						else
						{
						  //$input_exams[] = '<a href="'.link_to( array('OPTION_SET_CLASS' => false, CONTROLLER => 'exam', ACTION => 'solve', ID => $exam['exam_id']) ).'"><img src="'.DIR_IMAGES.'ui/0.png" width="10" /> '.$exam['test_name'].' - Реши сега!</a>';
						  $input_exams[] = '<a href="'.link_to( array('OPTION_SET_CLASS' => false, CONTROLLER => 'exam', ACTION => 'solve', ID => $exam['exam_id']) ).'" class=""><img src="'.DIR_IMAGES.'ui/test_icon.png" /> '.$exam['test_name'].' - Реши сега!</a>';

						  //$input_exams[] = '<a href="#" class=""><img src="'.DIR_IMAGES.'ui/test_icon.png" /> '.$exam['test_name'].' - Закъсня!</a>';
						}
					}
				}

				$tmp['input_exams'] = implode('<hr style="margin:0px !important; height:8px !important; border:0px;" />', $input_exams);
				//if($role_status == 'finished'){ $tmp['input_exams'] = '<b><i style="color:silver;">Приключил</i></b>'; }

				$hw = get_all_homeworks('ch.course_id = '.$course['course_id'].' ORDER BY homework_id DESC LIMIT 1');

				$time_icon = '';
				if(Only_One_Exists($hw))
				{
					$hw = reset($hw);
					// title="'.getUserLastOnline($load_user_id).'"
					if(!haveHomeworks($load_user_id, $hw['homework_id']))
					{
						//$time_icon = '<span title="Все още не сте качили последната домашна! Побързайте!" class="glyphicon glyphicon-time btn btn-danger btn-xs"></span>';
					}
					else
					{
						$time_icon = ' style="color:gray;"';
					}
				}
				else
				{
					$time_icon = ' style="color:gray;"';
				}

				$url_materials = site_url($course['course_after_url']);
				$icon_materials = '<a href="'.$url_materials.'" title="Кликнете тук за да достъпите материалите" class="hidden-lg hidden-md"><img src="'.DIR_IMAGES.'ui/document_icon_n.png" alt="Materials" /></a>';
				$tmp['materials'] = '<a href="'.$url_materials.'" class="">Вижте материалите</a>';

				$url_homeworks = link_to( array('OPTION_SET_CLASS' => false, CONTROLLER => 'user', ACTION => 'homeworks', ID => $course['course_id']) ).($is_admin_user ? '&user_id='.$load_user_id : '');
				$icon_homeworks = '<a href="'.$url_homeworks.'" title="Кликнете тук за да достъпите домашните" class="hidden-lg hidden-md"'.$time_icon.'><span class="glyphicon glyphicon-check"></span></a>';
				$tmp['home_works'] = '<a href="'.$url_homeworks.'" class=""> Виж списък</a>';
			}
			elseif($course['role'] == 'rejected') //Прецакан потребител, който се е провалил на входните тестове
			{
				$role_status = 'rejected';
				$course_filters['rejected']['enabled'] += 1;

				$tmp['title'] = '<a href="'.site_url($course['course_url']).'"><span class="">'.$course['course_name'].'</span></a>';
				$tmp['status'] = '<span class="red">некласиран</span>';

				$required_exams = getRequiredTestsPerCourse($course['course_id']);
				$taken_exams = getRequiredTestsPerUser($course['course_id'], $load_user_id);
				$input_exams = array();

				foreach($required_exams as $exam)
				{
					//pre_print($exam);
					if(isset($taken_exams[$exam['exam_id']]))
					{
					  //$input_exams[] = '<a target="_blank" href="'.link_to( array('OPTION_SET_CLASS' => false, CONTROLLER => 'exam', ACTION => 'view', ID => $taken_exams[$exam['exam_id']]['history_id']) ).'"><img src="'.DIR_IMAGES.'ui/1.png" width="10" /> '.$exam['test_name'].' - '.$taken_exams[$exam['exam_id']]['score'].'%</a>';
						$input_exams[] = '<a target="_blank" href="'.link_to( array('OPTION_SET_CLASS' => false, CONTROLLER => 'exam', ACTION => 'view', ID => $taken_exams[$exam['exam_id']]['history_id']) ).'" class=""><img src="'.DIR_IMAGES.'ui/test_icon.png" /> '.$exam['test_name'].' - '.$taken_exams[$exam['exam_id']]['score'].'%</a>';
					}
					else
					{
					  //$input_exams[] = '<a href="'.link_to( array('OPTION_SET_CLASS' => false, CONTROLLER => 'exam', ACTION => 'solve', ID => $exam['exam_id']) ).'"><img src="'.DIR_IMAGES.'ui/0.png" width="10" /> '.$exam['test_name'].' - Реши сега!</a>';
						$input_exams[] = '<a href="#" class=""><img src="'.DIR_IMAGES.'ui/test_icon.png" /> '.$exam['test_name'].' - Нерешен!</a>';
					}
				}

				$tmp['input_exams'] = implode('<hr style="margin:0px !important; height:8px !important; border:0px;" />', $input_exams);
			}

			if($course['role'] == 'teacher') //Гаден злобен даскал || is_assistant()
			{
				//pre_print($course);
				if($course['form_id'] == 1 && $course['group_number'] == 1)
				{
					$status_info = 'Лектор';
				}

				if($course['form_id'] == 2 && $course['group_number'] == 0)
				{
					$status_info = 'Асистент';
				}

				//$course['course_name'] = str_replace('/frontend', 'FE', $course['course_after_url']);
				$tmp['title'] = '<a href="'.site_url($course['course_after_url']).'"><span class="">'.$course['course_name'].'</span></a>';
				$tmp['status'] = '<span class="green" title="Активен курс">'.$status_info.'</span>';

				$required_exams = getCurrentTestsPerCourse($course['course_id']);
				$taken_exams = getCurrentTestsPerUser($course['course_id'], $load_user_id);
				$input_exams = array();

				foreach($required_exams as $exam)
				{
					//pre_print($exam);
					if(true)
					{
						$tmp_exam  = '';
						$tmp_exam .= '<a href="'.link_to( array('OPTION_SET_CLASS' => false, CONTROLLER => 'exam', ACTION => 'statistic', ID => $exam['exam_id'], MOP_PARAM => '0') ).'" target="_blank"><button>&Sigma;</button></a> ';
						//$tmp_exam .= '<a href="'.link_to( array('OPTION_SET_CLASS' => false, CONTROLLER => 'question', ACTION => 'index', 'test_id' => $exam['test_id']) ).'""><button><span class="glyphicon glyphicon-question-sign"></span></button></a> ';

						if(isset($taken_exams[$exam['exam_id']]))
						{
						    $tmp_exam .= '<a target="_blank" href="'.link_to( array('OPTION_SET_CLASS' => false, CONTROLLER => 'exam', ACTION => 'view', ID => $taken_exams[$exam['exam_id']]['history_id']) ).'" class="" title="Виж теста!"><img src="'.DIR_IMAGES.'ui/1.png" alt="Виж теста!" />'.$exam['test_name'].' - '.$taken_exams[$exam['exam_id']]['score'].'%</a>';
						}
						else
						{
							if(have_permissions($load_user_id, $exam['exam_id']))
							{
								$tmp_exam .= '<a href="'.link_to( array('OPTION_SET_CLASS' => false, CONTROLLER => 'exam', ACTION => 'solve', ID => $exam['exam_id']) ).'" class="" title="Реши сега!"><img src="'.DIR_IMAGES.'ui/test_icon.png" alt="Реши сега!" />'.$exam['test_name'].'</a>';

							}
							else
							{
								$tmp_exam .= '<a href="#" class="" title="Късно е вече!"><img src="'.DIR_IMAGES.'ui/0.png" alt="Късно е!" />'.$exam['test_name'].'</a>';
							}
						}

						$input_exams[] =  $tmp_exam;
					}
				}

				$tmp['input_exams'] = implode('<hr style="margin:0px !important; height:8px !important; border:0px;" />', $input_exams);
				//if(!is_assistant()){$tmp['input_exams'] = '<span class="">Взети успешно</span>';}


				$tmp['materials'] = '<a href="'.site_url($course['course_after_url']).'" class="">Вижте материалите</a>';
				$tmp['home_works'] = '<a class="" href="'.link_to( array('OPTION_SET_CLASS' => false, CONTROLLER => 'homeworks', ACTION => 'index', 'course_id' => $course['course_id']) ).'"> <button>Проверка</button> </a>'; //'<a href="'.link_to( array('OPTION_SET_CLASS' => false, CONTROLLER => 'user', ACTION => 'homeworks', ID => $course['course_id']) ).'" class="">Вижте домашните</a>';
			}

			if($course['role'] == 'observer') // Наблюдател - Директор или друг вид надзирател :)
			{
				//pre_print($course);
				if($course['form_id'] == 1 && $course['group_number'] == 1)
				{
					$status_info = 'Наблюдател';
				}

				if($course['form_id'] == 2 && $course['group_number'] == 0)
				{
					$status_info = 'Наблюдател';
				}

				//$course['course_name'] = str_replace('/frontend', 'FE', $course['course_after_url']);
				$tmp['title'] = '<a href="'.site_url($course['course_after_url']).'"><span class="">'.$course['course_name'].'</span></a>';
				$tmp['status'] = '<span class="green" title="Активен курс">'.$status_info.'</span> ';

				$now = date('Y-m-d');

				$user_button = '<button><span class="glyphicon glyphicon-user"></span></button>';

				// Времето за кандидатстване е изтекло, но курсът не е почнал все още
				if($course['candidate_until'] <= $now && $now <= $course['start_date'])
				{
					// Показваме абсолютно всички потребители към този курс
					$user_button = '<a href="'.link_to( array(CONTROLLER => 'user', ACTION => 'index', 'course_id' => $course['course_id'], 'OPTION_SET_CLASS' => false, 'is_send' => 1, MOP_PARAM => 0) ).'">'.$user_button.'</a>';
				}
				else
				{
				if ($now < $course['candidate_until'])
				{ // Времето за кандидатстване не е приключило още. Показваме само кандидатите за този курс
						$user_button = '<a href="'.link_to( array(CONTROLLER => 'user', ACTION => 'index', 'course_id' => $course['course_id'], 'OPTION_SET_CLASS' => false, 'is_send' => 1, MOP_PARAM => 0, 'role' => 'candidate') ).'">'.$user_button.'</a>';
					}
				else
				{ // Курсът вече е започнал... Показваме всички приети курсисти
						$user_button = '<a href="'.link_to( array(CONTROLLER => 'user', ACTION => 'index', 'course_id' => $course['course_id'], 'OPTION_SET_CLASS' => false, 'is_send' => 1, MOP_PARAM => 0, 'role' => 'student') ).'">'.$user_button.'</a>';
					}
				}

				$tmp['status'] .= '<a href="'.header_link(array(CONTROLLER => 'course', ACTION => 'add', ID => $course['course_id'])).'"><button type="button"><img src="'.DIR_IMAGES.'/ui/calendar_icon.png" alt="Calendar" title="Дати и присъствия" height="16" style="position:relative; top:-1px;" /></button></a>';
				$tmp['status'] .= $user_button;

				$required_exams = getCurrentTestsPerCourse($course['course_id']);
				$taken_exams = getCurrentTestsPerUser($course['course_id'], $load_user_id);
				$input_exams = array();

				foreach($required_exams as $exam)
				{
					//pre_print($exam);
					if(true)
					{
						$tmp_exam  = '';
						$tmp_exam .= '<a href="'.link_to( array('OPTION_SET_CLASS' => false, CONTROLLER => 'exam', ACTION => 'statistic', ID => $exam['exam_id'], MOP_PARAM => '0') ).'" target="_blank"><button>&Sigma;</button></a> ';
						//$tmp_exam .= '<a href="'.link_to( array('OPTION_SET_CLASS' => false, CONTROLLER => 'question', ACTION => 'index', 'test_id' => $exam['test_id']) ).'""><button><span class="glyphicon glyphicon-question-sign"></span></button></a> ';

						if(isset($taken_exams[$exam['exam_id']]))
						{
						    $tmp_exam .= '<a target="_blank" href="'.link_to( array('OPTION_SET_CLASS' => false, CONTROLLER => 'exam', ACTION => 'view', ID => $taken_exams[$exam['exam_id']]['history_id']) ).'" class="" title="Виж теста!"><img src="'.DIR_IMAGES.'ui/1.png" alt="Виж теста!" />'.$exam['test_name'].' - '.$taken_exams[$exam['exam_id']]['score'].'%</a>';
						}
						else
						{
							if(have_permissions($load_user_id, $exam['exam_id']))
							{
								$tmp_exam .= '<a href="'.link_to( array('OPTION_SET_CLASS' => false, CONTROLLER => 'exam', ACTION => 'solve', ID => $exam['exam_id']) ).'" class="" title="Реши сега!"><img src="'.DIR_IMAGES.'ui/test_icon.png" alt="Реши сега!" />'.$exam['test_name'].'</a>';

							}
							else
							{
								$tmp_exam .= '<a href="#" class="" title="Късно е вече!"><img src="'.DIR_IMAGES.'ui/0.png" alt="Късно е!" />'.$exam['test_name'].'</a>';
							}
						}

						$input_exams[] =  $tmp_exam;
					}
				}

				$tmp['input_exams'] = implode('<hr style="margin:0px !important; height:8px !important; border:0px;" />', $input_exams);
				//if(!is_assistant()){$tmp['input_exams'] = '<span class="">Взети успешно</span>';}


				$tmp['materials'] = '<a href="'.site_url($course['course_after_url']).'" class="">Вижте материалите</a>';
				$tmp['home_works'] = '<a class="" href="'.link_to( array('OPTION_SET_CLASS' => false, CONTROLLER => 'homeworks', ACTION => 'index', 'course_id' => $course['course_id']) ).'"> <button>Проверка</button> </a>'; //'<a href="'.link_to( array('OPTION_SET_CLASS' => false, CONTROLLER => 'user', ACTION => 'homeworks', ID => $course['course_id']) ).'" class="">Вижте домашните</a>';
			}

			// Ако курсиста има сертификат за този прародител, проверяваме дали сертификата е точно за този курс
			//if(isset($user_certificates[$course['descendant_of']]) && $user_certificates[$course['descendant_of']]['course_id'] == $course['course_id'])
			if(isset($user_certificates[$course['course_id']]))
			{
				if(file_exists($user_certificates[$course['course_id']]['file_path']))
				{
					$cert_icon = '<span title="Имате сертификат за този курс. Кликнете за да го видите!" class="glyphicon glyphicon-certificate" style="color:yellow; border:2px dotted yellow; border-radius:50%; padding:2px 1px 2px 2px; background-color:red;"></span>';
					$cert_icon = '<a href="'.site_url($user_certificates[$course['course_id']]['file_path']).'">'.$cert_icon.'</a>';
				}
				else
				{
					$cert_icon = '<span title="Имате сертификат за този курс. В момента подготвяме файла!" class="glyphicon glyphicon-certificate" style="color:lime; border:2px dotted lime; border-radius:50%; padding:2px 1px 2px 2px; background-color:blue;"></span>';

					$have_new_certificate = true;
				}

				$tmp['title'] =  $cert_icon.' '.$tmp['title'];

				$role_status = 'certific';
				$course_filters['certific']['enabled'] += 1;
				$course_filters['finished']['enabled'] -= 1;
			}

			$tmp['status'] .= '<i class="course_'.$role_status.'"></i>';
			$tmp['title'] .= '<br />'.$tmp['status'].'<br />'.$icon_materials.' '.$icon_homeworks;
			unset($tmp['status']);
			$courses_array[] = $tmp;
		}

		//=================================== BEGIN MAIL FUNCTIONALITY ===================================

//<span class="glyphicon glyphicon-refresh" id="btn_refresh_mails" style="float: right;"></span>


		//=================================== END OF MAIL FUNCTIONALITY ===================================


		if(!$is_member_of_course && date('Y-m-d') <= '2016-08-30')
		{
			$courses_array[] = array('<a href="'.header_link(array(CONTROLLER => 'course', ACTION => 'AddToUser', ID => 8, 'role' => 'student', 'form_id' => 2, 'group' => 0)).'" class="btn btn-success">Запиши ме за конкурса</a>', '','','','');
		}

		if(date('Y-m-d') <= '2017-04-30' && count($my_courses) == 0)
		{
			$courses_array[] = array('<a href="'.site_url('/courses/').'" class="btn btn-success">Избери курс</a>', '','','','');
		}

		//if($user_id == 15)<i class="fa fa-caret-down"></i>
		if(count($user_certificates) > 0 || $course_filters['currents']['enabled'])
		{
			$courses_array[] = array('<a href="'.header_link(array(CONTROLLER => 'homeworks', ACTION => 'index', VIEW => 'random')).'" class="btn btn-success">Провери домашна - Получи Бонус!</a>', '','','','');
		}

		$filter_courses = '<span class="button-dropdown" data-buttons="dropdown">
                    <a href="#" class="button button-rounded button-tiny"><span class="glyphicon glyphicon-filter"></span> Филтрирай</a>

                    <ul class="button-dropdown-menu-below" style="display: none; width:160px;">';

		foreach($course_filters as $key => $filter)
		{
            if($filter['enabled'])
			{
				$filter_courses .= '<li class="button-dropdown-divider"><label style="margin-left:10px;"><input type="checkbox" class="filter_courses" id="course_'.$key.'" value="course_'.$key.'" checked="checked" /> '.$filter['label'].'</label></li>';
			}
		}

		$filter_courses .= '
					</ul>
                </span>';

		$columns = array(
			$filter_courses.' Име на курса ',
			//array('text' => 'Статус', 'attr' => 'width="175"'),
			array('text' => 'Изпити', 'attr' => 'class="hidden-xs hidden-sm"'), // , 'attr' => 'width="322"'
			array('text' => 'Материали', 'attr' => 'width="160" class="hidden-xs hidden-sm"'), // 160 - 222
			array('text' => 'Домашни', 'attr' => 'width="130" class="hidden-xs hidden-sm"')    // 100 - 130
		);

		//if($user_id == 1){Pre_Print($courses_array);} exit;
		set('have_new_certificate', $have_new_certificate);
		set('mail_notifications', $mail_notifications);
		set('new_mails_count', $new_mails_count);
		set('courses_array', $courses_array);
		set('load_user_id', $load_user_id);
		set('message', $message);
		set('columns', $columns);
		database_close();
	}

	function action_user_HomeWorks()
	{
		Global $source, $is_admin_user, $user_id, $_connection;

		if(isset($_GET['user_id']) && is_super_admin())
		{
			$load_user_id = $_GET['user_id']; //get_query_var('id','2','news');
		}
		else
		{
			//get_currentuserinfo();
			$load_user_id = get_current_user_id();
		}

		if(isset($source[ID]))
		{
			$course_id = $source[ID];
		}
		else
		{
			echo 'Course_ID is required field!'; exit;
		}

		database_open();

		$TAGS_ARRAY = get_all_tags();

		//$course = to_assoc_array(exec_query('SELECT * FROM course_members AS cm LEFT JOIN courses AS c ON cm.course_id = c.course_id WHERE cm.role = "student" AND cm.course_id = '.(int) $course_id.' AND cm.user_id = '.(int) $load_user_id));
		$course = to_assoc_array(exec_query('SELECT * FROM course_members AS cm LEFT JOIN courses AS c ON cm.course_id = c.course_id WHERE cm.course_id = '.(int) $course_id.' AND cm.user_id = '.(int) $load_user_id));
		if(Only_one_exists($course))
		{
			$course = reset($course);
			//pre_print($course);

			if($course['role'] != 'student' && $course['role'] != 'teacher')
			{
				page_not_access();
			}
		}
		else
		{
			page_not_access();
		}

//		$user_data = to_assoc_array(exec_query('SELECT user_id, group_number FROM course_members WHERE user_id = ' . (int) $user_id));
//		pre_print($user_data);

		// Look Into table "course_homeworks_tags"
		$report_tag_begin = getTagData(13, 'tag_name');
		$report_tag_end = getTagData(16, 'tag_name');

		$decrypt_tag_begin = getTagData(19, 'tag_name');
		$decrypt_tag_end = getTagData(20, 'tag_name');

		// Report is submited!
		if(!empty($_POST))
		{
			if(isset($_POST['uh_id']))
			{
				$uh_id = (int) decrypt($_POST['uh_id']);
				$homework = get_user_homeworks('uh.id = '.$uh_id);

				if(Only_One_Exists($homework))
				{
					$homework = reset($homework);
					//pre_print($homework);

					if($homework['user_id'] == $user_id)
					{
						if(strpos($homework['score_arguments'], $report_tag_begin) === false)
						{
							$new_data = array(
								'score' => anti_abs($homework['score']),
								'score_arguments' => $homework['score_arguments'].PHP_EOL.'Поставена оценка '.abs($homework['score']).'% от Потребител '.$decrypt_tag_begin.encrypt($homework['teacher_id']).$decrypt_tag_end.'.'.$report_tag_begin.mysqli_real_escape_string($_connection, $_POST['report_text']).$report_tag_end
							);

							update_query('user_homeworks', $new_data, 'id = '.$uh_id);
							add_success_message('Домашната е докладвана успешно!');
						}
						else
						{
							add_error_message('Ти вече си се оплакал... Не можеш да се оплакваш отново!');
						}
					}
					else
					{
						add_error_message('НЕ ПИПАЙТЕ ЧУЖДИ ДОМАШНИ!');
					}
				}
				else
				{
					add_error_message('Домашна №'.$uh_id.' НЕ съществува!');
				}
			}
			else
			{
				add_error_message('Липсва поле за сигурност #'.__LINE__.'!');
			}
		}

		$hw_array = array();
		$homeworks = getHomeworksVisibility('course_id = '.(int) $course_id . ' AND group_number=' . (int) $course['group_number'].' ORDER BY homework_title DESC');
		$user_homeworks = getHomeworksPerUser($load_user_id);

		
		$now = strtotime('now') + TIME_CORRECTION;

		foreach($homeworks as $hw)
		{
			$hw['start_date'] = strtotime($hw['start_date']);
			$hw['end_date'] = strtotime($hw['end_date']);

			$hw['homework_url_title'] = explode('/',$hw['homework_url']);
			$hw['homework_url_title'] = end($hw['homework_url_title']);

			// Точката в началото на файла със заданието показва, че той е само за премиум акаунтите.
			// Точката се поставя само във формата за добавяне на домашна, но физическия файл не започва с точка!
			$is_vip_homework = (substr($hw['homework_url_title'],0,1) == '.');
			$is_vip_for_homework = getVipStatus($course_id, date('Y-m-d', $hw['start_date']), $load_user_id);
			$ip_check = ( ($hw['for_office_only'] == 1 && is_office_ip()) || $hw['for_office_only'] == 0);

			$tmp = array(
				'title' => '<a href="#"><span class="">'.$hw['homework_title'].'</span></a>',
				'descr' => '<span class="">Няма прикачено задание</span>',
				//'start_date' => date('d.m.Y г.', $hw['start_date']),
				//'end_date' => date('d.m.Y, H:i ', $hw['end_date']),
				'my_hw' => '',
				'score' => ''
			);


			if($is_vip_homework)
			{
				if(!REQUIRE_VIP || $is_vip_for_homework)
				{
					$hw['homework_url_title'] = substr($hw['homework_url_title'], 1);

					$hw['homework_url'] = explode('/', $hw['homework_url']);
					$hw['homework_url'][count($hw['homework_url'])-1] = $hw['homework_url_title'];
					$hw['homework_url'] = implode('/', $hw['homework_url']);
				}
				else
				{
					$hw['homework_url'] = '#';
					$hw['homework_url_title'] = 'Premium задание';

					$tmp['my_hw'] = '<span class="">Само за закупилите <a href="#">premium достъп</a>.</span>';
					$tmp['descr'] = '<a href="#" class="red"><img class="wpba-document" src="'.SITE_URL.'/'.LOCAL_PATH.'/images/ui/document_icon_n.png" /> Premium задание</a>';
					$tmp['score'] = (PROMOTE_VIP ? '<button>Купи Premium</button>' : '');
				}
			}

			if($ip_check)
			{
				if(isset($hw['homework_url']) && $hw['homework_url'] != '')
				{
					$tmp['descr'] = '<a href="'.$hw['homework_url'].'" class="'.($is_vip_homework && !(!REQUIRE_VIP || $is_vip_for_homework) ? 'red' : 'link').'"><img class="wpba-document" src="'.SITE_URL.'/'.LOCAL_PATH.'/images/ui/document_icon_n.png" /> '.$hw['homework_url_title'].'</a>';
				}
			}
			else
			{
				$tmp['descr'] = '<span class="">Недостъпно задание</span>';
			}

			//$tmp['my_hw'] = (false ? '<a href="#" class="link"><img class="wpba-document" src="'.SITE_URL.'/'.LOCAL_PATH.'/images/ui/document_icon_n.png" /> Моето домашно</a>' : '<a href="'.header_link( array(CONTROLLER => 'user', ACTION => 'uploadhomework', ID => 1) ).'user_id='.$user_id.'"><button class="btn btn-ar btn-block button button-border-primary button-rounded upload_button">Качи домашна <img src="'.SITE_URL.'/'.LOCAL_PATH.'/images/ui/upload_homework.png" /></button></a>');
			//$tmp['start-date'] = ;
			//$tmp['end-date'] = ;

			if($is_admin_user || ($hw['start_date'] <= $now && $now <= $hw['end_date'] && $ip_check))
			{
				$upload_url = '/course/?c=user&a=uploadhomework&id='.$hw['homework_id'].'&user_id='.$load_user_id.'&course='.$course_id.(isset($_REQUEST['layout']) ? '&layout='.$_REQUEST['layout'] : '');
				$date_is_ok = true;
				$show_homework = true;
			}
			else
			{
				// ТУК НЯМА ГРЕШКА! Това ще се преобразува до: <a href="#" data-toggle="modal" data-target="#myModal">
				$upload_url = '#" data-toggle="modal" data-target="#NonActiveHWModal';
				$date_is_ok = false;
				$show_homework = true;

				if($now < $hw['start_date'])
				{
					$show_homework = false;
				}
			}

			if(isset($user_homeworks[$hw['homework_id']])) // Домашната е качена
			{
				//$download_link = str_replace('/uploads/homeworks/', '/uploads/homeworks/', $user_homeworks[$hw['homework_id']]['upload_name']);
				$download_link = SITE_URL.$user_homeworks[$hw['homework_id']]['upload_name'];
				//die($download_link);
				if($ip_check)
				{
					$tmp['my_hw'] = '<a href="'.$download_link.'" class="link"><button class="btn btn-ar button button-border-primary button-rounded upload_button'.($user_homeworks[$hw['homework_id']]['score'] != 0 ? ' btn-block' : '').'"><img class="wpba-document" src="'.SITE_URL.'/'.LOCAL_PATH.'/images/ui/document_icon_n.png" /> Моето домашно</button></a> ';
				}
				else
				{
					$tmp['my_hw'] = '<a href="#" class="link"><button class="btn btn-ar button button-border-primary button-rounded upload_button'.($user_homeworks[$hw['homework_id']]['score'] != 0 ? ' btn-block' : '').'" style="color:red !important;">Недостъпен файл</button></a> ';
				}

				if($user_homeworks[$hw['homework_id']]['score'] != 0)
				{
					$tmp['score'] = '<a href="#" data-toggle="modal" data-target="#ArgumentsModal_'.$hw['homework_id'].'"><span class="green">'.($user_homeworks[$hw['homework_id']]['score'] != -1 ? abs($user_homeworks[$hw['homework_id']]['score']).' ' : '').($user_homeworks[$hw['homework_id']]['score'] < 0 ? '<span class="glyphicon glyphicon-time"></span>' : '%').'</span></a>';

					if($user_homeworks[$hw['homework_id']]['score'] < 0)
					{
						$arguments = 'Вашето възражение все още не е разгледано!';
						if($user_homeworks[$hw['homework_id']]['score'] == -1)
						{
							$arguments = 'Домашната е заключена и се проверява в момента!';
						}
					}
					else
					{
						$arguments = $user_homeworks[$hw['homework_id']]['score_arguments'];
					}

					// Ако няма възражения, добавяме името на курсиста оценител в криптиран вид по подобие на това при домашните по които има възражения
					if(strpos($arguments, $report_tag_begin) === false)
					{
						$arguments .= '[lt]br /[gt]Оценена от потребител: '.$decrypt_tag_begin.encrypt($user_homeworks[$hw['homework_id']]['teacher_id']).$decrypt_tag_end;
					}

					$arguments = htmlspecialchars(stripslashes($arguments));
					$arguments = str_replace(array('[lt]', '[gt]'), array('<', '>'), $arguments);
					$arguments = str_replace( array_column($TAGS_ARRAY, 'tag_name'), array_column($TAGS_ARRAY, 'tag_code'), $arguments);

					$report_form = ''; // Не изтривайте този ред! Нужен е за да се рестартира $report_form. В противен случай ще ви изведе многократно формата...

					$report_form .= '<textarea name="report_text" style="width:100%; border:1px solid silver;" placeholder="Моля обяснете срещу кое точно възразявате!"></textarea>';
					$report_form .= '<input type="hidden" name="uh_id" value="'.encrypt($user_homeworks[$hw['homework_id']]['id']).'" />';
					$report_form .= '<span style="float:left;">Вижте: <a href="/forum/viewtopic.php?f=19&t=5" target="_blank">"Кога се подава възражение"</a></span>';
					$report_form .= '<button type="submit">Изпрати възражение <span class="glyphicon glyphicon-exclamation-sign" style="color:maroon;"></span></button>';

					$report_form  = '<summary class="pull-left" style="color:red; font-weight:bold; cursor: pointer;">Не съм съгласен с оценката </summary><form method="post">'.$report_form.'</form>';
					$report_form  = '<details class="pull-left" style="width:100%;">'.$report_form.'</details>';

					//$report_form .= pre_print($user_homeworks[$hw['homework_id']]);

					echo '<div class="modal fade" id="ArgumentsModal_'.$hw['homework_id'].'" tabindex="-1" role="dialog" aria-labelledby="ArgumentsModalLabel_'.$hw['homework_id'].'" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
										<h4 class="modal-title" id="myModalLabel">Аргументи</h4>
									</div>
									<div class="modal-body">
										<span style="font-family: "Open Sans" !important; font-size: 13px !important; font-weight: 400 !important; color: #000000 !important;">
											'.$arguments.'
										</span>
									</div>
									<div class="modal-footer">
									'.(strpos($user_homeworks[$hw['homework_id']]['score_arguments'], $report_tag_begin) === false ? $report_form : '').'
									</div>
								</div>
							</div>
						</div>';
				}
				else
				{
					$tmp['my_hw'] .= '<a href="'.$upload_url.'" style="float:right;"><button class="btn btn-ar button button-border-primary button-rounded upload_button">Замести <img src="'.SITE_URL.'/'.LOCAL_PATH.'/images/ui/upload_homework.png" /></button></a>';

					$tmp['score'] = '<span class="">N/A</span>';
				}
			}
			else
			{
				if(!$is_vip_homework || !REQUIRE_VIP || $is_vip_for_homework)
				{
					$tmp['my_hw'] = '<a href="'.$upload_url.'"><button class="btn btn-ar btn-block button button-border-primary button-rounded upload_button">Качи домашна <img src="'.SITE_URL.'/'.LOCAL_PATH.'/images/ui/upload_homework.png" /></button></a>';
					$tmp['score'] = '<span class="">N/A</span>';
				}
			}

			if($show_homework)
			{
				$hw_array[] = $tmp;
			}
		}

		$columns = array(
			'Тема',
			'Домашно задание',
			//'Начална дата',
			//'Крайна дата',
			array(
				'text' => 'Моето домашно',
				//'attr' => 'width="341"'
			),
			'Оценка'
		);

		set('course', $course);
		set('hw_array', $hw_array);
		set('columns', $columns);
		set('now', $now);
		database_close();
	}


	function action_user_UploadHomework()
	{
		//phpinfo();
		Global $source, $is_admin_user, $is_super_admin;

		if (isset($source[ID]))
		{
			$homework_id = (int) $source[ID];
		}
		else
		{
			echo 'Homework ID is required field!';
			exit;
		}

		if (isset($_GET['course']))
		{
			$course_id = $_GET['course'];
		}
		else
		{
			echo 'Course ID is required field!';
			exit;
		}

		if(isset($_GET['user_id']) && $is_super_admin)
		{
			$load_user_id = $_GET['user_id']; //get_query_var('id','2','news');
		}
		else
		{
			//get_currentuserinfo();
			$load_user_id = get_current_user_id();
		}


		database_open();

		$allow = true; //Boolean value, true - allow homework upload, false - display alert with back link

		$course = to_assoc_array(exec_query('SELECT * FROM course_members AS cm LEFT JOIN courses AS c ON cm.course_id = c.course_id WHERE cm.role = "student" AND cm.course_id = '.(int) $course_id.' AND cm.user_id = '.(int) $load_user_id));
		if(Only_one_exists($course))
		{
			$course = reset($course);
//			echo 'course allow';
//			pre_print($course);
		}
		else
		{
//			echo 'course dissalow';
			$allow = false;
//			page_not_access();
		}

//		$user_data = to_assoc_array(exec_query('SELECT user_id, group_number FROM course_members WHERE user_id = ' . (int) $load_user_id));
//		pre_print($user_data);
//		pre_print($user_data[0]['group_number']);

		$homework = to_assoc_array(exec_query('SELECT * FROM course_homeworks AS ch LEFT JOIN objects_visibility AS ov ON ch.homework_id = ov.object_id WHERE ov.object_type = "homework" AND homework_id=' . $homework_id . ' AND group_number=' . (int) $course['group_number']));

	if (Only_One_Exists($homework))
	{
			$homework = reset($homework);
//			echo 'homework allow';
//			pre_print($homework);

			$homework['start_date'] = strtotime($homework['start_date']);
			$homework['end_date'] = strtotime($homework['end_date']);
			$now = strtotime('now');

			$ip_check = ( ($homework['for_office_only'] == 1  && is_office_ip()) || $homework['for_office_only'] == 0);

			if($is_admin_user || ($homework['start_date'] <= $now && $now <= $homework['end_date'] && $ip_check))
		{

		}
			else
			{
				$allow = false;
			}
		}
		else
		{
			$allow = false;
			echo 'HomeWork ID is not found!'; exit;
		}


//		Define variables
		$data = array();

		$data['allow'] = $allow;
		$data['user_id'] = $load_user_id;
		$data['homework_id'] = $homework_id;
		$data['upload_date'] = date("Y-m-d_H-i-s");

	if ($allow && !empty($_POST))
	{
		try
		{

			// Undefined | Multiple Files | $_FILES Corruption Attack
			// If this request falls under any of them, treat it invalid.
			if (
					!isset($_FILES['upfile']['error']) ||
					is_array($_FILES['upfile']['error'])
			)
			{
				throw new RuntimeException('Invalid parameters.');
			}

				// Check $_FILES['upfile']['error'] value.
			switch ($_FILES['upfile']['error'])
			{
				case UPLOAD_ERR_OK:
					break;
				case UPLOAD_ERR_NO_FILE:
					throw new RuntimeException('No file sent.');
				case UPLOAD_ERR_INI_SIZE:
				case UPLOAD_ERR_FORM_SIZE:
					throw new RuntimeException('Exceeded filesize limit.');
				default:
					throw new RuntimeException('Unknown errors.');
			}

				// You should also check filesize here.
			if ($_FILES['upfile']['size'] > 250000000)
			{
				throw new RuntimeException('Exceeded filesize limit.');
			}

			// DO NOT TRUST $_FILES['upfile']['mime'] VALUE !!
			// Check MIME Type by yourself.

			// Енев: Ще разкоментираме тази проверка, когато се пусне extension-a който е нужен за finfo(); !
			$finfo = new finfo(FILEINFO_MIME_TYPE);
			//if($load_user_id == 15){echo $finfo->file($_FILES['upfile']['tmp_name']); exit;}
			if (false === $ext = array_search (
							$finfo->file($_FILES['upfile']['tmp_name']),
							array(
									'7zip' => 'application/x-7z-compressed',
									'zip' => 'application/zip',
									'zip1' => 'application/octet-stream',
									'rar' => 'application/x-rar-compressed',
									'rar1' => 'application/octet-stream',
									'rar2' => 'application/x-rar',
									'tar' => 'application/x-tar',
									'tar1' => 'application/tar',
									'tar.gz' => 'application/tar+gzip',
									'tar.gz1' => 'application/x-tar',
									'tar.gz2' => 'application/x-gzip'
							),
							true
				))
			{//echo $finfo->file($_FILES['upfile']['tmp_name']); exit;
				throw new RuntimeException('Invalid file format.');
			}

			if(is_numeric(substr($ext, -1)))
			{
				$ext = substr($ext, 0, -1);
			}

			// You should name it uniquely.
			// DO NOT USE $_FILES['upfile']['name'] WITHOUT ANY VALIDATION !!
			// On this example, obtain safe unique name from its binary data.
			if(file_exists('uploads/homeworks/' . $homework['course_id']))
			{
				//exit('LINE:'.__LINE__);
			}
			else {
				echo 'DIRECTORY NOT EXIST!'; exit;
			}
			$new_file_name = LOCAL_PATH . 'uploads/homeworks/' . $homework['course_id'] . '/' . $data['homework_id'] . '_' . $data['user_id'] . '_' . $data['upload_date'] . '.' . $ext;
			if (!move_uploaded_file(
				$_FILES['upfile']['tmp_name'], $new_file_name
			))
			{
				throw new RuntimeException('Failed to move uploaded file.');
			}

			if (exec_query('INSERT INTO user_homeworks (`user_id`, `homework_id`, `upload_date`, `upload_name`) VALUES("'.$data['user_id'].'", "'.$data['homework_id'].'", "'.$data['upload_date'].'", "'.$new_file_name.'")'))
			{
					$upload = 'File is uploaded successfully.';
			}
			else
			{
				$upload = 'Could not save homework to databse.';
			}
		}
		catch (RuntimeException $e)
		{

			$upload = $e->getMessage();

		}
	}
	else
	{
		$upload = false;
	}

		$data['have_homeworks'] = haveHomeworks($data['user_id'], $data['homework_id']);

		$hw_array = array();
//		$homeworks = to_assoc_array(exec_query('SELECT * FROM course_homeworks AS ch WHERE course_id = '.(int) $course_id));
		//$homeworks = to_assoc_array(exec_query('SELECT * FROM course_homeworks AS ch LEFT JOIN user_homeworks AS uh ON ch.homework_id = uh.homework_id WHERE course_id = '.(int) $course_id.' AND user_id = '.(int) $user_id));
//		pre_print($homeworks);

		set('homework', $homework);
		set('upload', $upload);
		set('data', $data);
		database_close();

	}

	function action_user_hari()
	{
		Global $is_admin_user;

		if(!$is_admin_user)
		{
			header('location:'.link_to( array(CONTROLLER => 'user', ACTION => 'view', 'OPTION_SET_CLASS' => false) ));
			exit;
		}
	}

	function action_user_pdf()
	{
		Global $user_id, $is_admin_user, $source, $admins_array, $html2pdf;

		if(!empty($_POST) && isset($_POST['submit'])) // Form is sent...
		{
			database_open();
			if(false)
			{
				// Проверка за неправомерно използване на шаблони!
				$templates = get_all_templates();
				$templates = array_column($templates, 'template_content');
				$use_template = false;

				foreach($templates as $template)
				{
					if(stripos($_POST['message'], $template) !== false)
					{
						$use_template = true;
					}
				}

				if($use_template)
				{
					$_POST['message'] = str_ireplace($templates, '', $_POST['message']);
					add_warning_message('Не Ви е позволено да използвате шаблони!');
				}
			}

			if(isset($_POST['send_to']) && !empty($_POST['send_to']))
			{
				$template = getTemplateData(6, 'template_content');

				$html2pdf = new Spipu\Html2Pdf\Html2Pdf;
				$html2pdf->pdf->SetDisplayMode('fullpage');
				foreach($_POST['send_to'] as $key => $value)
				{
					$template_data = array(
						'STUDENT_NAME' => transliterate(null, $value),
						'CURRENT_DATE' => date('d.m.Y').' г.'
					);

					$page_content = replace_template_data($template , $template_data);

					$html2pdf->writeHTML($page_content);
				}

				$html2pdf->output('users.pdf');
			}
			else
			{
				//echo 'Няма реципиенти!';
			}

			database_close();

			//add_success_message('Файлът е успешно генериран!');
			//header('location:'.header_link(array(CONTROLLER => 'mail', ACTION => 'index')));
			//exit;
		}
	}

	function action_user_alerts()
	{
	}

	function action_user_login()
	{
		check_access('LOGIN');

		if(!empty($_POST))
		{
			if(!isset($_POST['user']) || !isset($_POST['pass']))
			{
				add_error_message( translate('ALL_FIELDS_REQUIRED') );
			}
			else
			{
				database_open();
				$success_login = load_user_session($_POST['user'], $_POST['pass']);
				database_close();

				//add_info_message(last_query());

				if($success_login)
				{
					if(isset($_POST['remember_me']))
					{
						setcookie('auto_login', $_SESSION[CURRENT_USER]['user_email'].'|'.$_SESSION[CURRENT_USER]['activation_key'], time() + (60 * 60 * 24 * 30), '/');
					}

					if(isset($_REQUEST['back_to']))
					{
						$back_to = rawurldecode($_REQUEST['back_to']);
						header('location:'.$back_to);
						exit;
					}

					header('location:'.header_link(array(CONTROLLER => 'user', ACTION => 'view')));
					exit;
				}
				else
				{
					add_error_message( translate('MSG_WRONG_USER_OR_PASS') );
				}
			}
		}
	}

	function action_user_logout()
	{
		unset($_SESSION[CURRENT_USER]);
		setcookie('auto_login', false, time() - (60 * 60 * 24 * 30), '/');
		add_info_message('Вие излязохте успешно от профила си!');
		header('location:'.header_link(array(CONTROLLER => 'user', ACTION => 'login')));
		exit;
	}

	function action_user_register()
	{
		//$MINIMUM_AGE_LIMIT = 16;
		$MINIMUM_AGE_LIMIT  = null;
		check_access('REGISTER_USER');
		$_SESSION['FIX_FOOTER'] = false;

		if(!empty($_POST))
		{

			$errors = array();
			$required_fields = array('first_name', 'last_name', 'pass1', 'pass2', 'email', 'phone');

			check_required_fields($required_fields);

			if(!isset($_POST['accept_tc']))
			{
				$errors[] = translate('MSG_MUST_ACCEPT').' '.translate('THE_TERMS_AND_CONDITIONS').'!';
			}

			if(isset($_POST['born_date']))
			{
				$current_year = date('Y');
				$user_year = date('Y', strtotime($_POST['born_date']));
				$user_age = $current_year - $user_year;
				if( isset($MINIMUM_AGE_LIMIT) && $user_age < $MINIMUM_AGE_LIMIT)
				{
					$errors[] = str_ireplace('{AGE_VAR}', $user_age, translate('MSG_MINIMUM_AGE_REQUIRED'));
				}
			}

			if($_POST['pass1'] != $_POST['pass2'])
			{
				$errors[] = translate('MSG_PASSWORDS_MISMATCH');
			}

			// All is ok for now, check is it old User to keep its old User_ID
			if (is_production())
			{
				database_open('enevsoft_wa');
				$wa_user = check_WA_email($_POST['email']);
				database_close();
			}

			database_open();
			if(!check_email_exists($_POST['email']))
			{
				$errors[] = translate('MSG_EMAIL_EXISTS');
			}

			if(!empty($errors))
			{
				add_error_message($errors);
			}

			if(!has_error_messages()) // All is OK
			{
				// Register into DataBase
				$insert_data = array(
					'user_email' => $_POST['email'],
					'user_pass' => $_POST['pass1'],
					'first_name' => $_POST['first_name'],
					'last_name' => $_POST['last_name'],
					'avatar' => $_POST['avatar'],
					'phone' => $_POST['phone'],
					'email' => $_POST['email'],
					'facebook' => $_POST['facebook'],
					'sex' => $_POST['sex'],
					'born_date' => $_POST['born_date'],
					//'reg_date' => date('Y-m-d H:i:s'),
					//'facebook' => $_POST['facebook'],
					'activation_key' => md5(date('Y-m-d H:i:s'))
					//,'parent_id' => $_POST['come_from']
				);

				if(!is_null($wa_user)) // Потребителя го има в базата данни - за негово улеснение, го регистрираме със същото ID
				{
					$insert_data['user_id'] = $wa_user;
				}

				// World Logged Friends Me
				//  4       3      2    1
				$default_privacy = array(
					'first_name' => 4,
					'last_name' => 4,
					'avatar' => 4,
					'phone' => 2,
					'email' => 3,
					'facebook' => 3,
					'sex' => 1,
					'born_date' => 2
				);

				//database_open();
				$id = insert_query('users', $insert_data);
				$id = insert_query('user_settings', ['user_privacy' => serialize($default_privacy)]);
				//database_close();

				if($id)
				{
					add_success_message( translate('MSG_REGISTER_COMPLETE') );
					header('location:'.header_link( array(CONTROLLER => 'user', ACTION => 'login') ));
					exit;
				}
				else
				{
					add_unknown_error(__LINE__, __FILE__);
				}
			}

			$default_data = $_POST;
		}
		else
		{
			$default_data = array(
			'first_name' => '',
			'last_name' => '',
			'pass1' => '',
			'pass2' => '',
			'phone' => '',
			'email' => '',
			'facebook' => '',
			'come_from' => 0,
			'explain_field' => '',
			'born_date' => '',
				'sex' => 2,
				'avatar' => DEFAULT_MISSING_AVATAR
			);
		}

		set('default_data', $default_data);
	}

	// SUB Page
	function action_user_ChangePass()
	{
		if(!empty($_POST))
		{
			$required_fields = array('pass1', 'pass2');
			check_required_fields($required_fields);

			if($_POST['pass1'] != $_POST['pass2'])
			{
				add_error_message(translate('MSG_PASSWORDS_MISMATCH'));
			}

			if(!has_error_messages())
			{
				database_open();
				update_query('users', array('user_pass' => escape_string($_POST['pass1']) ), set_where('user_id', '=', look_for_user()));
				add_success_message( translate('MSG_UPDATE_COMPLETE') );
				database_close();
			}

			header('location:'.$_SERVER['HTTP_REFERER']);
			exit;
		}
	}

	function action_user_UploadAvatar()
	{
		database_open();
		echo json_encode(upload_file('avatar_file', 'avatars'));
		database_close();
	}

function action_user_forgotpass()
{
	//search in DB for current user and email , and if we have match send the password via maill
	if (isset($_POST['submit']))
	{
		database_open();
		$email	 = escape_string($_POST['email']);
		$query	 = get_all_users('user_email = "'.$email.'"');


		if (count($query) > 0)
		{
			$user	 = reset($query);
			$user_id = $user['user_id'];

			$user_token = $user['activation_key'];

			$newToken = md5(date('Y-m-d H:i:s'));
			update_query('users', array('activation_key' => $newToken), set_where('user_id', '=', $user_id));

			$subject = $_POST['name'];
			$to		 = $email;
			$from	 = "Plovidvweb.com";

			//data
			$msg	 = "<form role='form'  method='get' >
                        <a href='".get_site_url()."user/resetpass?key=$newToken'>Click here </a>
                        <div class='row'>"
				."</form>";
			//Headers
			$headers = "MIME-Version: 1.0\r\n";
			$headers .= "Content-type: text/html; charset=UTF-8\r\n";
			$headers .= "From: <".$from.">";

			mail($to, $subject, $msg, $headers);


			add_success_message(translate('MSG_MESSAGE_SENT_COMPLETE'));
		}
		else
		{
			add_error_message(translate('MSG_EMAIL_NOT_EXISTS'));
		}
	}
}

function action_user_resetpass()
{
	if (isset($_GET["key"]))
	{

		database_open();
		$token	 = escape_string($_GET["key"]);
		$query	 = get_all_users('activation_key = "'.$token.'"');
		if (Only_One_Exists($query))
		{
			$user	 = reset($query);
			$user_id = $user['user_id'];



			if (!empty($_POST))
			{
				$required_fields = array('pass1', 'pass2');
				check_required_fields($required_fields);

				if ($_POST['pass1'] != $_POST['pass2'])
				{
					add_error_message(translate('MSG_PASSWORDS_MISMATCH'));
				}

				if (!has_error_messages())
				{
					$newToken = md5(date('Y-m-d H:i:s'));

					$update_data = array(
						'user_pass' => escape_string($_POST['pass1']),
						'activation_key' => $newToken);

					update_query('users', $update_data, set_where('user_id', '=', $user_id));

					add_success_message(translate('MSG_UPDATE_COMPLETE'));
					database_close();
				}
				header('Refresh: 2;'.header_link(array(CONTROLLER => 'user', ACTION => 'login')));
				exit;
			}
		}
		else
		{
			add_error_message(translate('MSG_UNKNOWN_ERROR'));
		}
	}
	else
	{
		header('location:'.header_link(array(CONTROLLER => 'page', ACTION => '404')));
		exit;
	}
}

function action_user_loginFB()
{
	if(!empty($_REQUEST))
	{
		database_open();
		if(!check_email_exists(escape_string($_REQUEST['email'])))
		{
			$result = getUserByMail(escape_string($_REQUEST['email']));
			if(count($result)<=1)
			{
			   $result = reset($result);
			   $user_email = $result['user_email'];
			   if($result['avatar'] == '' && $result['avatar'] == DEFAULT_MISSING_AVATAR)
			   {
				   action_user_upload_fb_avatar($user_email);
			   }

			   $success_login = load_user_session($result['user_email'], $result['user_pass']);
			}
		}
		else // This email not exists into Database
		{
			$rnd_password = base64_encode(random_bytes(12));
			$insert_data = array(
				'user_email' => $_REQUEST['email'],
				'user_pass' => $rnd_password,
				'first_name' => $_REQUEST['first_name'],
				'last_name' => $_REQUEST['last_name'],
				//'avatar' => $filename_avatar,
				'phone' => '',
				'email' => $_REQUEST['email'],
				'facebook' => $_POST['facebook'],
				'sex' => $_POST['sex'],
				'born_date' => $_POST['born_date'],
				'activation_key' => md5(date('Y-m-d H:i:s'))

			);

			$default_privacy = array(
				'first_name' => 4,
				'last_name' => 4,
				'avatar' => 4,
				'phone' => 2,
				'email' => 3,
				'facebook' => 3,
				'sex' => 1,
				'born_date' => 2
			);

			// Да се разкоментира, когато приложението бъде активирано
//			insert_query('users', $insert_data);
//			$new_user_id = insert_query('user_settings', ['user_privacy' => serialize($default_privacy)]);
//			action_user_upload_fb_avatar($user_email);

			action_user_loginFB();

			//header('location:'.header_link(array(CONTROLLER => 'user', ACTION => 'view')));
			//exit;
		}
		echo (int) $success_login;
		database_close();
		exit;
	}

}


function action_user_login_google()
{

	if(!empty($_REQUEST))
	{
		database_open();

		if(!check_email_exists($_REQUEST['email']))
		{

			$result = getUserByMail(escape_string($_REQUEST['email']));
			if(count($result)<=1)
			{
				$result = reset($result);
				$user_email = $result['user_email'];

				if($result['avatar'] == '' && $result['avatar'] == DEFAULT_MISSING_AVATAR)
				{
					action_user_upload_fb_avatar($user_email);
				}

				$success_login = load_user_session($result['user_email'], $result['user_pass']);
			}
		}
		else
		{	//if the email is NOT IN the DB

			if(check_email_exists(escape_string($_REQUEST['email'])))
			{

				$rnd_password = base64_encode(random_bytes(12));
				$insert_data = array(
					'user_email' => $_REQUEST['email'],
					'user_pass' => $rnd_password,
					'first_name' => $_REQUEST['name'],
					'last_name' => $_REQUEST['last_name'],
					//'avatar' => $filename_avatar,
					'phone' => 'Моля въведете телефон И СИ СМЕНЕТЕ ПАРОЛАТА ',
					'email' => $_REQUEST['email'],
					'facebook' => $_POST['facebook'],
					'sex' => $_POST['sex'],
					'born_date' => $_POST['born_date'],
					'activation_key' => md5(date('Y-m-d H:i:s'))
				);

				$default_privacy = array(
					'first_name' => 4,
					'last_name' => 4,
					'avatar' => 4,
					'phone' => 2,
					'email' => 3,
					'facebook' => 3,
					'sex' => 1,
					'born_date' => 2
				);

				insert_query('users', $insert_data);
				$new_user_id = insert_query('user_settings', ['user_privacy' => serialize($default_privacy)]);
				action_user_upload_fb_avatar($user_email);

				action_user_loginFB();

			}else
			{
				exit('sdfsdf');
			}

		}
		//echo __LINE__;
		echo (int) $success_login;
		database_close();
		exit;
	}
}

function action_user_upload_fb_avatar($user_email)
{
	$user_email = getUserByMail(escape_string($_REQUEST['email']));
	$user_email = reset($user_email);
	$user_id=$user_email['user_id'];

	$filename_avatar = 'FB_AVATAR_user_id='.$user_id .'_'.$user_email['user_email'].'.jpg';

	$avatar_fb = escape_string($_REQUEST['picture']['data']['url']);
	$destination = DIR_UPLOADS."avatars/";

	$ch = curl_init ($avatar_fb);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
	$raw=curl_exec($ch);
	curl_close ($ch);

	$fp = fopen($destination .$filename_avatar,'w');
	fwrite($fp, $raw);
	fclose($fp);
	//escape strings
	exec_query("UPDATE users SET avatar ='/$destination$filename_avatar'  WHERE user_id ='$user_id'");
}

	function action_user_import()
	{
		Global $is_admin_user;
		if($is_admin_user)
		{

		}
		else
		{
			page_not_access();
		}

		if(!empty($_POST))
		{
			// Import users
		}
		else
		{
			// Show imported files
		}
	}
?>