<?php
	// Question Controller
	function action_question_index()
	{
		Global $source, $_connection, $QUESTIONS_TYPES, $QUESTIONS_GROUPS, $is_admin_user, $user_id;

		if(!$is_admin_user)
		{
			page_not_access();
			exit;
		}

		database_open();
		//CopyTest(1, 25); CopyTest(2, 25); CopyTest(3, 25); exit;
		if(!isset($source[VIEW]) || $source[VIEW] == 'all')
		{
			$view = 'all';
			//header('location: '.link_to(array('OPTION_SET_CLASS' => false)).'&'.VIEW.'=all');
		}
		else
		{
			$view = strtolower($source[VIEW]);
		}

		$query_string = '';
		switch($view)
		{
			case 'all': $query_string = '1'; break; // Show All Questions
			case 'wrong': $query_string = 'is_correct = 0'; break; // Show wrong questions only
			case 'reported': $query_string = 'question_id IN (SELECT question_id FROM marked_questions)'; break;
			default: $query_string = '1'; $view = 'all'; break; // Show All Questions
		}

		$tests = get_all_tests('1=1 ORDER BY test_name ASC');

		if(isset($source['test_id']) && $source['test_id'] >= 0)
		{
			$query_string .= ' AND tests.test_id = '.(int) $source['test_id'];
		}
		else
		{
			$source['test_id'] = -1;
		}
		$tests_dropdown = ReturnDropDown($tests, $source['test_id']);

		if(isset($_REQUEST['word']))
		{
			$query_string .= ' AND (question_title LIKE "%'.mysqli_real_escape_string($_connection, $_REQUEST['word']).'%" OR question_id IN (SELECT question_id FROM test_question_answers WHERE answer_title LIKE "%'.mysqli_real_escape_string($_connection, $_REQUEST['word']).'%"))';
		}

		if(isset($source[ORDER_BY]) && $source[ORDER_BY] != '')
		{
			$query_string .= ' ORDER BY '.mysqli_real_escape_string($_connection, $source[ORDER_BY]);
		}

		if(SHOW_DEBUG_INFO){ echo '<!-- QUERY: '.$query_string.' -->'; }

		$questions = get_all_questions($query_string);
		foreach($questions as $k => $v)
		{
			$questions[$k]['functions'] = '<div style="text-align:center;">';
			$questions[$k]['functions'] .= '<a href="'.link_to(array(CONTROLLER => 'question', ACTION => 'edit', ID => $v['question_id'])).'"><img src="'.DIR_IMAGES.'ui/application_form_edit.png" alt="" /></a>';
			$questions[$k]['functions'] .= '<a href="#"><img src="'.DIR_IMAGES.'ui/views.gif" alt="" /></a>';
			$questions[$k]['functions'] .= '<a href="'.link_to(array(CONTROLLER => 'question', ACTION => 'delete', ID => $v['question_id'], 'OPTION_SET_CLASS' => false)).'" class="delete_btn"><img src="'.DIR_IMAGES.'ui/trash-can-delete.png" alt="" height="16" /></a>';
			$questions[$k]['functions'] .= '</div>';
			$questions[$k]['question_type'] = $QUESTIONS_TYPES[$v['question_type']];
			$questions[$k]['is_active'] = '<img src="'.DIR_IMAGES.'ui/'.(int) $v['is_active'].'.png" alt="'.$v['is_active'].'" />';
			$questions[$k]['is_correct'] = '<img src="'.DIR_IMAGES.'ui/'.(int) $v['is_correct'].'.png" alt="'.$v['is_correct'].'" />';
			$questions[$k]['question_group'] = '<span style="padding:0px 7px; background-color:'.$QUESTIONS_GROUPS[$v['question_group']].';">'.$v['question_group'].'</span>';
			$questions[$k]['test_id'] = '<a href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element('test_id', $_SERVER['QUERY_STRING']).'&' : '').'test_id='.$v['test_id'].'" title="'.$v['test_name'].'">'.$v['test_id'].'</a>';
			$questions[$k]['question_title'] = stripslashes($v['question_title']);

			if(substr($v['question_title'], 0, 4) == 'img=')
			{
				$questions[$k]['question_title'] = '<a href="'.DIR_IMAGES.substr($v['question_title'],4).'" target="_blank"><img src="'.DIR_IMAGES.substr($v['question_title'],4).'" style="border:1px solid black; height:50px;"></a>';
				$is_image = true;
			}
			else
			{
				$is_image = false;
				$max_chars_on_row = 125;

				if(mb_strlen($questions[$k]['question_title']) > $max_chars_on_row)
				{
					$make_short_title = true;
				}
				else
				{
					$make_short_title = false;
				}

				$questions[$k]['question_title'] = htmlspecialchars($questions[$k]['question_title']);

				if($make_short_title && (!isset($_REQUEST['show_answers']) || $_REQUEST['show_answers'] == 0))
				{
					$questions[$k]['question_title'] = excerpt_details_summary($questions[$k]['question_title'], $max_chars_on_row);
				}

				//$questions[$k]['question_title'] = mb_strlen($questions[$k]['question_title']).$questions[$k]['question_title'];
			}

			if(isset($_REQUEST['show_answers']) && $_REQUEST['show_answers'] == 1)
			{
				$answers = getAnswersPerQuestion($v['question_id'], $v['question_type']);
				//pre_print($answers); exit;

				// Да фиксна това когато реша въпроса с анкетите
				$input_type = 'radio';
				$is_poll = false;
				$is_exam = true;

				$answers_are_ok = true;
				foreach($answers as $answer)
				{
					//pre_print($answer);

					$debug_info = '';

					if(SHOW_DEBUG_INFO)
					{
						$debug_info = '<span class="debug '.($answer['is_correct'] == 1 ? 'answer_success' : 'answer_error').'"'.($answer['answer_value'] > 1 ? ' style="background-color:blue;"' : '').'>('.($is_exam ? ($answer['is_correct'] == 1 ? '+' : '-') : '+').$answer['answer_value'].')</span> ';
						//Model_HelperFunctions::Pre_Print($answer);
					}

					// Check if this is image
					$answer['answer_title'] = htmlspecialchars(stripslashes(trim($answer['answer_title'])));
					if(substr($answer['answer_title'], 0, 4) == 'img=')
					{
						$answer['answer_title'] = '<img src="'.DIR_IMAGES.substr($answer['answer_title'],4).'" alt="'.$answer['answer_id'].':'.substr($answer['answer_title'],4).'" style="border:1px solid black; height:50px;">';
					}

					$questions[$k]['question_title'] .= '<br>'.($is_image ? '<br>' : '').'
						<label style="margin-left:10px;" class="padding_left_right_5px">
							'.$debug_info.$answer['answer_title'].'
						</label>';

					if(($answer['is_correct'] && 0 == $answer['answer_value']) || ($answer['is_correct'] && 1 < $answer['answer_value']) || (!$answer['is_correct'] && 0 < $answer['answer_value']) || 0 > $answer['answer_value'])
					{
						$answers_are_ok = false;
					}
				}

				if(!$answers_are_ok)
				{
					$questions[$k]['question_title'] = '<br><b class="warning" style="color:white; background-color:red; border:2px solid maroon; display:block; font-size:22px;">ВНИМАНИЕ!</b><br>'.$questions[$k]['question_title'];
				}
			}

			unset($questions[$k]['test_name']);
		}

		//Pre_Print($questions);
		set('questions', $questions);
		set('tests_dropdown',$tests_dropdown);
		//set('source', $source);
	}

	function action_question_mark()
	{
		Global $user_id;
		//Pre_Print($_POST, true);

		if(!empty($_POST))
		{
			database_open();
			$question_id = $_POST['ID'];

			//$result = update_query('test_questions', array('is_wrong' => 1), set_where('question_id', '=', $question_id));
			if(true)
			{
				if($_POST['msg'] != '')
				{
					$result = insert_query('marked_questions', array('question_id' => $question_id, 'user_id' => $user_id, 'message' => $_POST['msg'], 'reported_on' => time()));
				}

				if($result)
				{
					$tmp = array('status' => true, 'message' => 'Благодарим Ви! \nВъпросът беше маркиран!');
				}
				else
				{
					$tmp = array('status' => false, 'message' => 'Възникна проблем, при маркирането на въпроса! Съжелявам!');
				}
			}
			else
			{
				$tmp = array('status' => false, 'message' => 'Възникна проблем, при маркирането на въпроса!');
			}

			database_close();
			return json_encode($tmp);
		}
	}

	function action_question_edit()
	{
		Global $source, $_connection, $is_admin_user, $user_id;

		database_open();

		if(!$is_admin_user)
		{
			page_not_access();
			exit;
		}

		if(!isset($source[ID]))
		{
			header( 'location:'.header_link( array(CONTROLLER => 'question', ACTION => 'index') ) );
			exit;
		}

		$question = getQuestionData($source[ID]);

		if(count($question) != 1)
		{
			if(count($question) == 0)
			{
				add_error_message('Въпросът не съществува!');
			}
			else
			{
				add_error_message('Неизвестен проблем!');
			}
		}
		else
		{
			if(!empty($_POST) && isset($_POST['question_id']) && $_POST['question_id'] == $source[ID])
			{
				//pre_print($_POST);

				// Update Answers
				exec_query('DELETE FROM test_question_answers WHERE question_id = '.(int) $_POST['question_id']);
				if(isset($_POST['answers']) && is_array($_POST['answers']) && count($_POST['answers']) > 0)
				{
					$answers_array = array();
					foreach($_POST['answers'] as $answer_index => $answer)
					{
						$answers_array[] = '('.(int) $_POST['question_id'].', "'.mysqli_real_escape_string($_connection, $answer['title']).'", "'.(int) $answer['value'].'", 2,'.($answer_index == (int) $_POST['correct_answer_index'] ? 1 : 0).')';
					}

					exec_query('INSERT INTO test_question_answers(question_id, answer_title, answer_value, answer_type, is_correct) VALUES'.implode(',', $answers_array));
					unset($_POST['correct_answer_index']);
					unset($_POST['answers']);
				}

				// Update Question
				update_query('test_questions', $_POST, 'question_id = '.$_POST['question_id']);

				$question = getQuestionData($source[ID]);
			}

			$question = reset($question);
		}

		database_close();

		set('question', $question);
	}

	function action_question_add()
	{
		Global $source, $_connection, $is_admin_user, $user_id;

		//$default_test_id = 5;

		if(!$is_admin_user)
		{
			page_not_access();
			exit;
		}

		database_open();

		if(!empty($_POST) && isset($_POST['question_title']) && $_POST['question_title'] != '')
		{
			//pre_print($_POST);
			$_POST['question_type'] = 2;

			// Ако не е зададен тест, задаваме този който е по подразбиране.
			if(!isset($_POST['test_id']))
			{
				if(isset($default_test_id))
				{
					$_POST['test_id'] = $default_test_id;
				}
				else
				{
					$_POST['test_id'] = 0;
				}
			}

			$correct_answer_index = $_POST['correct_answer_index'];
			$answers = $_POST['answers'];

			unset($_POST['correct_answer_index']);
			unset($_POST['answers']);

			$new_question_id = insert_query('test_questions', $_POST);

			// Update Answers
			if(isset($answers) && is_array($answers) && count($answers) > 0)
			{
				$answers_array = array();
				foreach($answers as $answer_index => $answer)
				{
					$answers_array[] = '('.(int) $new_question_id.', "'.mysqli_real_escape_string($_connection, $answer['title']).'", "'.(int) $answer['value'].'", 2,'.($answer_index == (int) $correct_answer_index ? 1 : 0).')';
				}

				exec_query('INSERT INTO test_question_answers(question_id, answer_title, answer_value, answer_type, is_correct) VALUES'.implode(',', $answers_array));
			}

			header('location:'.header_link( array(CONTROLLER => 'question', ACTION => 'edit', ID => $new_question_id) ) );
			exit;
		}

		database_close();
		//require(LOCAL_PATH.DIR_VIEWS.'question/edit.php');

		//set('default_test_id', $default_test_id);
	}

	function action_question_delete()
	{
		Global $source, $is_admin_user;

		if($is_admin_user)
		{
			if(is_office_ip())
			{
				database_open();

				if(isset($source[ID]) && 0 < (int) $source[ID])
				{
					//pre_print($source);
					delete_query('test_questions', 'question_id = '.$source[ID]);
					header('location:'.link_to(array(CONTROLLER => 'question', ACTION => 'index', 'OPTION_SET_CLASS' => false)));
					exit;
				}

				database_close();
			}
			else
			{
				echo '<span style="color:red;">Този функционалност може да се използва само от залата на Академията!<br />Вашият IP: '.$_SERVER['REMOTE_ADDR'].' не е сред разрешените!</span>';
			}
		}
		else
		{
			page_not_access();
			exit;
		}
	}

	function action_question_fillanswers()
	{
		Global $source, $_connection, $is_admin_user;

		if(!$is_admin_user)
		{
			page_not_access();
			exit;
		}

		database_open();

		for($new_question_id = 154; $new_question_id <= 161; $new_question_id++)
		{
			//pre_print($_POST);
			$answers = array(
				2 => 'Категорично НЕ',
				3 => 'По скоро Не',
				4 => 'Може би',
				5 => 'По скоро Да',
				6 => 'Категорично Да'
			);

			// Update Answers
			if(isset($answers) && is_array($answers) && count($answers) > 0)
			{
				$answers_array = array();
				foreach($answers as $answer_index => $answer)
				{
					$answers_array[] = '('.(int) $new_question_id.', "'.$answer.'", "'.$answer_index.'", 2,'.($answer_index == 4 ? 1 : 0).')';
				}

				//exec_query('INSERT INTO test_question_answers(question_id, answer_title, answer_value, answer_type, is_correct) VALUES'.implode(',', $answers_array));
			}
		}

		database_close();
		//echo 'Ready!';
	}

	function action_question_serialize()
	{
		//Global $controller_functions;

		if(isset($_GET['array']))
		{
			foreach($_GET['array'] as &$v)
			{
				$v = (int) $v;
			}

			unset($v);
			pre_print($_GET['array']);
			echo serialize($_GET['array']);
		}
		else
		{
			echo '$_GET["array"] is empty!';
		}
		//pre_print($controller_functions);
	}
?>