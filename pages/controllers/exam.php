<?php
	// Exam Controller
	function action_exam_index()
	{
		$db = debug_backtrace();
		Global $source, $_connection;

		database_open();

        $query_string = '1=1';

		//pre_print($db,1);


        if(!isset($source[VIEW]))
		{
			// Set Default Value
			$view = 'all';
			//$view = 'all';
		}
		else
		{
			$view = strtolower($source[VIEW]);
		}




		if(!isset($source['course_id']))
		{
			$source['course_id'] = 0;
		}

		if(isset($source['course_id']) && $source['course_id'] != '-1')
		{
			$courses_filter = $source['course_id'];
			if(0 == $courses_filter)
			{
				//$courses_filter = array_column(getCurrentCourses(),'course_id');
			}

			if(is_numeric($courses_filter) && $courses_filter > 0)
			{
				$query_string .= ' AND rt.course_id = '.(int) $courses_filter;
			}

			if(is_array($courses_filter))
			{
				$query_string .= ' AND rt.course_id IN ('.implode(',', $courses_filter).')';
			}
		}

		if(isset($source['test_id']) && $source['test_id'] != '-1')
		{
            if(strpos($source['test_id'], ',')) // If we have format like homework_id=1,2,3 convert it to array
			{
				$source['test_id'] = explode(',', $source['test_id']);
			}

			if(is_numeric($source['test_id']))
			{
				$query_string .= ' AND rt.test_id = '.(int) $source['test_id'];
			}

			if(is_array($source['test_id']))
			{
				$query_string .= ' AND rt.test_id IN ('.implode(',', $source['test_id']).')';
			}
		}

		if(isset($source['exam_type']) && $source['exam_type'] != '-')
		{
			$query_string .= ' AND rt.exam_type = "'.mysqli_real_escape_string($_connection, $source['exam_type']).'"';
		}

        if(isset($source['user_id']))
		{
			if(strpos($source['user_id'], ',')) // If we have format like homework_id=1,2,3 convert it to array
			{
				$source['user_id'] = explode(',', $source['user_id']);
			}

			if(is_array($source['user_id']))
			{
				$query_string .= ' AND eh.user_id IN ('.implode(', ', $source['user_id']).')';
			}
			else
			{
				$query_string .= ' AND eh.user_id = '.(int) $source['user_id'];
			}
		}

        if(isset($_GET['exam_id']))
		{
			if(strpos($source['exam_id'], ',')) // If we have format like homework_id=1,2,3 convert it to array
			{
				$source['exam_id'] = explode(',', $source['exam_id']);
			}

			if(is_array($source['exam_id']) && count($source['exam_id']) > 0)
			{
				$query_string .= ' AND rt.exam_id IN ('.implode(', ', $source['exam_id']).')';
			}
			elseif(is_numeric($source['exam_id']))
			{
				$query_string .= ' AND rt.exam_id = '.(int) $source['exam_id'];
			}
		}
		if(isset($source[ORDER_BY]) && $source[ORDER_BY] != '')
		{
			$query_string .= ' ORDER BY '.mysqli_real_escape_string($_connection, $source[ORDER_BY]);
		}
		else
		{
			$query_string .= ' ORDER BY history_id DESC';
		}



		$exams = get_exams_list($query_string);

		/*
		foreach($questions as $k => $v)
		{
			$questions[$k]['question_type'] = $QUESTIONS_TYPES[$v['question_type']];
			$questions[$k]['is_active'] = '<img src="images/ui/'.(int) $v['is_active'].'.png" alt="'.$v['is_active'].'" />';
			$questions[$k]['is_correct'] = '<img src="images/ui/'.(int) $v['is_correct'].'.png" alt="'.$v['is_correct'].'" />';
			$questions[$k]['question_group'] = '<span style="padding:0px 7px; background-color:'.$QUESTIONS_GROUPS[$v['question_group']].';">'.$v['question_group'].'</span>';
			$questions[$k]['test_id'] = '<a href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element('test_id', $_SERVER['QUERY_STRING']).'&' : '').'test_id='.$v['test_id'].'" title="'.$v['test_name'].'">'.$v['test_id'].'</a>';

			unset($questions[$k]['test_name']);
		} */
		//pre_print($exams,1); exit;

		set('exams', $exams);
		//set('source', $source);
	}

	function action_exam_solve()
	{
		Global $source, $user_id, $is_admin_user;

		$exam_id = $source[ID];
		$is_exam = true;

		database_open();
		// Проверка дали този изпит съществува

		// Проверка дали този изпит вече не е решен
		if(is_already_taken($exam_id, $user_id)) // is_already_taken($exam_id, $user_id)
		{
			// Редирект към Преглед на вече решения изпит
			$history_id = getTakenId($exam_id, $user_id);

			header('location:'.link_to(array(CONTROLLER => 'exam', ACTION => 'view', ID => $history_id, 'OPTION_SET_CLASS' => false)));
			exit;
		}
		else
		{
			// Проверка дали имаме права да решим този изпит
			if(have_permissions($user_id, $exam_id))
			{
				// Зареждаме данните за изпита
				$exam_data = getExamData($exam_id);
				//pre_print($exam_data, 1);

				if(Exam_Exists($exam_data))
				{
					$exam_data = reset($exam_data);

					if(($exam_data['for_office_only'] == 1 && is_office_ip()) || $exam_data['for_office_only'] == 0)
					{
						if(isset($_POST['evaluate']))
						{
							if(!isset($_POST['exam_id']))
							{
								echo 'Problem';
								//$this->redirect(Model_HelperFunctions::getBaseURI());
								exit;
							}

							$exam_id = $_POST['exam_id'];

							$weight = $exam_data['weight'];
							$course_id = $exam_data['course_id'];

							if($exam_data['exam_type'] != 'poll')
							{
								$response = Evaluate_Exam($exam_id, $_POST);
							}
							else
							{
								//$response = Evaluate_Poll($exam_id, $_POST);
								$response = Evaluate_Exam($exam_id, $_POST);
							}

							$exam_questions = $response['exam_questions'];
							$percent = $response['percent'];

							//Pre_Print($response);set('exam_questions', $exam_questions);set('is_admin_user', true);

							if(!is_null($user_id))
							{
								$total_time = strtotime('now') - $_SESSION['exam_start_time'];
								unset($_SESSION['exam_start_time']);

								//if(SHOW_DEBUG_INFO){pre_print($response['exam_questions']); exit;}
								update_query('course_members', array('score' => 'score + '.($percent * $weight)), 'user_id = '.$user_id.' AND course_id ='.$course_id);
								$history_id = insert_query('exams_history', array('exam_id', 'user_id', 'json_data', 'groups_scores', 'score', 'is_pass', 'solved_on_date', 'solved_for_time'), array($exam_id, $user_id, serialize($response['exam_questions']), serialize($response['groups_scores']), $response['percent'], $response['is_pass'], time(), $total_time ) );

								$_SESSION['exam_info'] = array(
									'course_id' => $course_id,
									'exam_id' => $exam_id,
									'history_id' => $history_id,
									'exam_type' => $exam_data['exam_type']
								);

								//echo $history_id;

								header('location:'.link_to(array(CONTROLLER => 'exam', ACTION => 'success', ID => $exam_id, 'OPTION_SET_CLASS' => false)));

								/*
								$is_paused = Model_Exam::is_paused($exam_id, $this->user->id);

								if($is_paused)
								{
									DB::delete('paused_exams')->where('record_id', '=', $is_paused)->execute();
								}
								*/
								//echo link_to(array(CONTROLLER => 'exam', ACTION => 'view', ID => $history_id, 'OPTION_SET_CLASS' => false);
								//header('location:'.link_to(array(CONTROLLER => 'exam', ACTION => 'view', ID => $history_id, 'OPTION_SET_CLASS' => false)));
							}
						}
						else
						{

							$_SESSION['exam_start_time'] = strtotime('now');
							if(!is_numeric($exam_data['questions_number'])) // Не е число, значи трябва да е сериализиран масив
							{
								$exam_data['questions_number'] = unserialize($exam_data['questions_number']);
							}

							$exam_questions = get_random_questions($exam_data['questions_number'], $exam_data['test_id']);
							$percent = 0;

							// Randomize questions order & answers order. by this way will be hard 2 student to have identical exams, even if they have identical questions
							if($exam_data['exam_type'] != 'poll')
							{
								$is_exam = true;
							}
							else // This is poll
							{
								$is_exam = false;
							}

							if($exam_data['shuffle_exam_components'])
							{
								$exam_questions = shuffle_exam_components($exam_questions, 'answers');
							}
							// Pre_Print($exam_questions, true);

							// Взимаме типовете на въпросите
							$questions_groups = exec_query('SELECT * FROM questions_groups ORDER BY group_id ASC');
							$questions_groups = to_assoc_array($questions_groups);
							//Pre_Print($questions_groups, true);

							if(false) //(isset($_SESSION['active_exam']) && $_SESSION['active_exam'] != $exam_data['exam_id'])
							{
								add_error_message('Не можете да решавате 2 теста едновременно!<br />Довършете първо тест №'.$_SESSION['active_exam'].'!');
								//$exam_questions = $_SESSION['exam_questions'];
							}
							else
							{
								$_SESSION['active_exam'] = $exam_data['exam_id'];
								$_SESSION['exam'][$exam_data['exam_id']]['questions'] = $exam_questions;
							}
							/*
							$view->set('object_type', $object_type);
							*/
						}

						set('is_live_preview', false);
						set('Requested_From','exam');
						set('exam_data', $exam_data);
						set('is_exam', $is_exam);

						if(isset( $exam_data['test_seconds']) ){ set('test_seconds', $exam_data['test_seconds']); }
						if(isset( $exam_data['exam_id'])	  ){ set('exam_id', $exam_data['exam_id']);			  }
						if(isset( $exam_data['test_id'])	  ){ set('test_id', $exam_data['test_id']);			  }
						if(isset( $questions_groups)		  ){ set('questions_groups', $questions_groups);	  }
						if(isset( $exam_questions)			  ){ set('exam_questions', $exam_questions);		  }
						if(isset( $percent)					  ){ set('percent', $percent);						  }
					}
					else
					{
						$ip = $_SERVER['REMOTE_ADDR'];
						add_error_message('Този тест може да се реши само от залата на Академията!<br />Вашият IP: '.$ip.' не е сред разрешените! '.($is_admin_user ? '<a href="'.header_link(array(CONTROLLER => 'logs', ACTION => 'AddIpAddress', 'ip' => $ip)).'">Добави сега</a>' : ''));
					}
				}
				else
				{
					//page_not_exists();
					add_error_message('Exam not Exists!');
				}
			}
			else
			{
				//page_not_access();
				add_error_message('Нямате права за достъп до тази страница!');
			}
		}

		database_close();
	}

	function action_exam_view()
	{

		Global $source, $user_id, $is_admin_user;

		$exam_history_id = $source[ID];

		database_open();
		$history_data = ReadTakenExam($exam_history_id);

        if(Only_One_Exists($history_data))
		{
			$history_data = reset($history_data);

			$title = 'Тест';
            $title .= ' #'.$history_data['exam_id']; //.' - '.$exam_data['object_name'];

			$exam_questions = unserialize($history_data['json_data']);
			set('Requested_From', 'view');
			set('is_live_preview', false);
			set('exam_questions', $exam_questions);
			set('percent', $history_data['score']);
			set('exam_id', $history_data['exam_id']);
			set('history_data', $history_data);
			set('title', $title);
			set('is_admin_user', $is_admin_user);
			//require(DIR_VIEWS.'exam/solve.php');
			/*
			$this->template->title = 'Exam View';

			$view = $this->template->content = View::factory ( 'exam/preview' );
			*/
			//Pre_Print($exam_questions);
		}
		else
		{
			if(SHOW_DEBUG_INFO)
            {
                echo 'Something wrong, while loading history data on line '.__LINE__.' into file '.__FILE__;
                //Pre_Print($history_data, true);
            }

            //Model_HelperFunctions::GoTo_PageNotFound('exam');
			page_not_found();
		}
	}

	function action_exam_export()
	{
		Global $source, $user_id, $is_admin_user;

		$exam_history_id = $source[ID];

		database_open();
		$history_data = ReadTakenExam($exam_history_id);

        if(Only_One_Exists($history_data))
		{
			$history_data = reset($history_data);
			//Pre_Print($history_data);

			/*
            $exam_data = getExamData($history_data['exam_id']);
            $exam_data = reset($exam_data);

			if(Model_Exam::is_exam($exam_data))
            {
                $title = 'Exam';
            }
            else
            {
                $title = 'Poll';
            }
            */
			$title = 'Тест';
            $title .= ' #'.$history_data['exam_id']; //.' - '.$exam_data['object_name'];

			$exam_questions = unserialize($history_data['json_data']);
			set('Requested_From', 'view');
			set('is_live_preview', false);
			set('exam_questions', $exam_questions);
			set('percent', $history_data['score']);
			set('exam_id', $history_data['exam_id']);
			set('history_data', $history_data);
			set('title', $title);
			set('is_admin_user', $is_admin_user);
			//require(DIR_VIEWS.'exam/solve.php');
			/*
			$this->template->title = 'Exam View';

			$view = $this->template->content = View::factory ( 'exam/preview' );
			*/
			//Pre_Print($exam_questions);
		}
		else
		{
			if(SHOW_DEBUG_INFO)
            {
                echo 'Something wrong, while loading history data on line '.__LINE__.' into file '.__FILE__;
                //Pre_Print($history_data, true);
            }

            //Model_HelperFunctions::GoTo_PageNotFound('exam');
			page_not_found();
		}
	}

	function action_exam_success()
	{
		Global $source, $user_id, $is_admin_user;

		header('location: '.header_link([CONTROLLER =>  'user', ACTION => 'view']));
		exit;

		$exam_id = $source[ID];

        if(isset($_SESSION['exam_info']))
		{
			database_open();
			//$required_tests = getRequiredTestsPerCourse($_SESSION['exam_info']['course_id']);
			//$user_tests = getRequiredTestsPerUser($_SESSION['exam_info']['course_id'], $user_id);

			$required_tests = getTestsPerCourse($_SESSION['exam_info']['course_id'], 'rt.exam_type = "'.$_SESSION['exam_info']['exam_type'].'"');
			$user_tests = getTestsPerUser($_SESSION['exam_info']['course_id'], $user_id, $_SESSION['exam_info']['exam_type']);

			//pre_print($required_tests);
			//pre_print($user_tests);

			$estimated_tests_count = count($required_tests) - count($user_tests);
			if($estimated_tests_count > 0)
			{
				foreach($user_tests as $test)
				{
					if(isset($required_tests[$test['exam_id']]))
					{
						unset($required_tests[$test['exam_id']]);
					}
				}

				$next_test = reset($required_tests);
				$next_test_id = $next_test['exam_id'];
			}
			else
			{
				//
			}
			

			if(!isset($next_test_id))
			{
				$next_test_id = 0;
			}

			set('estimated_tests_count', $estimated_tests_count);
			set('next_test_id', $next_test_id);

			unset($_SESSION['exam_info']);
			unset($_SESSION['active_exam']);
			//unset($_SESSION['exam_questions']);


			/*
			set('is_live_preview', false);
			set('exam_questions', $exam_questions);
			set('percent', $history_data['score']);
			set('exam_id', $history_data['exam_id']);
			set('history_data', $history_data);
			set('title', $title);
			set('is_admin_user', $is_admin_user);
			*/
		}
		else
		{
			//page_not_found();
			header('location: '.site_url('/profile-2/'));
			exit;
		}
	}



	function action_exam_recalculate()
	{
		Global $source;

		if(isset($source[ID]))
		{
			$where = 'history_id = '.(int) $source[ID];
		}
		else
		{
			$where = null;
		}

		database_open();
		$exams_array = get_history_exams($where);


		//Pre_Print($exams_array);
		foreach($exams_array as $exam)
		{
			$exam_questions = unserialize($exam['json_data']);
			Pre_Print($exam_questions, true);

			$random_questions = get_all_questions('question_id IN ('.implode(',', array_column($exam_questions, question_id)).')');
			foreach($random_questions as $key => $value)
			{
				$random_questions[$key]['question_title'] = str_replace("'","`",$value['question_title']);
				$random_questions[$key]['answers'] = getAnswersPerQuestion($value['question_id'], $value['question_type']);
			}

			$_SESSION['exam'][$exam['exam_id']]['questions'] = $random_questions;

			foreach($exam_questions as $question_id => $question)
			{
				foreach($question['answers'] as $answer_id => $answer)
				{
					if(isset($answer['is_checked']) && $answer['is_checked'] == 1)
					{
						$exam_questions[$question_id] = $answer_id;
						break;
					}
				}
			}



			$exam_response = Evaluate_Exam($exam['exam_id'], array('questions' => $exam_questions) );
			//Pre_Print($exam_response, true);
			//echo $exam_response['percent'].'<br>';
		}

		database_close();
	}

	function action_exam_statistic()
	{
		Global $source, $user_id, $is_admin_user, $is_super_admin;

		if(isset($source[ID]))
		{
			$where = 'exam_id = '.(int) $source[ID];
		}
		else
		{
			add_error_message('Exam_ID is important field!');
		}

		database_open();
		$exam_data = getExamData((int) $source[ID]);

		if(Exam_Exists($exam_data))
		{
			$exam_data = reset($exam_data);
			$is_poll = ($exam_data['weight'] == 0);
			$course_data = getCourseData($exam_data['course_id']);
			$course_data = reset($course_data);

			
			if($is_super_admin)
			{
				$has_access_permissions = true;
				$has_filter_permissions = true;
				$has_member_permissions = true;
			}
			else
			{
				if($is_admin_user) 
				{
					$has_access_permissions = true;
					$has_filter_permissions = !$is_poll;
					$has_member_permissions = false;
				}
				else 
				{
					$has_access_permissions = false;
					$has_filter_permissions = false;
					$has_member_permissions = false;
				}

				// Load Course Info
				$course_info = get_all_courses('course_id = '.$exam_data['course_id']);
				if(Only_One_Exists($course_info))
				{
					pre_print($course_info);
					$course_info = reset($course_info);

					if(!$is_admin_user)
					{
						if($course_info['teacher_id'] == $user_id)
						{
							$is_course_author = true;
							$has_access_permissions = true;
							$has_filter_permissions = !$is_poll;
							$has_member_permissions = !$is_poll;

							$member_msg = 'Само администратор може да вижда статистика за Анкети!';
						}
						else
						{
							$is_course_author = false;
						}
					}
					else
					{
						if($is_poll)
						{
							$has_member_permissions = !is_admin_user($course_info['teacher_id']) || $course_info['teacher_id'] == $user_id;
							if(!$has_member_permissions)
							{
								$member_msg = 'Само супер администратор може да вижда анкети за други Администратори!';
							}
						}
						else
						{
							$has_member_permissions = true;
						}
					}
				}
				else
				{
					add_error_message('Липсва информация за курс #'.$exam_data['course_id'].'!');
				}
			}

			if(!$has_access_permissions) // $exam_data['weight'] == 0 && !$has_permissions
			{
				page_not_access();
			}
			else
			{
				if(!$has_member_permissions)
				{
					if(!isset($member_msg))
					{
						$member_msg = 'Нямате достъп до конкретния ресурс!';
					}

					add_error_message($member_msg);
				}
			}

			if(!has_error_messages())
			{
				$except_users = array();
				$except_questions = array();

				if(isset($_GET['except_users']) || isset($_GET['except_questions']))
				{
					if($has_filter_permissions)
					{
						$except_users = $_GET['except_users'];
						$except_questions = $_GET['except_questions'];
					}
					else
					{
						add_warning_message('Филтрирането по курсисти / въпроси е недостъпно за вас!');

						$except_users = array();
						$except_questions = array();
					}
				}

				if(true) // $is_admin_user
				{
					$exams_array = get_history_exams($where, '*');

					$all_questions = array();

					$average_score = array_sum(array_column($exams_array, 'score')) / count($exams_array);
					$highest_score = max(array_column($exams_array, 'score'));
					 $lowest_score = min(array_column($exams_array, 'score'));

					$min_array = array();
					$max_array = array();

					//pre_print($exams_array);
					$users_array = array_column($exams_array, 'user_id');
					$users_roles = getCourseMember($exam_data['course_id'], null, 'student', 'user_id');
					//pre_print($users_roles,1);

					foreach($exams_array as $k => $exam)
					{
						if($exam['score'] == $lowest_score){ $min_array[] = '<a style="color:white;" target="_blank" href="'.link_to(array(CONTROLLER => 'user', ACTION => 'view', ID => $exam['user_id'])).'" title="View User #'.$exam['user_id'].'">'.$exam['user_id'].'</a>'; }
						if($exam['score'] == $highest_score){ $max_array[] = '<a style="color:white;" target="_blank" href="'.link_to(array(CONTROLLER => 'user', ACTION => 'view', ID => $exam['user_id'])).'" title="View User #'.$exam['user_id'].'">'.$exam['user_id'].'</a>'; }

						$exam_questions = unserialize($exam['json_data']);
						//$exams_array[$k]['json_data'] = $exam_questions;

						if(!in_array($exam['user_id'], $except_users))
						{
							foreach($exam_questions as $question)
							{
								//pre_print($question);
								$question_id = $question['question_id'];
								if(!in_array($question_id, $except_questions)) // true?!
								{
									foreach($question['answers'] as $answer_id => $answer)
									{
										if(isset($answer['is_checked']))
										{
											if($question['question_type'] == 2 && $answer['is_checked'] == 1)
											{
												if(isset($all_questions[$question_id]))
												{
													$all_questions[$question_id]['count'] = $all_questions[$question_id]['count'] + 1;
													$all_questions[$question_id]['point'] = $all_questions[$question_id]['point'] + $answer['answer_value'];
													$all_questions[$question_id]['info' ] = $all_questions[$question_id]['info'] . $exam['user_id'].' -> '.$answer['answer_value'].'<br>';
												}
												else
												{
													$all_questions[$question_id] = array('point' => $answer['answer_value'], 'count' => 1, 'title' => $question['question_title'], 'info' => $exam['user_id'].' -> '.$answer['answer_value'].'<br>');
												}
												break;
											}
											elseif( ($question['question_type'] == 3 || $question['question_type'] == 4) && $answer['is_checked'] != '')
											{
												$info_user = '<a class="hover_eye" href="'.link_to(array(CONTROLLER => 'user', ACTION => 'view', ID => $exam['user_id'])).'" title="View User #'.$exam['user_id'].'">'.$exam['user_id'].'</a>';
												if(!$is_super_admin)
												{
													$info_user = '<div class="smile_image hover_ok" alt=":)"></div>';
												}
												$info = array( $info_user, stripslashes($answer['is_checked']) );
												if(isset($all_questions[$question_id]))
												{
													$all_questions[$question_id]['info' ][] = $info;
												}
												else
												{
													$all_questions[$question_id] = array('title' => $question['question_title'], 'info' => array( $info ) );
												}
												break;

											}
										}
									}
								}
							}
						}
					}

					//$then = strtotime('2013-07-21 14:50:00'); $dif = ($then + 84942360); echo date('Y-m-d H:i:s', $dif);
					@ uasort($all_questions, function($a,$b){ return ($a['point'] / $a['count'])<($b['point'] / $b['count']);} );
					$highest_score .=  '%<span style="color:white;">, достигнат от: '.implode(', ', $max_array).'</span>';
					 $lowest_score .=  '%<span style="color:white;">, постигнат от: '.implode(', ', $min_array).'</span>';
					 $average_score = round($average_score, 2).'%';

					set('has_filter_permissions', $has_filter_permissions);
					set('except_questions', $except_questions);
					set('test_name', $exam_data['test_name']);
					set('all_questions', $all_questions);
					set('average_score', $average_score);
					set('highest_score', $highest_score);
					set('lowest_score', $lowest_score);
					set('except_users', $except_users);
					set('users_roles', $users_roles);
					set('users_array', $users_array);
					set('course_data', $course_data);
				}

				database_close();
			}
		}
		else
		{
			//page_not_found();
		}
	}

	function action_exam_copy()
	{
		Global $source, $is_admin_user, $user_id;

		if($is_admin_user)
		{
			if(isset($source[ID]))
			{
				database_open();
				CopyTest($source[ID]);
				echo 'Copy is ready!';
				//pre_print($exams); exit;
				database_close();
			}
			else
			{
				echo 'Exam_ID is required!';
			}
		}
		else
		{
			page_not_access();
		}
	}

	function action_exam_add()
	{
		Global $source, $is_admin_user, $user_id, $is_super_admin;

		$exam_types = array(
			'precondition' => 'Предварителен',
			'current' => 'Междинен',
			'final' => 'Финален',
			'poll' => 'Анкета'
		);

		if($is_admin_user)
		{
			if(!empty($_POST))
			{
				database_open();

				if(!isset($_POST['test_id']) || $_POST['test_id'] < 0) // Разрешаваме 0 - Въпроси без Тест, в случай че искаме да кажем, че има изпит, но теста към него не е създаден!
				{
					add_error_message('Test ID is important!');
				}
				else
				{
					if( !Test_Exists($_POST['test_id']) )
					{
						add_error_message('Test #'.$_POST['test_id'].' not exists! Try again later!');
					}
				}

				if(!isset($_POST['course_id']) || $_POST['course_id'] <= 0)
				{
					add_error_message('Course ID is important!');
				}
				else
				{
					if( !Course_Exists($_POST['course_id']) )
					{
						add_error_message('Course #'.$_POST['course_id'].' not exists! Try again later!');
					}
				}

				if(!isset($_POST['type']) || !in_array($_POST['type'], array_keys($exam_types) ) )
				{
					add_error_message('Type is important!');
				}
				else
				{
					// Insert new record
					switch (strtolower($_POST['type']))
					{
						case 'precondition':
							$weight = 1;
							break;
						case 'poll':
							$weight = 0;
							break;
						case 'current':
							$weight = -1;
							break;
						case 'final':
							$weight = -1;
							break;
						default :
							add_error_message('Unknown exam type!');
							break;
					}

					if (!isset($weight)) //  || !isset($shuffle) || !isset($office)
					{
						add_error_message('Check your switch case construction on line: '.__LINE__.'; <br />File '.__FILE__);
					}
				}

				if(isset($_POST['questions_number']))
				{
					if(is_numeric($_POST['questions_number']) && $_POST['questions_number'] <= 0)
					{
						add_error_message('Questions number must be > 0 !');
					}

					if(!is_numeric($_POST['questions_number']) && !is_serialized($_POST['questions_number']))
					{
						add_error_message('How many questions? Questions number must be positive interger or Serialized Array !');
					}
				}
				else
				{
					add_error_message('Questions Number is Important!');
				}

				if(!isset($_POST['test_minutes']) || $_POST['test_minutes'] <= 0)
				{
					add_error_message('Time is important!');
				}
				else
				{
					if( $_POST['test_minutes'] > 60 || $_POST['test_minutes'] < 10)
					{
						add_error_message('Time must be between 10 and 60 minutes!');
					}
				}

				if(isset($source[ID]))
				{
					$edit_exam = (int) $source[ID];
				}
				else
				{
					$edit_exam = 0;
				}

				if(!has_error_messages())
				{
					$data_array = array(
						'exam_type' => $_POST['type'],
						'test_id' => $_POST['test_id'],
						'course_id' => $_POST['course_id'],
						'test_seconds' => $_POST['test_minutes'] * 60,
						'questions_number' => $_POST['questions_number'],
						'shuffle_exam_components' => (int) $_POST['shuffle'],
						'for_office_only' => (int) $_POST['office'],
						'weight' => $weight
					);

					if(!$edit_exam) // Insert new exam
					{
						$new_exam_id = insert_query('required_tests', $data_array);
						if($new_exam_id)
						{
							add_success_message('All is perfect!');
						}
						else
						{
							add_error_message('Can not insert new RT exam!');
						}
					}
					else
					{
						if($is_super_admin) //$is_super_admin
						{
							$new_exam_id = $edit_exam;
							$result = update_query('required_tests', $data_array, 'exam_id = '.$edit_exam);

							if($result)
							{
								add_success_message('Промените са запазени успешно!');
								delete_query('objects_visibility', 'object_type = "exam" AND object_id = '.$edit_exam, 0);
							}
							else
							{
								add_error_message('Неочакван проблем при запазване на промените!');
							}
						}
						else
						{
							add_error_message('Редакция на изпит е разрешена само за Супер Администратор!<br />Понеже НЕ сте, Вие можете само да добавяте и преглеждате изпити!');
						}
					}

					if(!has_error_messages())
					{
						$current_exams = getCurrentTestsPerCourse($_POST['course_id']);
						$current_exams_weight = round( (0.3 / count($current_exams)) , 3);

						$final_exams = getFinalTestsPerCourse($_POST['course_id']);
						$final_exams_weight = round( (0.25 / count($final_exams)) , 3);

						update_query('required_tests', array('weight' => $current_exams_weight), 'course_id = "'.(int) $_POST['course_id'].'" AND exam_type = "current"');
						update_query('required_tests', array('weight' => $final_exams_weight), 'course_id = "'.(int) $_POST['course_id'].'" AND exam_type = "final"');

						if(isset($_POST['visibility']) && is_array($_POST['visibility']) && count($_POST['visibility']) > 0)
						{
							foreach($_POST['visibility'] as $visibility)
							{
								$data_visibility = array(
									'object_id' => $new_exam_id,
									'object_type' => 'exam',

									// Set Default Date Values
									'start_date' => date('Y-m-d H:i:s'),
									'end_date' => date('Y-m-d ', strtotime('+1 week')).'23:59:59'
								);

								$data_visibility['group_number'] = $visibility['group_number'];
								$data_visibility['start_date'] = $visibility['start_date'];
								$data_visibility['end_date'] = $visibility['end_date'];

								$new_visibility_id = insert_query('objects_visibility', $data_visibility);
							}
						}
					}
				}

				database_close();
			}

			set('exam_types', $exam_types);
		}
		else
		{
			page_not_access();
		}
	}

	function action_exam_manage()
	{
		Global $is_admin_user, $source, $user_id, $_connection, $assistants_array, $admins_array;
		database_open();

		if(!$is_admin_user && !is_assistant())
		{
			header('location:'.link_to( array(CONTROLLER => 'user', ACTION => 'view', 'OPTION_SET_CLASS' => false) ));
			exit;
		}

		$query_string = '1=1';

		if(!isset($source['course_id']))
		{
			$source['course_id'] = 0;
		}

		if(isset($source['course_id']) && $source['course_id'] != '-1')
		{
			$courses_filter = $source['course_id'];
			if(0 == $courses_filter)
			{
				$courses_filter = array_column(getCurrentCourses(),'course_id');
			}

			if(is_numeric($courses_filter))
			{
				$query_string .= ' AND c.course_id = '.(int) $courses_filter;
			}

			if(is_array($courses_filter))
			{
				$query_string .= ' AND c.course_id IN ('.implode(',', $courses_filter).')';
			}
		}

		if(isset($source['test_id']) && $source['test_id'] != '-1')
		{
			if(is_numeric($source['test_id']))
			{
				$query_string .= ' AND t.test_id = '.(int) $source['test_id'];
			}

			if(is_array($source['test_id']))
			{
				$query_string .= ' AND t.test_id IN ('.implode(',', $source['test_id']).')';
			}
		}

		if(isset($source['exam_type']) && $source['exam_type'] != '-')
		{
			$query_string .= ' AND exam_type = "'.mysqli_real_escape_string($_connection, $source['exam_type']).'"';
		}

		if(isset($source['for_office_only']) && $source['for_office_only'] != '-1')
		{
			$query_string .= ' AND for_office_only = '.(int) $source['for_office_only'];
		}
		else
		{
			$source['for_office_only'] = -1;
		}

		if(isset($source['weight']) && $source['weight'] != '')
		{
			$query_string .= ' AND weight = '.($source['weight'] / 100);
		}

		if(isset($source[ORDER_BY]) && $source[ORDER_BY] != '')
		{
			$query_string .= ' ORDER BY '.mysqli_real_escape_string($_connection, $source[ORDER_BY]);
		}
		else
		{
			$query_string .= ' ORDER BY exam_id';
		}

		// Get User Homeworks
		//$homeworks = get_all_homeworks('ch.course_id IN (SELECT course_id FROM courses WHERE start_date <= "'.$current_date.'" AND "'.$current_date.'" <= end_date) ORDER BY course_id, homework_title');
		$courses_exams = get_all_exams($query_string);
		$query_string = last_query();

		database_close();

		set('courses_exams', $courses_exams);
		set('query_string', $query_string);
	}
?>