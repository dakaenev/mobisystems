<?php
	define('_ACT_', 'act');
	define('_PLATFORM_', 'platform');
	define('_ACTIVATION_KEY_', 'activationKey');

	define('_TABLE_KEYS_', 'ms_keys');
	define('_TABLE_PLATFORMS_', 'ms_platforms');

	
	function action_api_mobisystems()
	{
		Global $source, $is_empty_layout;

		$supported_actions = ['check', 'activate', 'info'];
		$_REQUEST['layout'] = 'empty';

		$errors = array();
		$response = array(
			'status' => '',
			'message' => ''
		);

		if(isset($_REQUEST[_ACT_]))
		{
			if(is_string($_REQUEST[_ACT_]))
			{
				$_REQUEST[_ACT_] = mb_strtolower($_REQUEST[_ACT_]);

				if(in_array($_REQUEST[_ACT_], $supported_actions))
				{
					switch( $_REQUEST[_ACT_])
					{
						case 'info':
							$function_name = 'show_all_keys';
							break;
						case 'check':
							$function_name = 'validate_key';
							break;
						case 'activate':
							$function_name = 'activate_key';
							break;
					}

					if(function_exists($function_name))
					{
						database_open();
						$function_name($response);
						database_close();
					}
				}
				else
				{
					$errors[] = translate('Parameter ACT is not recognized! Allowed values: '. implode(' | ', $supported_actions));
				}
			}
			else
			{
				$errors[] = translate('Parameter ACT must be string!');
			}
		}
		else
		{
			$errors[] = translate('Missing required parameter ACT. It is required!');
		}

		if(count($errors))
		{
			$response['status'] = 'error';
			$response['message'] = implode(PHP_EOL, $errors);
		}

		echo json_encode($response);
		exit;
	}

	function check_input_params()
	{
		$errors = array();
		$params = array();

		if(isset($_REQUEST[_PLATFORM_]))
		{
			$params[] = set_where('platform_name', '=', $_REQUEST[_PLATFORM_]);
		}
		else
		{
			$errors[] = translate('Platform is required!');
		}

		if(isset($_REQUEST[_ACTIVATION_KEY_]))
		{
			$params[] = set_where('key_code', '=', $_REQUEST[_ACTIVATION_KEY_]);
		}
		else
		{
			$errors[] = translate('Activation Key is required!');
		}

		if(!empty($errors))
		{
			$response['status'] = 'error';
			$response['message'] = implode(PHP_EOL, $errors);
			echo json_encode($response);
			exit;
		}

		return implode(' AND ', $params);
	}

	function activate_key(&$response)
	{
		$params = check_input_params();
		$keys_data = get_keys_full_info($params);
		if(Only_One_Exists($keys_data))
		{
			$key_data = reset($keys_data);
			//pre_print($key_data); exit;

			if(is_null($key_data['activated_on']) || '' == $key_data['activated_on'])
			{
				update_query(_TABLE_KEYS_, ['activated_on' => date('Y-m-d H:i:s')], 'key_id = '.(int) $key_data['key_id']);
				$response['message'] = 'Key are successfully activated!';
				$response['status'] = 'success';
			}
			else
			{
				$response['status'] = 'error';
				$response['message'] = translate('Sorry this code was activated on '.$key_data['activated_on']);
			}
		}
		else
		{
			$response['status'] = 'error';
			$response['message'] = translate('Something wrong in file '.__FILE__.', line: '.__LINE__);
		}
	}

	function validate_key(&$response)
	{
		$params = check_input_params();
		$keys_data = get_keys_full_info($params);
		if(count($keys_data))
		{
			$response['message'] = 'Key are valid';
			$response['status'] = 'success';
			$response['data'] = $keys_data;
		}
		else
		{
			$response['status'] = 'error';
			$response['message'] = translate('Wrong platform/key combination');
		}
	}

	function show_all_keys(&$response)
	{
		$keys_data = get_keys_full_info();
		if(count($keys_data))
		{
			$response['message'] =  'See data';
			$response['status'] = 'success';
			$response['data'] = $keys_data;
		}
		else
		{
			$response['status'] = 'error';
			$response['message'] = translate('Empty result in file '.__FILE__.', line: '.__LINE__);
		}
	}

	function get_keys_full_info($params_string = '')
	{
		$query =  'SELECT platform_name, key_id, key_code, activated_on FROM '._TABLE_KEYS_.' AS k '
				. 'INNER JOIN '._TABLE_PLATFORMS_.' AS p ON k.platform_id = p.platform_id '
				. (!empty($params_string) ? 'WHERE '.$params_string : '');

		return to_assoc_array(exec_query($query));
	}
?>