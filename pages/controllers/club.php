<?php

	function action_club_index()
	{
		Global $source, $_connection;
		database_open();

		$query_string = '1';

		if(isset($source['course_id']) && $source['course_id'] > 0)
		{
			if(is_numeric($source['course_id']))
			{
				$query_string .= ' AND up.course_id = '.(int) $source['course_id'];
			}
			elseif(strpos($source['course_id'], ',')) // If we have format like user_id=1,2,3 convert it to array
			{
				$source['course_id'] = explode(',', $source['course_id']);
			}

			if(is_array($source['course_id']))
			{
				$query_string .= ' AND up.course_id IN ('.implode(',', $source['course_id']).')';
			}
		}

		if(isset($source['user_id']))
		{
			if(strpos($source['user_id'], ',')) // If we have format like user_id=1,2,3 convert it to array
			{
				$source['user_id'] = explode(',', $source['user_id']);
			}

			if(is_array($source['user_id']))
			{
				$query_string .= ' AND up.user_id IN ('.implode(', ', $source['user_id']).')';
			}
			else
			{
				$query_string .= ' AND up.user_id = '.(int) $source['user_id'];
			}
		}

		if(isset($source['ancestor_id']) && $source['ancestor_id'] >= 0)
		{
			$query_string .= ' AND c.descendant_of = '.(int) $source['ancestor_id'];
		}

		if(isset($_GET['full_name']) && trim($_GET['full_name']) != '')
		{
			$query_string .= ' AND '.like_trans(array('u.first_name', 'u.last_name'), $_GET['full_name']); // За да използваме тази функция, трябва да е отворена връзката с базата данни!
		}

		if(isset($source['method']) && $source['method'] >= 0)
		{
			$query_string .= ' AND up.method = '.(int) $source['method'];
		}
		else
		{
			$source['method'] = -1;
		}

		if(isset($source['not_pay']) && $source['not_pay'] == 1)
		{
			$query_string .= ' AND up.end_date < "'.date('Y-m-d').'"';
		}

		if(isset($source[ORDER_BY]) && $source[ORDER_BY] != '')
		{
			$query_string .= ' ORDER BY '.mysqli_real_escape_string($_connection, $source[ORDER_BY]);
		}

		$payments = get_all_payments($query_string);
		$query_string = last_query();
		database_close();

		set('payments', $payments);
		set('query_string', $query_string);
	}

	function action_club_premium()
	{
		Global $user_id, $vip_prices, $is_admin_user, $is_super_admin;

		$user = (isset($_GET['user_id']) ? $_GET['user_id'] : $user_id);
		$money = (isset($_GET['amount']) ? $_GET['amount'] : 0);
		$period = (isset($_GET['period']) ? $_GET['period'] : 0);
		$course = (isset($_GET['course_id']) ? $_GET['course_id'] : -1);
		$validation = md5($user.'_'.$course);
		$url_ok = '/?user_id='.$user.'&course_id='.$course.'&key='.$validation;
		$descr = '/?user_id='.$user.'&course_id='.$course;
		database_open();

		if($course > 0)
		{
			$course_data = getCourseData($course);

			if(Only_One_Exists($course_data))
			{
				$course_data = reset($course_data);
				$course_exists = true;
				$max_period = ceil( (strtotime($course_data['end_date']) - strtotime('now')) / (60*60*24*31) );
				//echo $max_period;
			}
			else
			{
				$course_exists = false;
				$max_period = 12;
			}
		}
		else
		{
			$course_exists = false;
			$max_period = 12;
		}

		if(isset($_GET['key']))
		{
			if($_GET['key'] == $validation || $is_super_admin)
			{
				$cm_data = array(
					'course_id' => $course,
					'user_id' => $user,
					'role' => (date('Y-m-d') < $course_data['start_date'] ? 'candidate' : 'student'),
					'form_id' => '1',
					'group_number' => '1',
					'created_on' => time(),
					'created_from' => $user_id
				);

				$data = array(
					'user_id' => $user,
					'course_id' => $course,
					'amount' => $money,
					'method' => 1,
					'start_date' => date('Y-m-d'),
					'end_date' => date('Y-m-d', strtotime('+'.$period.' month'))
				);


				$id = insert_query('user_payments', $data);
				if($course_exists && !getCourseMemberID($course, $user))
				{
					insert_query('course_members', $cm_data);
				}

				if($id)
				{
					header('location:'. header_link(array(CONTROLLER  => 'club', ACTION => 'premium', VIEW => 'thanks')));
					exit;
				}
				else
				{
					add_error_message('Неочакван проблем на линия '.__LINE__.'!<br /><a href="">Свържете се с нас</a> по телефона за да разрешим проблема!');
				}
			}
			else
			{
				add_error_message('Некоректно подадени данни за параметър $key!<br />Ако срещате трудност <a href="">свържете се с нас</a> по телефона!');
			}
		}

		if(isset($_GET[VIEW]) && 'cancel' == $_GET[VIEW])
		{
			add_error_message('Съжаляваме, че се отказахте от поръчката!<br />Ако срещате трудност <a href="">свържете се с нас</a> по телефона!');
		}

		if(isset($_GET[VIEW]) && 'thanks' == $_GET[VIEW])
		{
			add_success_message('Вие доказахте, че сте сериозен!<br />Благодарим Ви, за Вашата подкрепа!');
		}

		$have_discount = DISCOUNT_START;

		if(is_null($look_for_user))
		{
			$look_for_user = $user_id;
		}

		$UserCertificates = getCertificates(null, $look_for_user);
		$have_discount += count($UserCertificates) * DISCOUNT_STEP;

		// Намаление за асистенти и администратори
		if(is_assistant($look_for_user) || is_admin_user($look_for_user))
		{
			$have_discount += DISCOUNT_STEP * 2;
		}

		if($have_discount > DISCOUNT_MAX)
		{
			// Set Maximum Discount Limit
			$have_discount = DISCOUNT_MAX;
		}


		$vip_info = array();
		foreach($vip_prices as $month => $month_price)
		{
			$discount_month_price = $month_price - ($month_price * $have_discount / 100);
			$discount_price = abs($month) * $discount_month_price;

			$button_options = array(
				'price' => $discount_price,
				'period' => abs($month),
				'url_ok' => $url_ok,
				'descr' => $descr
			);

			$temp = array();
			$temp['descr'] = '<span style="padding:5px 10px 0;">'.abs($month). ' месечен абонамент </span>';
			$temp['month'] = '<span style="padding:5px 10px 0;">'.($have_discount ? $month_price.' - '.$have_discount.'% = '.$discount_month_price : $month_price).' лева / месец </span>';
			$temp['price'] = '<span style="padding:5px 10px 0;">'.$discount_price.' лв. </span>';

			if($course >= 0)
			{
				if($month <= $max_period)
				{
					$temp['epbtn'] = ($month < 0 ? '<span class="red center">Очаквайте</span>' : '<span class="center">'.ePay_Button($button_options).'</span>');
				}
				else
				{
					$temp['epbtn'] = '<span class="center"><strong>не е наличен</strong></span>';
				}
			}

			if(0 < $month && $month <= $max_period)
			{
				$vip_info[] = $temp;
			}
		}

		$vip_columns = array(
			array( 'attr' => 'class="warning"', 'text' => 'Описание' ),
			array( 'attr' => 'class="warning"', 'text' => 'Цена на месец' ),
			array( 'attr' => 'class="warning"', 'text' => 'Сума' ),
			array( 'attr' => 'class="warning"', 'text' => '&nbsp;' )
		);

		if($course < 0)
		{
			array_pop($vip_columns);
		}

		database_close();

		set('vip_columns', $vip_columns);
		set('course_data', $course_data);
		set('vip_info', $vip_info);
		set('url_ok', $url_ok);
		set('course', $course);
		set('user', $user);
	}

	function action_club_add()
	{
		Global $source, $is_super_admin, $user_id;

		database_open();
		$default = array();

		$payment_id = (int) $source[ID];
		$is_edit = ($payment_id != 0);

		if(!$is_super_admin)
		{
			add_error_message('Достъп до плащанията е разрешен само за Супер Администратор!');
		}
		else
		{
			if(empty($_POST))
			{
				if(!$is_edit) // This is New - Load Default Data
				{
					$default['user_id'] = (isset($_GET['default_user']) ? $_GET['default_user'] : '');
					$default['course_id'] = (isset($_GET['default_course']) ? $_GET['default_course'] : 0);
					$default['amount'] = (isset($_GET['default_amount']) ? $_GET['default_amount'] : '');
					$default['method'] = -1;
					$default['start_date'] = (isset($_GET['default_start']) ? $_GET['default_start'] : ''); //date('Y-m-d')
					//$default['end_date'] = date('Y-m-d', ( isset($_GET['default_start']) ? $_GET['default_start'] + 60*60*24*7 : strtotime('+1 week') ) );
				}
				else // This is Edit - Load Exam Data
				{ add_warning_message('Тази форма все още не променя записаните курсове!!!');
					$payment_data = get_all_payments('id = '.$payment_id);

					if(Only_One_Exists($payment_data))
					{
						$default = reset($payment_data);

						$date_diff = strtotime($default['end_date']) - strtotime($default['start_date']);
						$default['period'] = round( $date_diff / (60*60*24*31), 0 );
					}
					else
					{
						add_error_message('Не съществува запис #'.$payment_id.'!');
						//header('location:'.header_link(array(CONTROLLER => 'club', ACTION => 'index')));
						//exit;
					}
				}

				if(isset($default['user_id']) && '' != $default['user_id'])
				{
					$getNameQuery = 'SELECT CONCAT(first_name," ",last_name) AS full_name FROM users WHERE user_id = '.$default['user_id'];
					$user_data = to_assoc_array(exec_query($getNameQuery));

					if(Only_One_Exists($user_data))
					{
						$user_data = reset($user_data);
						$default['full_name'] = $user_data['full_name'];
					}
					else
					{
						$default['full_name'] = 'Неизвестен Потребител!';
					}
				}
			}
			else // Form is sent
			{
				$default = $_POST;
				unset($_POST['period']);
				unset($_POST['full_name']);

				$error_messages = array();

				if(!isset($_POST['user_id']) || 0 == (int) $_POST['user_id'])
				{
					$error_messages[] = '- Изберете потребител';
				}

				if(!isset($_POST['course_id']) || $_POST['course_id'] <= 0)
				{
					$error_messages[] = '- Изберете курс';
					$course_exists = false;
				}
				else
				{
					$course_data = getCourseData($_POST['course_id']);

					if(Only_One_Exists($course_data))
					{
						$course_data = reset($course_data);
						$course_exists = true;
					}
					else
					{
						$course_exists = false;
						$error_messages[] = '- Избраният от Вас курс НЕ съществува!';
					}
				}

				if(!isset($_POST['method']) || $_POST['method'] < 0)
				{
					$error_messages[] = '- Изберете метод на плащане';
				}

				if(!isset($_POST['amount']) || $_POST['amount'] < 0 || $_POST['amount'] == '')
				{
					$error_messages[] = '- Сумата трябва да е положително число или поне нула';
				}

				if(!isset($_POST['start_date']) || $_POST['start_date'] == '0000-00-00' || $_POST['start_date'] == '')
				{
					$error_messages[] = '- Въведете начална дата';
				}

				if(!isset($_POST['end_date']) || $_POST['end_date'] == '0000-00-00' || $_POST['end_date'] == '')
				{
					$error_messages[] = '- Въведете крайна дата';
				}

				if( (isset($_POST['start_date']) && isset($_POST['end_date'])) && $_POST['start_date'] > $_POST['end_date'])
				{
					$error_messages[] = '- Проверете датите: Начална дата > Крайна дата ?!';
				}


				if(empty($error_messages))
				{
					if(!$is_edit)
					{
						$new_payment_id = insert_query('user_payments', $_POST);

						if(getCourseMemberID($_POST['course_id'], $_POST['user_id']))
						{
							add_warning_message('Този потребител вече е член на курса! Моля <a href="'.header_link(array(CONTROLLER => 'user', ACTION => 'edit', ID => $_POST['user_id'])).'" target="_blank">проверете</a>!');
						}
						else
						{
							$cm_data = array(
								'course_id' => $_POST['course_id'],
								'user_id' => $_POST['user_id'],
								'role' => (date('Y-m-d') < $course_data['start_date'] ? 'candidate' : 'student'),
								'form_id' => '1',
								'group_number' => '1',
								'created_on' => time(),
								'created_from' => $user_id
							);

							insert_query('course_members', $cm_data);
						}
					}
					else
					{
						update_query('user_payments', $_POST, 'id = '.$payment_id);
						$new_payment_id = $payment_id;
					}

					if($new_payment_id)
					{
						add_success_message('Данните са записани успешно!');
						header('location:'.header_link(array(CONTROLLER => 'club', ACTION => 'add', ID => $new_payment_id)));
						exit;
					}
					else
					{
						add_warning_message('Възникна неочакван проблем на ред ' . __LINE__.'!<br />Моля опитайте отново!');
					}
				}
				else
				{
					add_warning_message('Моля внесете корекции:<br />'.implode('<br  />', $error_messages));
				}
			}
		}

		database_close();
		set('default', $default);
		set('payment_id', $payment_id);
		set('course_exists', $course_exists);
	}
?>