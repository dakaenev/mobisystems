<?php
	// Page Controller
	function action_page_index()
	{
		$_SESSION['SHOW_ADS'] = false;
	}

	function action_page_contacts()
	{
		Global $admins_array;
		$require_code = true;

		if(!empty($_POST))
		{
			database_open();
			$default_data = $_POST;
			//pre_print($_SESSION);

			$required_fields = array('person_name', 'person_mail', 'mail_about', 'mail_text', 'captcha_code');
			check_required_fields($required_fields);

			if(!isset($_POST['accept_tc']))
			{
				add_error_message( translate('MSG_MUST_ACCEPT').' '.translate('THE_TERMS_AND_CONDITIONS').'!' );
			}

			if(!check_captcha())
			{
				add_error_message( translate('MSG_CAPTCHA_MISMATCH') );
			}

			if(!has_error_messages())
			{
				$subscriber = subscribe_user($_POST);

				if($subscriber)
				{
					// User is ready. Insert new Message from it to Administrator
					$mail_data = array(
						'mail_subject' => $_POST['mail_about'],
						'mail_content' => $_POST['mail_text'],
						'created_from' => $subscriber
					);
					$mail_id = insert_mail($mail_data, $admins_array);

					if($mail_id)
					{
						add_success_message( translate('MSG_MESSAGE_SENT_COMPLETE') );
						header('location:'. header_link(array(CONTROLLER => 'page', ACTION => 'contacts')));
						exit;
					}
					else
					{
						add_error_message( translate('MSG_MESSAGE_NOT_SENT') );
					}
				}
				else
				{
					add_error_message( translate('MSG_USER_SAVING_UNCOMPLETE') );
				}
			}

			database_close();
		}
		else
		{
			$default_data = array('person_name' => '', 'person_mail' => '', 'mail_about' => '', 'mail_text' => '', 'captcha_code' => '');

			if(is_user_logged_in())
			{
				//$require_code = false;
				$default_data['person_name'] = $_SESSION[CURRENT_USER]['first_name'].' '.$_SESSION[CURRENT_USER]['last_name'].'" readonly="readonly';
				$default_data['person_mail'] = $_SESSION[CURRENT_USER]['user_email'].'" readonly="readonly'; // Тук и на горния ред няма грешка!!!
			}

			if(!$require_code)
			{
				$default_data['captcha_code'] = $_SESSION['code'];
			}
		}

		set('require_code', $require_code);
		set('default_data', $default_data);
	}

	function action_page_404()
	{

	}

	function action_page_401()
	{

	}

	function action_page_TermsAndConditions()
	{

	}

	function action_page_CookiesPolicy()
	{

	}

	function action_page_gdpr()
	{
	
	}

	function action_page_About()
	{
		$_SESSION['FIX_FOOTER'] = false;
		echo '';
	}

	function action_page_CoWorking()
	{
		$_SESSION['SHOW_ADS'] = false;
	}

	function action_page_FileGetContents()
	{
		echo file_get_contents($_REQUEST['url']);
	}

	function action_page_sms()
	{
		Global $is_ajax;

		if(!empty($_POST))
		{
			// Проверка за наличие на задължителни полета
			if(!isset($_POST['action']) || !isset($_POST['phone']))
			{
				add_error_message('Всички полета са задължителни!');
			}

			// Проверка за валидност на данните
			if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
			{
				add_error_message('Моля въведете валиден е-мейл адрес!');
			}

			database_open();
			$phone_number = validatePhone($_POST['phone']);
			$phone_id = get_phone_id($phone_number);
			$original_id = $phone_id;
			if(!has_error_messages())
			{

				if('send_otp' == $_POST['action'])
				{

					if(!$phone_id)
					{
						// Вкарваме го в базата данни със статус "регистриран"
						$phone_id = insert_query(TABLE_PHONES, ['phone' => $phone_number, 'email' => $_POST['email'], 'reg_status' => STATUS_REG_NEW]);
					}

					$still_active_otp = get_all_otp($phone_number, true);
					$active_time = date('Y-m-d H:i:s', strtotime('+'.FREEZE_TIME.' minute'));
					//print_r($still_active_otp); exit;
					if(empty($still_active_otp)) // Явно всички временни пароли са, по-стари от Лимита заложен в FREEZE_TIME (1 минута в нашия пример)
					{
						$new_otp = Generate_OTP_Code(6);
						insert_query(TABLE_PHONES_OTP, ['active_to' => $active_time, 'otp_value' => $new_otp, 'phone_id' => $phone_id]);
						insert_query(TABLE_PHONES_SMS, ['sms_text' => 'Нова временна парола: '.$new_otp, 'phone_id' => $phone_id]);
						add_success_message('Новата парола пристига веднага. ('.$new_otp.')');
					}
					else
					{
						$last_otp = reset($still_active_otp);
						$time_difference = strtotime($last_otp['active_to']) - strtotime('now');
						add_warning_message('Трябва да изчакате '.$time_difference.' секунди за нова парола.');
					}
				}

				if('check_otp' == $_POST['action'])
				{
					if(!isset($_POST['otp']) || '' == trim($_POST['otp']))
					{
						add_error_message('Моля попълнете временна еднократна парола (OTP)!');
					}
					else
					{
						$auth = ['email' => $_POST['email'], 'otp' => $_POST['otp']];
						$phone_id = get_phone_id($phone_number, $auth);
						//print_r($phone_id); exit;

						if($phone_id && !has_error_messages())
						{
							$_SESSION['WRONG_LOGIN_COUNT'] = 0;
							add_success_message('Welcome to SMS Bump');
							// Променяме го в базата данни със статус "потвърден"
							update_query(TABLE_PHONES, ['reg_status' => STATUS_REG_VERIFIED, 'login_status' => STATUS_LOG_ALLOWED, 'banned_to' => '0000-00-00 00:00:00'], ['id' => $phone_id]);
							insert_query(TABLE_PHONES_LOG, ['phone_id' => $phone_id, 'try_login_with' => $_POST['email'].'|'.$_POST['otp'], 'log_status' => 'success']);
							insert_query(TABLE_PHONES_SMS, ['sms_text' => 'Welcome to SMS Bump with otp: '.$_POST['otp'], 'phone_id' => $phone_id]);
						}
						else
						{
							$_SESSION['WRONG_LOGIN_COUNT']++;
							add_error_message('Грешни входни данни! <br />Оставащи опити: '.(ALLOWED_WRONG_LOGIN - $_SESSION['WRONG_LOGIN_COUNT']));
							insert_query(TABLE_PHONES_LOG, ['phone_id' => $phone_id, 'try_login_with' => $_POST['email'].'|'.$_POST['otp'], 'log_status' => 'wrong']);

							if($_SESSION['WRONG_LOGIN_COUNT'] >= ALLOWED_WRONG_LOGIN)
							{
								$_SESSION['WRONG_LOGIN_COUNT'] = 0;
								update_query(TABLE_PHONES, ['login_status' => STATUS_LOG_BAN, 'banned_to' => date('Y-m-d H:i:s', strtotime('+'.COOLDOWN_TIME.' minute')) ], ['id' => $original_id]);
							}
						}
					}
				}
			}
			database_close();

			if($is_ajax && have_messages())
			{
				foreach($_SESSION[_APP_][_CLASS_MESSENGER] as $row)
				{
					echo br2nl($row['message']);
				}

				clear_all_messages();
				exit;
			}
		}
	}
?>
