<?php
	// Certificates Controller
	function action_certificates_index()
	{
		Global $source, $_connection, $QUESTIONS_TYPES, $QUESTIONS_GROUPS, $is_admin_user;

		if(!$is_admin_user)
		{
			page_not_access();
			exit;
		}

		database_open();
		$query_string = '1';

		if(isset($source['from_city']) && $source['from_city'] != '')
		{
			$query_string .= ' AND uc.serial_number LIKE "'.mysqli_real_escape_string($_connection, $source['from_city']).'%"';
		}

		if(isset($source['course_id']) && $source['course_id'] > 0)
		{
			if(is_numeric($source['course_id']))
			{
				$query_string .= ' AND uc.course_id = '.(int) $source['course_id'];
			}
			elseif(strpos($source['course_id'], ',')) // If we have format like user_id=1,2,3 convert it to array
			{
				$source['course_id'] = explode(',', $source['course_id']);
			}

			if(is_array($source['course_id']))
			{
				$query_string .= ' AND uc.course_id IN ('.implode(',', $source['course_id']).')';
			}
		}

		if(isset($source['user_id']))
		{
			if(strpos($source['user_id'], ',')) // If we have format like user_id=1,2,3 convert it to array
			{
				$source['user_id'] = explode(',', $source['user_id']);
			}

			if(is_array($source['user_id']))
			{
				$query_string .= ' AND uc.user_id IN ('.implode(', ', $source['user_id']).')';
			}
			else
			{
				$query_string .= ' AND uc.user_id = '.(int) $source['user_id'];
			}
		}

		if(isset($source['ancestor_id']) && $source['ancestor_id'] >= 0)
		{
			$query_string .= ' AND c.descendant_of = '.(int) $source['ancestor_id'];
		}

		if(isset($_GET['full_name']) && trim($_GET['full_name']) != '')
		{
			$query_string .= ' AND '.like_trans(array('u.first_name', 'u.last_name'), $_GET['full_name']);
		}

		if(isset($source[ORDER_BY]) && $source[ORDER_BY] != '')
		{
			$query_string .= ' ORDER BY '.mysqli_real_escape_string($_connection, $source[ORDER_BY]);
		}

		// $source['not_show'] Values: 2 - Show Both, 1  - Not Show Existing; 0 - Show Only Existing Certificates
		$one_type_only = (isset($source['not_show']) && $source['not_show'] != 2);
		//if(SHOW_DEBUG_INFO){ echo ''.$query_string.''; }

		$users = array();
		$certificates = array();
		$certificates_array = get_all_certificates($query_string);

		//pre_print($certificates_array);

		$tmp_users = array_column($certificates_array, 'user_id');
		$tmp_users = array_unique($tmp_users);

		if(count($tmp_users) > 0)
		{
			$tmp_users = to_assoc_array(exec_query('SELECT user_id, course_id, want_certificate FROM course_members WHERE role="student" and user_id IN ('.implode(', ', $tmp_users).')'));
		}
		else
		{
			$tmp_users = array();
		}

		$tmp_certificates = array();
		foreach($tmp_users as $v)
		{
			if(isset($tmp_certificates[$v['user_id']]))
			{
				$tmp_certificates[$v['user_id']][$v['course_id']] = $v['want_certificate'];
			}
			else
			{
				$tmp_certificates[$v['user_id']] = array($v['course_id'] => $v['want_certificate']);
			}
		}
		unset($tmp_users);
		//pre_print($tmp_certificates);

		foreach($certificates_array as $k => $certificate)
		{
			// Set Default Values
			$tmp = array(
				'file_path' => '',
				'user_id' => '',
				'course_id' => '',
				'serial_number' => ''
			);

			// Assign with real values...
			if(file_exists($certificate['file_path']))
			{
				$tmp['file_path'] = '<a target="result_frame" href="'.site_url($certificate['file_path']).'"><span title="" class="glyphicon glyphicon-certificate wa-certificate-icon-0"></span></a>';
				$certificate_exists = true;
			}
			else
			{
				pre_print($certificate);
				$tmp['file_path'] = '<span title="" class="glyphicon glyphicon-certificate wa-certificate-icon-1" title="'.$certificate['file_path'].'"></span>';
				$certificate_exists = false;
			}

			$tmp['user_id'] = '&nbsp;<a href="'.header_link(array(CONTROLLER => 'user', ACTION => 'view', ID => $certificate['user_id'])).'">'.$certificate['first_name'].' '.$certificate['last_name'].'</a>&nbsp;';
			$tmp['course_id'] = '&nbsp;<a href="#" title="'.$certificate['course_id'].'">'.$certificate['course_name'].'</a>&nbsp;';
			$tmp['serial_number'] = '&nbsp;'.$certificate['serial_number'].'&nbsp;';
			$tmp['certificates_count'] = '&nbsp;<a href="'.header_link(array(CONTROLLER => 'certificates', ACTION => 'index', 'user_id' => $certificate['user_id'])).'" title="">'.$certificate['certificates_count'].'</a>&nbsp;';


			if($one_type_only)
			{
				if(($source['not_show'] == 1 && !$certificate_exists) || ($source['not_show'] == 0 && $certificate_exists))
				{

					$tmp['aim'] = my_phone_format($certificate['aim']);
					$tmp['want_certificate'] = '';
					$tmp['mail_checkbox'] = '<input type="checkbox" name="send_to['.$certificate['user_id'].']" value="'.$certificate['user_id'].'" checked="checked" style="margin-left:2px;" />';

					switch($tmp_certificates[$certificate['user_id']][$certificate['course_id']])
					{
						case '-1': $tmp['want_certificate'] = '<b>НЕ КАЗВАМ!</b>'; break;
						case  '0': $tmp['want_certificate'] = '<span class="red">НЕ ИСКАМ!</span>'; break;
						case  '1': $tmp['want_certificate'] = '<span class="green">ДА ИСКАМ!</span>'; break;

						default: $tmp['want_certificate'] = '<i>Неизвестно :(</i>'; break;
					}

					$users[] = $certificate['first_name'].' '.$certificate['last_name'].'|'.$certificate['user_id'];
					$certificates[] = $tmp;
				}
			}
			else
			{
				$users[] = $certificate['first_name'].' '.$certificate['last_name'].'|'.$certificate['user_id'];
				$certificates[] = $tmp;
			}
		}
		unset($tmp_certificates);

		// Use this for datalist functionality
		$users = array_unique($users);
		usort($users, function($a,$b){ return mb_strtoupper($a) > mb_strtoupper($b); } );

		foreach($users as $key => $value)
		{
			$value = explode('|', $value);
			$users[$key] = '<option value="'.$value[0].'">'.$value[1].'</option>';
		}

		set('one_type_only', $one_type_only);
		set('certificates', $certificates);
		set('query_string', last_query());
		set('users', $users);
	}



	function certificates_check()
	{
		Global $source;
	}

	function action_certificates_export()
	{
		database_open();
		export_certificates();
		database_close();
	}

	function action_certificates_setAnswer()
	{
		Global $source;

		if(!empty($_POST) && isset($_POST['cm']) && is_array($_POST['cm']) && count($_POST['cm']) > 0)
		{
			database_open();
			foreach($_POST['cm'] as $cm_id => $want_certificate)
			{
				update_query('course_members', array('want_certificate' => $want_certificate), 'id = '.(int) $cm_id);
			}
			database_close();

			add_success_message('Записахме вашето желание! Благодарим Ви!');
		}

		header('location: '.header_link( array(CONTROLLER => 'user', ACTION => 'view', ID => $source[ID]) ));
		exit;
	}
?>