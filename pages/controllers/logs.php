<?php
	function action_logs_index()
	{
		Global $source, $_connection, $is_admin_user, $user_id;

		if(!$is_admin_user)
		{
			page_not_access();
			exit;
		}

		database_open();

		if(!isset($source[VIEW]) || $source[VIEW] == 'all')
		{
			$view = 'all';
		}
		else
		{
			$view = strtolower($source[VIEW]);
		}

		$query_string = '';
		switch($view)
		{
			case 'all': $query_string = '1'; break; // Show All Questions
			//case 'wrong': $query_string = 'is_correct = 0'; break; // Show wrong questions only
			//case 'reported': $query_string = 'question_id IN (SELECT question_id FROM marked_questions)'; break;
			default: $query_string = '1'; $view = 'all'; break; // Show All Questions
		}

		$query_string .= ' AND request_url <> "http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'].'"';
//		$tests = get_all_tests('1=1 ORDER BY test_name ASC');

		if(isset($source['user_id']) && $source['user_id'] > 0)
		{
			$query_string .= ' AND ul.user_id = '.(int) $source['user_id'];
		}

//		$tests_dropdown = ReturnDropDown($tests, $source['test_id']);

		if(isset($_REQUEST['request_url']))
		{
			$query_string .= ' AND (request_url LIKE "%'.mysqli_real_escape_string($_connection, $_REQUEST['request_url']).'%" OR ip_address LIKE "%'.mysqli_real_escape_string($_connection, $_REQUEST['request_url']).'%" )';
		}

		if(isset($_REQUEST['from_place']) && $_REQUEST['from_place'] >= 0)
		{
			$query_string .= ' AND ip_address '.($_REQUEST['from_place'] == 0 ? 'NOT ' : '').'LIKE "78.130.%"';
		}

		if(isset($_REQUEST['from_date']) && $_REQUEST['from_date'] != '')
		{
			$query_string .= ' AND log_time >= '.strtotime($_REQUEST['from_date'].' 00:00:00');
		}

		if(isset($_REQUEST['to_date']) && $_REQUEST['to_date'] != '')
		{
			$query_string .= ' AND log_time <= '.strtotime($_REQUEST['to_date'].' 23:59:59');
		}

		if(isset($source[ORDER_BY]) && $source[ORDER_BY] != '')
		{
			$query_string .= ' ORDER BY '.mysqli_real_escape_string($_connection, $source[ORDER_BY]);
		}
		else // Да премахна този елсе, когато направя страницирането
		{
			$query_string .= ' ORDER BY log_id DESC LIMIT 500';
		}

		$logs = array();
		$logs_array = get_all_logs($query_string);
		$query_string = last_query();

		//Pre_Print($logs_array, true);
		foreach($logs_array as $k => $log)
		{
			$logs[$k]['log_id'] = '<a href="#" data-toggle="modal" data-target="#LogInfoModal" class="load_log_info" data-log_id="'.$log['log_id'].'">'.$log['log_id'].'</a>';
			$logs[$k]['user_id'] = '<a href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element('user_id', $_SERVER['QUERY_STRING']).'&' : '').'user_id='.$log['user_id'].'">'.$log['user_id'].'</a>';;
			$logs[$k]['request_url'] = '<a href="'.$log['request_url'].'">'.$log['request_url'].'</a>';
			$logs[$k]['ip_address'] = $log['ip_address'];
			$logs[$k]['log_time'] = date('Y-m-d H:i:s', $log['log_time']);

			//$questions[$k]['functions'] = '<a href="'.link_to(array(CONTROLLER => 'question', ACTION => 'edit', ID => $v['question_id'])).'"><img src="'.DIR_IMAGES.'ui/application_form_edit.png" alt="" /></a>';
			//$questions[$k]['functions'] .= '<a href="#"><img src="'.DIR_IMAGES.'ui/views.gif" alt="" /></a>';
			//$questions[$k]['functions'] .= '<a href="'.link_to(array(CONTROLLER => 'question', ACTION => 'delete', ID => $v['question_id'], 'OPTION_SET_CLASS' => false)).'" class="delete_btn"><img src="'.DIR_IMAGES.'ui/trash-can-delete.png" alt="" height="16" /></a>';
			//$questions[$k]['is_active'] = '<img src="'.DIR_IMAGES.'ui/'.(int) $v['is_active'].'.png" alt="'.$v['is_active'].'" />';
			//$questions[$k]['is_correct'] = '<img src="'.DIR_IMAGES.'ui/'.(int) $v['is_correct'].'.png" alt="'.$v['is_correct'].'" />';
			//$questions[$k]['question_group'] = '<span style="padding:0px 7px; background-color:'.$QUESTIONS_GROUPS[$v['question_group']].';">'.$v['question_group'].'</span>';
			//$questions[$k]['test_id'] = '<a href="?'.($_SERVER['QUERY_STRING'] != '' ? remove_url_element('test_id', $_SERVER['QUERY_STRING']).'&' : '').'test_id='.$v['test_id'].'" title="'.$v['test_name'].'">'.$v['test_id'].'</a>';
			//$questions[$k]['question_title'] = stripslashes($v['question_title']);
		}

		//Pre_Print($logs, true);
		set('logs', $logs);
		set('query_string',$query_string);
	}

	function action_logs_see()
	{
		Global $source, $_connection, $is_admin_user, $user_id;

		if($is_admin_user)
		{
			database_open();
			$log_info = getLogData($source[ID]);
			database_close();

			$result  = '<div class="modal-content" id="log_info">';
			$result .= '	<div class="modal-header">';
			$result .= '		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
			$result .= '		<h4 class="modal-title" id="myModalLabel">Детайлна информация за №'.$source[ID].'</h4>';
			$result .= '	</div>';
			$result .= '	<div class="modal-body">';

			if(Only_One_Exists($log_info))
			{
				$log_info = reset($log_info);

				//$result .= '<a href="'.link_to( array(CONTROLLER => 'user', ACTION => 'view', ID => $log_info['user_id']) ).'">'.$log_info['first_name'].' '.$log_info['last_name'].'</a>';
				$result .= '<pre>'.print_r(unserialize($log_info['full_info']), true).'</pre>';
			}
			else
			{
				$result .= '<b style="color:red;">Не откривам данни!</b>';
			}

			$result .= '	</div>';
			$result .= '	<div class="modal-footer"><a href="'.link_to( array(CONTROLLER => 'user', ACTION => 'view', ID => $log_info['user_id']) ).'">'.$log_info['first_name'].' '.$log_info['last_name'].'</a></div>';
			$result .= '</div>';
			//$result .= '</span>';
			echo $result;
		}
		else
		{
			page_not_access();
			exit;
		}
	}

	function action_logs_distinct()
	{
		Global $source, $_connection, $is_admin_user, $user_id;

		if($is_admin_user || is_tester())
		{
			database_open();
			$logs_links = to_assoc_array(exec_query('SELECT DISTINCT request_url FROM user_logs ORDER BY request_url'));
			database_close();

			set('logs_links', $logs_links);
		}
		else
		{
			page_not_access();
			exit;
		}
	}

	function action_logs_SetPresentedUsers()
	{
		Global $source, $is_admin_user, $user_id, $is_super_admin;

		if(isset($source[ID]))
		{
			database_open();

			$sql = 'SELECT * FROM course_dates WHERE id = '.(int) $source[ID];
			//echo $sql;
			$course_date_record = to_assoc_array(exec_query($sql));

			if(Only_One_Exists($course_date_record))
			{
				$course_date_record = reset($course_date_record);

				$office_ip = getOfficeAddr($course_date_record['lecture_date']);
				foreach($office_ip as &$ip)
				{
					$ip = '"'.$ip.'"';
				}

				if(count($office_ip))
				{
					$office_ip = implode(',', $office_ip);

					$logs_array = to_assoc_array(exec_query('SELECT DISTINCT(user_id) FROM user_logs AS ul WHERE ip_address IN ('.$office_ip.') AND user_id IN (SELECT user_id FROM course_members WHERE course_id = '.$course_date_record['course_id'].') AND log_time BETWEEN '.strtotime($course_date_record['lecture_date'].' 00:00:00').' AND '.strtotime($course_date_record['lecture_date'].' 23:59:59')), 'user_id');
					$users_count = count($logs_array);

					$logs_array = ','.implode(',', array_keys($logs_array)).',';

					$update_data = array(
						'present_users' => $logs_array,
						'users_count' => $users_count
					);

					update_query('course_dates', $update_data, 'id = '.$course_date_record['id']);
				}
				else
				{
					$users_count = '?';
					$logs_array = '?';
				}

				echo json_encode( array('users_count' => $users_count, 'users_list' => $logs_array) );
			}
			else
			{
				echo 'Course Dates Not Found!';
			}

			database_close();
		}
		else
		{
			echo 'ID is required!';
		}
	}

	function action_logs_decrypt()
	{
		Global $source, $is_admin_user, $user_id, $is_super_admin;

		$span_id = 'decrypted_value';
		if($is_admin_user && $is_super_admin)
		{
			if(isset($_REQUEST[VIEW]))
			{
				if(isset($_GET[VIEW]))
				{
					echo '<span id="'.$span_id.'">'.decrypt($_GET[VIEW]).'</span>';
				}
				else
				{
					echo '<details open="open"><summary>Превод:</summary><p>'.decrypt($_POST[VIEW]).'</p></span>';
				}
			}
			else
			{
				if(isset($_GET[VIEW]))
				{
					echo '<span id="'.$span_id.'">'.$_GET[VIEW].'</span>';
				}
				else
				{
					add_error_message('View parameter not found!');

					show_all_messages();
					clear_all_messages();
				}
			}
		}
		else
		{
			if(isset($_GET[VIEW]))
			{
				echo '<span id="'.$span_id.'">'.$_GET[VIEW].'</span>';
			}
			else
			{
				page_not_access();
				exit;
			}
		}
	}

	function action_logs_addresses()
	{

	}

	function action_logs_AddIpAddress()
	{
		Global $source, $is_admin_user, $user_id,  $_connection;

		if($is_admin_user)
		{
			database_open();

			if(isset($source[ID]))
			{
				$is_edit = true;
				$record = get_all_addresses('id = '.(int) $source[ID]);

				if(Only_One_Exists($record))
				{
					$form_data = reset($record);
				}
				else
				{
					add_error_message('IP Address not exists!');
				}
			}
			else
			{
				$is_edit = false;

				// Default Data for new record
				$form_data = array(
					'ip_address' => (isset($_GET['ip']) ? $_GET['ip'] : $_SERVER['REMOTE_ADDR']),
					'start_date' => date('Y-m-d'),
					'end_date' => '9999-12-31',
					'created_from' => $user_id,
					'description' => '',
					'replace_id' => 0
				);
			}

			if(!empty($_POST))
			{
				$form_data = $_POST; // Показваме това, което сме изпратили през формата, за да опитаме отново!

				$data = array(
					'ip_address' => $_POST['ip_address'],
					'start_date' => $_POST['start_date'],
					'end_date' => $_POST['end_date'],
					'created_from' => $user_id,
					'description' => html_entity_decode(stripslashes($_POST['description']))
				);

				if (!filter_var($_POST['ip_address'], FILTER_VALIDATE_IP))
				{
					add_warning_message($_POST['ip_address'].' не е валиден IP адрес!');
				}

				//add_warning_message('Test...');
				if(!have_messages()) // Ако има сериозни грешки, няма да добавяме / редактираме нищо
				{
					if($is_edit)
					{
						$result = update_query('office_ip_addresses', $data, 'id = '.(int) $source[ID]);
						$goto_id = (int) $source[ID];

						// При промяне опцията за заместване / добавяне не е разрешена затова я премахваме!
						if(isset($form_data['replace_id']))
						{
							unset($form_data['replace_id']);
						}
					}
					else
					{
						$result = insert_query('office_ip_addresses', $data);
						$goto_id = $result;

						// Ако е зададено ID за заместване, спираме неговата валидност!
						exec_query('UPDATE office_ip_addresses SET end_date = "'.date('Y-m-d').'", description = CONCAT(description, ". Заместено с ID: '.$goto_id.' от Потребител: '.$user_id.'") WHERE id='.$form_data['replace_id'].' LIMIT 1');
					}

					if($result)
					{
						add_success_message('Адресът е '.($is_edit ? 'заместен' : 'добавен').' успешно!');
						header('location: '.header_link(array(CONTROLLER => 'logs', ACTION => 'AddIpAddress', ID => $goto_id)));
					}
					else
					{
						add_error_message('Неуспешно записване! <br />Нещо се обърка на ред '.__LINE__.' във файл '.__FILE__);
					}
				}
			}


			database_close();
			set('is_edit', $is_edit);
			set('form_data', $form_data);
		}
		else
		{
			page_not_access();
			exit;
		}
	}
?>