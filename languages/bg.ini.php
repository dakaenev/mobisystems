;<?php
;die(); // For further security
;/*
MENU_HOME = "Начало";
MENU_CONTACTS = "Контакти";

MENU_COURSES = "Курсове";
MENU_BADGES = "Значки";
MENU_FRIENDS = "Приятели";
MENU_PROJECTS = "Проекти";
MENU_SETTINGS = "Настройки";

LANGUAGE_NEXT = "English";

BTN_CANCEL = "Отказ";
BTN_DELETE = "Изтриване";
BTN_FORM_RESET = "Изчисти";
BTN_FORM_SEND = "Изпрати";
BTN_FORM_CHANGE = "Смени";

ADD_CATEGORY = "Добавяне на нова Категория";
ADD_INVAR = "Добавяне на нов инвариант";
BACK_TO_CATEGORIES = "Назад към категориите";
CATEGORY = "Категория";

DESCRIPTION = "Описание:";

LABEL_EDIT = "Редактиране";
LABEL_HELP = "Помощ";
LABEL_PASS = "Парола";
LABEL_SETTINGS = "Настройки";
LABEL_USER = "Потребител";
LABEL_NAME = "Име";
CONTACTS = "Контакти";

LABEL_UPDATE_PASS = "Смяна на парола";

INFO = "Информация";
ALL_FIELDS_REQUIRED = "Всички полета са задължителни!";

CONTACTS_FORM_TEXT = ""; "Lorem ipsum Ex aliqua tempor nisi laboris dolor id laborum irure minim tempor in sit dolore amet sit esse nostrud tempor nulla consequat aute in nostrud laboris sint ullamco amet nisi pariatur officia nulla pariatur in id et labore dolore ad sit.";

MONDAY = "Понеделник";
WEDNESDAY = "Сряда";
FRIDAY = "Петък";
SATURDAY = "Събота";
SUNDAY = "Неделя";

WORK_TIME = "Работно време";
BREAK_TIME = "Обедна почивка";
CLOSED = "Затворено";

MESSAGE_WAIT_WORK_TIME = "Спазвайте работното време!\nТелефонния номер ще се покаже само в работно време!";

YOUR_NAME = "Вашето име";
EMAIL_ADDRESS = "Е-поща";
MESSAGE = "Съобщение";
MESSAGES = "Съобщения";

SEND_MESSAGE = "Изпратете ни съобщение";

SYSTEM_SETTINGS_PHONE = "0886-600-256";
SYSTEM_SETTINGS_ADDRESS = "гр. Пловдив, бул. 6-ти Септември №113";

LABEL_SHOW = "покажи";

; GLOBAL DEFINITIONS
REGISTER = "Регистрация";
LOGIN = "Вход";
SITE_MOTO = "Plovdiv - Web";
SITE_SUB_MOTO = "Пловдивско Уеб Общество";

; LOGIN DEFINITIONS
REMEMBER_ME = "Запомни ме";
FORGOT_YOUR_PASSWORD = "Забрави си паролата?";
REGISTER_NOW = "Все още не си наш член? Регистрирай се сега!";


; REGISTER DEFINITIONS
CLICK_TO_UPLOAD = "Кликнете тук за да <br>прикачите снимка";
UPLOAD = "Прикачи";
READ_AND_ACCEPT =  "Прочетох и приемам";
THE_TERMS_AND_CONDITIONS = "общите условия";
FIRST_NAME = "Вашето име";
LAST_NAME = "Фамилия";
PHONE = "Телефон";
BORN_DATE = "Рождена дата";
LABEL_PASS_AGAIN = "Парола (Отново)";
FB_PROFILE_URL = '<a href="#" id="fb_preview" target="_blank">URL адрес</a> към твоя FaceBook';
NOT_USE_FB_URL = "Нямам FB";
REGISTERED_ON = "Регистриране"; // Създаден на
LAST_LOGIN = "Последно тук"; // Последен вход
COMMING_SOON = "Очаквайте скоро";
PUBLIC_PROFILE = "Публичен профил";
EMAIL = "Е-поща";
FACEBOOK = "FaceBook";
LABEL_UPDATE_PROFILE = "Обновяване на профила";
LABEL_SEX = "Пол";
SEX_MALE = "Мъж";
SEX_OTHER = "Само аз си знам";
SEX_FEMALE = "Жена";

ABP_MESSAGE = 'Нашият сайт се финансира честно чрез реклами';
ABP_MESSAGE_2 = "Нашият сайт се финансира честно чрез реклами.";
WHY_SEE_ADS = "Защо виждам тази реклама?";
DEFAULT_SELECT_LABEL = "-- Изберете --";
COME_FROM = "От къде научихте за нас?";
COME_FROM_ADS = "Реклама";
COME_FROM_FRIEND = "Приятел";
COME_FROM_FACEBOOK = "FaceBook";
COME_FROM_GOOGLE = "Google";
COME_FROM_OTHER = "Друго";
WHO_FRIEND = "Кой приятел?";
FRIEND_NAME = "Името на Вашият приятел";
PLEASE_EXPLAIN = "Моля пояснете...";


; ERROR MESSAGES
MSG_MUST_ACCEPT = "Необходимо е да приемете";
MSG_PASSWORDS_MISMATCH = "Двете пароли не съвпадат!";
MSG_WRONG_USER_OR_PASS = "Грешен потребител или парола!";
MSG_REQUIRED_FIELD = "Задължително поле!";
MSG_MINIMUM_AGE_REQUIRED = "Съжаляваме, но сте прекалено млад";
MSG_UNKNOWN_SEX = "Не можем да разпознаем вашият пол само по името Ви :)\nМоля попълнете го ръчно!";
UNKNOWN_ERROR_TEMPLATE = 'Възникна неочаквана грешка на ред {LINE_VAR} във файл "...{FILE_VAR}"<br>Опитайте отново, или се <a href="{CONTACT_US_URL}">свържете с нас</a>';
MSG_USER_NOT_EXISTS = 'Такъв потребител не съществува!';
MSG_COURSE_NOT_EXISTS = 'Такъв курс не съществува!';
MSG_EMAIL_NOT_EXISTS = 'Такъв е-мейл не съществува!' ;
MSG_EMAIL_EXISTS = 'Такъв е-мейл вече съществува!' ;
MSG_CAPTCHA_MISMATCH = "Въведеният код за сигурност не съвпада!";
MSG_MESSAGE_NOT_SENT = 'Неуспешно изпращане на съобщение!';
MSG_USER_SAVING_UNCOMPLETE = 'Неуспешно записване на потребителя!';
MSG_EMPTY_RESULT = 'Няма записи, отговарящи на посочените критерии!';
MSG_UNKNOWN_ERROR = 'Нещо се обърка !';
; SUCCESS MESSAGES
MSG_REGISTER_COMPLETE = "Вие се регистрирахте успешно и вече можете да влезете в профила си!";
MSG_MUST_REGISTER_OR_LOGIN = 'За да продължите нататък трябва да се <a href="{REGISTER_URL}">регистрирате</a> или да <a href="{LOGIN_URL}">влезете</a> в профила си!';

MSG_UPDATE_COMPLETE = 'Промените са запазени успешно!';
MSG_MESSAGE_SENT_COMPLETE = "Съобщението е изпратено успешно!";
MSG_MESSAGE_SENT_TO_EMAIL = "Изпратихме ви съобщение на е-мейла";

; INFO MESSAGES
MSG_FB_PREVIEW_INFO = 'Тук можете да тествате дали всичко работи!\n1. Поставете URL към вашият FB профил в полето отдолу\n2. След като промените долното поле кликнете отново тук.\n3. Ако всичко е наред ще се отвори Вашият ФБ профил в нов таб!';

OPEN_IN_NEW_TAB = "Отвори в нов прозорец";
NEW_MESSAGE = "Ново съобщение";

ROLE_0 = "Гост";
ROLE_1 = "Новак";
ROLE_2 = "Последовател";
ROLE_3 = "Редактор";
ROLE_4 = "Преводач";
ROLE_5 = "Автор на курс";
ROLE_6 = "Сметководител";
ROLE_7 = "Наблюдател";
ROLE_8 = "Съмишленик";
ROLE_9 = "Администратор";
ROLE_10 = "Супер Админ"

MEMBERS_BEHIND_US = "Повече от <span>{MEMBERS_COUNT} членове</span> зад нас";
WHAT_DO_WE_DO = "Какво предлагаме";

ARE_YOU_HUMAN = "Are you Human?";
PROVE_HUMANITY = "Докажете, че сте човек, като въведете кода от картинката";


PRIVACY_4_XS = 'Видимо на <a href="#">публичната Ви страница</a><span class="hidden-xs">, достъпно за всеки дори извън системата</span>';
PRIVACY_3_XS = 'Видимо за всички логнати членове';
PRIVACY_2_XS = 'Видимо само за вашите приятели';
PRIVACY_1_XS = '<span class="hidden-md hidden-lg">Видимо само за Вас и никой друг</span>';

PRIVACY_4_LG = "";
PRIVACY_3_LG = "";
PRIVACY_2_LG = 'Видимо само за потребители, които сте маркирали, като Ваши приятели. Останалите потребители ще виждат надпис "само за приятели"';
PRIVACY_1_LG = '<span class="hidden-xs hidden-sm">Видимо само за Вас и никой друг. <span class="hidden-sm">Останалите потребителите ще виждат надпис "не казвам". </span></span>';

KNOW_MORE = "Научи още";
Page_not_found_title = "Страницата липсва";
Page_not_found_subtitle = "Порадвай се на следния виц за извинение";

MONTH_01 = "Януари";
MONTH_02 = "Февруари";
MONTH_03 = "Март";
MONTH_04 = "Април";
MONTH_05 = "Май";
MONTH_06 = "Юни";
MONTH_07 = "Юли";
MONTH_08 = "Август";
MONTH_09 = "Септември";
MONTH_10 = "Октомври";
MONTH_11 = "Ноември";
MONTH_12 = "Декември";

MONTH_JANUARY = "Януари";
MONTH_FEBRUARY = "Февруари";
MONTH_MARCH = "Март";
MONTH_APRIL = "Април";
MONTH_MAY = "Май";
MONTH_JUNE = "Юни";
MONTH_JULY = "Юли";
MONTH_AUGUST = "Август";
MONTH_SEPTEMBER = "Септември";
MONTH_OCTOBER = "Октомври";
MONTH_NOVEMBER = "Ноември";
MONTH_DECEMBER = "Декември";

STATUS = "Статус";
MATERIALS = "Материали";
CALENDAR = "Календар";
HOMEWORKS = "Домашни";
EXAMS = "Изпити";
AVATAR = "Снимка";

MISSING_FIRST_NAME = "Собствено име";
MISSING_LAST_NAME = "Фамилно име";
MISSING_AVATAR = "Профилна снимка";
MISSING_PHONE = "Телефонен номер";
MISSING_EMAIL = "Имейл адрес";
MISSING_FACEBOOK = "Facebook профил";
MISSING_SEX = "Пол";
MISSING_BORN_DATE = "Дата на раждане";
MISSING_LAST_LOCATION = "Последно местоположение";
MISSING_PARENT_ID = "От къде научихте за нас";
MISSING_SUMMARY = "Вашият профил е непълен! Моля, <a href="{EDIT_USER_URL}">редактирайте</a>:"

YOUR_contribution = "Вашият принос:";
YOUR_contribution_MODAL_LINK = "Научи повече";
YOUR_CONTRIBUTION_MODAL_TITLE = 'Как се изчислява приноса?'
;*/
;?>
