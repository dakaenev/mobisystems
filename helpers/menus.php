<?php
	function get_all_menus($where = null)
	{
		$QGet = exec_query('SELECT * FROM menus AS m '.(!is_null($where) ? ' WHERE '.$where : ''));

		return to_assoc_array($QGet);
	}

	function generate_menu_item($menu_item_data)
	{
		if($menu_item_data['link_text'] == '---')
		{
			return '<li class="divider"></li>';
		}
		else
		{
			return '<li class="cstMenu"><a href="'.(substr($menu_item_data['link_href'], 0, 4) == 'http' ? $menu_item_data['link_href'] : get_site_url().$menu_item_data['link_href']).'" class="'.$menu_item_data['link_class'].'"  target="'.$menu_item_data['link_open'].'">'.$menu_item_data['link_text'].'</a></li>';
		}
	}

	function getMenuItems($menu_name)
	{
		Global $_connection;

		return to_assoc_array(exec_query('SELECT mi.* FROM menus AS m LEFT JOIN menus_items AS mi ON m.id = mi.menu_id WHERE m.menu_name = "'.mysqli_real_escape_string($_connection, $menu_name).'" ORDER BY mi.link_order, mi.id ASC'), 'id');
	}

	function navbar_menu($menu_name)
	{
		$menu_items = array();
		$menu_items_array = getMenuItems($menu_name);
		//pre_print($menu_items_array, true);

		if(!is_user_logged_in())
		{
			// Remove it by menu_items.id (Primary Key)
			unset($menu_items_array[2]);
		}

		foreach($menu_items_array as $record)
		{
			if(!isset($menu_items[$record['parent_id']]))
			{
				$menu_items[$record['parent_id']] = array($record);
			}
			else
			{
				$menu_items[$record['parent_id']][] = $record;
			}
		}

		$tmp = '';


		foreach($menu_items[0] as $main_item)
		{
			if(!isset($menu_items[$main_item['id']]))
			{
				$tmp .= generate_menu_item($main_item);
			}
			else
			{
				$tmp .= '<li class="dropdown cstMenu" >';
				$tmp .= '<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">'.$main_item['link_text'].'</a>';
				$tmp .= '<ul class="dropdown-menu dropdown-menu-left animated-2x animated fadeIn">';

				foreach($menu_items[$main_item['id']] as $child_item)
				{
					if(!isset($menu_items[$child_item['id']]))
					{
						$tmp .= generate_menu_item($child_item);
					}
					else
					{
						$tmp .= '<li class="dropdown-submenu">';
						$tmp .= '<a href="javascript:void(0);" class="has_children">'.$child_item['link_text'].'</a>';
						$tmp .= '<ul class="dropdown-menu dropdown-menu-left">';

						foreach($menu_items[$child_item['id']] as $ancestor_item)
						{
							$tmp .= generate_menu_item($ancestor_item);
						}

						$tmp .= '</ul>';
						$tmp .= '</li>';
					}
				}
				$tmp .= '</ul>';
				$tmp .= '</li>';
			}
		}

		echo '<ul class="nav navbar-nav">'.$tmp.'</ul>';
	}
?>