<?php
	function JS_Location_Functions()
	{
		$code = '
		function getLocation() {
			if (navigator.geolocation) {
			  navigator.geolocation.getCurrentPosition(showPosition);
			} else {
			  alert("Geolocation is not supported by this browser.");
			}
		}

		function showPosition(position) {
			alert("Latitude: " + position.coords.latitude +  "\nLongitude: " + position.coords.longitude);
		}


		function savePosition(position){

		}';

		return trim($code).PHP_EOL.PHP_EOL;
	}

	function JS_SetCookie($return = false)
	{
		$code = '
		function setCookie(cname, cvalue, exdays) {
			var d = new Date();
			d.setTime(d.getTime() + (exdays*24*60*60*1000));
			var expires = "expires="+ d.toUTCString();
			document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
		}';

		if($return)
		{
			return trim($code);
		}
		else
		{
			return trim($code).PHP_EOL.PHP_EOL;
		}
	}

	function JS_Cookie_Accept()
	{
		define_once('ACCEPT_COOKIES', 'ACCEPT_COOKIES');
		if(!isset($_COOKIE[ ACCEPT_COOKIES ]))
		{
			$code = '
				<div class="row">
					<div class="col-md-12">
						<div class="alert alert-border alert-info" style="margin-bottom:0;">
						<i class="fa fa-info"></i> Този сайт използва бисквитки. <br class="hidden-xs">Продължавайки нататък, вие се съгласявате с <a href="'.header_link([CONTROLLER => 'page', ACTION => 'Cookies-Policy']).'">нашата политика за бисквитките</a>.
						<button type="button" class="btn btn-info pull-right accept-cookies" data-dismiss="alert" aria-hidden="true" style="position:relative; bottom: 17px;">Съгласен съм</button>
						</div>
					</div>
				</div>
				<script>
				'.JS_SetCookie(true).'
				$(document).ready(function(){
					$(".accept-cookies").click(function(){
						setCookie("'.ACCEPT_COOKIES.'", "Agree", 30);
					});
				});
				</script>';

			echo trim($code).PHP_EOL.PHP_EOL;
		}
	}

	function AJAX_Avatar_Upload($form_id = 'avatarUploadForm')
	{
		$code = "
		$('body').on('submit', '#$form_id', (function(e) {
			e.preventDefault();
			var formData = new FormData(this);

			$.ajax({
				type:'POST',
				url: $(this).attr('action'),
				data:formData,
				dataType: 'json',
				cache:false,
				contentType: false,
				processData: false,
				success:function(data){
					console.log(data);
					if(data.status == 'success'){
						$('#avatar_address').val('/' + data.file_path);
						$('#avatar_preview').attr('src', '/' + data.file_path);
						//$('#avatar_preview').css('padding-top', 0);
					}else{
						alert(data.messages)
					}
				},
				error: function(data){
					console.log('error');
					console.log(data);
				}
			});
		}));

		$('body').on('change', '#avatar_file', function() {
			$('#$form_id').submit();
		});";

		return trim($code).PHP_EOL.PHP_EOL;
	}

	function JS_Select_All()
	{
		// Може би трябва да се помисли за параметри на {.select-all} и data({target})

		$code = "
		$('body').on('click','.select-all', function(){
			$('.' + $(this).data('target')).prop('checked', true);
		});";

		return trim($code).PHP_EOL.PHP_EOL;
	}

	function JS_Name_Privacy_Change()
	{
		// Може би трябва да се помисли за параметри на {.select-all} и data({target})

		$code = "
		$('body').on('click','.privacy-first_name, .privacy-last_name', function(){
			var new_val = $(this).val();

			$('#privacy-first_name-' + new_val).prop('checked', true);
			$('#privacy-last_name-' + new_val).prop('checked', true);
			$('#privacy-full_name').val(new_val);
		});";

		return trim($code).PHP_EOL.PHP_EOL;
	}

	function JS_Refresh_Mails($look_for_user = null)
	{
		if(is_null($look_for_user))
		{
			$look_for_user = get_current_user_id();
		}

		$code = '
		$("body").on("click", "a.refresh_messages", function(){
			var myParrent = $(this).closest("table").parent();

			myParrent.html(\'<span id="status"></span>\');
			myParrent.load("'.header_link(array(CONTROLLER => 'mail', ACTION => 'show', ID => $look_for_user)).' #show_mails_table");
		});';

		return trim($code).PHP_EOL.PHP_EOL;
	}

	function JS_Restore_Mails()
	{
		$code = '
		$("body").on("click", ".restore_message", function(){
			var mail_id = $(this).data("mail_id");
			var this_row = $(this).parent().parent();

			$.post( "'.header_link(array(CONTROLLER => 'mail', ACTION => 'restore', 'layout' => 'empty')).'" + "&mr_id="+mail_id, function( data ) {
				if(data == "success"){
					$("*", this_row).removeClass("deleted_mail");
					$(this).parent().html(\'<input type="checkbox" class="del_chkbox" name="del_chkbox[]" value="\' + mail_id + \'" />\');
				}else{
					alert("Error! :(");
				}
			});
		});';

		return trim($code).PHP_EOL.PHP_EOL;
	}

	function JS_Delete_Mails()
	{
		$code = '
		$("body").on("click", "#btn_delete_mails", function(){
			var chkbox_id = "input.del_chkbox:checked";

			if($(chkbox_id).length > 0){
				var answer = confirm("Наистина ли искате да изтриете тези съобщения?");

				if(answer == true){
					$.post( "'.header_link(array(CONTROLLER => 'mail', ACTION => 'delete', 'layout' => 'empty')).'", $( "#mail_form" ).serialize() );
					$(chkbox_id).parent().parent().remove();
				}
			}else{
				alert("Първо изберете съобщенията, които искате да изтриете!");
			}
		});';

		return trim($code).PHP_EOL.PHP_EOL;
	}

	function JS_Load_Mails($look_for_user = null, $reload_time_seconds = 60)
	{
		if(is_null($look_for_user))
		{
			$look_for_user = get_current_user_id();
		}

		$code = '
		function LoadMails(){
			let myFirstPromise = new Promise((resolve, reject) => {
				$("#MessagesBody").load("'.header_link(array(CONTROLLER => 'mail', ACTION => 'show', ID => $look_for_user)).' #show_mails_table");
				setTimeout(function(){ resolve($("#new_mails_count").text()); }, 1000);
			});

			myFirstPromise.then((successMessage) => { // successMessage ???
				successMessage = $("#new_mails_count").text(); // successMessage ???
				//console.log("Promise successMessage: "+ successMessage);
				$("#badge_mails_count").text(successMessage);
				if(0 == successMessage){
					$("#mailbox_button").removeClass("btn-danger");
				}else{
					$("#mailbox_button").addClass("btn-danger");
				}
			});
		}


		var intervalID = setInterval( LoadMails, ' . ($reload_time_seconds * 1000) . ' );
		setTimeout(LoadMails, 500);';

		return trim($code).PHP_EOL.PHP_EOL;
	}

	function JS_Mails_Functions($look_for_user = null)
	{
		if(is_null($look_for_user))
		{
			$look_for_user = get_current_user_id();
		}

		JS_Refresh_Mails();
		JS_Restore_Mails();
		JS_Delete_Mails();
		JS_Load_Mails();
	}

	function Show_Mails_Modal()
	{
		$code = '
		<form id="mail_form">
			<div class="modal fade" id="mails_modal" tabindex="-1" role="dialog" aria-labelledby="MailsModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<button type="button" class="close size-toggle"><i class="glyphicon glyphicon-transfer"></i></button>
							<h4 class="modal-title" id="MessagesLabel">'.translate('MESSAGES').'</h4>
						</div>
						<div class="modal-body" id="MessagesBody"></div>
						<div class="modal-footer" style="padding:2px;">
						'.(false ? '<a href="'.header_link(array(CONTROLLER => 'mail', ACTION => 'create')).'" class="normal_link" id="btn_create_mail"><button type="button" style="width:75px; font-weight:bold;"><img src="/images/ui/add_page_16.gif" alt="New Message"> Ново</button></a>' : '').'
						</div>
					</div>
				</div>
			</div>
		</form>';

		return trim($code).PHP_EOL.PHP_EOL;
	}

	function Create_Mails_Modal($send_to)
	{
		$code = '
		<div class="modal fade" id="mails_modal" tabindex="-1" role="dialog" aria-labelledby="MailsModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<button type="button" class="close size-toggle"><i class="glyphicon glyphicon-transfer"></i></button>
						<h4 class="modal-title" id="MessagesLabel">'.translate('NEW_MESSAGE').'</h4>
					</div>
					<div class="modal-body" id="MessagesBody" data-src="'.header_link(array(CONTROLLER => 'mail', ACTION => 'create', 'send_to['.$send_to.']' => $send_to)).' #mail_form"></div>
				</div>
			</div>
		</div>';

		return trim($code).PHP_EOL.PHP_EOL;
	}
?>