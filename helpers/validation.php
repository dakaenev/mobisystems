<?php
	function check_captcha($variable = 'captcha_code', $session_string = 'code')
	{
		return ($_REQUEST[$variable] == $_SESSION[$session_string]);
	}

	function check_required_fields($required_fields)
	{
		$errors = array();
		foreach($required_fields as $field_name)
		{
			if(!isset($_REQUEST[$field_name]) || $_REQUEST[$field_name] == '')
			{
				$errors[] = $field_name.' - '.translate('MSG_REQUIRED_FIELD');
			}
		}

		if(!empty($errors))
		{
			add_error_message($errors);
			return false;
		}
		else
		{
			return true;
		}
	}

	function check_user_fields($user)
	{
		//pre_print($user);
		$errors = array();
		$exclude_fields = ['parent_id', 'last_location'];
		$all_fields = count($user['data']);
		$correct_fields = 0;
		foreach($user['data'] as $k => $v)
		{
			if($v == '' && !in_array($k, $exclude_fields))
			{
				$errors[] = translate('MISSING_'.$k.'');
			}
			else
			{
				$correct_fields++;
			}
		}
		update_query('users', [ 'score' => (int) $correct_fields / $all_fields * 100 ], [ 'user_id' => $user['data']['user_id'] ]); // Update user score. Next time it will be correct
		if(!empty($errors))
		{
			$message = translate('MISSING_SUMMARY');
			$message = str_ireplace('{EDIT_USER_URL}', header_link([CONTROLLER => 'user',ACTION => 'edit', ID => $user['data']['user_id']]), $message);
			$message = $message.'<br><ul><li>'.implode('</li><li>', $errors).'</li></ul>';

			add_info_message($message);
		}
	}

	// Return True if email_NOT exits
	function check_email_exists($email)
	{
		$email = escape_string($email);
		$result_array = get_all_users('user_email = "'. $email .'" OR email = "'. $email .'"');
		//pre_print($result_array);
		return (count($result_array) == 0);
	}

	function check_WA_email($email)
	{
		$email = escape_string($email);
		$result_array = to_assoc_array(exec_query('SELECT ID FROM wp_users WHERE user_email = "'. $email .'" OR user_login = "'. $email .'"'));

		if(Only_One_Exists($result_array))
		{
			$result_array = reset($result_array);

			return $result_array['ID'];
		}
		else
		{
			return null;
		}
	}

	function JS_Validate_Bulgarian_Phone()
	{
		$validation = '
		$("input[type=tel]").keypress(function(event){
			var ew = event.which;

			// Това е първата цифра... Тя трябва да бъде 0 !!!
			if($(this).val().length == 0 && ew != 48){
				alert("Телефонния номер задължително трябва да започва с 0!");
				return false;
			}

			if($(this).val().length == 10){
				alert("Телефонния номер не може да има повече от 10 символа!");
				return false;
			}

			if(48 <= ew && ew <= 57) return true;
			return false;
		});';

		echo trim($validation);
	}

	function JS_Validate_Bulgarian_Only()
	{
		$validation = '
		$(".accept_bg_only").keypress(function(event){
			var ew = event.which;

			if(1040 <= ew && ew <= 1103) return true;
			//alert(ew);
			alert("Създали сме писменост на цял Свят - ПИШИ НА КИРИЛИЦА!\n\nThe Bulgarian version accepts bulgarian alphabet only! \nNon-Bulgarians, please switch to English version!");
			return false;
		});';

		echo trim($validation);
	}

	function JS_Validate_English_Only()
	{
		$validation = '
		$(".accept_en_only").keypress(function(event){
			var ew = event.which;

			if(65 <= ew && ew <= 90) return true;
			if(97 <= ew && ew <= 122) return true;
			//alert(ew);
			return false;
		});';

		echo trim($validation);
	}

	function JS_Validate_Digits_Only()
	{
		$validation = '
		$(".accept_digits_only").keypress(function(event){
			var ew = event.which;

			if(48 <= ew && ew <= 57) return true;
			return false;
		});';

		echo trim($validation);
	}
?>