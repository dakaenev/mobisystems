<?php
	Global $vip_prices, $payment_methods, $payment_icons, $epay_config;

	define_once('DISCOUNT_MAX', 0); 
	define_once('DISCOUNT_STEP', 5);  
	define_once('DISCOUNT_START', 0); 

	define_once('WA_BANK_IBAN', 'BG44 ESPY 4004 0032 3688 34');
	define_once('WA_BANK_BIC', 'ESPYBGS1');
	define_once('WA_CARD_NAME', 'Йордан Божидаров Енев');

	$vip_prices = array(
			1 => '30.00', // 1 месец
			2 => '27.50', // 2 месеца
		    4 => '25.00', // 4 месеца
		  -12 => '20.00'  // 12 месеца
	);

	$payment_methods = array(
		0 => 'неизвестен',
		1 => 'ePay Online',
		2 => 'IBAN превод',
		3 => 'на ръка',
	);

	$payment_icons = array(
		0 => '<i class="fa fa-usd"></i>',
		1 => '<i class="fa fa-money"></i>',
		2 => '<i class="fa fa-credit-card"></i>',
		3 => '<i class="fa fa-thumbs-o-up"></i>'
	);

	$epay_config = array(
		0 => array(
			'NAME' => 'Plovdiv Web',
			'MIN' => '3870568462',
			'CHECKSUM' => '46386a3a07808cd853322c1a41e1f9c57ab2f5f1'
		),
		1 => array(
			'NAME' => 'Yordan Enev',
			'MIN' => '3870568462',
			'CHECKSUM' => '46386a3a07808cd853322c1a41e1f9c57ab2f5f1'
		),
		2 => array(
			'NAME' => 'Deniz Kadir',
			'MIN' => '8843863234',
			'CHECKSUM' => 'd0554a4ff4ba5eb9b8d6ce410b0e1574fd31543f'
		)
	);
	// ============================================================================================================================
	function ePay_Button($button_options, $key = 0)
	{
		Global $epay_config;

		$total_value = $button_options['price'];
			 $url_ok = $button_options['url_ok'];

			$url_ok .= '&amount='.$total_value.'&period='.$button_options['period'];
		$description = ''.(isset($button_options['descr']) ? ': '.$button_options['descr'].'&period='.$button_options['period'] : '');
		 $url_cancel = (isset($button_options['url_cancel']) ? $button_options['url_cancel'] : '/?v=cancel');

		$button = '
		<form method="POST" action="https://www.epay.bg/v3main/paylogin">
		<input type="hidden" name="MIN" value="'.$epay_config[$key]['MIN'].'">
		<input type="hidden" name="TOTAL" value="'.$total_value.'" class="course_all">
		<input type="hidden" name="DESCR" value="'.$description.'">
		<input type="hidden" name="URL_OK" value="'.$url_ok.'">
		<input type="hidden" name="URL_CANCEL" value="'.$url_cancel.'">
		<input type="hidden" name="ENCODING" value="utf8">
		<input type="hidden" name="CHECKSUM" value="'.$epay_config[$key]['CHECKSUM'].'">
		<input type="image" src="https://online.datamax.bg/epaynow/a07.gif" alt="Плащане on-line">
		</form>';

		return $button;
	}

	function get_all_payments($where = null, $use_key = null)
	{
		$query = exec_query('SELECT up.*, CONCAT(u.first_name, " ", u.last_name) AS full_name, c.course_name, c.start_date AS course_start_date FROM user_payments AS up LEFT JOIN courses AS c ON up.course_id = c.course_id LEFT JOIN users AS u ON u.user_id = up.user_id'.(!is_null($where) ? ' WHERE '.$where : ''));

		return to_assoc_array($query, $use_key);
	}

	function getVipUsersPerDate($look_for_date)
	{
		return get_all_payments('start_date <= "'.$look_for_date.'" AND "'.$look_for_date.'" <= end_date', 'user_id');
	}

	function getVipUsersPerCourse($course_id)
	{
		return get_all_payments('up.course_id = '.$course_id, 'user_id');
	}

	function getVipCoursesPerUser($user_id)
	{
		return get_all_payments('up.user_id = '.(int) $user_id, 'course_id');
	}

	function getVip($course_id, $look_for_date = null, $look_for_user = null)
	{
		Global $user_id;

		if(is_null($look_for_user))
		{
			$look_for_user = $user_id;
		}

		if(is_null($look_for_date))
		{
			$look_for_date = date('Y-m-d');
		}

		$have_vip = exec_query('SELECT up.* FROM user_payments AS up WHERE user_id = '.$look_for_user.' AND course_id = '.$course_id.' AND (start_date <= "'.$look_for_date.'" AND "'.$look_for_date.'" <= end_date)');
		return to_assoc_array($have_vip);
	}

	function getVipStatus($course_id, $look_for_date = null, $look_for_user = null)
	{
		$have_vip = getVip($course_id, $look_for_date, $look_for_user);
		return Only_One_Exists( $have_vip );
	}
?>