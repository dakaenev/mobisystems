<?php
	// Files helper
	define('MAX_FILE_SIZE', 2.5 * pow(1024, 2), true);
	define('DIR_UPLOADS', 'uploads/', true);

	function get_all_extensions($where = null)
	{
		$QGet = exec_query('SELECT * FROM files_extensions AS fe'.(!is_null($where) ? ' WHERE '.$where : ''));

		return to_assoc_array($QGet);
	}

	function get_allowed_extensions()
	{
		$allowed = get_all_extensions('is_allowed = 1');
		return array_column($allowed, 'mime_type', 'extension');
	}

	function get_all_files($where = null)
	{

	}

	function today_directory($parent = null)
	{
		$default_perms = 0777;

		if(is_null($parent))
		{
			$parent = DIR_UPLOADS.'others'.DS;
		}

		$components = array(
			date('Y'),
			date('m'),
			date('d')
		);

		$today_dir = $parent.implode(DS, $components);

		if(!file_exists($today_dir) && is_dir($today_dir))
		{
			mkdir($today_dir, $default_perms, true);
		}

		if(!is_writable($today_dir))
		{
			chmod($today_dir, $default_perms);
			clearstatcache();
		}

		return $today_dir;
	}

	function get_files_for_directory($directory)
    {
		if(file_exists($directory))
		{
			// create an array to hold directory list
			$results = array();

			// create a handler for the directory
			$handler = opendir($directory);

			// open directory and walk through the filenames
			while ($file = readdir($handler))
			{
				// if file isn't this directory or its parent, add it to the results
				if ($file != "." && $file != "..") {
					$results[] = $file;
				}
			}

			// tidy up: close the handler
			closedir($handler);

			// done!
			return $results;
		}

		return false;
    }

	function FileSizeConvert($bytes)
    {
        $bytes = floatval($bytes);
        $arBytes = array(
            0 => array(
                    "UNIT" => "TB",
                    "VALUE" => pow(1024, 4)
            ),
            1 => array(
                    "UNIT" => "GB",
                    "VALUE" => pow(1024, 3)
            ),
            2 => array(
                    "UNIT" => "MB",
                    "VALUE" => pow(1024, 2)
            ),
            3 => array(
                    "UNIT" => "KB",
                    "VALUE" => 1024
            ),
            4 => array(
                    "UNIT" => "B",
                    "VALUE" => 1
            ),
        );

        foreach($arBytes as $arItem)
        {
            if($bytes >= $arItem["VALUE"])
            {
                $result = $bytes / $arItem["VALUE"];
                $result = str_replace(".", "," , strval(round($result, 2)))." ".$arItem["UNIT"];
                break;
            }
        }
        return $result;
    }

	function safe_filename($name)
	{
		$name = str_replace("#", "No.", $name);
		$name = str_replace("$", "Dollar", $name);
		$name = str_replace("%", "Percent", $name);
		$name = str_replace("&", "and", $name);

		$except = array('\\', '/', ':', '*', '?', '"', '<', '>', '|', '^');
		return str_replace($except, '', $name);
	}

	function upload_file($file_key = 'ufile', $sub_directory = null)
	{
		Global $user_id;

		$errors = array();
		$response = array();

		if(is_null($sub_directory))
		{
			$sub_directory = today_directory();
		}
		else
		{
			if(substr($sub_directory, -1) != DIRECTORY_SEPARATOR)
			{
				$sub_directory .= DIRECTORY_SEPARATOR;
			}
		}

		if(isset($_FILES[$file_key])) //  && is_uploaded_file($_FILES[$file_key][['tmp_name']])
		{
			$file = $_FILES[$file_key];

			// Check $_FILES['upfile']['error'] value.
			switch ($file['error'])
			{
				case UPLOAD_ERR_OK:
					break;
				case UPLOAD_ERR_NO_FILE:
					$errors[] = translate('No_file_sent.');
				case UPLOAD_ERR_INI_SIZE:
				case UPLOAD_ERR_FORM_SIZE:
					$errors[] = translate('Exceeded_filesize_limit.');
				default:
					$errors[] = translate('Unknown_errors.');
			}

			// You should also check filesize here.
			if ($file['size'] > MAX_FILE_SIZE)
			{
				$errors[] = translate('Exceeded_filesize_limit. (' . FileSizeConvert(MAX_FILE_SIZE) . ')');
			}

			$finfo = finfo_open(FILEINFO_MIME_TYPE);
			$file['mime'] = finfo_file($finfo, $file['tmp_name']);
			$file['ext'] = array_search($file['mime'], get_allowed_extensions(), true);

			if(is_numeric(substr($file['ext'], -1)))
			{
				$file['ext'] = substr($file['ext'], 0, -1);
			}

			if(false === $file['ext'])
			{
				$errors[] = translate('Invalid_file_format ').'('.$file['mime'].')';
			}
			//pre_print($file);

			if(empty($errors))
			{
				$now = strtotime('now') + TIME_CORRECTION;
				$new_file_name = $user_id . '_' . $now . '.' . $file['ext'];
				$new_file_name = DIR_UPLOADS . $sub_directory . $new_file_name;

				if(move_uploaded_file($file['tmp_name'], $new_file_name))
				{
					$response = array(
						'file_name' => $file['name'],
						'file_path' => $new_file_name,
						'file_size' => $file['size'],
						'file_type' => $file['type'],
						'file_hash' => md5_file($new_file_name),
						'created_from' => $user_id,
						'created_on' => date('Y-m-d H:i:s')
					);

					$file_id = insert_query('files', $response);

					if(!$file_id)
					{
						$errors[] = translate('MSG_NOT_SAVED_TO_DB');
					}
				}
				else
				{
					//echo $new_file_name;
					$errors[] = translate('Failed to move uploaded file.');
				}
			}
		}
		else
		{
			$errors[] = 'Wrong params for Function '.__FUNCTION__.'!';
		}


		$response['status'] = (empty($errors) ? 'success' : 'error');
		$response['messages'] = (empty($errors) ? 'ok' : implode(PHP_EOL, $errors));

		return $response;
	}

	function getFileExtension($file_name)
	{
		$file_components = explode('.', $file_name);
		return end($file_components);
	}
?>