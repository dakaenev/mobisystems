<?php
	define('DEFAULT_LANGUAGE', 'bg', true);
	define('DIR_LANGUAGES', 'languages/', true);
	define('MARK_TRANSLATIONS', false, true);
	define('MARK_TRANSLATIONS_PARAM', 'mt', true);

	$_Dictionary = array();

	function get_language()
	{
		if(isset($_SESSION['CURRENT_LANGUAGE']))
		{
			if(isset($_GET['lang']))
			{
				// Use new language
				$Lang_CODE = strtolower($_GET['lang']);
			}
			else
			{
				// Use current language
				$Lang_CODE = $_SESSION['CURRENT_LANGUAGE'];
			}
		}
		else
		{
			// Set Default Language
			$Lang_CODE = DEFAULT_LANGUAGE;
		}

		return $Lang_CODE;
	}

	function set_language()
	{
		$current_language = get_language();
		$lang_file = DIR_LANGUAGES.$current_language.'.ini.php';

		if(file_exists($lang_file)) //Check if lang exist
		{
			$Lang_CODE = $current_language;
		}
		else
		{
			// New Language not exists, so we use default language
			$Lang_CODE = DEFAULT_LANGUAGE;
		}

		switch($Lang_CODE)
		{
			case 'bg': $Lang_ID = 1; break;
			case 'en': $Lang_ID = 2; break;
		}

		$_SESSION['CURRENT_LANGUAGE'] = $Lang_CODE;
		define('LANG_CODE', $Lang_CODE, true);
		define('LANG_ID', $Lang_ID, true);

		load_language();
	}

	function load_language()
	{
		Global $_Dictionary;

		$lang_file = DIR_LANGUAGES.$_SESSION['CURRENT_LANGUAGE'].'.ini.php';
		if(file_exists($lang_file))
		{
			$_Dictionary = parse_ini_file($lang_file, false);
			foreach($_Dictionary as $k => $v)
			{
				$t = strtoupper($k);
				if($t !== $k)
				{
					$_Dictionary[$t] = $v;
					unset($_Dictionary[$k]);
				}
			}
		}
		else
		{
			echo 'Езиковия файл '.$lang_file.' липсва! :('; exit;
			$_Dictionary = array();
		}
	}

	function translate($name, $forced_strip_tags = false)
	{
		Global $_Dictionary, $is_admin_user;

		$name = strtoupper($name);

		if(isset($_Dictionary[$name]))
		{
			$translation = $_Dictionary[$name];
		}
		else
		{
			$translation = '<mark class="error_message">['.$name.']</mark>';
		}

		if($is_admin_user && (MARK_TRANSLATIONS || isset($_REQUEST[MARK_TRANSLATIONS_PARAM]) ) )
		{
			$translation = '<mark class="translation">'.$translation.'</mark>';
		}
		else
		{
			$forced_strip_tags = true;
		}


		if($forced_strip_tags)
		{
			$translation = strip_tags($translation,'<br><a><span>');
		}

		return $translation;
	}

	function is_default_language()
	{
		return (DEFAULT_LANGUAGE == LANG_CODE);
	}

	//function import_file(){}
	//function export_file(){}
	//function import_data(){}
	//function export_data(){}
?>