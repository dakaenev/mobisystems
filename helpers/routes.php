<?php
	//pre_print($_SERVER);
	$is_simple = true;

	switch(mb_strtolower($_SERVER['SCRIPT_URL']))
	{
		case '/courses/' : $_SERVER['SCRIPT_URL'] = '/course/index/'; break;
		case '/contacts/' : $_SERVER['SCRIPT_URL'] = '/page/contacts/'; break;
		case '/co-working/' : $_SERVER['SCRIPT_URL'] = '/page/co-working/'; break;

		default: $is_simple = false;
	}
?>