<?php
	function get_all_sms($phone_number, $active_only = false)
	{
		Global $_connection;

		$active_time = date('Y-m-d H:i:s', strtotime('-'.FREEZE_TIME.' minute'));
		$sms_query = 'SELECT phones_sms.* FROM '.TABLE_PHONES_SMS.' AS phones_sms '
					. 'INNER JOIN '.TABLE_PHONES.' AS phones ON phones_sms.phone_id = phones.id '
					. 'WHERE phones.phone = ? '.($active_only ? 'AND sent_timestamp > "'.$active_time.'"' : '')
					. 'ORDER BY phones_sms.id DESC';

		$all_sms = mysqli_prepare($_connection, $sms_query);
		mysqli_stmt_bind_param($all_sms, 's', $phone_number);
		mysqli_stmt_execute($all_sms);

		$results = mysqli_stmt_get_result($all_sms);

		$sms_rows = array();
		while($rows = mysqli_fetch_assoc($results))
		{
			$sms_rows[] = $rows;
		}

		return $sms_rows;
	}

	function get_all_otp($phone_number, $active_only = false)
	{
		Global $_connection;

		$now_time = date('Y-m-d H:i:s', strtotime('now'));
		$otp_query = 'SELECT phones_otp.* FROM '.TABLE_PHONES_OTP.' AS phones_otp '
					. 'INNER JOIN '.TABLE_PHONES.' AS phones ON phones_otp.phone_id = phones.id '
					. 'WHERE phones.phone = ? '.($active_only ? 'AND (phones_otp.active_from <= "'.$now_time.'" AND "'.$now_time.'" <= phones_otp.active_to) ' : '')
					. 'ORDER BY phones_otp.id DESC';

		//echo $otp_query;
		$all_otp = mysqli_prepare($_connection, $otp_query);
		mysqli_stmt_bind_param($all_otp, 's', $phone_number);
		mysqli_stmt_execute($all_otp);

		$results = mysqli_stmt_get_result($all_otp);

		$otp_rows = array();
		while($rows = mysqli_fetch_assoc($results))
		{
			$otp_rows[] = $rows;
		}

		return $otp_rows;
	}

	// Вариант с чисто процедурно mysqli и ескейпване чрез mysqli_real_escape_string()
	function get_phone_id($phone_number, $auth = null)
	{
		Global $_connection;

		if(isset($auth) && is_array($auth))
		{
			$now_time = date('Y-m-d H:i:s', strtotime('now'));
			$auth_clause = ' AND (phones.email = "'.escape_string($_POST['email']).'" AND phones_otp.otp_value = "'.escape_string($_POST['otp']).'")'
						 . ' AND (phones_otp.active_from <= "'.$now_time.'" AND "'.$now_time.'" <= phones_otp.active_to) ';
		}


		$sms_query = 'SELECT phones.id, phones.login_status, phones.banned_to FROM '.TABLE_PHONES.' AS phones '
					. 'INNER JOIN '.TABLE_PHONES_OTP.' AS phones_otp ON phones_otp.phone_id = phones.id '
					. 'WHERE phones.phone = "'.escape_string($phone_number).'" '
					. ($auth_clause ? $auth_clause : '')
					. 'ORDER BY phones_otp.id DESC LIMIT 1';



		$results = mysqli_query($_connection, $sms_query);
		//echo $sms_query;
		if(mysqli_num_rows($results))
		{
			$row = mysqli_fetch_assoc($results);
			if($row['login_status'] == STATUS_LOG_BAN && strtotime($row['banned_to']) > strtotime('now'))
			{
				add_error_message('Вие сте баннат до '.$row['banned_to']);
				return $row['id']; 
			}
			// Тук можех да го напиша и на ред: return mysqli_fetch_assoc($results)['id'], но
			// това може да създаде проблем с по-старите версии на РНР, затова го пиша на 2 реда с цел съвместимост.
			return $row['id'];
		}
		else
		{
			return null;
		}
	}

	function validatePhone($phone_number)
	{
		// Премахваме всичко, което не е цифра, за да унифицираме номера
		$phone_number = str_ireplace([' ', '(', ')', '-'], '', $phone_number);

		// Преобразуваме: 0877... => 359877...
		if('0' == substr($phone_number, 0, 1))
		{
			$phone_number = DEFAULT_PHONE_CODE . substr($phone_number, 1);
		}

		if('359' != substr($phone_number, 0, 3)){
			add_error_message('Грешка #'.__LINE__.': Неразпознат код за държава! <br />Работим само с български номера!');
			return false;
		}

		if(12 != strlen($phone_number)){
			add_error_message('Грешка #'.__LINE__.': Телефонния номер има грешна дължина!');
			return false;
		}

		return $phone_number;
	}

	function Generate_OTP_Code($characters_count)
	{
		$possible = '0123456789';
		$code = '';
		$i = 0;
		while ($i < $characters_count) {
			$code .= substr($possible, mt_rand(0, strlen($possible)-1), 1);
			$i++;
		}

		return $code;
	}


	if(!function_exists('br2nl'))
	{
		function br2nl($string)
		{
			return preg_replace('/\<br(\s*)?\/?\>/i', PHP_EOL, $string);
		}
	}
?>