<?php
	define_once('CURRENT_USER', 'CURRENT_USER');
	define_once('USER_ACCESS', 'USER_ACCESS');
	define_once('SYSTEM_ROLES', 'SYSTEM_ROLES');
	define_once('ROLE_PERMISSIONS', 'ROLE_PERMISSIONS');
	define_once('SYSTEM_PERMISSIONS', 'SYSTEM_PERMISSIONS');
	define_once('LOGIN_TIMESTAMP', 'LOGIN_TIMESTAMP');
	define_once('UPDATED_ON', 'UPDATED_ON');

// Да се оправи
	define_once('ROLE_GUEST', 0);
	define_once('ROLE_REGISTERED', 1);
	define_once('ROLE_APPROVED', 2);
	define_once('ROLE_EDITOR', 3);
	define_once('ROLE_TRANSLATOR', 4);
	define_once('ROLE_COURSE_CREATOR', 5);
	define_once('ROLE_TREASURER', 6);
	define_once('ROLE_OBSERVER', 7);
	define_once('ROLE_CONTRIBUTOR', 8);
	define_once('ROLE_ADMIN', 9);
	define_once('ROLE_SUPER_ADMIN', 10);


	/*
	 * $_SESSION[USER_ACCESS] - Правата за достъпа на потребителя. С Тези данни работят функциите за проверка на достъпа!
	 * $_SESSION[CURRENT_USER][USER_ACCESS] - Правата за достъп на логнатия потребител. Това и горното може да са различни!
	 *
	 * Ако двете са различни значи потребителя се е дегизирал и е взел чужди права, както при PhpBbForum
	*/
	function user_is($role)
	{
        $user_role = getUserRole();

		if($user_role['role_name'] === $role)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function user_can($permission) 
	{
        if(is_string($permission))
        {
			$permission = trim($permission);
			if(in_array($permission, $_SESSION[USER_ACCESS]))
			{
				return true;
			}
			else
			{

				if(!in_array($permission, $_SESSION[SYSTEM_PERMISSIONS]))
				{
					insert_query('access_permissions', array('permission' => $permission));
					//add_error_message('Няма такъв permission ,но беше добавен !!!');
					refreshSystemRoles();
				}
				return false;
			}
        }
	}

	function check_access($permission) //приема array от permission-и и string изброяване на permission-и
	{
       $access = true;
        if(is_string($permission))
        {
            $permission = explode(',' , $permission);
		}

        if(is_array($permission))
		{
          foreach($permission as $perm)
          {
            $access = $access && user_can($perm);
          }
        }
		  
		if($access)
		{
			return 1; // Return True
		}
		else
		{
			if(user_is('super_admin'))
			{
				return -1; // Return True
			}
			else
			{
				page_not_access();
				return 0; // Return False
			}
		}
	}

	function check_login()
	{
		if(!is_user_logged_in())
		{
			$message = login_or_register();
			page_not_access($message);
			exit;
		}
	}

	function login_or_register()
	{
		$current_page = $_SERVER['SCRIPT_URI'].(!empty($_SERVER['QUERY_STRING']) ? '?'.$_SERVER['QUERY_STRING']: '');
		$register_url = header_link(array(CONTROLLER => 'user', ACTION => 'register'));
		$login_url = header_link(array(CONTROLLER => 'user', ACTION => 'login', 'back_to' => rawurlencode($current_page)));

		$message = translate('MSG_MUST_REGISTER_OR_LOGIN');
		$message = str_ireplace(array('{REGISTER_URL}', '{LOGIN_URL}'), array($register_url, $login_url), $message);

		return $message;
	}

	function getUserRole()
	{
        if(!is_null($_SESSION[CURRENT_USER]))
        {
			return $_SESSION[SYSTEM_ROLES][$_SESSION[CURRENT_USER]['role_id']];
        }
		else
		{
			return $_SESSION[SYSTEM_ROLES][ROLE_GUEST];
		}
	}

	function getUserAccess($role)
	{
        $all_roles = array_column($_SESSION[SYSTEM_ROLES], 'role_name');
        $role_id = array_search($role , $all_roles);

		if(is_numeric($role_id))
		{
			return $_SESSION[SYSTEM_ROLES][$role_id][ROLE_PERMISSIONS];
		}
		else
		{
			add_error_message('Фатална грешка: Не беше намерена ролята');
			return false;
		}
	}

	function showAccessModal()
	{

	}


	function update_last_login()
	{
		$update_data = array(
			'last_ip' => $_SERVER['REMOTE_ADDR'],
			'last_login' => date('Y-m-d H:i:s')
		);

		update_query('users', $update_data, set_where('user_id', '=', get_current_user_id()));
	}

	function import_wa_users()
	{
		database_open();
		//$wa_users =
		database_close();
	}

	function look_for_user()
	{
		Global $source, $user_id;
 
		if(isset($source[ID]) && is_numeric($source[ID]) && user_can('SEE_PROFILE_OTHERS')) //user_can('SEE_PROFILE_OTHERS')
		{
			return $source[ID];  
		}
		else
		{
			return $user_id;  
		}
	}

	function load_user_session($email, $key_or_pass)
	{
		$where_clause = '('.set_where('user_email', '=', $email).' OR '.set_where('email', '=', $email).') AND ('.set_where('user_pass', '=', md5($key_or_pass)).' OR '.set_where('activation_key', '=', $key_or_pass).')';
		//$where_clause = 'user_email = "'.$email.'" AND user_pass = "'.$key_or_pass.'"'; echo $where_clause; exit;
		$user_data = to_assoc_array(select_query('users', $where_clause));

		//add_info_message(last_query());

		if(Only_One_Exists($user_data))
		{
			$_SESSION[CURRENT_USER] = reset($user_data);
			$_SESSION[USER_ACCESS] = $_SESSION[SYSTEM_ROLES][$_SESSION[CURRENT_USER]['role_id']][ROLE_PERMISSIONS];
            $_SESSION[CURRENT_USER][USER_ACCESS] = $_SESSION[USER_ACCESS];
			$_SESSION[CURRENT_USER][LOGIN_TIMESTAMP] = date('Y-m-d H:i:s');

			update_last_login();
			return true;
		}
		else
		{
			return false;
		}
	}

	function refreshSystemRoles($changes = null)
	{
		$_SESSION[SYSTEM_ROLES] = to_assoc_array(select_query('access_roles', null , array('role_id','role_name','updated_on')),'role_id');
		$role_permssions = to_assoc_array(exec_query('SELECT arp.role_id , ap.permission FROM access_roles_permissions AS arp JOIN access_permissions AS ap ON arp.permission_id = ap.id'));

		$_SESSION[SYSTEM_PERMISSIONS] = array_column(get_all_permissions(),'permission');


		if(is_string($changes))
		{
			$changes = explode(',', $changes);
		}

		if(is_array($changes))
		{
			foreach ($changes as $change)
			{
				$_SESSION[SYSTEM_ROLES][$change][UPDATED_ON] = date('Y-m-d H:i:s');
				update_query('access_roles', array('updated_on' => $_SESSION[SYSTEM_ROLES][$change][UPDATED_ON]) ,array('role_id' => $change));
			}
		}

		foreach ($role_permssions as $rp)
		{
			$_SESSION[SYSTEM_ROLES][$rp['role_id']][ROLE_PERMISSIONS][] = $rp['permission'];
		}
	}

	function refreshUserAccess()
	{
		$_SESSION[USER_ACCESS] = $_SESSION[SYSTEM_ROLES][$_SESSION[CURRENT_USER]['role_id']][ROLE_PERMISSIONS];
        $_SESSION[CURRENT_USER][USER_ACCESS] = $_SESSION[USER_ACCESS];
	}

	function get_all_permissions($like_trans = null)
	{
		$result = exec_query('SELECT * from access_permissions ' . (isset($like_trans)? 'WHERE ' . like_trans(array('permission'), $like_trans): ''));

		return to_assoc_array($result , 'id');
	}

	function get_all_roles()
	{
		$result = exec_query('SELECT * from access_roles ');

		return to_assoc_array($result , 'role_id');
	}
?>